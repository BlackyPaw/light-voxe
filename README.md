# LightVoxE #

This is a small OpenGL game engine project of mine. It is written in C++ and contains native abstraction for both Windows and Linux.

### Dependencies ###

The project depends on the following libraries:
- OpenGL
- glew
- SOIL (Simple OpenGL Image Library)
- tinyxml2
- OpenAL

### License ###

All rights reserved. You must not use the code in any way without my explicit permission to do so.

### Who do I talk to? ###

If you would like to contact me you can simply send me a message.