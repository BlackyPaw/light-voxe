/*
 * AudioBuffer.hpp
 *
 *  Created on: 18.07.2014
 *      Author: euaconlabs
 */

#ifndef AUDIOBUFFER_HPP_
#define AUDIOBUFFER_HPP_

#include <VoxE/Native/Atomic.hpp>

namespace VX
{
	/////////////////////////////////////////////////////////////
	/// \enum AudioFormat
	/// \brief Describes possible values for an audio format
	///		passable to an audio buffer.
	/////////////////////////////////////////////////////////////
	enum class AudioFormat : vxU16
	{
		Mono8	 = 0x1100,
		Mono16	 = 0x1101,
		Stereo8	 = 0x1102,
		Stereo16 = 0x1103
	};

	/////////////////////////////////////////////////////////////
	/// \class AudioBuffer
	/// \brief Implementation of an audio buffer an audio source
	///		may read.
	/// An audio buffer basically is a storage for raw PCM data.
	/// It furthermore stores several meta-data about its content.
	/////////////////////////////////////////////////////////////
	class AudioBuffer
	{
	public:

		AudioBuffer();
		AudioBuffer(const AudioBuffer& b);
		~AudioBuffer();

		/////////////////////////////////////////////////////////////
		/// Creates a buffer object. As of now streaming is not
		/// implemented yet so the method has to be given a buffer
		/// containing the raw PCM data immediately.
		/// \param format The format of the PCM data.
		/// \param pcm The PCM data buffer.
		/// \param size The size of the PCM data buffer in bytes.
		/// \param frequency The frequency of the PCM data.
		/////////////////////////////////////////////////////////////
		void Create(const AudioFormat& format, const void* pcm, const vxU32 size, const vxI32 frequency);

		/////////////////////////////////////////////////////////////
		/// Gets the AudioBuffer's native handle.
		/////////////////////////////////////////////////////////////
		vxU32 GetHandle() const;

		/////////////////////////////////////////////////////////////
		/// Checks whether the audio buffer contains valid data.
		/////////////////////////////////////////////////////////////
		vxBool IsValid() const;
		/////////////////////////////////////////////////////////////
		/// Destroys the audio buffer immediately. This method does
		/// NOT destroy the buffer' internals. It will only detach
		// and reset this instance. The actual data will be destroyed
		/// when the last instance gets deleted.
		/////////////////////////////////////////////////////////////
		void Destroy();

		AudioBuffer& operator= (const AudioBuffer& b);

	private:

		void Copy(const AudioBuffer& b);

		vxU32* mReferences;
		vxU32  mHandle;
	};
}

#endif /* AUDIOBUFFER_HPP_ */
