/*
 * AudioListener.hpp
 *
 *  Created on: 04.07.2014
 *      Author: euaconlabs
 */

#ifndef AUDIOLISTENER_HPP_
#define AUDIOLISTENER_HPP_

#include <VoxE/Core/ISingleton.hpp>
#include <VoxE/Core/Vector.hpp>
#include <VoxE/Native/Atomic.hpp>

namespace VX
{
	/////////////////////////////////////////////////////////////
	/// \class AudioListener
	/// \brief Listener class for the audio interface.
	/// The audio listener basically is the object which "hears"
	/// all sounds to be played. As there may only be one listener
	/// object in any scene at any time this class is a singleton.
	/// At any time a function of the listener is called or one
	/// of its properties is changed this always refers to the
	/// currently active ALContext. Therefore there must be an
	/// ALContext current whenever this is done.
	/////////////////////////////////////////////////////////////
	class AudioListener : public ISingleton<AudioListener>
	{
	public:

		/////////////////////////////////////////////////////////////
		/// Sets the listener's position expressed in world coordinate
		/// space.
		/// \param x The x-coordinate of the listener's position.
		/// \param y The y-coordinate of the listener's position.
		/// \param z The z-coordinate of the listener's position.
		/////////////////////////////////////////////////////////////
		void SetPosition(const vxF32 x, const vxF32 y, const vxF32 z);
		/////////////////////////////////////////////////////////////
		/// Sets the listener's position expressed in world coordinate
		/// space.
		/// \param pos A three-element vector containing the listener's position.
		/////////////////////////////////////////////////////////////
		void SetPosition(const Vector3f& pos);
		/////////////////////////////////////////////////////////////
		/// Gets the listener's position in world coordinates.
		/////////////////////////////////////////////////////////////
		const Vector3f& GetPosition() const;

	private:

		AudioListener();
		~AudioListener();

		Vector3f mPosition;
	};
}

#endif /* AUDIOLISTENER_HPP_ */
