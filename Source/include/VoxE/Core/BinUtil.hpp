#ifndef BINUTIL_HPP
#define BINUTIL_HPP

#include <VoxE/Native/Atomic.hpp>

namespace VX
{

	// DEFINES
#define ORDER_LITTLE_ENDIAN 0x00
#define ORDER_BIG_ENDIAN 0x01

#define ORDER_LITTLE_ENDIAN_BYTES 0x04030201
#define ORDER_BIG_ENDIAN_BYTES 0x01020304

	// UNIONS

	union U32toF32
	{
		vxU32 i;
		vxF32 f;
	};

	union U32toC8v
	{
		vxU32 i;
		vxU8 cv[4];
	};

	union L64toD64
	{
		vxU64 l;
		vxF64 d;
	};

	union C8vtoU32
	{
		vxU8 cv[4];
		vxU32 u;
	};

	namespace
	{
		C8vtoU32 system_order = { { 1, 2, 3, 4 } };
	}
#define NATIVE_ORDER (system_order.u)

	/// ENDIANNESS DETECTION UTILITIES

	inline vxBool isBigEndian()
	{
		return (NATIVE_ORDER == ORDER_BIG_ENDIAN_BYTES);
	}

	inline vxBool isLittleEndian()
	{
		return (NATIVE_ORDER == ORDER_LITTLE_ENDIAN_BYTES);
	}

	/// SWAPPING UTILITIES

	inline vxU16 swap16(const vxU16 s)
	{
		return ((s & 0x00FF) << 8 | (s & 0xFF00) >> 8);
	}

	inline vxU32 swap32(const vxU32 i)
	{
		return ((i & 0x000000FF) << 24 | (i & 0x0000FF00) << 8 |
			(i & 0x00FF0000) >> 8 | (i & 0xFF000000) >> 24);
	}

	inline vxF32 swap32f(const vxF32 f)
	{
		U32toF32 u;
		u.f = f;

		u.i = swap32(u.i);
		return u.f;
	}

	inline vxU64 swap64(const vxU64 l)
	{
		return ((l & 0x00000000000000FF) << 56 | (l & 0x000000000000FF00) << 40 |
			(l & 0x0000000000FF0000) << 24 | (l & 0x00000000FF000000) << 8 |
			(l & 0x000000FF00000000) >> 8 | (l & 0x0000FF0000000000) >> 24 |
			(l & 0x00FF000000000000) >> 40 | (l & 0xFF00000000000000) >> 56);
	}

	inline vxF64 swap64d(const vxF64 d)
	{
		L64toD64 u;
		u.d = d;

		u.l = swap64(u.l);
		return u.d;
	}

	inline vxU16 swap16tob(const vxU16 s)
	{
		if (isLittleEndian())
			return swap16(s);
		return s;
	}

	inline vxU32 swap32tob(const vxU32 i)
	{
		if (isLittleEndian())
			return swap32(i);
		return i;
	}

	inline vxF32 swap32ftob(const vxF32 f)
	{
		if (isLittleEndian())
			return swap32f(f);
		return f;
	}

	inline vxU64 swap64tob(const vxU64 l)
	{
		if (isLittleEndian())
			return swap64(l);
		return l;
	}

	inline vxF64 swap64dtob(const vxF64 d)
	{
		if (isLittleEndian())
			return swap64d(d);
		return d;
	}

	inline vxU16 swap16tol(const vxU16 s)
	{
		if (isBigEndian())
			return swap16(s);
		return s;
	}

	inline vxU32 swap32tol(const vxU32 i)
	{
		if (isBigEndian())
			return swap32(i);
		return i;
	}

	inline vxF32 swap32ftol(const vxF32 f)
	{
		if (isBigEndian())
			return swap32f(f);
		return f;
	}

	inline vxU64 swap64tol(const vxU64 l)
	{
		if (isBigEndian())
			return swap64(l);
		return l;
	}

	inline vxF64 swap64dtol(const vxF64 d)
	{
		if (isBigEndian())
			return swap64d(d);
		return d;
	}

	inline vxU16 swap16n(const vxU16 s, const vxU8 order)
	{
		if (order == ORDER_BIG_ENDIAN)
		{
			if (isBigEndian())
				return s;
			else
				return swap16(s);
		}
		else if (order == ORDER_LITTLE_ENDIAN)
		{
			if (isLittleEndian())
				return s;
			else return swap16(s);
		}
		return s;
	}

	inline vxU32 swap32n(const vxU32 i, const vxU8 order)
	{
		if (order == ORDER_BIG_ENDIAN)
		{
			if (isBigEndian())
				return i;
			else
				return swap32(i);
		}
		else if (order == ORDER_LITTLE_ENDIAN)
		{
			if (isLittleEndian())
				return i;
			else return swap32(i);
		}
		return i;
	}

	inline vxF32 swap32fn(const vxF32 f, const vxU8 order)
	{
		if (order == ORDER_BIG_ENDIAN)
		{
			if (isBigEndian())
				return f;
			else
				return swap32f(f);
		}
		else if (order == ORDER_LITTLE_ENDIAN)
		{
			if (isLittleEndian())
				return f;
			else return swap32f(f);
		}
		return f;
	}

	inline vxU64 swap64n(const vxU64 l, const vxU8 order)
	{
		if (order == ORDER_BIG_ENDIAN)
		{
			if (isBigEndian())
				return l;
			else
				return swap64(l);
		}
		else if (order == ORDER_LITTLE_ENDIAN)
		{
			if (isLittleEndian())
				return l;
			else
				return swap64(l);
		}
	}

	inline vxF64 swap64dn(const vxF64 d, const vxU8 order)
	{
		if (order == ORDER_BIG_ENDIAN)
		{
			if (isBigEndian())
				return d;
			else
				return swap64d(d);
		}
		else if (order == ORDER_LITTLE_ENDIAN)
		{
			if (isLittleEndian())
				return d;
			else
				return swap64d(d);
		}
	}

}

#endif // BINUTIL_HPP