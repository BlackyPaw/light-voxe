#ifndef DELEGATE_HPP
#define DELEGATE_HPP

//////////////////////////////////////////////////////////////
/// Engine
//////////////////////////////////////////////////////////////
#include <VoxE/Core/CompilerInfo.hpp>
#include <VoxE/Native/Atomic.hpp>

//////////////////////////////////////////////////////////////
/// STL
//////////////////////////////////////////////////////////////
#include <memory.h>

namespace VX
{
	//////////////////////////////////////////////////////////////
	/// Private namespace
	///
	/// Please note that we use as less doxygen documentation as
	/// possible here since these types are usually invisible to
	/// the user. This does not mean the types and functions
	/// should not be documented, but they should be doxygen in-
	/// compatible.
	/// Also nearly everything should be declared inline here
	/// for performance reasons.
	///
	/// Note that the code used here is actually just a renamed
	/// and at some points extended version of the FastDelegate
	/// library written by Don Clugston which is released into
	/// the public domain.
	//////////////////////////////////////////////////////////////
	namespace Priv
	{
		//////////////////////////////////////////////////////////////
		/// SimpleCast
		///
		/// Simple casting utility simialar to static_cast().
		//////////////////////////////////////////////////////////////
		template<typename O, typename I>
		inline O SimpleCast(I i)
		{
			return i;
		}

		//////////////////////////////////////////////////////////////
		/// UnionHack
		///
		/// Goes around C++'s type system and converts two types using
		/// a hack.
		//////////////////////////////////////////////////////////////
		template<typename O, typename I>
		union UnionHack
		{
			I mInput;
			O mOutput;
		};

		//////////////////////////////////////////////////////////////
		/// \brief Casting utility
		///
		/// This little hack uses unions to convert between any two
		/// given data types or pointers. It is used to convert static
		/// functions to member function pointers forth and back.
		/// \remark Actually this conversion is extremely ugly and
		///			should be avoided where ever that's possible.
		//////////////////////////////////////////////////////////////
		template<typename O, typename I>
		inline O UnionHackCast(const I i)
		{
			UnionHack<O, I> u;
			// Generating a compile-time error message if the hack is not allowed on the compiler:
			typedef int Error_UnionHackCast_is_not_allowed[sizeof(I) == sizeof(u) && sizeof(I) == sizeof(O) ? 1 : -1];
			u.mInput = i;
			return u.mOutput;
		}

#if defined(__MEDIUM__) // Disable the availability of the function hack on DOS compilers running in medium memory mode.
#undef DelegatesAllowFunctionHack
#endif
		//////////////////////////////////////////////////////////////
		/// This VoidDummy type is necessary since some compilers
		/// namely MSVC6 don't allow pointers to functions which have
		/// a return type of void.
		//////////////////////////////////////////////////////////////
#if defined(CompilerMSVC6)
		typedef const void* VoidDummy;
#else
		typedef void VoidDummy;
#endif

		/// The VoidDummy workaround also needs some convertion objects
		/// to be defined:
		template<typename T>
		class ConvertToVoidDummy { public: typedef T Type; };

		template<>
		class ConvertToVoidDummy<void> { public: typedef VoidDummy Type; };

		template<typename T>
		class ConvertFromVoidDummy { public: typedef T Type; };

		template<>
		class ConvertFromVoidDummy<VoidDummy> { public: typedef void Type; };

		//////////////////////////////////////////////////////////////
		/// We now need to have a default type to which we may apply
		/// any given member functions since C++ require member
		/// function pointers to be restricted to one class type.
		/// So we've got to create a dummy here.
		/// Actually in order to disable all optimizations the
		/// compiler could possibly perform we need to only forward
		/// declare that particular type without defining it at any
		/// point. This step is necessary since we are only then able
		/// to make assumptions on where the actual method body
		/// resides in memory in the case of a member function
		/// pointer and in since we want to support both static
		/// functions and pointers to those as well as member function
		/// pointers.
		//////////////////////////////////////////////////////////////

		// Special case for compilers which use the Microsoft MPF table
		// in order to store meta-data about member methods:
#if defined(CompilerUsingMCMFP)

#if defined(CompilerHasInheritanceKeywords)

		// Use the inheritance keywords in order to define the dummy
		// as single-inherited thereby allowing us to use the smallest
		// MFP size available which also gives us maximum speed and
		// efficiency by being 4 bytes in size:
		class __single_inheritance ClassTypeDummy;

#endif // End of CompilerHasInheritanceKeywords

		// On most other compilers an empty class will have a MFP of
		// 4 bytes, too.
		class ClassTypeDummy {};

#else

		// If the compiler does not use the Microsoft MFP table
		// we can usually simply forward declare the type.
		class ClassTypeDummy;

#endif // End of CompilerUsingMCMFP

		static const vxU32 kSingleMemberFunctionPtrSize = sizeof(void (ClassTypeDummy::*)());

		// The following structure is a helper which converts a member function pointer
		// into its simplest form given the this pointer of the object instance.
		template<vxU32 _Size>
		struct SimplifyMemberFunctionPointer
		{
			template<typename T, typename TFunctionType, typename GenericMemberFunctionType>
			inline static ClassTypeDummy* Simplify(T* pThis, TFunctionType functionToBind, GenericMemberFunctionType& boundFunction)
			{
				// Unsupported size: Force compile failure:
				typedef int Error_Unsupported_member_function_pointer_for_the_given_compiler[_Size - 0xFFFFFFFF];
				return nullptr;
			}
		};

		// For single_inherited member function pointers on non-standard and for all
		// member function pointers on non-standard compilers the simplification
		// step is done by this template specialization:
		template<>
		struct SimplifyMemberFunctionPointer<kSingleMemberFunctionPtrSize>
		{
			template<typename T, typename TFunctionType, typename GenericMemberFunctionType>
			inline static ClassTypeDummy* Simplify(T* pThis, TFunctionType functionToBind, GenericMemberFunctionType& boundFunction)
			{
#if defined(__DMC__)
				// The digital mars compiler does not allow casting between different
				// MFPs so we have to use a union cast. I'm sorry but there's unluckily
				// really no way around it:
				boundFunction = UnionHackCast<GenericMemberFunctionType>(functionToBind);
#else
				boundFunction = reinterpret_cast<GenericMemberFunctionType>(functionToBind);
#endif
				return reinterpret_cast<ClassTypeDummy*>(pThis);
			}
		};

		// Now we need some workarounds for all those damn compilers which are
		// no standard-compliant (Microsoft,....).
		// Note that in this block of code it is unluckily necessary to use some
		// hacks since this problem may simply not be solved in any other way.
#if defined(CompilerUsingMCMFP)

		// Since the size MFP Microsoft and Intel use for multiple inheritance member
		// function pointer is another int more in size we have to create a template
		// for the size of this new MFP size. Please be aware that we really have to
		// use the basic integer type and NOT any other typedefed type which might
		// vary based on the platform:
		template<>
		struct SimplifyMemberFunctionPointer<kSingleMemberFunctionPtrSize + sizeof(int)>
		{
			template<typename T, typename TFunctionType, typename GenericMemberFunctionType>
			inline static ClassTypeDummy* Simplify(T* pThis, TFunctionType functionToBind, GenericMemberFunctionType& boundFunction)
			{
				// We will need to perform a specialized union cast here and we will
				// need to know how the internal structure of a member function pointer
				// in the Microsoft compiler looks like. Here it is:
				union
				{
					TFunctionType mFunction;
					struct
					{
						GenericMemberFunctionType mAddress; // <- The pointers address. Basically the same as in a single_inherited member function pointer.
						int mDelta; // <- The delta to be offset in the _vtable.
					} mMFP;
				} u;

				// Check if this union cast is possible at all and not, generate a compile-time error:
				// This is done by checking if the size of the given function perfectly matches our
				// representation of what it might compiler-internally look like:
				typedef int Error_UnionHackCast_is_not_allowed[sizeof(functionToBind) == sizeof(u.mMFP) ? 1 : -1];
				u.mFunction = functionToBind;
				boundFunction = u.mMFP.mAddress;
				// We cast pThis to an U8 ptr since U8 is guaranteed to be 8-bits in size and since
				// since we'got the number of bytes we are supposed to add to the pointer in order
				// to find the member function in the _vtable we want to make sure the pointer
				// arithmetic is done correctly since pointers add the number of bytes their underlying
				// is in size with each value added:
				return reinterpret_cast<ClassTypeDummy*>(reinterpret_cast<U8*>(pThis)+u.mMFP.mDelta);
			}
		};

		// For virtual inheritance we now get really dirty. First of all here's what the member function pointer
		// of a virtually inherited class looks like:
		struct VirtualMicrosoftMFP
		{
			void(ClassTypeDummy::*mCodeptr)(); // <- Use a silly member we call codeptr. This pointer actually points the function code.
			int mDelta;
			int mIndexInVTable;
		};

		// Basically on virtual inheritance we exploit the fact that
		// the compiler calls the mCodeptr every time unlike the other
		// members of the MFP. We now make a silly function which
		// is used as the codeptr but actually simply returns the this
		// pointer that has really been used.
		// Class dummy for virtual inheritance:
		struct VirtualClassTypeDummy : virtual public ClassTypeDummy
		{
			typedef VirtualClassTypeDummy* (VirtualClassTypeDummy::*TestPtr)();
			// This method is now used to grab the correct this pointer:
			VirtualClassTypeDummy* GetThis() { return this; }
		};

		// Now we can finally create the simplification template for virtually inherited member function pointers:
		template<>
		struct SimplifyMemberFunctionPointer<kSingleMemberFunctionPtrSize + 2 * sizeof(int)>
		{
			template<typename T, typename TFunctionType, typename GenericMemberFunctionType>
			inline static ClassTypeDummy* Simplify(T* pThis, TFunctionType functionToBind, GenericMemberFunctionType& boundFunction)
			{
				// Again we will have to use a union cast since again there's no way around it:
				union
				{
					TFunctionType mFunction;
					ClassTypeDummy* (T::*mTestPtrFunc)();
					VirtualMicrosoftMFP mMFP;
				} u;

				u.mFunction = functionToBind;
				boundFunction = reinterpret_cast<GenericMemberFunctionType>(u.mMFP.mCodeptr);
				// Use another union for retrieving the this pointer:
				union
				{
					VirtualClassTypeDummy::TestPtr mTestPtrFunction;
					VirtualMicrosoftMFP mMFP;
				} u2;
				// Check if all union casts are allowed:
				typedef int Error_UnionHackCast_is_not_allowed[(sizeof(functionToBind) == sizeof(u.mMFP)
					&& sizeof(functionToBind) == sizeof(u.mTestPtrFunc)
					&& sizeof(u2.mTestPtrFunction) == sizeof(u2.mMFP)) ? 1 : -1];
				u2.mTestPtrFunction = &VirtualClassTypeDummy::GetThis;
				u.mMFP.mCodeptr = u2.mMFP.mCodeptr;
				return ((pThis->*u.mTestPtrFunc)());
			}
		};

		// Now the dirties of all hacks used follows: The one for unknown inheritance member function pointers:
#if defined(CompilerMSVC6)

		// In MSVC6 there is a compiler bug which may generate incorrect code by setting the index
		// in the VTable to 0 which may result in a call to an incorrect function.
		// Unluckily there's no way around this so we will need to create a compile-time error
		// if we encounter this case. I'm sorry.
		template<>
		struct SimplifyMemberFunctionPointer<kSingleMemberFunctionPtrSize + 3 * sizeof(int)>
		{
			template<typename T, typename TFunctionType, typename GenericMemberFunctionType>
			inline static ClassTypeDummy* Simplify(T* pThis, TFunctionType functionToBind, GenericMemberFunctionType& boundFunction)
			{
				// Create the error:
				typedef int Error_MSVC6_Compiler_Bug[-1];
				return nullptr;
			}
		};

#else

		template<>
		struct SimplifyMemberFunctionPointer<kSingleMemberFunctionPtrSize + 3 * sizeof(int)>
		{
			template<typename T, typename TFunctionType, typename GenericMemberFunctionType>
			inline static ClassTypeDummy* Simplify(T* pThis, TFunctionType functionToBind, GenericMemberFunctionType& boundFunction)
			{
				// Member function pointers for unknown inheritance types are 16 bytes long.
				// This cast may again not be done by regular operations but only by the
				// union hack cast. I'm so sorry.
				union
				{
					TFunctionType mFunction;
					struct
					{
						GenericMemberFunctionType mAddress;
						int mDelta;
						int mVTableDisp; // The number of bytes to add to the this pointer in order to find the VTable.
						int mVTableIndex;
					} mMFP;
				} u;
				// Check if the union hack cast will work:
				typedef int Error_UnionHackCast_is_not_allowed[sizeof(TFunctionType) == sizeof(u.mMFP) ? 1 : -1];
				u.mFunction = functionToBind;
				boundFunction = u.mMFP.mAddress;
				int delta = 0;
				if (u.mMFP.mVTableIndex) // If this holds true, we know that virtual inheritance is used.
				{
					// First we want to actually reach / find the VTable:
					const int* vtable = *reinterpret_cast<const int* const*>(reinterpret_cast<const U8*>(pThis)+u.mMFP.mVTableDisp);
					delta = u.mMFP.mVTableDisp + *reinterpret_cast<const int*>(reinterpret_cast<U8*>(vtable)+u.mMFP.mVTableIndex);
				}
				// The delta integer now holds the number of bytes to actually add to the this pointer,
				// so we can finally return:
				return reinterpret_cast<ClassTypeDummy*>(reinterpret_cast<U8*>(pThis)+u.mMFP.mDelta + delta);
			}
		};

#endif // End of CompilerMSVC6

#endif // End of CompilerUsingMCMFP

		//////////////////////////////////////////////////////////////
		/// Now we can actually create the structure that will hold
		/// the delegate. There even is a hack available which
		/// reduces its size by 4 bytes but which can only be used if
		/// the size of a data pointer is at least as big as the size
		/// of a code pointer. We can then store the function pointer
		/// into the this pointer in order and use union hack casts
		/// to convert between those two types.
		//////////////////////////////////////////////////////////////
		class DelegateStorage
		{
		public:

			//////////////////////////////////////////////////////////////
			/// Constructors
			//////////////////////////////////////////////////////////////
#if !defined(DelegatesAllowFunctionHack)
			DelegateStorage() : mThisPtr(nullptr), mFunctionPtr(nullptr), mStaticFunctionPtr(nullptr) {}
			inline void Reset() { mThisPtr = nullptr; mFunctionPtr = nullptr; mStaticFunctionPtr = nullptr; }
#else
			DelegateStorage() : mThisPtr(nullptr), mFunctionPtr(nullptr) {}
			inline void Reset() { mThisPtr = nullptr; mFunctionPtr = nullptr; }
#endif
			DelegateStorage(const DelegateStorage& d) { Copy(d); }

			//////////////////////////////////////////////////////////////
			/// Methods
			//////////////////////////////////////////////////////////////
#if !defined(DelegatesAllowFunctionHack)
			inline bool IsEqual(const DelegateStorage& d) const
			{
				if (mFunctionPtr != d.mFunctionPtr) return false;
				if (mStaticFunctionPtr != d.mStaticFunctionPtr) return false;
				if (mStaticFunctionPtr == nullptr) return (mThisPtr == d.mThisPtr);
				else return true;
			}
#else
			inline bool IsEqual(const DelegateStorage& d) const
			{
				return (mThisPtr == d.mThisPtr && mFunctionPtr == d.mFunctionPtr);
			}
#endif
			inline bool IsLess(const DelegateStorage& d) const
			{
#if !defined(DelegatesAllowFunctionHack)
				if (mStaticFunctionPtr != nullptr || d.mStaticFunctionPtr != nullptr)
					return mStaticFunctionPtr < d.mStaticFunctionPtr;
#endif
				if (mThisPtr != d.mThisPtr) return mThisPtr < d.mThisPtr;
				return (memcmp(&mFunctionPtr, &d.mFunctionPtr, sizeof(mFunctionPtr)) < 0);
			}

			//////////////////////////////////////////////////////////////
			/// Operators
			//////////////////////////////////////////////////////////////
			inline bool operator! () const
			{
				return (mThisPtr == nullptr && mFunctionPtr == nullptr);
			}
			inline bool Empty() const
			{
				return !(*this);
			}

			inline DelegateStorage& operator= (const DelegateStorage& d)
			{
				Copy(d);
				return *this;
			}

			inline bool operator== (const DelegateStorage& d)
			{
				return IsEqual(d);
			}
			inline bool operator!= (const DelegateStorage& d)
			{
				return !IsEqual(d);
			}
			inline bool operator< (const DelegateStorage& d)
			{
				return IsLess(d);
			}
			inline bool operator<= (const DelegateStorage& d)
			{
				return (IsLess(d) || IsEqual(d));
			}
			inline bool operator> (const DelegateStorage& d)
			{
				return (!IsLess(d) && !IsEqual(d));
			}
			inline bool operator>= (const DelegateStorage& d)
			{
				return (!IsLess(d));
			}

		protected:

			//////////////////////////////////////////////////////////////
			/// Methods
			//////////////////////////////////////////////////////////////
			void Copy(const DelegateStorage& d)
			{
				mThisPtr = d.mThisPtr;
				mFunctionPtr = d.mFunctionPtr;
#if !defined(DelegatesAllowFunctionHack)
				mStaticFunctionPtr = d.mStaticFunctionPtr;
#endif
			}

			//////////////////////////////////////////////////////////////
			/// Members
			//////////////////////////////////////////////////////////////

			typedef void (ClassTypeDummy::*MemberFunctionType)();
			ClassTypeDummy* mThisPtr;
			MemberFunctionType mFunctionPtr;

#if !defined(DelegatesAllowFunctionHack)
			typedef void(*FunctionType)();
			FunctionType mStaticFunctionPtr;
#endif
		};

		//////////////////////////////////////////////////////////////
		/// BaseDelegate
		///
		/// This class now manages a real delegate. It takes three
		/// types as template parameters which respectively stand for:
		///		- TGenericMemberFunction: Type of the member function
		///			to keep. It has to be bound to the ClassTypeDummy
		///			class.
		///		- TFunction: Type of the static function to keep.
		///		- TFunctionVC6W: Type of the static function
		///			workaround used for MSVC6 which cannot keep
		///			function pointers for function which return void.
		//////////////////////////////////////////////////////////////
		template<typename TGenericMemberFunction, typename TFunction, typename TFunctionVC6W>
		class BaseDelegate : public DelegateStorage
		{
		public:

			//////////////////////////////////////////////////////////////
			/// Function binders
			//////////////////////////////////////////////////////////////
#if !defined(DelegatesGCCWorkaround)
			template<typename T, typename TFunctionType>
			inline void BindMemberFunction(T* pThis, TFunctionType functionToBind)
			{
				mThisPtr = SimplifyMemberFunctionPointer<sizeof(functionToBind)>::Simplify(pThis, functionToBind, mFunctionPtr);
#if !defined(DelegatesAllowFunctionHack)
				mStaticFunctionPtr = nullptr;
#endif
			}
#else
			// Workaround for the bug found in GCC
			// GCC basically does not recognize constness in its MFPs when
			// the type is a template:
			template<typename T, typename TFunctionType>
			inline void BindMemberFunction(T* pThis, TFunctionType functionToBind)
			{
				BindConstMemberFunction(pThis, functionToBind);
#if !defined(DelegatesAllowFunctionHack)
				mStaticFunctionPtr = nullptr;
#endif
			}
#endif

			template<typename T, typename TFunctionType>
			inline void BindConstMemberFunction(const T* pThis, TFunctionType functionToBind)
			{
				mThisPtr = SimplifyMemberFunctionPointer<sizeof(functionToBind)>::Simplify(const_cast<T*>(pThis), functionToBind, mFunctionPtr);
#if !defined(DelegatesAllowFunctionHack)
				mStaticFunctionPtr = nullptr;
#endif
			}

			inline ClassTypeDummy* GetThisPtr() const { return mThisPtr; }
			inline TGenericMemberFunction GetFunctionPtr() const { return reinterpret_cast<TGenericMemberFunction>(mFunctionPtr); }

#if !defined(DelegatesAllowFunctionHack)

			template<typename TDerivedClass>
			inline void Copy(TDerivedClass* pParent, const DelegateStorage& d)
			{
				DelegateStorage::Copy(d);
				if (mStaticFunctionPtr != nullptr)
				{
					// We've got a member function pointer here, and currently
					// it points to the function calculated after the delegate
					// storage. But we need it to be offset after our new class
					// BaseDelegate so we've got to convert the this pointer:
					mThisPtr = reinterpret_cast<ClassTypeDummy*>(pParent);
				}
			}

			template<typename TDerivedClass, typename TParentInvokerSignature>
			inline void BindStaticFunction(TDerivedClass* pParent, TParentInvokerSignature staticInvokerSignature, TFunction functionToBind)
			{
				if (functionToBind == nullptr) mFunctionPtr = nullptr;
				else BindMemberFunction(pParent, staticInvokerSignature);
				mStaticFunctionPtr = reinterpret_cast<FunctionType>(functionToBind);
			}

			inline TFunctionVC6W GetStaticFunctionPtr() const { return reinterpret_cast<TFunctionVC6W>(mStaticFunctionPtr); }

#else

			template<typename TDerivedClass>
			inline void Copy(TDerivedClass* pParent, const DelegateStorage& d)
			{
				DelegateStorage::Copy(d);
			}

			template<typename TDerivedClass, typename TParentInvokerSignature>
			inline void BindStaticFunction(TDerivedClass* pParent, TParentInvokerSignature staticInvokerSignature, TFunction functionToBind)
			{
				if (functionToBind == nullptr) mFunctionPtr = nullptr;
				else BindMemberFunction(pParent, staticInvokerSignature);

				// We now use the hack to store the function pointer into the this pointer.
				// Therefore we need to make sure, the size of the this pointer is equal
				// to the size of the function pointer. If this does not hold true, we
				// force a compile-time error here:
				typedef int Error_Failed_to_use_function_hack[sizeof(ClassTypeDummy*) == sizeof(functionToBind) ? 1 : -1];
				mThisPtr = UnionHackCast<ClassTypeDummy*>(functionToBind);
			}

			inline TFunctionVC6W GetStaticFunctionPtr() const
			{
				// Again we need to make sure the pointer sizes are equal so the
				// hack works on this compiler. Afterwards we simply convert the
				// this pointer back into a function pointer:
				typedef int Error_Failed_to_use_function_hack[sizeof(TFunctionVC6W) == sizeof(this) ? 1 : -1];
				return UnionHackCast<TFunctionVC6W>(this);
			}
#endif

			inline bool IsEqualToStaticFunctionPointer(TFunction p)
			{
				if (p == nullptr) return Empty();
				else return (p == reinterpret_cast<TFunction>(GetStaticFunctionPtr()));
			}
		};
	}

	template<typename RetType = Priv::VoidDummy>
	class Delegate0
	{
	private:
		typedef typename Priv::ConvertFromVoidDummy<RetType>::Type DesiredRetType;
		typedef DesiredRetType(*StaticFunctionPtr)();
		typedef RetType(*StaticFunctionPtrVC6W)();
		typedef RetType(Priv::ClassTypeDummy::*MemberFunctionPtr)();
		typedef Priv::BaseDelegate<MemberFunctionPtr, StaticFunctionPtr, StaticFunctionPtrVC6W> DelegateType;

	public:

		typedef Delegate0 Type;

		//////////////////////////////////////////////////////////////
		/// Constructors
		//////////////////////////////////////////////////////////////
		Delegate0() { Reset(); }
		Delegate0(const Delegate0& d) { mDelegate.Copy(this, d.mDelegate); }
		template<typename X, typename Y>
		Delegate0(Y* pThis, DesiredRetType(X::*functionToBind)()) { Bind(pThis, functionToBind); }
		template<typename X, typename Y>
		Delegate0(const Y* pThis, DesiredRetType(X::*functionToBind)()) { Bind(pThis, functionToBind); }
		Delegate0(DesiredRetType(*functionToBind)()) { Bind(functionToBind); }

		//////////////////////////////////////////////////////////////
		/// Operators
		//////////////////////////////////////////////////////////////
		Delegate0& operator= (const Delegate0& d)
		{
			mDelegate.Copy(this, d.mDelegate);
			return *this;
		}
		Delegate0& operator= (DesiredRetType(*functionToBind)())
		{
			Bind(functionToBind);
			return *this;
		}
		bool operator== (const Delegate0& d) const
		{
			return mDelegate.IsEqual(d.mDelegate);
		}
		bool operator!= (const Delegate0& d) const
		{
			return !mDelegate.IsEqual(d.mDelegate);
		}
		bool operator< (const Delegate0& d) const
		{
			return mDelegate.IsLess(d.mDelegate);
		}
		bool operator<= (const Delegate0& d) const
		{
			return mDelegate.IsLess(d.mDelegate) || mDelegate.IsEqual(d.mDelegate);
		}
		bool operator> (const Delegate0& d) const
		{
			return d.mDelegate.IsLess(mDelegate);
		}
		bool operator>= (const Delegate0& d) const
		{
			return d.mDelegate.IsLess(mDelegate) || mDelegate.IsEqual(d.mDelegate);
		}

		template<typename X, typename Y>
		inline void Bind(Y* pThis, DesiredRetType(X::*functionToBind)())
		{
			mDelegate.BindMemberFunction(Priv::SimpleCast<Y*>(pThis), functionToBind);
		}

		template<typename X, typename Y>
		inline void Bind(const Y* pThis, DesiredRetType(X::*functionToBind)())
		{
			mDelegate.BindConstMemberFunction(Priv::SimpleCast<const Y*>(pThis), functionToBind);
		}

		inline void Bind(DesiredRetType(*functionToBind)())
		{
			mDelegate.BindStaticFunction(this, &Delegate0::StaticFunctionInvoker, functionToBind);
		}

		RetType operator() () const
		{
			return Invoke();
		}
		RetType Invoke() const
		{
			return ((mDelegate.GetThisPtr()->*(mDelegate.GetFunctionPtr()))());
		}

		inline bool operator== (StaticFunctionPtr p)
		{
			return mDelegate.IsEqualToStaticFunctionPointer(p);
		}
		inline bool operator!= (StaticFunctionPtr p)
		{
			return !mDelegate.IsEqualToStaticFunctionPointer(p);
		}
		inline bool operator! () const
		{
			return !mDelegate;
		}
		inline bool Empty() const
		{
			return !mDelegate;
		}

	private:
		void Reset() { mDelegate.Reset(); }
		RetType StaticFunctionInvoker() const
		{
			return ((*(mDelegate.GetStaticFunctionPtr()))());
		}

		DelegateType mDelegate;

	public:
		operator bool() const
		{
			return Empty();
		}
	};

	template<typename Param1, typename RetType = Priv::VoidDummy>
	class Delegate1
	{
	private:
		typedef typename Priv::ConvertFromVoidDummy<RetType>::Type DesiredRetType;
		typedef DesiredRetType(*StaticFunctionPtr)(Param1);
		typedef RetType(*StaticFunctionPtrVC6W)(Param1);
		typedef RetType(Priv::ClassTypeDummy::*MemberFunctionPtr)(Param1);
		typedef Priv::BaseDelegate<MemberFunctionPtr, StaticFunctionPtr, StaticFunctionPtrVC6W> DelegateType;

	public:

		typedef Delegate1 Type;

		//////////////////////////////////////////////////////////////
		/// Constructors
		//////////////////////////////////////////////////////////////
		Delegate1() { Reset(); }
		Delegate1(const Delegate1& d) { mDelegate.Copy(this, d.mDelegate); }
		template<typename X, typename Y>
		Delegate1(Y* pThis, DesiredRetType(X::*functionToBind)(Param1)) { Bind(pThis, functionToBind); }
		template<typename X, typename Y>
		Delegate1(const Y* pThis, DesiredRetType(X::*functionToBind)(Param1)) { Bind(pThis, functionToBind); }
		Delegate1(DesiredRetType(*functionToBind)(Param1)) { Bind(functionToBind); }

		//////////////////////////////////////////////////////////////
		/// Operators
		//////////////////////////////////////////////////////////////
		Delegate1& operator= (const Delegate1& d)
		{
			mDelegate.Copy(this, d.mDelegate);
			return *this;
		}
		Delegate1& operator= (DesiredRetType(*functionToBind)(Param1))
		{
			Bind(functionToBind);
			return *this;
		}
		bool operator== (const Delegate1& d) const
		{
			return mDelegate.IsEqual(d.mDelegate);
		}
		bool operator!= (const Delegate1& d) const
		{
			return !mDelegate.IsEqual(d.mDelegate);
		}
		bool operator< (const Delegate1& d) const
		{
			return mDelegate.IsLess(d.mDelegate);
		}
		bool operator<= (const Delegate1& d) const
		{
			return mDelegate.IsLess(d.mDelegate) || mDelegate.IsEqual(d.mDelegate);
		}
		bool operator> (const Delegate1& d) const
		{
			return d.mDelegate.IsLess(mDelegate);
		}
		bool operator>= (const Delegate1& d) const
		{
			return d.mDelegate.IsLess(mDelegate) || mDelegate.IsEqual(d.mDelegate);
		}

		template<typename X, typename Y>
		inline void Bind(Y* pThis, DesiredRetType(X::*functionToBind)(Param1))
		{
			mDelegate.BindMemberFunction(Priv::SimpleCast<Y*>(pThis), functionToBind);
		}

		template<typename X, typename Y>
		inline void Bind(const Y* pThis, DesiredRetType(X::*functionToBind)(Param1))
		{
			mDelegate.BindConstMemberFunction(Priv::SimpleCast<const Y*>(pThis), functionToBind);
		}

		inline void Bind(DesiredRetType(*functionToBind)(Param1))
		{
			mDelegate.BindStaticFunction(this, &Delegate1::StaticFunctionInvoker, functionToBind);
		}

		RetType operator() (Param1 p1) const
		{
			return Invoke(p1);
		}
		RetType Invoke(Param1 p1) const
		{
			return ((mDelegate.GetThisPtr()->*(mDelegate.GetFunctionPtr()))(p1));
		}

		operator bool() const
		{
			return Empty();
		}

		inline bool operator== (StaticFunctionPtr p)
		{
			return mDelegate.IsEqualToStaticFunctionPointer(p);
		}
		inline bool operator!= (StaticFunctionPtr p)
		{
			return !mDelegate.IsEqualToStaticFunctionPointer(p);
		}
		inline bool operator! () const
		{
			return !mDelegate;
		}
		inline bool Empty() const
		{
			return !mDelegate;
		}

	private:
		void Reset() { mDelegate.Reset(); }
		RetType StaticFunctionInvoker(Param1 p1) const
		{
			return ((*(mDelegate.GetStaticFunctionPtr()))(p1));
		}

		DelegateType mDelegate;
	};

	template<typename Param1, typename Param2, typename RetType = Priv::VoidDummy>
	class Delegate2
	{
	private:
		typedef typename Priv::ConvertFromVoidDummy<RetType>::Type DesiredRetType;
		typedef DesiredRetType(*StaticFunctionPtr)(Param1, Param2);
		typedef RetType(*StaticFunctionPtrVC6W)(Param1, Param2);
		typedef RetType(Priv::ClassTypeDummy::*MemberFunctionPtr)(Param1, Param2);
		typedef Priv::BaseDelegate<MemberFunctionPtr, StaticFunctionPtr, StaticFunctionPtrVC6W> DelegateType;

	public:

		typedef Delegate2 Type;

		//////////////////////////////////////////////////////////////
		/// Constructors
		//////////////////////////////////////////////////////////////
		Delegate2() { Reset(); }
		Delegate2(const Delegate2& d) { mDelegate.Copy(this, d.mDelegate); }
		template<typename X, typename Y>
		Delegate2(Y* pThis, DesiredRetType(X::*functionToBind)(Param1, Param2)) { Bind(pThis, functionToBind); }
		template<typename X, typename Y>
		Delegate2(const Y* pThis, DesiredRetType(X::*functionToBind)(Param1, Param2)) { Bind(pThis, functionToBind); }
		Delegate2(DesiredRetType(*functionToBind)(Param1, Param2)) { Bind(functionToBind); }

		//////////////////////////////////////////////////////////////
		/// Operators
		//////////////////////////////////////////////////////////////
		Delegate2& operator= (const Delegate2& d)
		{
			mDelegate.Copy(this, d.mDelegate);
			return *this;
		}
		Delegate2& operator= (DesiredRetType(*functionToBind)(Param1, Param2))
		{
			Bind(functionToBind);
			return *this;
		}
		bool operator== (const Delegate2& d) const
		{
			return mDelegate.IsEqual(d.mDelegate);
		}
		bool operator!= (const Delegate2& d) const
		{
			return !mDelegate.IsEqual(d.mDelegate);
		}
		bool operator< (const Delegate2& d) const
		{
			return mDelegate.IsLess(d.mDelegate);
		}
		bool operator<= (const Delegate2& d) const
		{
			return mDelegate.IsLess(d.mDelegate) || mDelegate.IsEqual(d.mDelegate);
		}
		bool operator> (const Delegate2& d) const
		{
			return d.mDelegate.IsLess(mDelegate);
		}
		bool operator>= (const Delegate2& d) const
		{
			return d.mDelegate.IsLess(mDelegate) || mDelegate.IsEqual(d.mDelegate);
		}

		template<typename X, typename Y>
		inline void Bind(Y* pThis, DesiredRetType(X::*functionToBind)(Param1, Param2))
		{
			mDelegate.BindMemberFunction(Priv::SimpleCast<Y*>(pThis), functionToBind);
		}

		template<typename X, typename Y>
		inline void Bind(const Y* pThis, DesiredRetType(X::*functionToBind)(Param1, Param2))
		{
			mDelegate.BindConstMemberFunction(Priv::SimpleCast<const Y*>(pThis), functionToBind);
		}

		inline void Bind(DesiredRetType(*functionToBind)(Param1, Param2))
		{
			mDelegate.BindStaticFunction(this, &Delegate2::StaticFunctionInvoker, functionToBind);
		}

		RetType operator() (Param1 p1, Param2 p2) const
		{
			return Invoke(p1, p2);
		}
		RetType Invoke(Param1 p1, Param2 p2) const
		{
			return ((mDelegate.GetThisPtr()->*(mDelegate.GetFunctionPtr()))(p1, p2));
		}

		operator bool() const
		{
			return Empty();
		}

		inline bool operator== (StaticFunctionPtr p)
		{
			return mDelegate.IsEqualToStaticFunctionPointer(p);
		}
		inline bool operator!= (StaticFunctionPtr p)
		{
			return !mDelegate.IsEqualToStaticFunctionPointer(p);
		}
		inline bool operator! () const
		{
			return !mDelegate;
		}
		inline bool Empty() const
		{
			return !mDelegate;
		}

	private:
		void Reset() { mDelegate.Reset(); }
		RetType StaticFunctionInvoker(Param1 p1, Param2 p2) const
		{
			return ((*(mDelegate.GetStaticFunctionPtr()))(p1, p2));
		}

		DelegateType mDelegate;
	};

	template<typename Param1, typename Param2, typename Param3, typename RetType = Priv::VoidDummy>
	class Delegate3
	{
	private:
		typedef typename Priv::ConvertFromVoidDummy<RetType>::Type DesiredRetType;
		typedef DesiredRetType(*StaticFunctionPtr)(Param1, Param2, Param3);
		typedef RetType(*StaticFunctionPtrVC6W)(Param1, Param2, Param3);
		typedef RetType(Priv::ClassTypeDummy::*MemberFunctionPtr)(Param1, Param2, Param3);
		typedef Priv::BaseDelegate<MemberFunctionPtr, StaticFunctionPtr, StaticFunctionPtrVC6W> DelegateType;

	public:

		typedef Delegate3 Type;

		//////////////////////////////////////////////////////////////
		/// Constructors
		//////////////////////////////////////////////////////////////
		Delegate3() { Reset(); }
		Delegate3(const Delegate3& d) { mDelegate.Copy(this, d.mDelegate); }
		template<typename X, typename Y>
		Delegate3(Y* pThis, DesiredRetType(X::*functionToBind)(Param1, Param2, Param3)) { Bind(pThis, functionToBind); }
		template<typename X, typename Y>
		Delegate3(const Y* pThis, DesiredRetType(X::*functionToBind)(Param1, Param2, Param3)) { Bind(pThis, functionToBind); }
		Delegate3(DesiredRetType(*functionToBind)(Param1, Param2, Param3)) { Bind(functionToBind); }

		//////////////////////////////////////////////////////////////
		/// Operators
		//////////////////////////////////////////////////////////////
		Delegate3& operator= (const Delegate3& d)
		{
			mDelegate.Copy(this, d.mDelegate);
			return *this;
		}
		Delegate3& operator= (DesiredRetType(*functionToBind)(Param1, Param2, Param3))
		{
			Bind(functionToBind);
			return *this;
		}
		bool operator== (const Delegate3& d) const
		{
			return mDelegate.IsEqual(d.mDelegate);
		}
		bool operator!= (const Delegate3& d) const
		{
			return !mDelegate.IsEqual(d.mDelegate);
		}
		bool operator< (const Delegate3& d) const
		{
			return mDelegate.IsLess(d.mDelegate);
		}
		bool operator<= (const Delegate3& d) const
		{
			return mDelegate.IsLess(d.mDelegate) || mDelegate.IsEqual(d.mDelegate);
		}
		bool operator> (const Delegate3& d) const
		{
			return d.mDelegate.IsLess(mDelegate);
		}
		bool operator>= (const Delegate3& d) const
		{
			return d.mDelegate.IsLess(mDelegate) || mDelegate.IsEqual(d.mDelegate);
		}

		template<typename X, typename Y>
		inline void Bind(Y* pThis, DesiredRetType(X::*functionToBind)(Param1, Param2, Param3))
		{
			mDelegate.BindMemberFunction(Priv::SimpleCast<Y*>(pThis), functionToBind);
		}

		template<typename X, typename Y>
		inline void Bind(const Y* pThis, DesiredRetType(X::*functionToBind)(Param1, Param2, Param3))
		{
			mDelegate.BindConstMemberFunction(Priv::SimpleCast<const Y*>(pThis), functionToBind);
		}

		inline void Bind(DesiredRetType(*functionToBind)(Param1, Param2, Param3))
		{
			mDelegate.BindStaticFunction(this, &Delegate3::StaticFunctionInvoker, functionToBind);
		}

		RetType operator() (Param1 p1, Param2 p2, Param3 p3) const
		{
			return Invoke(p1, p2, p3);
		}
		RetType Invoke(Param1 p1, Param2 p2, Param3 p3) const
		{
			return ((mDelegate.GetThisPtr()->*(mDelegate.GetFunctionPtr()))(p1, p2, p3));
		}

		operator bool() const
		{
			return Empty();
		}

		inline bool operator== (StaticFunctionPtr p)
		{
			return mDelegate.IsEqualToStaticFunctionPointer(p);
		}
		inline bool operator!= (StaticFunctionPtr p)
		{
			return !mDelegate.IsEqualToStaticFunctionPointer(p);
		}
		inline bool operator! () const
		{
			return !mDelegate;
		}
		inline bool Empty() const
		{
			return !mDelegate;
		}

	private:
		void Reset() { mDelegate.Reset(); }
		RetType StaticFunctionInvoker(Param1 p1, Param2 p2, Param3 p3) const
		{
			return ((*(mDelegate.GetStaticFunctionPtr()))(p1, p2, p3));
		}

		DelegateType mDelegate;
	};

	template<typename Param1, typename Param2, typename Param3, typename Param4, typename RetType = Priv::VoidDummy>
	class Delegate4
	{
	private:
		typedef typename Priv::ConvertFromVoidDummy<RetType>::Type DesiredRetType;
		typedef DesiredRetType(*StaticFunctionPtr)(Param1, Param2, Param3, Param4);
		typedef RetType(*StaticFunctionPtrVC6W)(Param1, Param2, Param3, Param4);
		typedef RetType(Priv::ClassTypeDummy::*MemberFunctionPtr)(Param1, Param2, Param3, Param4);
		typedef Priv::BaseDelegate<MemberFunctionPtr, StaticFunctionPtr, StaticFunctionPtrVC6W> DelegateType;

	public:

		typedef Delegate4 Type;

		//////////////////////////////////////////////////////////////
		/// Constructors
		//////////////////////////////////////////////////////////////
		Delegate4() { Reset(); }
		Delegate4(const Delegate4& d) { mDelegate.Copy(this, d.mDelegate); }
		template<typename X, typename Y>
		Delegate4(Y* pThis, DesiredRetType(X::*functionToBind)(Param1, Param2, Param3, Param4)) { Bind(pThis, functionToBind); }
		template<typename X, typename Y>
		Delegate4(const Y* pThis, DesiredRetType(X::*functionToBind)(Param1, Param2, Param3, Param4)) { Bind(pThis, functionToBind); }
		Delegate4(DesiredRetType(*functionToBind)(Param1, Param2, Param3, Param4)) { Bind(functionToBind); }

		//////////////////////////////////////////////////////////////
		/// Operators
		//////////////////////////////////////////////////////////////
		Delegate4& operator= (const Delegate4& d)
		{
			mDelegate.Copy(this, d.mDelegate);
			return *this;
		}
		Delegate4& operator= (DesiredRetType(*functionToBind)(Param1, Param2, Param3, Param4))
		{
			Bind(functionToBind);
			return *this;
		}
		bool operator== (const Delegate4& d) const
		{
			return mDelegate.IsEqual(d.mDelegate);
		}
		bool operator!= (const Delegate4& d) const
		{
			return !mDelegate.IsEqual(d.mDelegate);
		}
		bool operator< (const Delegate4& d) const
		{
			return mDelegate.IsLess(d.mDelegate);
		}
		bool operator<= (const Delegate4& d) const
		{
			return mDelegate.IsLess(d.mDelegate) || mDelegate.IsEqual(d.mDelegate);
		}
		bool operator> (const Delegate4& d) const
		{
			return d.mDelegate.IsLess(mDelegate);
		}
		bool operator>= (const Delegate4& d) const
		{
			return d.mDelegate.IsLess(mDelegate) || mDelegate.IsEqual(d.mDelegate);
		}

		template<typename X, typename Y>
		inline void Bind(Y* pThis, DesiredRetType(X::*functionToBind)(Param1, Param2, Param3, Param4))
		{
			mDelegate.BindMemberFunction(Priv::SimpleCast<Y*>(pThis), functionToBind);
		}

		template<typename X, typename Y>
		inline void Bind(const Y* pThis, DesiredRetType(X::*functionToBind)(Param1, Param2, Param3, Param4))
		{
			mDelegate.BindConstMemberFunction(Priv::SimpleCast<const Y*>(pThis), functionToBind);
		}

		inline void Bind(DesiredRetType(*functionToBind)(Param1, Param2, Param3, Param4))
		{
			mDelegate.BindStaticFunction(this, &Delegate4::StaticFunctionInvoker, functionToBind);
		}

		RetType operator() (Param1 p1, Param2 p2, Param3 p3, Param4 p4) const
		{
			return Invoke(p1, p2, p3, p4);
		}
		RetType Invoke(Param1 p1, Param2 p2, Param3 p3, Param4 p4) const
		{
			return ((mDelegate.GetThisPtr()->*(mDelegate.GetFunctionPtr()))(p1, p2, p3, p4));
		}

		operator bool() const
		{
			return Empty();
		}

		inline bool operator== (StaticFunctionPtr p)
		{
			return mDelegate.IsEqualToStaticFunctionPointer(p);
		}
		inline bool operator!= (StaticFunctionPtr p)
		{
			return !mDelegate.IsEqualToStaticFunctionPointer(p);
		}
		inline bool operator! () const
		{
			return !mDelegate;
		}
		inline bool Empty() const
		{
			return !mDelegate;
		}

	private:
		void Reset() { mDelegate.Reset(); }
		RetType StaticFunctionInvoker(Param1 p1, Param2 p2, Param3 p3, Param4 p4) const
		{
			return ((*(mDelegate.GetStaticFunctionPtr()))(p1, p2, p3, p4));
		}

		DelegateType mDelegate;
	};

	template<typename Param1, typename Param2, typename Param3, typename Param4, typename Param5, typename RetType = Priv::VoidDummy>
	class Delegate5
	{
	private:
		typedef typename Priv::ConvertFromVoidDummy<RetType>::Type DesiredRetType;
		typedef DesiredRetType(*StaticFunctionPtr)(Param1, Param2, Param3, Param4, Param5);
		typedef RetType(*StaticFunctionPtrVC6W)(Param1, Param2, Param3, Param4, Param5);
		typedef RetType(Priv::ClassTypeDummy::*MemberFunctionPtr)(Param1, Param2, Param3, Param4, Param5);
		typedef Priv::BaseDelegate<MemberFunctionPtr, StaticFunctionPtr, StaticFunctionPtrVC6W> DelegateType;

	public:

		typedef Delegate5 Type;

		//////////////////////////////////////////////////////////////
		/// Constructors
		//////////////////////////////////////////////////////////////
		Delegate5() { Reset(); }
		Delegate5(const Delegate5& d) { mDelegate.Copy(this, d.mDelegate); }
		template<typename X, typename Y>
		Delegate5(Y* pThis, DesiredRetType(X::*functionToBind)(Param1, Param2, Param3, Param4, Param5)) { Bind(pThis, functionToBind); }
		template<typename X, typename Y>
		Delegate5(const Y* pThis, DesiredRetType(X::*functionToBind)(Param1, Param2, Param3, Param4, Param5)) { Bind(pThis, functionToBind); }
		Delegate5(DesiredRetType(*functionToBind)(Param1, Param2, Param3, Param4, Param5)) { Bind(functionToBind); }

		//////////////////////////////////////////////////////////////
		/// Operators
		//////////////////////////////////////////////////////////////
		Delegate5& operator= (const Delegate5& d)
		{
			mDelegate.Copy(this, d.mDelegate);
			return *this;
		}
		Delegate5& operator= (DesiredRetType(*functionToBind)(Param1, Param2, Param3, Param4, Param5))
		{
			Bind(functionToBind);
			return *this;
		}
		bool operator== (const Delegate5& d) const
		{
			return mDelegate.IsEqual(d.mDelegate);
		}
		bool operator!= (const Delegate5& d) const
		{
			return !mDelegate.IsEqual(d.mDelegate);
		}
		bool operator< (const Delegate5& d) const
		{
			return mDelegate.IsLess(d.mDelegate);
		}
		bool operator<= (const Delegate5& d) const
		{
			return mDelegate.IsLess(d.mDelegate) || mDelegate.IsEqual(d.mDelegate);
		}
		bool operator> (const Delegate5& d) const
		{
			return d.mDelegate.IsLess(mDelegate);
		}
		bool operator>= (const Delegate5& d) const
		{
			return d.mDelegate.IsLess(mDelegate) || mDelegate.IsEqual(d.mDelegate);
		}

		template<typename X, typename Y>
		inline void Bind(Y* pThis, DesiredRetType(X::*functionToBind)(Param1, Param2, Param3, Param4, Param5))
		{
			mDelegate.BindMemberFunction(Priv::SimpleCast<Y*>(pThis), functionToBind);
		}

		template<typename X, typename Y>
		inline void Bind(const Y* pThis, DesiredRetType(X::*functionToBind)(Param1, Param2, Param3, Param4, Param5))
		{
			mDelegate.BindConstMemberFunction(Priv::SimpleCast<const Y*>(pThis), functionToBind);
		}

		inline void Bind(DesiredRetType(*functionToBind)(Param1, Param2, Param3, Param4, Param5))
		{
			mDelegate.BindStaticFunction(this, &Delegate5::StaticFunctionInvoker, functionToBind);
		}

		RetType operator() (Param1 p1, Param2 p2, Param3 p3, Param4 p4, Param5 p5) const
		{
			return Invoke(p1, p2, p3, p4, p5);
		}
		RetType Invoke(Param1 p1, Param2 p2, Param3 p3, Param4 p4, Param5 p5) const
		{
			return ((mDelegate.GetThisPtr()->*(mDelegate.GetFunctionPtr()))(p1, p2, p3, p4, p5));
		}

		operator bool() const
		{
			return Empty();
		}

		inline bool operator== (StaticFunctionPtr p)
		{
			return mDelegate.IsEqualToStaticFunctionPointer(p);
		}
		inline bool operator!= (StaticFunctionPtr p)
		{
			return !mDelegate.IsEqualToStaticFunctionPointer(p);
		}
		inline bool operator! () const
		{
			return !mDelegate;
		}
		inline bool Empty() const
		{
			return !mDelegate;
		}

	private:
		void Reset() { mDelegate.Reset(); }
		RetType StaticFunctionInvoker(Param1 p1, Param2 p2, Param3 p3, Param4 p4, Param5 p5) const
		{
			return ((*(mDelegate.GetStaticFunctionPtr()))(p1, p2, p3, p4, p5));
		}

		DelegateType mDelegate;
	};

	template<typename Param1, typename Param2, typename Param3, typename Param4, typename Param5, typename Param6, typename RetType = Priv::VoidDummy>
	class Delegate6
	{
	private:
		typedef typename Priv::ConvertFromVoidDummy<RetType>::Type DesiredRetType;
		typedef DesiredRetType(*StaticFunctionPtr)(Param1, Param2, Param3, Param4, Param5, Param6);
		typedef RetType(*StaticFunctionPtrVC6W)(Param1, Param2, Param3, Param4, Param5, Param6);
		typedef RetType(Priv::ClassTypeDummy::*MemberFunctionPtr)(Param1, Param2, Param3, Param4, Param5, Param6);
		typedef Priv::BaseDelegate<MemberFunctionPtr, StaticFunctionPtr, StaticFunctionPtrVC6W> DelegateType;

	public:

		typedef Delegate6 Type;

		//////////////////////////////////////////////////////////////
		/// Constructors
		//////////////////////////////////////////////////////////////
		Delegate6() { Reset(); }
		Delegate6(const Delegate6& d) { mDelegate.Copy(this, d.mDelegate); }
		template<typename X, typename Y>
		Delegate6(Y* pThis, DesiredRetType(X::*functionToBind)(Param1, Param2, Param3, Param4, Param5, Param6)) { Bind(pThis, functionToBind); }
		template<typename X, typename Y>
		Delegate6(const Y* pThis, DesiredRetType(X::*functionToBind)(Param1, Param2, Param3, Param4, Param5, Param6)) { Bind(pThis, functionToBind); }
		Delegate6(DesiredRetType(*functionToBind)(Param1, Param2, Param3, Param4, Param5, Param6)) { Bind(functionToBind); }

		//////////////////////////////////////////////////////////////
		/// Operators
		//////////////////////////////////////////////////////////////
		Delegate6& operator= (const Delegate6& d)
		{
			mDelegate.Copy(this, d.mDelegate);
			return *this;
		}
		Delegate6& operator= (DesiredRetType(*functionToBind)(Param1, Param2, Param3, Param4, Param5, Param6))
		{
			Bind(functionToBind);
			return *this;
		}
		bool operator== (const Delegate6& d) const
		{
			return mDelegate.IsEqual(d.mDelegate);
		}
		bool operator!= (const Delegate6& d) const
		{
			return !mDelegate.IsEqual(d.mDelegate);
		}
		bool operator< (const Delegate6& d) const
		{
			return mDelegate.IsLess(d.mDelegate);
		}
		bool operator<= (const Delegate6& d) const
		{
			return mDelegate.IsLess(d.mDelegate) || mDelegate.IsEqual(d.mDelegate);
		}
		bool operator> (const Delegate6& d) const
		{
			return d.mDelegate.IsLess(mDelegate);
		}
		bool operator>= (const Delegate6& d) const
		{
			return d.mDelegate.IsLess(mDelegate) || mDelegate.IsEqual(d.mDelegate);
		}

		template<typename X, typename Y>
		inline void Bind(Y* pThis, DesiredRetType(X::*functionToBind)(Param1, Param2, Param3, Param4, Param5, Param6))
		{
			mDelegate.BindMemberFunction(Priv::SimpleCast<Y*>(pThis), functionToBind);
		}

		template<typename X, typename Y>
		inline void Bind(const Y* pThis, DesiredRetType(X::*functionToBind)(Param1, Param2, Param3, Param4, Param5, Param6))
		{
			mDelegate.BindConstMemberFunction(Priv::SimpleCast<const Y*>(pThis), functionToBind);
		}

		inline void Bind(DesiredRetType(*functionToBind)(Param1, Param2, Param3, Param4, Param5, Param6))
		{
			mDelegate.BindStaticFunction(this, &Delegate6::StaticFunctionInvoker, functionToBind);
		}

		RetType operator() (Param1 p1, Param2 p2, Param3 p3, Param4 p4, Param5 p5, Param6 p6) const
		{
			return Invoke(p1, p2, p3, p4, p5, p6);
		}
		RetType Invoke(Param1 p1, Param2 p2, Param3 p3, Param4 p4, Param5 p5, Param6 p6) const
		{
			return ((mDelegate.GetThisPtr()->*(mDelegate.GetFunctionPtr()))(p1, p2, p3, p4, p5, p6));
		}

		operator bool() const
		{
			return Empty();
		}

		inline bool operator== (StaticFunctionPtr p)
		{
			return mDelegate.IsEqualToStaticFunctionPointer(p);
		}
		inline bool operator!= (StaticFunctionPtr p)
		{
			return !mDelegate.IsEqualToStaticFunctionPointer(p);
		}
		inline bool operator! () const
		{
			return !mDelegate;
		}
		inline bool Empty() const
		{
			return !mDelegate;
		}

	private:
		void Reset() { mDelegate.Reset(); }
		RetType StaticFunctionInvoker(Param1 p1, Param2 p2, Param3 p3, Param4 p4, Param5 p5, Param6 p6) const
		{
			return ((*(mDelegate.GetStaticFunctionPtr()))(p1, p2, p3, p4, p5, p6));
		}

		DelegateType mDelegate;
	};

	template<typename Param1, typename Param2, typename Param3, typename Param4, typename Param5, typename Param6, typename Param7, typename RetType = Priv::VoidDummy>
	class Delegate7
	{
	private:
		typedef typename Priv::ConvertFromVoidDummy<RetType>::Type DesiredRetType;
		typedef DesiredRetType(*StaticFunctionPtr)(Param1, Param2, Param3, Param4, Param5, Param6, Param7);
		typedef RetType(*StaticFunctionPtrVC6W)(Param1, Param2, Param3, Param4, Param5, Param6, Param7);
		typedef RetType(Priv::ClassTypeDummy::*MemberFunctionPtr)(Param1, Param2, Param3, Param4, Param5, Param6, Param7);
		typedef Priv::BaseDelegate<MemberFunctionPtr, StaticFunctionPtr, StaticFunctionPtrVC6W> DelegateType;

	public:

		typedef Delegate7 Type;

		//////////////////////////////////////////////////////////////
		/// Constructors
		//////////////////////////////////////////////////////////////
		Delegate7() { Reset(); }
		Delegate7(const Delegate7& d) { mDelegate.Copy(this, d.mDelegate); }
		template<typename X, typename Y>
		Delegate7(Y* pThis, DesiredRetType(X::*functionToBind)(Param1, Param2, Param3, Param4, Param5, Param6, Param7)) { Bind(pThis, functionToBind); }
		template<typename X, typename Y>
		Delegate7(const Y* pThis, DesiredRetType(X::*functionToBind)(Param1, Param2, Param3, Param4, Param5, Param6, Param7)) { Bind(pThis, functionToBind); }
		Delegate7(DesiredRetType(*functionToBind)(Param1, Param2, Param3, Param4, Param5, Param6, Param7)) { Bind(functionToBind); }

		//////////////////////////////////////////////////////////////
		/// Operators
		//////////////////////////////////////////////////////////////
		Delegate7& operator= (const Delegate7& d)
		{
			mDelegate.Copy(this, d.mDelegate);
			return *this;
		}
		Delegate7& operator= (DesiredRetType(*functionToBind)(Param1, Param2, Param3, Param4, Param5, Param6, Param7))
		{
			Bind(functionToBind);
			return *this;
		}
		bool operator== (const Delegate7& d) const
		{
			return mDelegate.IsEqual(d.mDelegate);
		}
		bool operator!= (const Delegate7& d) const
		{
			return !mDelegate.IsEqual(d.mDelegate);
		}
		bool operator< (const Delegate7& d) const
		{
			return mDelegate.IsLess(d.mDelegate);
		}
		bool operator<= (const Delegate7& d) const
		{
			return mDelegate.IsLess(d.mDelegate) || mDelegate.IsEqual(d.mDelegate);
		}
		bool operator> (const Delegate7& d) const
		{
			return d.mDelegate.IsLess(mDelegate);
		}
		bool operator>= (const Delegate7& d) const
		{
			return d.mDelegate.IsLess(mDelegate) || mDelegate.IsEqual(d.mDelegate);
		}

		template<typename X, typename Y>
		inline void Bind(Y* pThis, DesiredRetType(X::*functionToBind)(Param1, Param2, Param3, Param4, Param5, Param6, Param7))
		{
			mDelegate.BindMemberFunction(Priv::SimpleCast<Y*>(pThis), functionToBind);
		}

		template<typename X, typename Y>
		inline void Bind(const Y* pThis, DesiredRetType(X::*functionToBind)(Param1, Param2, Param3, Param4, Param5, Param6, Param7))
		{
			mDelegate.BindConstMemberFunction(Priv::SimpleCast<const Y*>(pThis), functionToBind);
		}

		inline void Bind(DesiredRetType(*functionToBind)(Param1, Param2, Param3, Param4, Param5, Param6, Param7))
		{
			mDelegate.BindStaticFunction(this, &Delegate7::StaticFunctionInvoker, functionToBind);
		}

		RetType operator() (Param1 p1, Param2 p2, Param3 p3, Param4 p4, Param5 p5, Param6 p6, Param7 p7) const
		{
			return Invoke(p1, p2, p3, p4, p5, p6, p7);
		}
		RetType Invoke(Param1 p1, Param2 p2, Param3 p3, Param4 p4, Param5 p5, Param6 p6, Param7 p7) const
		{
			return ((mDelegate.GetThisPtr()->*(mDelegate.GetFunctionPtr()))(p1, p2, p3, p4, p5, p6, p7));
		}

		operator bool() const
		{
			return Empty();
		}

		inline bool operator== (StaticFunctionPtr p)
		{
			return mDelegate.IsEqualToStaticFunctionPointer(p);
		}
		inline bool operator!= (StaticFunctionPtr p)
		{
			return !mDelegate.IsEqualToStaticFunctionPointer(p);
		}
		inline bool operator! () const
		{
			return !mDelegate;
		}
		inline bool Empty() const
		{
			return !mDelegate;
		}

	private:
		void Reset() { mDelegate.Reset(); }
		RetType StaticFunctionInvoker(Param1 p1, Param2 p2, Param3 p3, Param4 p4, Param5 p5, Param6 p6, Param7 p7) const
		{
			return ((*(mDelegate.GetStaticFunctionPtr()))(p1, p2, p3, p4, p5, p6, p7));
		}

		DelegateType mDelegate;
	};

	template<typename Param1, typename Param2, typename Param3, typename Param4, typename Param5, typename Param6, typename Param7, typename Param8, typename RetType = Priv::VoidDummy>
	class Delegate8
	{
	private:
		typedef typename Priv::ConvertFromVoidDummy<RetType>::Type DesiredRetType;
		typedef DesiredRetType(*StaticFunctionPtr)(Param1, Param2, Param3, Param4, Param5, Param6, Param7, Param8);
		typedef RetType(*StaticFunctionPtrVC6W)(Param1, Param2, Param3, Param4, Param5, Param6, Param7, Param8);
		typedef RetType(Priv::ClassTypeDummy::*MemberFunctionPtr)(Param1, Param2, Param3, Param4, Param5, Param6, Param7, Param8);
		typedef Priv::BaseDelegate<MemberFunctionPtr, StaticFunctionPtr, StaticFunctionPtrVC6W> DelegateType;

	public:

		typedef Delegate8 Type;

		//////////////////////////////////////////////////////////////
		/// Constructors
		//////////////////////////////////////////////////////////////
		Delegate8() { Reset(); }
		Delegate8(const Delegate8& d) { mDelegate.Copy(this, d.mDelegate); }
		template<typename X, typename Y>
		Delegate8(Y* pThis, DesiredRetType(X::*functionToBind)(Param1, Param2, Param3, Param4, Param5, Param6, Param7, Param8)) { Bind(pThis, functionToBind); }
		template<typename X, typename Y>
		Delegate8(const Y* pThis, DesiredRetType(X::*functionToBind)(Param1, Param2, Param3, Param4, Param5, Param6, Param7, Param8)) { Bind(pThis, functionToBind); }
		Delegate8(DesiredRetType(*functionToBind)(Param1, Param2, Param3, Param4, Param5, Param6, Param7, Param8)) { Bind(functionToBind); }

		//////////////////////////////////////////////////////////////
		/// Operators
		//////////////////////////////////////////////////////////////
		Delegate8& operator= (const Delegate8& d)
		{
			mDelegate.Copy(this, d.mDelegate);
			return *this;
		}
		Delegate8& operator= (DesiredRetType(*functionToBind)(Param1, Param2, Param3, Param4, Param5, Param6, Param7, Param8))
		{
			Bind(functionToBind);
			return *this;
		}
		bool operator== (const Delegate8& d) const
		{
			return mDelegate.IsEqual(d.mDelegate);
		}
		bool operator!= (const Delegate8& d) const
		{
			return !mDelegate.IsEqual(d.mDelegate);
		}
		bool operator< (const Delegate8& d) const
		{
			return mDelegate.IsLess(d.mDelegate);
		}
		bool operator<= (const Delegate8& d) const
		{
			return mDelegate.IsLess(d.mDelegate) || mDelegate.IsEqual(d.mDelegate);
		}
		bool operator> (const Delegate8& d) const
		{
			return d.mDelegate.IsLess(mDelegate);
		}
		bool operator>= (const Delegate8& d) const
		{
			return d.mDelegate.IsLess(mDelegate) || mDelegate.IsEqual(d.mDelegate);
		}

		template<typename X, typename Y>
		inline void Bind(Y* pThis, DesiredRetType(X::*functionToBind)(Param1, Param2, Param3, Param4, Param5, Param6, Param7, Param8))
		{
			mDelegate.BindMemberFunction(Priv::SimpleCast<Y*>(pThis), functionToBind);
		}

		template<typename X, typename Y>
		inline void Bind(const Y* pThis, DesiredRetType(X::*functionToBind)(Param1, Param2, Param3, Param4, Param5, Param6, Param7, Param8))
		{
			mDelegate.BindConstMemberFunction(Priv::SimpleCast<const Y*>(pThis), functionToBind);
		}

		inline void Bind(DesiredRetType(*functionToBind)(Param1, Param2, Param3, Param4, Param5, Param6, Param7, Param8))
		{
			mDelegate.BindStaticFunction(this, &Delegate8::StaticFunctionInvoker, functionToBind);
		}

		RetType operator() (Param1 p1, Param2 p2, Param3 p3, Param4 p4, Param5 p5, Param6 p6, Param7 p7, Param8 p8) const
		{
			return Invoke(p1, p2, p3, p4, p5, p6, p7, p8);
		}
		RetType Invoke(Param1 p1, Param2 p2, Param3 p3, Param4 p4, Param5 p5, Param6 p6, Param7 p7, Param8 p8) const
		{
			return ((mDelegate.GetThisPtr()->*(mDelegate.GetFunctionPtr()))(p1, p2, p3, p4, p5, p6, p7, p8));
		}

		operator bool() const
		{
			return Empty();
		}

		inline bool operator== (StaticFunctionPtr p)
		{
			return mDelegate.IsEqualToStaticFunctionPointer(p);
		}
		inline bool operator!= (StaticFunctionPtr p)
		{
			return !mDelegate.IsEqualToStaticFunctionPointer(p);
		}
		inline bool operator! () const
		{
			return !mDelegate;
		}
		inline bool Empty() const
		{
			return !mDelegate;
		}

	private:
		void Reset() { mDelegate.Reset(); }
		RetType StaticFunctionInvoker(Param1 p1, Param2 p2, Param3 p3, Param4 p4, Param5 p5, Param6 p6, Param7 p7, Param8 p8) const
		{
			return ((*(mDelegate.GetStaticFunctionPtr()))(p1, p2, p3, p4, p5, p6, p7, p8));
		}

		DelegateType mDelegate;
	};

	//////////////////////////////////////////////////////////////
	/// Function type syntax
	//////////////////////////////////////////////////////////////
#if defined(DelegatesAllowFunctionTypeSyntax)

	template<typename Signature>
	class Delegate;

	template<typename R>
	class Delegate<R()> : public Delegate0<R>
	{
	public:

		typedef Delegate0<R> BaseType;
		typedef Delegate Type;

		Delegate() : BaseType() {}

		template<typename X, typename Y>
		Delegate(Y* pThis, R(X::*functionToBind)()) :
			BaseType(pThis, functionToBind)
		{
		}

		template<typename X, typename Y>
		Delegate(const Y* pThis, R(X::*functionToBind)()) :
			BaseType(pThis, functionToBind)
		{
		}

		Delegate(R(*functionToBind)()) :
			BaseType(functionToBind)
		{
		}

		Delegate& operator= (const BaseType& d)
		{
			*static_cast<BaseType*>(this) = d;
		}
	};

	template<typename R, typename Param1>
	class Delegate<R(Param1)> : public Delegate1<Param1, R>
	{
	public:

		typedef Delegate1<Param1, R> BaseType;
		typedef Delegate Type;

		Delegate() : BaseType() {}

		template<typename X, typename Y>
		Delegate(Y* pThis, R(X::*functionToBind)(Param1)) :
			BaseType(pThis, functionToBind)
		{
		}

		template<typename X, typename Y>
		Delegate(const Y* pThis, R(X::*functionToBind)(Param1)) :
			BaseType(pThis, functionToBind)
		{
		}

		Delegate(R(*functionToBind)(Param1)) :
			BaseType(functionToBind)
		{
		}

		Delegate& operator= (const BaseType& d)
		{
			*static_cast<BaseType*>(this) = d;
		}
	};

	template<typename R, typename Param1, typename Param2>
	class Delegate<R(Param1, Param2)> : public Delegate2<Param1, Param2, R>
	{
	public:

		typedef Delegate2<Param1, Param2, R> BaseType;
		typedef Delegate Type;

		Delegate() : BaseType() {}

		template<typename X, typename Y>
		Delegate(Y* pThis, R(X::*functionToBind)(Param1, Param2)) :
			BaseType(pThis, functionToBind)
		{
		}

		template<typename X, typename Y>
		Delegate(const Y* pThis, R(X::*functionToBind)(Param1, Param2)) :
			BaseType(pThis, functionToBind)
		{
		}

		Delegate(R(*functionToBind)(Param1, Param2)) :
			BaseType(functionToBind)
		{
		}

		Delegate& operator= (const BaseType& d)
		{
			*static_cast<BaseType*>(this) = d;
		}
	};

	template<typename R, typename Param1, typename Param2, typename Param3>
	class Delegate<R(Param1, Param2, Param3)> : public Delegate3<Param1, Param2, Param3, R>
	{
	public:

		typedef Delegate3<Param1, Param2, Param3, R> BaseType;
		typedef Delegate Type;

		Delegate() : BaseType() {}

		template<typename X, typename Y>
		Delegate(Y* pThis, R(X::*functionToBind)(Param1, Param2, Param3)) :
			BaseType(pThis, functionToBind)
		{
		}

		template<typename X, typename Y>
		Delegate(const Y* pThis, R(X::*functionToBind)(Param1, Param2, Param3)) :
			BaseType(pThis, functionToBind)
		{
		}

		Delegate(R(*functionToBind)(Param1, Param2, Param3)) :
			BaseType(functionToBind)
		{
		}

		Delegate& operator= (const BaseType& d)
		{
			*static_cast<BaseType*>(this) = d;
		}
	};

	template<typename R, typename Param1, typename Param2, typename Param3, typename Param4>
	class Delegate<R(Param1, Param2, Param3, Param4)> : public Delegate4<Param1, Param2, Param3, Param4, R>
	{
	public:

		typedef Delegate4<Param1, Param2, Param3, Param4, R> BaseType;
		typedef Delegate Type;

		Delegate() : BaseType() {}

		template<typename X, typename Y>
		Delegate(Y* pThis, R(X::*functionToBind)(Param1, Param2, Param3, Param4)) :
			BaseType(pThis, functionToBind)
		{
		}

		template<typename X, typename Y>
		Delegate(const Y* pThis, R(X::*functionToBind)(Param1, Param2, Param3, Param4)) :
			BaseType(pThis, functionToBind)
		{
		}

		Delegate(R(*functionToBind)(Param1, Param2, Param3, Param4)) :
			BaseType(functionToBind)
		{
		}

		Delegate& operator= (const BaseType& d)
		{
			*static_cast<BaseType*>(this) = d;
		}
	};

	template<typename R, typename Param1, typename Param2, typename Param3, typename Param4, typename Param5>
	class Delegate<R(Param1, Param2, Param3, Param4, Param5)> : public Delegate5<Param1, Param2, Param3, Param4, Param5, R>
	{
	public:

		typedef Delegate5<Param1, Param2, Param3, Param4, Param5, R> BaseType;
		typedef Delegate Type;

		Delegate() : BaseType() {}

		template<typename X, typename Y>
		Delegate(Y* pThis, R(X::*functionToBind)(Param1, Param2, Param3, Param4, Param5)) :
			BaseType(pThis, functionToBind)
		{
		}

		template<typename X, typename Y>
		Delegate(const Y* pThis, R(X::*functionToBind)(Param1, Param2, Param3, Param4, Param5)) :
			BaseType(pThis, functionToBind)
		{
		}

		Delegate(R(*functionToBind)(Param1, Param2, Param3, Param4, Param5)) :
			BaseType(functionToBind)
		{
		}

		Delegate& operator= (const BaseType& d)
		{
			*static_cast<BaseType*>(this) = d;
		}
	};

	template<typename R, typename Param1, typename Param2, typename Param3, typename Param4, typename Param5, typename Param6>
	class Delegate<R(Param1, Param2, Param3, Param4, Param5, Param6)> : public Delegate6<Param1, Param2, Param3, Param4, Param5, Param6, R>
	{
	public:

		typedef Delegate6<Param1, Param2, Param3, Param4, Param5, Param6, R> BaseType;
		typedef Delegate Type;

		Delegate() : BaseType() {}

		template<typename X, typename Y>
		Delegate(Y* pThis, R(X::*functionToBind)(Param1, Param2, Param3, Param4, Param5, Param6)) :
			BaseType(pThis, functionToBind)
		{
		}

		template<typename X, typename Y>
		Delegate(const Y* pThis, R(X::*functionToBind)(Param1, Param2, Param3, Param4, Param5, Param6)) :
			BaseType(pThis, functionToBind)
		{
		}

		Delegate(R(*functionToBind)(Param1, Param2, Param3, Param4, Param5, Param6)) :
			BaseType(functionToBind)
		{
		}

		Delegate& operator= (const BaseType& d)
		{
			*static_cast<BaseType*>(this) = d;
		}
	};

	template<typename R, typename Param1, typename Param2, typename Param3, typename Param4, typename Param5, typename Param6, typename Param7>
	class Delegate<R(Param1, Param2, Param3, Param4, Param5, Param6, Param7)> : public Delegate7<Param1, Param2, Param3, Param4, Param5, Param6, Param7, R>
	{
	public:

		typedef Delegate7<Param1, Param2, Param3, Param4, Param5, Param6, Param7, R> BaseType;
		typedef Delegate Type;

		Delegate() : BaseType() {}

		template<typename X, typename Y>
		Delegate(Y* pThis, R(X::*functionToBind)(Param1, Param2, Param3, Param4, Param5, Param6, Param7)) :
			BaseType(pThis, functionToBind)
		{
		}

		template<typename X, typename Y>
		Delegate(const Y* pThis, R(X::*functionToBind)(Param1, Param2, Param3, Param4, Param5, Param6, Param7)) :
			BaseType(pThis, functionToBind)
		{
		}

		Delegate(R(*functionToBind)(Param1, Param2, Param3, Param4, Param5, Param6, Param7)) :
			BaseType(functionToBind)
		{
		}

		Delegate& operator= (const BaseType& d)
		{
			*static_cast<BaseType*>(this) = d;
		}
	};

	template<typename R, typename Param1, typename Param2, typename Param3, typename Param4, typename Param5, typename Param6, typename Param7, typename Param8>
	class Delegate<R(Param1, Param2, Param3, Param4, Param5, Param6, Param7, Param8)> : public Delegate8<Param1, Param2, Param3, Param4, Param5, Param6, Param7, Param8, R>
	{
	public:

		typedef Delegate8<Param1, Param2, Param3, Param4, Param5, Param6, Param7, Param8, R> BaseType;
		typedef Delegate Type;

		Delegate() : BaseType() {}

		template<typename X, typename Y>
		Delegate(Y* pThis, R(X::*functionToBind)(Param1, Param2, Param3, Param4, Param5, Param6, Param7, Param8)) :
			BaseType(pThis, functionToBind)
		{
		}

		template<typename X, typename Y>
		Delegate(const Y* pThis, R(X::*functionToBind)(Param1, Param2, Param3, Param4, Param5, Param6, Param7, Param8)) :
			BaseType(pThis, functionToBind)
		{
		}

		Delegate(R(*functionToBind)(Param1, Param2, Param3, Param4, Param5, Param6, Param7, Param8)) :
			BaseType(functionToBind)
		{
		}

		Delegate& operator= (const BaseType& d)
		{
			*static_cast<BaseType*>(this) = d;
		}
	};

#endif
}

#endif // DELEGATE_HPP
