#ifndef EVENT_HPP
#define EVENT_HPP

#include <VoxE/Native/Atomic.hpp>
#include <memory>

namespace VX
{
	typedef vxU32 EventID;
	static const EventID kInvalidEventID = 0xFFFFFFFF;

#define DECLARE_EVENT(id) static EventID GetEventID() \
	{ \
		return id; \
	}

	//////////////////////////////////////////////////////////////
	/// \class Event
	/// This class is the base class used for declaring custom
	/// events which may be queued and triggered by the event
	/// manager in order to decouple interdependent engine
	/// subsystems from each other.
	///
	/// Every event is defined by some unique event ID. This ID
	/// is the same in all instances of one event class in order
	/// to determine the type of an event at runtime.
	///
	/// Every class that inherits the Event class has to declare
	/// a static and public method called GetEventID() which
	/// takes no parameters but returns the event type's unique
	/// ID. This is done either manually or by using the
	/// DECLARE_EVENT macro. The DECLARE_EVENT macro has to be
	/// placed into a public scope.
	//////////////////////////////////////////////////////////////
	class Event
	{
	public:

		Event(const EventID id);
		virtual ~Event();

		DECLARE_EVENT(kInvalidEventID)

		//////////////////////////////////////////////////////////////
		/// Returns the event instance's unique event ID.
		//////////////////////////////////////////////////////////////
		const EventID GetID() const;

	private:

		EventID mID;
	};

	typedef std::shared_ptr<Event> EventPtr;
}

#endif // EVENT_HPP