/*
 * EventListener.hpp
 *
 *  Created on: 31.07.2014
 *      Author: euaconlabs
 */

#ifndef EVENTLISTENER_HPP_
#define EVENTLISTENER_HPP_

namespace VX
{
	//////////////////////////////////////////////////////////////
	/// \class EventListener
	/// \brief Allows for binding event handlers on specific
	/// 	object instances.
	/// The main reason for an event listener is that it allows
	/// for less error-prone code due to better wrapping and
	/// less code replication in classes.
	/// It keeps track about all event handlers currently bound
	/// and ensures that every event handler will be released
	/// when the object is destroyed.
	//////////////////////////////////////////////////////////////
	class EventListener
	{
	public:

		EventListener();
	};
}

#endif /* EVENTLISTENER_HPP_ */
