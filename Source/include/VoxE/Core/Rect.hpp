#ifndef RECT_HPP
#define RECT_HPP

#include <VoxE/Native/Atomic.hpp>
#include <VoxE/Core/Vector.hpp>

namespace VX
{
	template<typename T>
	struct Rect
	{
	public:

		T mLeft;
		T mTop;
		T mWidth;
		T mHeight;

		Rect() :
			mLeft(T(0)),
			mTop(T(0)),
			mWidth(T(0)),
			mHeight(T(0))
		{
		}

		Rect(const T& left, const T& top, const T& width, const T& height) :
			mLeft(left),
			mTop(top),
			mWidth(width),
			mHeight(height)
		{
		}

		vxBool IsInside(const Vector2<T>& point)
		{
			if (point.x < mLeft || point.x > (mLeft + mWidth) || point.y < mTop || point.y > (mTop + mHeight))
				return false;
			return true;
		}

		vxBool Intersects(const Rect<T>& rect)
		{
			if (mLeft < rect.mLeft + rect.mWidth  && mLeft + mWidth  > rect.mLeft &&
				mTop  < rect.mTop + rect.mHeight && mTop + mHeight > rect.mTop)
				return true;
			return false;
		}
	};

	typedef Rect<vxI32> IntRect;
	typedef Rect<vxU32> UIntRect;
	typedef Rect<vxF32> FloatRect;
}

#endif // RECT_HPP