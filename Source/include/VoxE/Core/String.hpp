#ifndef STRING_HPP
#define STRING_HPP

#include <string>

namespace VX
{
	typedef std::string vxString;
}

#endif // STRING_HPP
