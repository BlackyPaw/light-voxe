/*
 * StringTable.hpp
 *
 *  Created on: 08.07.2014
 *      Author: euaconlabs
 */

#ifndef STRINGTABLE_HPP_
#define STRINGTABLE_HPP_

#include <VoxE/Core/ISingleton.hpp>
#include <VoxE/Native/Atomic.hpp>

#include <unordered_map>

namespace VX
{
	typedef vxU32 StringID;

	/////////////////////////////////////////////////////////////
	/// \class StringTable
	/// \brief This class is responsible for interning strings.
	/// Basically this class interns any given string by hashing
	/// it using the FNV-1A algorithm and then inserting it into
	/// a table for later lookup. If two interned strings collide
	/// the older one will be replaced with the newer one.
	/////////////////////////////////////////////////////////////
	class StringTable : public ISingleton<StringTable>
	{
	public:

		StringTable();
		~StringTable();

		/////////////////////////////////////////////////////////////
		/// Interns the given string and returns its hash value. If
		/// len is set to 0 or obmitted the string is assumed to be
		/// null-terminated. Otherwise only len bytes of it will be
		/// interned.
		/// \param str The string to intern.
		/// \param len The number of bytes to hash.
		/////////////////////////////////////////////////////////////
		StringID InternString(const vxChar* str, const vxU32 len = 0);

		/////////////////////////////////////////////////////////////
		/// Looks up the string under the specified string id. The
		/// string returned must not be freed as it will be freed
		/// internally when the program terminates.
		/// \returns A null-terminated string corresponding to the
		///		given string id or nullptr if the string could not
		///		be found.
		/////////////////////////////////////////////////////////////
		const vxChar* LookupString(const StringID& id);

	private:

		StringID HashString(const vxChar* str, const vxU32 len);

		std::unordered_map<StringID, const vxChar*> mLookup;
	};
}

#endif /* STRINGTABLE_HPP_ */
