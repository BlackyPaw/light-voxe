#ifndef VECTOR_HPP
#define VECTOR_HPP

#include <VoxE/Native/Atomic.hpp>

//////////////////////////////////////////////////////////////
/// STL
//////////////////////////////////////////////////////////////
#include <cassert>
#include <cmath>

namespace VX
{
	//////////////////////////////////////////////////////////////
	/// \class Vector2
	/// \tparam The type to use in the vector.
	///
	/// Class for representing 2-dimensional vectors.
	//////////////////////////////////////////////////////////////
	template<typename T>
	struct Vector2
	{
	public:

		typedef T Value;

		union
		{
			struct { Value x; Value y; };
			struct { Value r; Value g; };
		};

		Vector2() :
			x(Value(0)), y(Value(0))
		{
		}

		Vector2(const Value& _x, const Value& _y) :
			x(_x), y(_y)
		{
		}

		T Length() const
		{
			return std::sqrt(x * x + y * y);
		}

		Vector2<T>& Normalize()
		{
			(*this) /= Length();
			return *this;
		}

		Vector2<T> Normalize() const
		{
			Vector2<T> tmp(*this);
			tmp /= Length();
			return tmp;
		}

		T Dot(const Vector2<T>& b) const
		{
			return (x * b.x + y * b.y);
		}

		Vector2<T>& operator+= (const Value& v)
		{
			x += v;
			y += v;
			return *this;
		}
		Vector2<T>& operator+= (const Vector2<T>& v)
		{
			x += v.x;
			y += v.y;
			return *this;
		}
		Vector2<T>& operator-= (const Value& v)
		{
			x -= v;
			y -= v;
			return *this;
		}
		Vector2<T>& operator-= (const Vector2<T>& v)
		{
			x -= v.x;
			y -= v.y;
			return *this;
		}
		Vector2<T>& operator*= (const Value& v)
		{
			x *= v;
			y *= v;
			return *this;
		}
		Vector2<T>& operator*= (const Vector2<T>& v)
		{
			x *= v.x;
			y *= v.y;
			return *this;
		}
		Vector2<T>& operator/= (const Value& v)
		{
			x /= v;
			y /= v;
			return *this;
		}
		Vector2<T>& operator/= (const Vector2<T>& v)
		{
			x /= v.x;
			y /= v.y;
			return *this;
		}

		bool operator== (const Vector2<T>& v)
		{
			return (x == v.x && y == v.y);
		}

		bool operator!= (const Vector2<T>& v)
		{
			return !((*this) == v);
		}

		T& operator[] (const vxU32 n)
		{
			assert(!(n < 0 && n > 1));
			return *((&x) + n);
		}
		T const& operator[] (const vxU32 n) const
		{
			assert(!(n < 0 && n > 1));
			return *((&x) + n);
		}

		Vector2<T> operator- () const
		{
			return Vector2<T>(-x, -y);
		}
	};

	template<typename T>
	Vector2<T> operator+ (const Vector2<T>& a, const T& b)
	{
		Vector2<T> tmp(a);
		tmp += b;
		return tmp;
	}
	template<typename T>
	Vector2<T> operator+ (const T& a, const Vector2<T>& b)
	{
		Vector2<T> tmp(b);
		tmp += a;
		return tmp;
	}
	template<typename T>
	Vector2<T> operator+ (const Vector2<T>& a, const Vector2<T>& b)
	{
		Vector2<T> tmp(a);
		tmp += b;
		return tmp;
	}
	template<typename T>
	Vector2<T> operator- (const Vector2<T>& a, const T& b)
	{
		Vector2<T> tmp(a);
		tmp -= b;
		return tmp;
	}
	template<typename T>
	Vector2<T> operator- (const Vector2<T>& a, const Vector2<T>& b)
	{
		Vector2<T> tmp(a);
		tmp -= b;
		return tmp;
	}
	template<typename T>
	Vector2<T> operator* (const Vector2<T>& a, const T& b)
	{
		Vector2<T> tmp(a);
		tmp *= b;
		return tmp;
	}
	template<typename T>
	Vector2<T> operator* (const T& a, const Vector2<T>& b)
	{
		Vector2<T> tmp(b);
		tmp *= a;
		return tmp;
	}
	template<typename T>
	Vector2<T> operator* (const Vector2<T>& a, const Vector2<T>& b)
	{
		Vector2<T> tmp(a);
		tmp *= b;
		return tmp;
	}
	template<typename T>
	Vector2<T> operator/ (const Vector2<T>& a, const T& b)
	{
		Vector2<T> tmp(a);
		tmp /= b;
		return tmp;
	}
	template<typename T>
	Vector2<T> operator/ (const Vector2<T>& a, const Vector2<T>& b)
	{
		Vector2<T> tmp(a);
		tmp /= b;
		return tmp;
	}

	//////////////////////////////////////////////////////////////
	/// \class Vector3
	/// \tparam The type to use in the vector.
	///
	/// Class for representing 2-dimensional vectors.
	//////////////////////////////////////////////////////////////
	template<typename T>
	struct Vector3
	{
	public:

		typedef T Value;

		union
		{
			struct { Value x; Value y; Value z; };
			struct { Value r; Value g; Value b; };
		};

		Vector3() :
			x(Value(0)), y(Value(0)), z(Value(0))
		{
		}

		Vector3(const Value& _x, const Value& _y, const Value& _z) :
			x(_x), y(_y), z(_z)
		{
		}

		T Length() const
		{
			return std::sqrt(x * x + y * y + z * z);
		}

		Vector3<T>& Normalize()
		{
			(*this) /= Length();
			return *this;
		}

		Vector3<T> Normalize() const
		{
			Vector3<T> tmp(*this);
			tmp /= Length();
			return tmp;
		}

		T Dot(const Vector3<T>& b) const
		{
			return (x * b.x + y * b.y + z * b.z);
		}

		Vector3<T> Cross(const Vector3<T>& b) const
		{
			return Vector3<T>(y * b.z - z * b.y, z * b.x - x * b.z, x * b.y - y * b.x);
		}

		Vector3<T>& operator+= (const Value& v)
		{
			x += v;
			y += v;
			z += v;
			return *this;
		}
		Vector3<T>& operator+= (const Vector3<T>& v)
		{
			x += v.x;
			y += v.y;
			z += v.z;
			return *this;
		}
		Vector3<T>& operator-= (const Value& v)
		{
			x -= v;
			y -= v;
			z -= v;
			return *this;
		}
		Vector3<T>& operator-= (const Vector3<T>& v)
		{
			x -= v.x;
			y -= v.y;
			z -= v.z;
			return *this;
		}
		Vector3<T>& operator*= (const Value& v)
		{
			x *= v;
			y *= v;
			z *= v;
			return *this;
		}
		Vector3<T>& operator*= (const Vector3<T>& v)
		{
			x *= v.x;
			y *= v.y;
			z *= v.z;
			return *this;
		}
		Vector3<T>& operator/= (const Value& v)
		{
			x /= v;
			y /= v;
			z /= v;
			return *this;
		}
		Vector3<T>& operator/= (const Vector3<T>& v)
		{
			x /= v.x;
			y /= v.y;
			z /= v.z;
			return *this;
		}

		bool operator== (const Vector3<T>& v)
		{
			return (x == v.x && y == v.y && z == v.z);
		}

		bool operator!= (const Vector3<T>& v)
		{
			return !((*this) == v);
		}

		T& operator[] (const vxU32 n)
		{
			assert(!(n < 0 && n > 2));
			return *((&x) + n);
		}
		T const& operator[] (const vxU32 n) const
		{
			assert(!(n < 0 && n > 2));
			return *((&x) + n);
		}

		Vector3<T> operator- () const
		{
			return Vector3<T>(-x, -y, -z);
		}
	};

	template<typename T>
	Vector3<T> operator+ (const Vector3<T>& a, const T& b)
	{
		Vector3<T> tmp(a);
		tmp += b;
		return tmp;
	}
	template<typename T>
	Vector3<T> operator+ (const T& a, const Vector3<T>& b)
	{
		Vector3<T> tmp(b);
		tmp += a;
		return tmp;
	}
	template<typename T>
	Vector3<T> operator+ (const Vector3<T>& a, const Vector3<T>& b)
	{
		Vector3<T> tmp(a);
		tmp += b;
		return tmp;
	}
	template<typename T>
	Vector3<T> operator- (const Vector3<T>& a, const T& b)
	{
		Vector3<T> tmp(a);
		tmp -= b;
		return tmp;
	}
	template<typename T>
	Vector3<T> operator- (const Vector3<T>& a, const Vector3<T>& b)
	{
		Vector3<T> tmp(a);
		tmp -= b;
		return tmp;
	}
	template<typename T>
	Vector3<T> operator* (const Vector3<T>& a, const T& b)
	{
		Vector3<T> tmp(a);
		tmp *= b;
		return tmp;
	}
	template<typename T>
	Vector3<T> operator* (const T& a, const Vector3<T>& b)
	{
		Vector3<T> tmp(b);
		tmp *= a;
		return tmp;
	}
	template<typename T>
	Vector3<T> operator* (const Vector3<T>& a, const Vector3<T>& b)
	{
		Vector3<T> tmp(a);
		tmp *= b;
		return tmp;
	}
	template<typename T>
	Vector3<T> operator/ (const Vector3<T>& a, const T& b)
	{
		Vector3<T> tmp(a);
		tmp /= b;
		return tmp;
	}
	template<typename T>
	Vector3<T> operator/ (const Vector3<T>& a, const Vector3<T>& b)
	{
		Vector3<T> tmp(a);
		tmp /= b;
		return tmp;
	}

	//////////////////////////////////////////////////////////////
	/// \class Vector4
	/// \tparam The type to use in the vector.
	///
	/// Class for representing 2-dimensional vectors.
	//////////////////////////////////////////////////////////////
	template<typename T>
	class Vector4
	{
	public:

		typedef T Value;

		union
		{
			struct { Value x; Value y; Value z; Value w; };
			struct { Value r; Value g; Value b; Value a; };
		};

		Vector4() :
			x(Value(0)), y(Value(0)), z(Value(0)), w(Value(0))
		{
		}

		Vector4(const Value& _x, const Value& _y, const Value& _z, const Value& _w) :
			x(_x), y(_y), z(_z), w(_w)
		{
		}

		T Length() const
		{
			return std::sqrt(x * x + y * y + z * z + w * w);
		}

		Vector4<T>& Normalize()
		{
			(*this) /= Length();
			return *this;
		}

		Vector4<T> Normalize() const
		{
			Vector4<T> tmp(*this);
			tmp /= Length();
			return tmp;
		}

		T Dot(const Vector4<T>& b) const
		{
			return (x * b.x + y * b.y + z * b.z + w * b.w);
		}

		Vector4<T>& operator+= (const Value& v)
		{
			x += v;
			y += v;
			z += v;
			w += v;
			return *this;
		}
		Vector4<T>& operator+= (const Vector4<T>& v)
		{
			x += v.x;
			y += v.y;
			z += v.z;
			w += v.w;
			return *this;
		}
		Vector4<T>& operator-= (const Value& v)
		{
			x -= v;
			y -= v;
			z -= v;
			w -= v;
			return *this;
		}
		Vector4<T>& operator-= (const Vector4<T>& v)
		{
			x -= v.x;
			y -= v.y;
			z -= v.z;
			w -= v.w;
			return *this;
		}
		Vector4<T>& operator*= (const Value& v)
		{
			x *= v;
			y *= v;
			z *= v;
			w *= v;
			return *this;
		}
		Vector4<T>& operator*= (const Vector4<T>& v)
		{
			x *= v.x;
			y *= v.y;
			z *= v.z;
			w *= v.w;
			return *this;
		}
		Vector4<T>& operator/= (const Value& v)
		{
			x /= v;
			y /= v;
			z /= v;
			w /= v;
			return *this;
		}
		Vector4<T>& operator/= (const Vector4<T>& v)
		{
			x /= v.x;
			y /= v.y;
			z /= v.z;
			w /= v.w;
			return *this;
		}

		bool operator== (const Vector4<T>& v)
		{
			return (x == v.x && y == v.y && z == v.z && w == v.w);
		}

		bool operator!= (const Vector4<T>& v)
		{
			return !((*this) == v);
		}

		T& operator[] (const vxU32 n)
		{
			assert(!(n < 0 && n > 3));
			return *((&x) + n);
		}
		T const& operator[] (const vxU32 n) const
		{
			assert(!(n < 0 && n > 3));
			return *((&x) + n);
		}

		Vector4<T> operator- () const
		{
			return Vector4<T>(-x, -y, -z, -w);
		}
	};

	template<typename T>
	Vector4<T> operator+ (const Vector4<T>& a, const T& b)
	{
		Vector4<T> tmp(a);
		tmp += b;
		return tmp;
	}
	template<typename T>
	Vector4<T> operator+ (const T& a, const Vector4<T>& b)
	{
		Vector4<T> tmp(b);
		tmp += a;
		return tmp;
	}
	template<typename T>
	Vector4<T> operator+ (const Vector4<T>& a, const Vector4<T>& b)
	{
		Vector4<T> tmp(a);
		tmp += b;
		return tmp;
	}
	template<typename T>
	Vector4<T> operator- (const Vector4<T>& a, const T& b)
	{
		Vector4<T> tmp(a);
		tmp -= b;
		return tmp;
	}
	template<typename T>
	Vector4<T> operator- (const Vector4<T>& a, const Vector4<T>& b)
	{
		Vector4<T> tmp(a);
		tmp -= b;
		return tmp;
	}
	template<typename T>
	Vector4<T> operator* (const Vector4<T>& a, const T& b)
	{
		Vector4<T> tmp(a);
		tmp *= b;
		return tmp;
	}
	template<typename T>
	Vector4<T> operator* (const T& a, const Vector4<T>& b)
	{
		Vector4<T> tmp(b);
		tmp *= a;
		return tmp;
	}
	template<typename T>
	Vector4<T> operator* (const Vector4<T>& a, const Vector4<T>& b)
	{
		Vector4<T> tmp(a);
		tmp *= b;
		return tmp;
	}
	template<typename T>
	Vector4<T> operator/ (const Vector4<T>& a, const T& b)
	{
		Vector4<T> tmp(a);
		tmp /= b;
		return tmp;
	}
	template<typename T>
	Vector4<T> operator/ (const Vector4<T>& a, const Vector4<T>& b)
	{
		Vector4<T> tmp(a);
		tmp /= b;
		return tmp;
	}

	typedef Vector2<vxI32> Vector2i;
	typedef Vector2<vxU32> Vector2ui;
	typedef Vector2<vxF32> Vector2f;
	typedef Vector2<vxF64> Vector2d;

	typedef Vector3<vxI32> Vector3i;
	typedef Vector3<vxU32> Vector3ui;
	typedef Vector3<vxF32> Vector3f;
	typedef Vector3<vxF64> Vector3d;

	typedef Vector4<vxI32> Vector4i;
	typedef Vector4<vxU32> Vector4ui;
	typedef Vector4<vxF32> Vector4f;
	typedef Vector4<vxF64> Vector4d;
}

#endif
