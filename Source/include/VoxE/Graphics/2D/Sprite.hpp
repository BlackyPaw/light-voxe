/*
 * Sprite.hpp
 *
 *  Created on: 26.06.2014
 *      Author: euaconlabs
 */

#ifndef SPRITE_HPP_
#define SPRITE_HPP_

#include <VoxE/Graphics/Color.hpp>
#include <VoxE/Graphics/Renderable.hpp>
#include <VoxE/Graphics/Texture.hpp>
#include <VoxE/Resources/ResourceHandle.hpp>

namespace VX
{
	class SpriteBatch;

	//////////////////////////////////////////////////////////////
	/// \class Sprite
	/// Sprite object, i.e. textured quads which may be scaled,
	/// positioned, and rotated. Please note that sprite cannot
	/// be drawn immediately. Instead a sprite batch is used to
	/// bundle multiple sprites together.
	//////////////////////////////////////////////////////////////
	class Sprite : public Renderable
	{
	public:

		Sprite();
		Sprite(const ResourceHandle<Texture>& texture);
		Sprite(const ResourceHandle<Texture>& texture, const IntRect& region);
		~Sprite();

		/////////////////////////////////////////////////////////////
		/// Gets the bounds of the sprite in its local coordinate
		/// space.
		/////////////////////////////////////////////////////////////
		const FloatRect GetLocalBounds() const;
		//////////////////////////////////////////////////////////////
		/// Sets the texture drawn onto the sprite.
		/// \param texture The texture to use.
		/// \param reset If set to true the texture region display
		///	will be reset to the new texture's dimensions.
		//////////////////////////////////////////////////////////////
		void SetTexture(const ResourceHandle<Texture>& texture, const vxBool reset = true);
		//////////////////////////////////////////////////////////////
		/// Gets the texture drawn onto the sprite.
		//////////////////////////////////////////////////////////////
		const ResourceHandle<Texture>& GetTexture() const;
		//////////////////////////////////////////////////////////////
		/// Sets the region of the texture to display. It is described
		/// by a rectangle with pixels as its unit.
		/// \param region The region of the texture to use.
		//////////////////////////////////////////////////////////////
		void SetRegion(const IntRect& region);
		//////////////////////////////////////////////////////////////
		/// Gets the region of the sprite's texture mapped onto the
		/// sprite.
		//////////////////////////////////////////////////////////////
		const IntRect& GetRegion() const;
		//////////////////////////////////////////////////////////////
		/// Sets the color applied to the sprite when rendering.
		//////////////////////////////////////////////////////////////
		void SetColor(const Color& color);
		//////////////////////////////////////////////////////////////
		/// Gets the color applied to the sprite when rendering.
		//////////////////////////////////////////////////////////////
		const Color& GetColor() const;

		//////////////////////////////////////////////////////////////
		/// Sets the anchor point of the sprite, i.e. the point all
		/// of the sprite's transformations are anchored at. It is given in
		/// the range from 0.0 to 1.0 where this matches the sprite's
		/// dimensions.
		/// \param x The anchor on the x-axis.
		/// \param y The anchor on the y-axis.
		//////////////////////////////////////////////////////////////
		void SetAnchor(const vxF32 x, const vxF32 y);
		//////////////////////////////////////////////////////////////
		/// Sets the anchor point of the sprite, i.e. the point all
		/// of the sprite's transformations are anchored at. It is given in
		/// the range from 0.0 to 1.0 where this matches the sprite's
		/// dimensions-
		/// \param anchor The anchor point.
		//////////////////////////////////////////////////////////////
		void SetAnchor(const Vector2f& anchor);
		//////////////////////////////////////////////////////////////
		/// Gets the sprite's anchor point. See SetAnchor() for further
		/// information.
		//////////////////////////////////////////////////////////////
		const Vector2f& GetAnchor() const;

		/////////////////////////////////////////////////////////////
		// Renderable
		/////////////////////////////////////////////////////////////
		void PreRender();
		void Render();
		void PostRender();

	private:

		ResourceHandle<Texture> mTexture;
		IntRect mRegion;
		Color mColor;
		Vector2f mAnchor;
	};
}

#endif /* SPRITE_HPP_ */
