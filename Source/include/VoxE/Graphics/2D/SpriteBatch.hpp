/*
 * SpriteBatch.hpp
 *
 *  Created on: 27.06.2014
 *      Author: euaconlabs
 */

#ifndef SPRITEBATCH_HPP_
#define SPRITEBATCH_HPP_

#include <VoxE/Graphics/BlendFactor.hpp>
#include <VoxE/Graphics/IndexBuffer.hpp>
#include <VoxE/Graphics/ShaderProgram.hpp>
#include <VoxE/Graphics/VertexBuffer.hpp>
#include <VoxE/Graphics/2D/Sprite.hpp>

namespace VX
{
	/////////////////////////////////////////////////////////////
	/// \class SpriteBatch
	/// A sprite batch is used to group together the primitive
	/// submission of multiple sprites to only one single driver
	/// call in order to increase performance.
	/// Even though a sprite batch can increase performance there
	/// is a guideline which should be followed:
	///		* Group sprites with the same texture together as the
	///		  sprite batch must always flush all data when the
	///		  texture changes. The very same goes for the blending
	///		  state.
	/////////////////////////////////////////////////////////////
	class SpriteBatch
	{
	public:

		/////////////////////////////////////////////////////////////
		/// Creates a new sprite batch capable of holding the given
		/// number of sprites at max.
		/// \param maxSprites The maximum number of sprites the batch
		///		can hold.
		/////////////////////////////////////////////////////////////
		SpriteBatch(const vxU32 maxSprites = 256);
		~SpriteBatch();

		/////////////////////////////////////////////////////////////
		/// Begins a drawing session. Before drawing anything this
		/// method must be called. It initializes the sprite batch and
		/// makes it ready for drawing. After all drawing has been done
		/// a call to End() is necessary.
		/////////////////////////////////////////////////////////////
		void Begin();
		/////////////////////////////////////////////////////////////
		/// Renders a sprite using the given description.
		/// \param texture The sprite's texture.
		/// \param bounds The sprite's local bounds.
		/// \param transform The sprite's transformation.
		/// \param region The sprite's texture region.
		/// \param color The color which should be applied to the sprite.
		/////////////////////////////////////////////////////////////
		void Draw(const ResourceHandle<Texture>& texture,
				   const FloatRect& bounds,
				   const Matrix4& transform,
				   const IntRect& region,
				   const Color& color);
		/////////////////////////////////////////////////////////////
		/// Flushes the sprite batch. This means that all data
		/// currently cached will be drawn immediately. This step is
		/// either done when ending a drawing session, when the number
		/// of sprites rendered exceeds the maximum number of sprites
		/// which may be in the cache at the same time or when the
		/// sprite batch's state changes (texture change, blending
		/// mode change, ...). The user can also force a flush himself.
		/////////////////////////////////////////////////////////////
		void Flush();
		/////////////////////////////////////////////////////////////
		/// Ends a drawing session. This cleans up the sprite batch's
		/// state and performs a final flush to ensure all data
		/// cached is actually drawn onto the render target.
		/////////////////////////////////////////////////////////////
		void End();

		/////////////////////////////////////////////////////////////
		/// Sets the shader to use for drawing.
		/// \param shader The shader to use.
		/////////////////////////////////////////////////////////////
		void SetShader(const ResourceHandle<ShaderProgram>& shader);
		/////////////////////////////////////////////////////////////
		/// Enables blending while drawing using the sprite batch.
		/////////////////////////////////////////////////////////////
		void EnableBlending();
		/////////////////////////////////////////////////////////////
		/// Disables blending for drawing with the sprite batch.
		/////////////////////////////////////////////////////////////
		void DisableBlending();
		/////////////////////////////////////////////////////////////
		/// Checks whether blending is enabled or not.
		/////////////////////////////////////////////////////////////
		const vxBool IsBlendingEnabled() const;
		/////////////////////////////////////////////////////////////
		/// Sets the blending function to use when drawing into the
		/// sprite batch.
		/////////////////////////////////////////////////////////////
		void SetBlendFunc(const BlendFactor& src, const BlendFactor& dst);
		/////////////////////////////////////////////////////////////
		/// Gets the source factor of the blending equation the
		/// sprite batch currently uses.
		/////////////////////////////////////////////////////////////
		const BlendFactor& GetBlendSrc() const;
		/////////////////////////////////////////////////////////////
		/// Gets the destination factor of the blending equation the
		/// sprite batch currently uses.
		/////////////////////////////////////////////////////////////
		const BlendFactor& GetBlendDst() const;

		/////////////////////////////////////////////////////////////
		/// Sets the projection matrix the sprite batch should use.
		/// \param projection The projection matrix to use.
		/////////////////////////////////////////////////////////////
		void SetProjectionMatrix(const Matrix4& projection);
		/////////////////////////////////////////////////////////////
		/// Gets the projection matrix the sprite batch uses.
		/////////////////////////////////////////////////////////////
		const Matrix4& GetProjectionMatrix() const;
		/////////////////////////////////////////////////////////////
		/// Sets the view matrix the sprite batch should use.
		/// \param view The view matrix to use.
		/////////////////////////////////////////////////////////////
		void SetViewMatrix(const Matrix4& view);
		/////////////////////////////////////////////////////////////
		/// Gets the view matrix the sprite batch uses.
		/////////////////////////////////////////////////////////////
		const Matrix4& GetViewMatrix() const;

		/////////////////////////////////////////////////////////////
		/// Checks whether the sprite batch is currently in a
		/// drawing session or not.
		/////////////////////////////////////////////////////////////
		const vxBool IsDrawing() const;

	private:

		SpriteBatch(const SpriteBatch&);
		SpriteBatch& operator= (const SpriteBatch&);

		void SwitchTexture(const ResourceHandle<Texture>& texture);

		/* The shader used by the sprite batch. */
		ResourceHandle<ShaderProgram> mShader;
		/* The maximum amount of sprites cacheable in the sprite batch. */
		const vxU32 mMaxSprites;
		/* The hardware buffers the sprite batch uses for caching data. */
		VertexBuffer mVertexBuffer;
		IndexBuffer mIndexBuffer;
		/* Whether blending is enabled or not. */
		vxBool mBlendingEnabled;
		/* The blending function factors. */
		BlendFactor mBlendSrc, mBlendDst;
		/* The number of sprites the sprite batch currently holds. */
		vxU32 mNumSprites;
		/* Mapped pointer to the sprite batch's buffers. These are advanced whenever a sprite is drawn. */
		vxF32* mVertices;
		vxU32* mIndices;
		/* The current texture. */
		ResourceHandle<Texture> mTexture;
		/* The current inverse texture dimensions used for calculating texture coordinates. */
		vxF32 mInverseWidth, mInverseHeight;
		/* Whether the sprite batch is currently in a drawing session or not. */
		vxBool mIsDrawing;
		/* The sprite batch's projection and view matrices. */
		Matrix4 mProjection, mView;
	};
}

#endif /* SPRITEBATCH_HPP_ */
