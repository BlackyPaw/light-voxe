/*
 * BlendFactor.hpp
 *
 *  Created on: 27.06.2014
 *      Author: euaconlabs
 */

#ifndef BLENDFACTOR_HPP_
#define BLENDFACTOR_HPP_

#include <VoxE/Native/Atomic.hpp>

namespace VX
{
	/////////////////////////////////////////////////////////////
	/// \enum BlendFactor
	/// Describes different blending factors which may be substituated
	/// into the common blending equation taking the following form:
	///		X = FactorSrc * Xsrc + FactorDst * Xdst;
	/////////////////////////////////////////////////////////////
	enum class BlendFactor : vxU16
	{
		Zero						= 0x0000,
		One							= 0x0001,
		SrcColor					= 0x0300,
		OneMinusSrcColor			= 0x0301,
		DstColor					= 0x0306,
		OneMinusDstColor			= 0x0307,
		SrcAlpha					= 0x0302,
		OneMinusSrcAlpha			= 0x0303,
		DstAlpha					= 0x0304,
		OneMinusDstAlpha			= 0x0305,
		ConstantColor				= 0x8001,
		OneMinusConstantColor		= 0x8002,
		ConstantAlpha				= 0x8003,
		OneMinusConstantAlpha		= 0x8004,
		SrcAlphaSaturate			= 0x0308
	};
}

#endif /* BLENDFACTOR_HPP_ */
