/*
 * BufferUsage.hpp
 *
 *  Created on: 25.06.2014
 *      Author: euaconlabs
 */

#ifndef BUFFERUSAGE_HPP_
#define BUFFERUSAGE_HPP_

#include <VoxE/Native/Atomic.hpp>

namespace VX
{
	//////////////////////////////////////////////////////////////
	/// \enum BufferUsage
	/// Describes the different types of usages available on OpenGL
	/// buffer objects. See the OpenGL wiki for further information.
	//////////////////////////////////////////////////////////////
	enum class BufferUsage : vxU16
	{
		StreamDraw = 0x88E0,
		StreamRead = 0x88E1,
		StreamCopy = 0x88E2,
		StaticDraw = 0x88E4,
		StaticRead = 0x88E5,
		StaticCopy = 0x88E6,
		DynamicDraw = 0x88E8,
		DynamicRead = 0x88E9,
		DynamicCopy = 0x88EA
	};

	//////////////////////////////////////////////////////////////
	/// \enum BufferAccess
	/// Describes different ways of accessing a mapped buffer's data.
	//////////////////////////////////////////////////////////////
	enum class BufferAccess : vxU16
	{
		ReadOnly = 0x88B8,
		WriteOnly = 0x88B9,
		ReadWrite = 0x88BA
	};
}

#endif /* BUFFERUSAGE_HPP_ */
