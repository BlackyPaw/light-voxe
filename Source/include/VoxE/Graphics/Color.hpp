/*
 * Color.hpp
 *
 *  Created on: 26.06.2014
 *      Author: euaconlabs
 */

#ifndef COLOR_HPP_
#define COLOR_HPP_

#include <VoxE/Native/Atomic.hpp>

namespace VX
{
	//////////////////////////////////////////////////////////////
	/// \class Color
	/// Describes a color value in the RGBA color model.
	//////////////////////////////////////////////////////////////
	class Color
	{
	public:

		struct { vxU8 mRed, mGreen, mBlue, mAlpha; };

		//////////////////////////////////////////////////////////////
		/// Constructs the default color (white).
		//////////////////////////////////////////////////////////////
		Color() : mRed(255), mGreen(255), mBlue(255), mAlpha(255) {}
		//////////////////////////////////////////////////////////////
		/// Constructs a new color from color components ranging from
		/// 0 to 255.
		/// \param red
		/// \param green
		/// \param blue
		/// \param alpha
		//////////////////////////////////////////////////////////////
		Color(const vxU8 red, const vxU8 green, const vxU8 blue, const vxU8 alpha) :
			mRed(red), mGreen(green), mBlue(blue), mAlpha(alpha) {}
		//////////////////////////////////////////////////////////////
		/// Constructs a new color from color components ranging from
		/// 0.0 to 1.0.
		/// \param red
		/// \param green
		/// \param blue
		/// \param alpha
		//////////////////////////////////////////////////////////////
		Color(const vxF32 red, const vxF32 green, const vxF32 blue, const vxF32 alpha) :
			mRed(static_cast<vxU8>(red * 0.003921569f)), mGreen(static_cast<vxU8>(green * 0.003921569f)),
			mBlue(static_cast<vxU8>(blue * 0.003921569f)), mAlpha(static_cast<vxU8>(alpha * 0.003921569f))
		{
		}

		//////////////////////////////////////////////////////////////
		/// Packs the color into a single 32-bit value.
		//////////////////////////////////////////////////////////////
		vxU32 Pack() const { return static_cast<vxU32>(mRed << 24 | mGreen << 16 | mBlue << 8 | mAlpha); }
	};
}

#endif /* COLOR_HPP_ */
