/*
 * GLDataType.hpp
 *
 *  Created on: 08.06.2014
 *      Author: euaconlabs
 */

#ifndef GLDATATYPE_HPP_
#define GLDATATYPE_HPP_

#include <VoxE/Native/Atomic.hpp>

namespace VX
{
	//////////////////////////////////////////////////////////////
	/// \enum DataType
	/// Describes possible values to pass as data type identifiers
	/// to OpenGL.
	//////////////////////////////////////////////////////////////
	enum class DataType : vxU16
	{
		Byte = 0x1400,
		UnsignedByte = 0x1401,
		Short = 0x1402,
		UnsignedShort = 0x1403,
		Int = 0x1404,
		UnsignedInt = 0x1405,
		Float = 0x1406,
		Double = 0x140A
	};
}

#endif /* GLDATATYPE_HPP_ */
