/*
 * GLFramebuffer.hpp
 *
 *  Created on: 11.06.2014
 *      Author: euaconlabs
 */

#ifndef GLFRAMEBUFFER_HPP_
#define GLFRAMEBUFFER_HPP_

#include <VoxE/Core/Rect.hpp>
#include <VoxE/Graphics/Texture.hpp>
#include <VoxE/Graphics/ImageData.hpp>
#include <VoxE/Native/Atomic.hpp>

namespace VX
{
	//////////////////////////////////////////////////////////////
	/// \class FrameBuffer
	/// Allows for the creation and usage of OpenGL framebuffer
	/// objects.
	//////////////////////////////////////////////////////////////
	class FrameBuffer
	{
	public:

		//////////////////////////////////////////////////////////////
		/// Creates a new framebuffer object given an image format
		/// for the color buffer, the desired width and height and
		/// optionally a boolean determining whether a depth buffer
		/// is needed or not. By default no depth buffer will be
		/// attached.
		/// \param f
		/// \param width
		/// \param height
		/// \param depth
		//////////////////////////////////////////////////////////////
		FrameBuffer(const ImageFormat& f, const vxU32 width, const vxU32 height, const vxBool depth = false);
		FrameBuffer(const FrameBuffer& fb);
		~FrameBuffer();

		//////////////////////////////////////////////////////////////
		/// Binds the framebuffer and makes it ready to use while
		/// simultaneously storing the current viewport for later
		/// restore.
		//////////////////////////////////////////////////////////////
		void Bind() const;
		//////////////////////////////////////////////////////////////
		/// Binds the framebuffer and makes it ready to use. This does
		/// not store the current viewport. In order not to restore
		/// an invalid viewport the framebuffer has to be unbound via
		/// UnBindDirect().
		/// \see UnBindDirect()
		//////////////////////////////////////////////////////////////
		void BindDirect() const;
		//////////////////////////////////////////////////////////////
		/// Unbinds the framebuffer and restores the saved, old viewport.
		//////////////////////////////////////////////////////////////
		void UnBind() const;
		//////////////////////////////////////////////////////////////
		/// Unbinds the framebuffer directly without restoring a saved
		/// viewport.
		//////////////////////////////////////////////////////////////
		void UnBindDirect() const;

		//////////////////////////////////////////////////////////////
		/// Gets the color attachment of this framebuffer.
		//////////////////////////////////////////////////////////////
		Texture* GetColorAttachment();
		//////////////////////////////////////////////////////////////
		/// Gets the color attachment of this framebuffer.
		//////////////////////////////////////////////////////////////
		const Texture* GetColorAttachment() const;
		//////////////////////////////////////////////////////////////
		/// Gets the width of the framebuffer.
		//////////////////////////////////////////////////////////////
		const vxU32 GetWidth() const;
		//////////////////////////////////////////////////////////////
		/// Gets the height of the framebuffer.
		//////////////////////////////////////////////////////////////
		const vxU32 GetHeight() const;
		//////////////////////////////////////////////////////////////
		/// Checks whether the framebuffer has a depth buffer or not.
		//////////////////////////////////////////////////////////////
		const vxBool HasDepth() const;

		//////////////////////////////////////////////////////////////
		/// Frees all resources belonging to the framebuffer thereby
		/// invalidating all copies of it.
		//////////////////////////////////////////////////////////////
		void Destroy();

		FrameBuffer& operator= (const FrameBuffer& fb);

	private:

		void Setup();
		void Copy(const FrameBuffer& fb);

		vxU32*		mReferences;
		vxU32		mHandle;
		ImageFormat	mFormat;
		vxU32		mWidth, mHeight;
		vxBool		mDepth;

		mutable IntRect mOldViewport;

		Texture		mColorAttachment;
		vxU32		mDepthAttachment;

	};
}

#endif /* GLFRAMEBUFFER_HPP_ */
