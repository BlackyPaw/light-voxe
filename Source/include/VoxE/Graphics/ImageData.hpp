/*
 * ImageData.hpp
 *
 *  Created on: 04.06.2014
 *      Author: euaconlabs
 */

#ifndef IMAGEDATA_HPP_
#define IMAGEDATA_HPP_

#include <VoxE/Native/Atomic.hpp>
#include <VoxE/Native/File.hpp>

namespace VX
{
	//////////////////////////////////////////////////////////////
	/// \enum ImageFormat
	/// This enumeration describes all supported image file formats
	/// as they are loaded into memory.
	//////////////////////////////////////////////////////////////
	enum class ImageFormat : vxU8
	{
		RGBA8888, ///< 8 bits per color channel; 32 bits in total
		RGB888 ///< 8 bits per color channel; 24 bits in total
	};

	//////////////////////////////////////////////////////////////
	/// \class ImageData
	/// Keeps track of image data and its meta-data respectively.
	//////////////////////////////////////////////////////////////
	class ImageData
	{
	public:

		ImageData();
		ImageData(const ImageFormat& f, const vxU32 width, const vxU32 height, void* pixels);
		ImageData(const ImageData& i);
		~ImageData();

		//////////////////////////////////////////////////////////////
		/// Loads an image from the given file.
		/// \returns A valid image data object on success, an invalid
		///		one on failure.
		//////////////////////////////////////////////////////////////
		static ImageData LoadFromFile(const File& f);
		//////////////////////////////////////////////////////////////
		/// Returns the image's format.
		//////////////////////////////////////////////////////////////
		const ImageFormat& GetFormat() const;
		//////////////////////////////////////////////////////////////
		/// Returns the array pointer containing the pixels of the image.
		//////////////////////////////////////////////////////////////
		const void* GetPixels() const;
		//////////////////////////////////////////////////////////////
		/// Returns the width of the image in pixels.
		//////////////////////////////////////////////////////////////
		const vxU32 GetWidth() const;
		//////////////////////////////////////////////////////////////
		/// Returns the height of the image in pixels.
		//////////////////////////////////////////////////////////////
		const vxU32 GetHeight() const;

		//////////////////////////////////////////////////////////////
		/// Destroys the image data thereby immediately invalidating
		/// its image data. All copies still trying to access the
		/// data will read invalid memory.
		//////////////////////////////////////////////////////////////
		void Destroy();
		//////////////////////////////////////////////////////////////
		/// Checks whether this object manages a valid resource or not.
		//////////////////////////////////////////////////////////////
		vxBool IsValid() const;

		ImageData& operator= (const ImageData& i);


	private:

		void Copy(const ImageData& i);

		vxU32* 		mReferences;
		ImageFormat	mFormat;
		void* 		mPixels;
		vxU32		mWidth, mHeight;
	};
}

#endif /* IMAGEDATA_HPP_ */
