/*
 * IndexBuffer.hpp
 *
 *  Created on: 25.06.2014
 *      Author: euaconlabs
 */

#ifndef INDEXBUFFER_HPP_
#define INDEXBUFFER_HPP_

#include <VoxE/Graphics/BufferUsage.hpp>
#include <VoxE/Graphics/DataType.hpp>
#include <VoxE/Native/Atomic.hpp>

namespace VX
{
	//////////////////////////////////////////////////////////////
	/// \class IndexBuffer
	/// Implements a hardware index buffer in OpenGL. A index
	/// buffer contains index data used to remove duplicate vertices
	/// in order to reduce memory usage and to speed up rendering.
	//////////////////////////////////////////////////////////////
	class IndexBuffer
	{
	public:

		IndexBuffer();
		IndexBuffer(const IndexBuffer& b);
		~IndexBuffer();

		//////////////////////////////////////////////////////////////
		/// Allocates the index buffer. The method is given the size
		/// in bytes to allocate and optionally a pointer to some data
		/// which will be copied into the newly allocated buffer object.
		/// It is also given the usage describing how the buffer object
		/// is intended to be used.
		/// \param size The size of the buffer in bytes.
		/// \param usage The usage of the buffer.
		/// \param data [Optional] If not nullptr the buffer will be
		///	filled with the data pointed to by this pointer.
		//////////////////////////////////////////////////////////////
		void Allocate(const vxU64 size, const BufferUsage& usage, const void* data = nullptr);

		//////////////////////////////////////////////////////////////
		/// Copies 'size' bytes from the data pointed to by 'data' into
		/// the buffer object at location 'offset'.
		/// \param size The size of the data to copy.
		/// \param data The data to copy. Must not be nullptr.
		/// \param offset An offset into the buffer object in bytes.
		/// \param direct If set to true the buffer object will not be
		///	bound automatically.
		//////////////////////////////////////////////////////////////
		void CopyData(const vxU64 size, const void* data, const vxI64 offset = 0, const vxBool direct = false);

		//////////////////////////////////////////////////////////////
		/// Sets the data type the index data is to be treated as.
		//////////////////////////////////////////////////////////////
		void SetIndexType(const DataType& type);
		//////////////////////////////////////////////////////////////
		/// Gets the data type the index data is to be treated as.
		//////////////////////////////////////////////////////////////
		const DataType& GetIndexType() const;

		//////////////////////////////////////////////////////////////
		// Maps the buffer and returns the mapped pointer.
		/// \param access The mode in which the buffer object's data is
		///	being operated on.
		/// \param direct If set to true the buffer object will not be
		///	bound automatically.
		//////////////////////////////////////////////////////////////
		void* Map(const BufferAccess& access, const vxBool direct = false);
		//////////////////////////////////////////////////////////////
		/// Unmaps the buffer. Afterwards no more data should be written
		/// into the pointer returned by Map().
		/// \param direct If set to true the buffer object will not be
		///	bound automatically.
		//////////////////////////////////////////////////////////////
		void UnMap(const vxBool direct = false);
		//////////////////////////////////////////////////////////////
		/// Binds the index buffer.
		//////////////////////////////////////////////////////////////
		void Bind() const;
		//////////////////////////////////////////////////////////////
		/// Unbinds the index buffer.
		//////////////////////////////////////////////////////////////
		void UnBind() const;
		//////////////////////////////////////////////////////////////
		/// Checks if the index buffer has been created yet or not.
		//////////////////////////////////////////////////////////////
		vxBool IsValid() const;
		//////////////////////////////////////////////////////////////
		/// Destroys the buffer completely. All further accesses will
		/// be invalid.
		//////////////////////////////////////////////////////////////
		void Destroy();

		IndexBuffer& operator= (const IndexBuffer& b);

	private:

		void Copy(const IndexBuffer& b);

		vxU32* mReferences;
		vxU32 mHandle;
		DataType mIndexType;
	};
}

#endif /* INDEXBUFFER_HPP_ */
