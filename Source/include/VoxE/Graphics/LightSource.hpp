/*
 * LightSource.hpp
 *
 *  Created on: 21.07.2014
 *      Author: euaconlabs
 */

#ifndef LIGHTSOURCE_HPP_
#define LIGHTSOURCE_HPP_

#include <VoxE/Core/Vector.hpp>
#include <VoxE/Graphics/Color.hpp>
#include <VoxE/Graphics/LightType.hpp>

namespace VX
{
	//////////////////////////////////////////////////////////////
	/// \class LightSource
	/// \brief Class for describing a light source to the rendering
	///		engine.
	/// This class is used to describe a basic light source to
	/// the rendering engine no matter whether it's 2D or 3D as
	/// most of the properties are necessary for both dimensions
	/// anyways. Furthermore every light source keeps track of its
	/// actual type which might be one of the types listed in the
	/// LightType enumeration.
	//////////////////////////////////////////////////////////////
	class LightSource
	{
	public:

		LightSource();
		~LightSource();

		Color GetColor() const;
		void SetColor(Color color);
		vxF32 GetIntensity() const;
		void SetIntensity(vxF32 intensity);
		Vector3f GetOrientation() const;
		void SetOrientation(Vector3f orientation);
		vxF32 GetRange() const;
		void SetRange(vxF32 range);
		LightType GetType() const;
		void SetType(LightType type);

	private:

		LightType mType; // The light source's type.
		Vector3f mOrientation; // Position for point- and spotlights; Direction for directional lights.
		vxF32 mRange; // The light source's range.
		vxF32 mIntensity; // The light's intensity.
		Color mColor; // The color of the light the source emits.
	};
}

#endif /* LIGHTSOURCE_HPP_ */
