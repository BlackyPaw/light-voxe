/*
 * LightType.hpp
 *
 *  Created on: 21.07.2014
 *      Author: euaconlabs
 */

#ifndef LIGHTTYPE_HPP_
#define LIGHTTYPE_HPP_

#include <VoxE/Native/Atomic.hpp>

namespace VX
{
	//////////////////////////////////////////////////////////////
	/// \enum LightType
	/// \brief Enumeration listing all possible types for light
	///		sources.
	//////////////////////////////////////////////////////////////
	enum class LightType : vxU8
	{
		Point,
		Directional,
		Spot
	};
}

#endif /* LIGHTTYPE_HPP_ */
