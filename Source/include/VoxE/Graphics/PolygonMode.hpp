/*
 * PolygonMode.hpp
 *
 *  Created on: 26.06.2014
 *      Author: euaconlabs
 */

#ifndef POLYGONMODE_HPP_
#define POLYGONMODE_HPP_

#include <VoxE/Native/Atomic.hpp>

namespace VX
{
	//////////////////////////////////////////////////////////////
	/// \enum PolygonMode
	/// Describes different polygon modes which might be used in
	/// order to display primitives.
	//////////////////////////////////////////////////////////////
	enum class PolygonMode : vxU16
	{
		Points 			= 0x0000,
		Lines			= 0x0001,
		LineLoop		= 0x0002,
		LineStrip		= 0x0003,
		Triangles		= 0x0004,
		TriangleStrip	= 0x0005,
		TriangleFan		= 0x0006
	};
}

#endif /* POLYGONMODE_HPP_ */
