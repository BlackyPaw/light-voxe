/*
 * Renderable.hpp
 *
 *  Created on: 24.07.2014
 *      Author: euaconlabs
 */

#ifndef RENDERABLE_HPP_
#define RENDERABLE_HPP_

#include <VoxE/Math/Transformable.hpp>

namespace VX
{
	/////////////////////////////////////////////////////////////
	/// \class Renderable
	/// \brief Interface for creating custom renderables which
	///		might be inserted for rendering into the render
	///		system.
	/////////////////////////////////////////////////////////////
	class Renderable : public Transformable
	{
	public:

		Renderable();
		virtual ~Renderable();

		/////////////////////////////////////////////////////////////
		/// Called before the actual rendering takes place. Used for
		/// preparing the renderer's state for example.
		/////////////////////////////////////////////////////////////
		virtual void PreRender();
		/////////////////////////////////////////////////////////////
		/// Called in order to actually render the renderable object.
		/////////////////////////////////////////////////////////////
		virtual void Render();
		/////////////////////////////////////////////////////////////
		/// Called after the actual rendering took place. Used for
		/// cleaning up the renderer's state.
		/////////////////////////////////////////////////////////////
		virtual void PostRender();

		/////////////////////////////////////////////////////////////
		/// Checks whether the renderable object is actually visible
		/// or not.
		/////////////////////////////////////////////////////////////
		vxBool IsVisible() const;
		/////////////////////////////////////////////////////////////
		/// Sets whether the renderable object is actually visible.
		/////////////////////////////////////////////////////////////
		void SetVisible(const vxBool visible);

	protected:

		// Boolean whether the renderable object is actually visible or not.
		vxBool mVisible;
	};
}

#endif /* RENDERABLE_HPP_ */
