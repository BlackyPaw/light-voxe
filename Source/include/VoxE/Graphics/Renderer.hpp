/*
 * Renderer.hpp
 *
 *  Created on: 30.06.2014
 *      Author: euaconlabs
 */

#ifndef RENDERER_HPP_
#define RENDERER_HPP_

#include <VoxE/Core/ISingleton.hpp>
#include <VoxE/Graphics/DataType.hpp>
#include <VoxE/Graphics/PolygonMode.hpp>
#include <VoxE/Graphics/ShaderProgram.hpp>
#include <VoxE/Graphics/Texture.hpp>
#include <VoxE/Graphics/UniformBuffer.hpp>
#include <VoxE/Graphics/VertexDescription.hpp>
#include <VoxE/Graphics/2D/SpriteBatch.hpp>
#include <VoxE/Math/Matrix4.hpp>
#include <VoxE/Resources/ResourceHandle.hpp>
#include <VoxE/Scene/MatrixStack.hpp>

namespace VX
{
	/////////////////////////////////////////////////////////////
	/// \class Renderer
	/// This singleton class wraps the basic primitive submission
	/// functionality provided by OpenGL. It furthermore holds
	/// information about any currently bound shaders and is
	/// responsive for passing shader parameters down the to the
	/// shader itself.
	/////////////////////////////////////////////////////////////
	class Renderer : public ISingleton<Renderer>
	{
	public:

		Renderer();
		~Renderer();

		/////////////////////////////////////////////////////////////
		/// Begins a rendering session. This means that all utilities
		/// and everything else the renderer needs to set up will be
		/// set up accordingly.
		/////////////////////////////////////////////////////////////
		void BeginSession();

		/////////////////////////////////////////////////////////////
		/// Returns a utility matrix stack. Please note that this
		/// matrix stack may be used from anywhere, so only use it
		/// in closed systems!
		/////////////////////////////////////////////////////////////
		MatrixStack& GetMatrixStack();
		/////////////////////////////////////////////////////////////
		/// Gets a global sprite batch which may be used to draw
		/// sprites as easily as possible from anywhere.
		/////////////////////////////////////////////////////////////
		SpriteBatch* GetSpriteBatch();

		/////////////////////////////////////////////////////////////
		/// Sets up a shader and makes it ready to for use. The
		/// method is also given the vertex description in order to
		/// correctly set up the vertex attributes to be passed to
		/// the shader.
		/// \param shader The shader to use.
		/////////////////////////////////////////////////////////////
		void SetupShader(const ShaderProgram* shader, const VertexDescription& vdesc);

		/////////////////////////////////////////////////////////////
		/// Sets the world matrix applied when rendering.
		/// \param world The world matrix to set.
		/////////////////////////////////////////////////////////////
		void SetWorldMatrix(const Matrix4& world);
		/////////////////////////////////////////////////////////////
		/// Sets the projection and view matrix applied when rendering.
		/// \param projView The projection and view matrix to set.
		/////////////////////////////////////////////////////////////
		void SetProjectionViewMatrix(const Matrix4& projView);

		/////////////////////////////////////////////////////////////
		/// Sets the diffuse texture to apply to a draw call.
		/// The texture will be bound automatically.
		/////////////////////////////////////////////////////////////
		void SetDiffuseTexture(const ResourceHandle<Texture>& texture);

		/////////////////////////////////////////////////////////////
		/// Draws a given number of polygons of the given type from
		/// enabled vertex arrays.
		/// \param mode The polygon mode of the polygons to draw.
		/// \param first The first vertex to be drawn.
		/// \param count The number of vertices to be drawn.
		/////////////////////////////////////////////////////////////
		void DrawArrays(const PolygonMode& mode, const vxI32 first, const vxI32 count);

		/////////////////////////////////////////////////////////////
		/// Draws a given number of polygons as elements using indices.
		/// \param mode The mode of the polygons.
		/// \param count The number of elements to draw.
		/// \param type The type of the indices.
		/// \param indices A pointer to the index data or an offset
		///	into the index buffer depending on whether an index buffer
		///	has been bound or not.
		/////////////////////////////////////////////////////////////
		void DrawElements(const PolygonMode& mode, const vxI32 count, const DataType& type, const void* indices);

		/////////////////////////////////////////////////////////////
		/// Ends a rendering session and leaves the renderer in a
		/// valid state.
		/////////////////////////////////////////////////////////////
		void EndSession();

	private:

		/* The utility matrix stack. */
		MatrixStack mMatrixStack;
		/* The utility sprite batch. */
		SpriteBatch mSpriteBatch;
		/* The shader currently used for rendering. */
		ResourceHandle<ShaderProgram> mCurrentShader;
		/* The uniform buffers used for storing the uniform data passed to shaders. */
		UniformBuffer mMatrixBuffer;
	};
}

#endif /* RENDERER_HPP_ */
