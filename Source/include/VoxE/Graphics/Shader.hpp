/*
 * GLShader.hpp
 *
 *  Created on: 08.06.2014
 *      Author: euaconlabs
 */

#ifndef GLSHADER_HPP_
#define GLSHADER_HPP_

#include <VoxE/Native/Atomic.hpp>
#include <VoxE/Native/File.hpp>

namespace VX
{
	//////////////////////////////////////////////////////////////
	/// \enum ShaderType
	/// This enumeration describes all supported types of shaders.
	//////////////////////////////////////////////////////////////
	enum class ShaderType : vxU8
	{
		Vertex,
		Fragment,
		TessEvaluation,
		TessControl,
		Geometry
	};

	//////////////////////////////////////////////////////////////
	/// \class Shader
	/// Class wrapping basic shader functionality for OpenGL. Note
	/// that shader have to be used via a GLShaderProgram.
	//////////////////////////////////////////////////////////////
	class Shader
	{
	public:

		Shader();
		Shader(const Shader& s);
		~Shader();

		//////////////////////////////////////////////////////////////
		/// Loads a shader from the given file. Also tries to compile
		/// it. Returns a valid shader object on success or an invalid
		/// one otherwise.
		/// \param t The type of the shader.
		/// \param f The file to load the shader from.
		//////////////////////////////////////////////////////////////
		static Shader LoadFromFile(const ShaderType& t, const File& f);
		//////////////////////////////////////////////////////////////
		/// Loads a shader from the given source code. Also tries
		/// to compile it. Returns a valid shader object on success or
		/// an invalid one otherwise.
		/// \param t The type of the shader.
		/// \param s The shader's source code.
		/// \param sz The size of the source string in bytes.
		//////////////////////////////////////////////////////////////
		static Shader LoadFromSource(const ShaderType& t, const vxChar* s, const vxU32 sz);

		//////////////////////////////////////////////////////////////
		/// Gets the shader object handle acquired from OpenGL.
		//////////////////////////////////////////////////////////////
		const vxU32 GetHandle() const;
		//////////////////////////////////////////////////////////////
		/// Gets the shader's type.
		//////////////////////////////////////////////////////////////
		const ShaderType GetType() const;

		//////////////////////////////////////////////////////////////
		/// Destroys the shader object thereby freeing its OpenGL
		/// handle and invalidating any copy of itself. Any copy still
		/// trying to access the data will read invalid memory.
		//////////////////////////////////////////////////////////////
		void Destroy();
		//////////////////////////////////////////////////////////////
		/// Checks whether this object manages a valid resource or not.
		//////////////////////////////////////////////////////////////
		vxBool IsValid() const;

		Shader& operator= (const Shader& s);

	private:

		void Copy(const Shader& s);

		vxU32*		mReferences;
		ShaderType 	mType;
		vxU32		mHandle;
	};
}

#endif /* GLSHADER_HPP_ */
