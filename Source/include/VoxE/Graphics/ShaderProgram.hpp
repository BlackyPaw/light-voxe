/*
 * GLShaderProgram.hpp
 *
 *  Created on: 08.06.2014
 *      Author: euaconlabs
 */

#ifndef GLSHADERPROGRAM_HPP_
#define GLSHADERPROGRAM_HPP_

#include <VoxE/Graphics/DataType.hpp>
#include <VoxE/Graphics/Shader.hpp>
#include <VoxE/Graphics/VertexAttribute.hpp>
#include <VoxE/Math/Matrix4.hpp>
#include <VoxE/Native/Atomic.hpp>

#include <map>
#include <vector>

namespace VX
{
	//////////////////////////////////////////////////////////////
	/// \class ShaderProgram
	/// This class manages an OpenGL shader program.
	//////////////////////////////////////////////////////////////
	class ShaderProgram
	{
	public:

		static const char* POSITION_ATTRIBUTE;
		static const char* NORMAL_ATTRIBUTE;
		static const char* TANGENT_ATTRIBUTE;
		static const char* COLOR_ATTRIBUTE;
		static const char* BITANGENT_ATTRIBUTE;
		static const char* TEXCOORD0_ATTRIBUTE;
		static const char* TEXCOORD1_ATTRIBUTE;

		static const vxI32 kPositionLocation;
		static const vxI32 kNormalLocation;
		static const vxI32 kTangentLocation;
		static const vxI32 kBitangentLocation;
		static const vxI32 kColorLocation;
		static const vxI32 kTexCoord0Location;

		ShaderProgram();
		ShaderProgram(const ShaderProgram& p);
		~ShaderProgram();

		//////////////////////////////////////////////////////////////
		/// Adds the given shader object to the program's list of
		/// shaders it will link together when linked. If called when
		/// the program is already linked this function has no effect.
		/// \param s The shader to add.
		//////////////////////////////////////////////////////////////
		void AddShader(const Shader& s);
		//////////////////////////////////////////////////////////////
		/// Tries link the program with all shaders added up to now.
		/// \returns True on success; False on failure.
		//////////////////////////////////////////////////////////////
		vxBool Link();

		//////////////////////////////////////////////////////////////
		/// Gets the shader program object handle acquired from OpenGL.
		//////////////////////////////////////////////////////////////
		const vxU32 GetHandle() const;
		//////////////////////////////////////////////////////////////
		/// Tells OpenGL to use the shader program object managed by
		/// this object.
		//////////////////////////////////////////////////////////////
		void Use() const;

		//////////////////////////////////////////////////////////////
		// Uniform modifers
		//////////////////////////////////////////////////////////////
		vxI32 GetUniformLocation(const vxString& name) const;

		void SetUniformi(const vxI32 location, const vxI32 v0) const;
		void SetUniformi(const vxI32 location, const vxI32 v0, const vxI32 v1) const;
		void SetUniformi(const vxI32 location, const vxI32 v0, const vxI32 v1, const vxI32 v2) const;
		void SetUniformi(const vxI32 location, const vxI32 v0, const vxI32 v1, const vxI32 v2, const vxI32 v3) const;
		void SetUniformi(const vxString& name, const vxI32 v0) const;
		void SetUniformi(const vxString& name, const vxI32 v0, const vxI32 v1) const;
		void SetUniformi(const vxString& name, const vxI32 v0, const vxI32 v1, const vxI32 v2) const;
		void SetUniformi(const vxString& name, const vxI32 v0, const vxI32 v1, const vxI32 v2, const vxI32 v3) const;

		void SetUniform1iv(const vxI32 location, const vxU32 count, const vxI32* values) const;
		void SetUniform2iv(const vxI32 location, const vxU32 count, const vxI32* values) const;
		void SetUniform3iv(const vxI32 location, const vxU32 count, const vxI32* values) const;
		void SetUniform4iv(const vxI32 location, const vxU32 count, const vxI32* values) const;
		void SetUniform1iv(const vxString& name, const vxU32 count, const vxI32* values) const;
		void SetUniform2iv(const vxString& name, const vxU32 count, const vxI32* values) const;
		void SetUniform3iv(const vxString& name, const vxU32 count, const vxI32* values) const;
		void SetUniform4iv(const vxString& name, const vxU32 count, const vxI32* values) const;

		void SetUniformf(const vxI32 location, const vxF32 v0) const;
		void SetUniformf(const vxI32 location, const vxF32 v0, const vxF32 v1) const;
		void SetUniformf(const vxI32 location, const vxF32 v0, const vxF32 v1, const vxF32 v2) const;
		void SetUniformf(const vxI32 location, const vxF32 v0, const vxF32 v1, const vxF32 v2, const vxF32 v3) const;
		void SetUniformf(const vxString& name, const vxF32 v0) const;
		void SetUniformf(const vxString& name, const vxF32 v0, const vxF32 v1) const;
		void SetUniformf(const vxString& name, const vxF32 v0, const vxF32 v1, const vxF32 v2) const;
		void SetUniformf(const vxString& name, const vxF32 v0, const vxF32 v1, const vxF32 v2, const vxF32 v3) const;

		void SetUniform1fv(const vxI32 location, const vxU32 count, const vxF32* values) const;
		void SetUniform2fv(const vxI32 location, const vxU32 count, const vxF32* values) const;
		void SetUniform3fv(const vxI32 location, const vxU32 count, const vxF32* values) const;
		void SetUniform4fv(const vxI32 location, const vxU32 count, const vxF32* values) const;
		void SetUniform1fv(const vxString& name, const vxU32 count, const vxF32* values) const;
		void SetUniform2fv(const vxString& name, const vxU32 count, const vxF32* values) const;
		void SetUniform3fv(const vxString& name, const vxU32 count, const vxF32* values) const;
		void SetUniform4fv(const vxString& name, const vxU32 count, const vxF32* values) const;

		void SetUniformMatrix(const vxI32 location, const Matrix4& values, const vxBool transpose = false) const;
		void SetUniformMatrix(const vxString& name, const Matrix4& values, const vxBool transpose = false) const;

		//////////////////////////////////////////////////////////////
		// Vertex attribute modifiers
		//////////////////////////////////////////////////////////////
		vxI32 GetAttributeLocation(const vxString& name) const;

		void EnableVertexAttribute(const vxI32 location) const;
		void EnableVertexAttribute(const vxString& name) const;

		void DisableVertexAttribute(const vxI32 location) const;
		void DisableVertexAttribute(const vxString& name) const;

		void SetVertexAttribute(const vxI32 location, const vxI32 size, const DataType& type, const vxBool normalized, const vxI32 stride, const void* pointer) const;
		void SetVertexAttribute(const vxString& name, const vxI32 size, const DataType& type, const vxBool normalized, const vxI32 stride, const void* pointer) const;

		//////////////////////////////////////////////////////////////
		/// Destroy the shader program object. All copies will be
		/// invalidated immediately and all resources obtained will be
		/// freed. Any further access will read from invalid memory.
		//////////////////////////////////////////////////////////////
		void Destroy();
		//////////////////////////////////////////////////////////////
		/// Checks whether this object manages a valid shader program
		/// object.
		//////////////////////////////////////////////////////////////
		vxBool IsValid() const;

		ShaderProgram& operator= (const ShaderProgram& p);

	private:

		//////////////////////////////////////////////////////////////
		/// Copies the shader program object. Please note that referenced
		/// shaders added via AddShader(...) are NOT copied. Therefore
		/// Link() has to be called on the same object as to which
		/// the shader were added.
		//////////////////////////////////////////////////////////////
		void Copy(const ShaderProgram& p);

		vxU32* mReferences;
		vxU32 mHandle;

		std::vector<Shader> mShaders;
	};
}

#endif /* GLSHADERPROGRAM_HPP_ */
