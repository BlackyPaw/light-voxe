/*
 * UniformBuffer.hpp
 *
 *  Created on: 30.06.2014
 *      Author: euaconlabs
 */

#ifndef UNIFORMBUFFER_HPP_
#define UNIFORMBUFFER_HPP_

#include <VoxE/Graphics/BufferUsage.hpp>
#include <VoxE/Native/Atomic.hpp>

namespace VX
{
	/////////////////////////////////////////////////////////////
	/// \class UniformBuffer
	/// Implements a hardware uniform buffer capable of holding
	/// uniform data attachable to uniform blocks in shader
	/// program objects.
	/////////////////////////////////////////////////////////////
	class UniformBuffer
	{
	public:

		UniformBuffer();
		UniformBuffer(const UniformBuffer& b);
		~UniformBuffer();

		//////////////////////////////////////////////////////////////
		/// Allocates the memory for the buffer and optionally copies
		/// the memory from a given pointer. This automatically binds
		/// the buffer, too. This actually makes a buffer 'valid'.
		/// An additional parameter 'usage' is required in order to
		/// inform OpenGL how the buffer is intended to be used. See
		/// the OpenGL wiki for further information.
		/// \param size The size of the buffer in bytes.
		/// \param usage The intended usage of the buffer.
		/// \param data [Optional] If not null the buffer will be filled
		///	with the data this pointer points to.
		//////////////////////////////////////////////////////////////
		void Allocate(const vxU64 size, const BufferUsage& usage, const void* data = nullptr);

		//////////////////////////////////////////////////////////////
		/// Copies 'size' bytes from the data pointed to by 'data' into
		/// the buffer object at location 'offset'.
		/// \param size The size of the data to copy.
		/// \param data The data to copy. Must not be nullptr.
		/// \param offset An offset into the buffer object in bytes.
		/// \param direct If set to true the buffer object will not be
		///	bound automatically.
		//////////////////////////////////////////////////////////////
		void CopyData(const vxU64 size, const void* data, const vxI64 offset = 0, const vxBool direct = false);

		//////////////////////////////////////////////////////////////
		// Maps the buffer and returns the mapped pointer.
		/// \param access The mode in which the buffer object's data is
		///	being operated on.
		/// \param direct If set to true the buffer object will not be
		///	bound automatically.
		//////////////////////////////////////////////////////////////
		void* Map(const BufferAccess& access, const vxBool direct = false);
		//////////////////////////////////////////////////////////////
		/// Unmaps the buffer. Afterwards no more data should be written
		/// into the pointer returned by Map().
		/// \param direct If set to true the buffer object will not be
		///	bound automatically.
		//////////////////////////////////////////////////////////////
		void UnMap(const vxBool direct = false);
		//////////////////////////////////////////////////////////////
		/// Binds the uniform buffer without attaching it to an
		/// uniform block binding point.
		//////////////////////////////////////////////////////////////
		void Bind() const;
		//////////////////////////////////////////////////////////////
		/// Binds the uniform buffer to the given uniform block
		/// binding point.
		/// \param binding The binding to bind the buffer to.
		//////////////////////////////////////////////////////////////
		void BindRange(const vxU32 binding) const;
		//////////////////////////////////////////////////////////////
		/// Binds a subset of the uniform buffer to the given uniform
		/// block binding point.
		/// \param binding The binding to bind the buffer to.
		/// \param offset The offset in bytes into the buffer.
		/// \param size The size of the subset to bind in bytes.
		//////////////////////////////////////////////////////////////
		void BindRange(const vxU32 binding, const vxI64 offset, const vxI64 size) const;
		//////////////////////////////////////////////////////////////
		/// Unbinds the uniform buffer.
		//////////////////////////////////////////////////////////////
		void UnBind() const;
		/////////////////////////////////////////////////////////////
		/// Checks whether the uniform buffer is valid, i.e. has been
		/// allocated or not.
		/////////////////////////////////////////////////////////////
		vxBool IsValid() const;
		/////////////////////////////////////////////////////////////
		/// Destroys all resources attached to the uniform buffer
		/// immediately.
		/////////////////////////////////////////////////////////////
		void Destroy();

		UniformBuffer& operator= (const UniformBuffer& b);

	private:

		void Copy(const UniformBuffer& b);

		vxU32* mReferences;
		vxU32  mHandle;
	};
}

#endif /* UNIFORMBUFFER_HPP_ */
