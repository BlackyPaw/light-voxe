/*
 * VertexAttribute.hpp
 *
 *  Created on: 09.06.2014
 *      Author: euaconlabs
 */

#ifndef VERTEXATTRIBUTE_HPP_
#define VERTEXATTRIBUTE_HPP_

#include <VoxE/Graphics/DataType.hpp>
#include <VoxE/Native/Atomic.hpp>

namespace VX
{
	//////////////////////////////////////////////////////////////
	/// \enum AttributeType
	/// Enumeration describing all possible usages of a vertex
	/// attribute.
	//////////////////////////////////////////////////////////////
	enum class AttributeSemantic : vxU8
	{
		Position = 0x10,
		Normal = 0x11,
		Tangent = 0x12,
		BiTangent = 0x13,
		Color = 0x14,
		TextureCoordinates = 0x15
	};

	//////////////////////////////////////////////////////////////
	/// \class VertexAttribute
	/// \brief Defines a generic vertex attribute given some information
	/// about it, like its offset in the vertex buffer object.
	///
	/// Every vertex attribute carries the following information
	/// with it:
	///		- offset The offset into the vertex buffer object in
	///				 bytes.
	///		- usage The usage the vertex attribute is intended to
	///				be used for.
	///		- elements The number of elements each attributes possesses.
	///		- unit The unit for texture coordinates describing
	///			   to which texture coordinate unit the attribute belongs.
	//////////////////////////////////////////////////////////////
	class VertexAttribute
	{
	public:

		VertexAttribute() :
			mOffset(0),
			mElements(0),
			mType(DataType::Int),
			mUsage(AttributeSemantic::Position),
			mNormalized(false),
			mUnit(0)
		{
		}

		VertexAttribute(const AttributeSemantic& semantic, const vxU32 offset, const vxU32 elements, const DataType& type, const vxBool normalized = false, const vxU32 unit = 0) :
			mOffset(offset),
			mElements(elements),
			mType(type),
			mUsage(semantic),
			mNormalized(normalized),
			mUnit(unit)
		{
		}

		//////////////////////////////////////////////////////////////
		/// Gets the offset of this attribute in bytes.
		//////////////////////////////////////////////////////////////
		const vxU32 GetOffset() const { return mOffset; }
		//////////////////////////////////////////////////////////////
		/// Gets the number of elements the attribute possesses.
		//////////////////////////////////////////////////////////////
		const vxU32 GetElements() const { return mElements; }
		//////////////////////////////////////////////////////////////
		/// Gets the data type of the attribute.
		//////////////////////////////////////////////////////////////
		const DataType& GetType() const { return mType; }
		//////////////////////////////////////////////////////////////
		/// Gets the usage type of the attribute.
		//////////////////////////////////////////////////////////////
		const AttributeSemantic& GetUsage() const { return mUsage; }
		//////////////////////////////////////////////////////////////
		/// Checks whether the attribute should be normalized or not.
		//////////////////////////////////////////////////////////////
		const vxBool IsNormalized() const { return mNormalized; }
		//////////////////////////////////////////////////////////////
		/// Gets the unit of the attribute. May be specified for
		/// texture coordinates. Is 0 by default and for all other
		/// usages apart from texture coordinates.
		//////////////////////////////////////////////////////////////
		const vxU32 GetUnit() const { return mUnit; }

	private:

		vxU32 mOffset;
		vxU32 mElements;
		DataType mType;
		AttributeSemantic mUsage;
		vxBool mNormalized;
		vxU32 mUnit;
	};
}

#endif /* VERTEXATTRIBUTE_HPP_ */
