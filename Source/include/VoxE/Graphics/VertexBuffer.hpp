/*
 * GLVertexBuffer.hpp
 *
 *  Created on: 25.06.2014
 *      Author: euaconlabs
 */

#ifndef GLVERTEXBUFFER_HPP_
#define GLVERTEXBUFFER_HPP_

#include <VoxE/Graphics/BufferUsage.hpp>
#include <VoxE/Graphics/VertexDescription.hpp>
#include <VoxE/Native/Atomic.hpp>

namespace VX
{
	//////////////////////////////////////////////////////////////
	/// \class VertexBuffer
	/// Implements a hardware vertex buffer in OpenGL. A vertex
	/// buffer contains generic vertex data defined by a vertex
	/// description so that the rendering engine can determine the
	/// data stored in the buffer.
	//////////////////////////////////////////////////////////////
	class VertexBuffer
	{
	public:

		VertexBuffer();
		VertexBuffer(const VertexBuffer& b);
		~VertexBuffer();

		//////////////////////////////////////////////////////////////
		/// Allocates the memory for the buffer and optionally copies
		/// the memory from a given pointer. This automatically binds
		/// the buffer, too. This actually makes a buffer 'valid'.
		/// An additional parameter 'usage' is required in order to
		/// inform OpenGL how the buffer is intended to be used. See
		/// the OpenGL wiki for further information.
		/// \param size The size of the buffer in bytes.
		/// \param usage The intended usage of the buffer.
		/// \param data [Optional] If not null the buffer will be filled
		///	with the data this pointer points to.
		//////////////////////////////////////////////////////////////
		void Allocate(const vxU64 size, const BufferUsage& usage, const void* data = nullptr);

		//////////////////////////////////////////////////////////////
		/// Copies 'size' bytes from the data pointed to by 'data' into
		/// the buffer object at location 'offset'.
		/// \param size The size of the data to copy.
		/// \param data The data to copy. Must not be nullptr.
		/// \param offset An offset into the buffer object in bytes.
		/// \param direct If set to true the buffer object will not be
		///	bound automatically.
		//////////////////////////////////////////////////////////////
		void CopyData(const vxU64 size, const void* data, const vxI64 offset = 0, const vxBool direct = false);

		//////////////////////////////////////////////////////////////
		/// Sets the vertex description for the data stored in this
		/// buffer object. If no description is available when
		/// rendering the rendering engine will not be able to render
		/// the vertices.
		/// \param desc The vertex description for this buffer object.
		//////////////////////////////////////////////////////////////
		void SetVertexDescription(const VertexDescription& desc);
		//////////////////////////////////////////////////////////////
		/// Gets the vertex description describing the data stored
		/// in this buffer object.
		//////////////////////////////////////////////////////////////
		const VertexDescription& GetVertexDescription() const;

		//////////////////////////////////////////////////////////////
		// Maps the buffer and returns the mapped pointer.
		/// \param access The mode in which the buffer object's data is
		///	being operated on.
		/// \param direct If set to true the buffer object will not be
		///	bound automatically.
		//////////////////////////////////////////////////////////////
		void* Map(const BufferAccess& access, const vxBool direct = false);
		//////////////////////////////////////////////////////////////
		/// Unmaps the buffer. Afterwards no more data should be written
		/// into the pointer returned by Map().
		/// \param direct If set to true the buffer object will not be
		///	bound automatically.
		//////////////////////////////////////////////////////////////
		void UnMap(const vxBool direct = false);
		//////////////////////////////////////////////////////////////
		/// Binds the vertex buffer.
		//////////////////////////////////////////////////////////////
		void Bind() const;
		//////////////////////////////////////////////////////////////
		/// Unbinds the vertex buffer.
		//////////////////////////////////////////////////////////////
		void UnBind() const;
		//////////////////////////////////////////////////////////////
		/// Checks whether the buffer is a valid hardware buffer or
		/// not, i.e. if it has been created or not.
		//////////////////////////////////////////////////////////////
		vxBool IsValid() const;
		//////////////////////////////////////////////////////////////
		/// Destroys the internal OpenGL buffer thereby invalidating
		/// every access to it.
		//////////////////////////////////////////////////////////////
		void Destroy();

		VertexBuffer& operator= (const VertexBuffer& b);

	private:

		void Copy(const VertexBuffer& b);

		vxU32* mReferences;

		vxU32 mHandle;
		VertexDescription mVertexDesc;
	};
}

#endif /* GLVERTEXBUFFER_HPP_ */
