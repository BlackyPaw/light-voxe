/*
 * VertexDescription.hpp
 *
 *  Created on: 24.06.2014
 *      Author: euaconlabs
 */

#ifndef VERTEXDESCRIPTION_HPP_
#define VERTEXDESCRIPTION_HPP_

#include <VoxE/Graphics/VertexAttribute.hpp>

#include <vector>

namespace VX
{
	//////////////////////////////////////////////////////////////
	/// \class VertexDescription
	/// \brief Holds a set of vertex attributes thereby describing
	///	a vertex's memory layout in a vertex buffer.
	//////////////////////////////////////////////////////////////
	class VertexDescription
	{
	public:

		VertexDescription();
		~VertexDescription();

		//////////////////////////////////////////////////////////////
		/// Adds an attribute to the description.
		/// \param attrib The attribute to add.
		//////////////////////////////////////////////////////////////
		void AddAttribute(const VertexAttribute& attrib);
		//////////////////////////////////////////////////////////////
		/// Gets an array containing all vertex attributes added.
		//////////////////////////////////////////////////////////////
		const VertexAttribute* GetAttributes() const;
		//////////////////////////////////////////////////////////////
		/// Gets the number of vertex attributes the description
		/// contains.
		//////////////////////////////////////////////////////////////
		vxU32 GetNumAttributes() const;
		//////////////////////////////////////////////////////////////
		/// Gets the stride between two adjacent vertices.
		//////////////////////////////////////////////////////////////
		vxU32 GetStride() const;

	private:

		std::vector<VertexAttribute> mAttributes;
		vxU32 mStride;
	};
}

#endif /* VERTEXDESCRIPTION_HPP_ */
