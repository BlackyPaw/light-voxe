#ifndef KEY_UP_EVENT_HPP
#define KEY_UP_EVENT_HPP

#include <VoxE/Core/Event.hpp>
#include <VoxE/HID/KeyboardKeys.hpp>

namespace VX
{
	//////////////////////////////////////////////////////////////
	/// \class KeyUpEvent
	/// This event is sent whenever a previously pressed key on a
	/// keyboard returns to a released state. It is preceded by a
	/// matching KeyDownEvent.
	/// \see KeyDownEvent
	//////////////////////////////////////////////////////////////
	class KeyUpEvent : public Event
	{
	public:

		KeyUpEvent(const KeyboardKeys& key);

		DECLARE_EVENT(0x7ECA488B)

		//////////////////////////////////////////////////////////////
		/// Gets the released key, i.e. the key the event contains.
		//////////////////////////////////////////////////////////////
		const KeyboardKeys& GetKey() const;

	private:

		const KeyboardKeys mKey;
	};
}

#endif // KEY_UP_EVENT_HPP