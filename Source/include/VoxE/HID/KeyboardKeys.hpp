#ifndef KEYBOARD_KEYS_HPP
#define KEYBOARD_KEYS_HPP

namespace VX
{
	//////////////////////////////////////////////////////////////
	/// \enum KeyboardKeys
	/// \brief This enumeration contains all keyboard keys which are
	/// tracked by the engine.
	//////////////////////////////////////////////////////////////
	enum KeyboardKeys
	{
		KEYBOARD_KEY_A = 0,
		KEYBOARD_KEY_B,
		KEYBOARD_KEY_C,
		KEYBOARD_KEY_D,
		KEYBOARD_KEY_E,
		KEYBOARD_KEY_F,
		KEYBOARD_KEY_G,
		KEYBOARD_KEY_H,
		KEYBOARD_KEY_I,
		KEYBOARD_KEY_J,
		KEYBOARD_KEY_K,
		KEYBOARD_KEY_L,
		KEYBOARD_KEY_M,
		KEYBOARD_KEY_N,
		KEYBOARD_KEY_O,
		KEYBOARD_KEY_P,
		KEYBOARD_KEY_Q,
		KEYBOARD_KEY_R,
		KEYBOARD_KEY_S,
		KEYBOARD_KEY_T,
		KEYBOARD_KEY_U,
		KEYBOARD_KEY_V,
		KEYBOARD_KEY_W,
		KEYBOARD_KEY_X,
		KEYBOARD_KEY_Y,
		KEYBOARD_KEY_Z,
		KEYBOARD_KEY_0,
		KEYBOARD_KEY_1,
		KEYBOARD_KEY_2,
		KEYBOARD_KEY_3,
		KEYBOARD_KEY_4,
		KEYBOARD_KEY_5,
		KEYBOARD_KEY_6,
		KEYBOARD_KEY_7,
		KEYBOARD_KEY_8,
		KEYBOARD_KEY_9,
		KEYBOARD_KEY_NUM_0,
		KEYBOARD_KEY_NUM_1,
		KEYBOARD_KEY_NUM_2,
		KEYBOARD_KEY_NUM_3,
		KEYBOARD_KEY_NUM_4,
		KEYBOARD_KEY_NUM_5,
		KEYBOARD_KEY_NUM_6,
		KEYBOARD_KEY_NUM_7,
		KEYBOARD_KEY_NUM_8,
		KEYBOARD_KEY_NUM_9,
		KEYBOARD_KEY_F1,
		KEYBOARD_KEY_F2,
		KEYBOARD_KEY_F3,
		KEYBOARD_KEY_F4,
		KEYBOARD_KEY_F5,
		KEYBOARD_KEY_F6,
		KEYBOARD_KEY_F7,
		KEYBOARD_KEY_F8,
		KEYBOARD_KEY_F9,
		KEYBOARD_KEY_F10,
		KEYBOARD_KEY_F11,
		KEYBOARD_KEY_F12,

		KEYBOARD_KEY_ESC,
		KEYBOARD_KEY_LCTRL,
		KEYBOARD_KEY_RCTRL,
		KEYBOARD_KEY_LSHIFT,
		KEYBOARD_KEY_RSHIFT,
		KEYBOARD_KEY_SPACE,
		KEYBOARD_KEY_BACKSPACE,
		KEYBOARD_KEY_ENTER,
		KEYBOARD_KEY_TAB,
		KEYBOARD_KEY_LEFT,
		KEYBOARD_KEY_UP,
		KEYBOARD_KEY_RIGHT,
		KEYBOARD_KEY_DOWN,
		KEYBOARD_KEY_CLEAR,
		KEYBOARD_KEY_PAUSE,
		KEYBOARD_KEY_CAPS_LOCK,
		KEYBOARD_KEY_PAGE_UP,
		KEYBOARD_KEY_PAGE_DOWN,
		KEYBOARD_KEY_END,
		KEYBOARD_KEY_HOME,
		KEYBOARD_KEY_SELECT,
		KEYBOARD_KEY_PRINT,
		KEYBOARD_KEY_EXECUTE,
		KEYBOARD_KEY_PRINT_SCREEN,
		KEYBOARD_KEY_INSERT,
		KEYBOARD_KEY_DELETE,
		KEYBOARD_KEY_HELP,
		KEYBOARD_KEY_MULTIPLY,
		KEYBOARD_KEY_ADD,
		KEYBOARD_KEY_SUBTRACT,
		KEYBOARD_KEY_DIVIDE,
		KEYBOARD_KEY_SEPARATOR,
		KEYBOARD_KEY_DECIMAL,
		KEYBOARD_KEY_NUM_LOCK,
		KEYBOARD_KEY_SCROLL_LOCK,
		KEYBOARD_KEY_PLUS,
		KEYBOARD_KEY_COMMA,
		KEYBOARD_KEY_MINUS,
		KEYBOARD_KEY_PERIOD,
		KEYBOARD_KEY_SEMICOLON,
		KEYBOARD_KEY_SLASH,
		KEYBOARD_KEY_TILDE,
		KEYBOARD_KEY_OPEN_BRACKET,
		KEYBOARD_KEY_CLOSE_BRACKET,
		KEYBOARD_KEY_BACKSLASH,
		KEYBOARD_KEY_QUOTES,

		KEYBOARD_NUM_KEYS
	};
}

#endif // KEYBOARD_KEYS_HPP
