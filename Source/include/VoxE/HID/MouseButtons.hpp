#ifndef MOUSE_BUTTONS_HPP
#define MOUSE_BUTTONS_HPP

namespace VX
{
	//////////////////////////////////////////////////////////////
	/// \enum MouseButtons
	/// This enumeration contains all detectable mouse buttons.
	//////////////////////////////////////////////////////////////
	enum MouseButtons
	{
		MOUSE_BUTTON_LEFT,
		MOUSE_BUTTON_RIGHT,
		MOUSE_BUTTON_MIDDLE,

		MOUSE_NUM_BUTTONS
	};
}

#endif // MOUSE_BUTTONS_HPP