#ifndef MOUSE_DOWN_EVENT_HPP
#define MOUSE_DOWN_EVENT_HPP

#include <VoxE/Core/Event.hpp>
#include <VoxE/Core/Vector.hpp>
#include <VoxE/HID/MouseButtons.hpp>

namespace VX
{
	//////////////////////////////////////////////////////////////
	/// \class MouseDownEvent
	/// This event is sent whenever is mouse button is held down.
	//////////////////////////////////////////////////////////////
	class MouseDownEvent : public Event
	{
	public:

		MouseDownEvent(const MouseButtons& button, const Vector2i& coords);

		DECLARE_EVENT(0xE4B38198);

		//////////////////////////////////////////////////////////////
		/// Gets the mouse button which was pressed down.
		//////////////////////////////////////////////////////////////
		const MouseButtons& GetButton() const;
		//////////////////////////////////////////////////////////////
		/// Gets the coordinates where the button was pressed down.
		//////////////////////////////////////////////////////////////
		const Vector2i& GetCoords() const;

	private:

		MouseButtons mButton;
		Vector2i mCoords;
	};
}

#endif // MOUSE_DOWN_EVENT_HPP