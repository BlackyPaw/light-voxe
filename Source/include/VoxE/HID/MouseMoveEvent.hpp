#ifndef MOUSE_MOVE_EVENT_HPP
#define MOUSE_MOVE_EVENT_HPP

#include <VoxE/Core/Event.hpp>
#include <VoxE/Core/Vector.hpp>

namespace VX
{
	//////////////////////////////////////////////////////////////
	/// \class MouseMoveEvent
	/// This event is sent whenever the position of the mouse
	/// changes.
	//////////////////////////////////////////////////////////////
	class MouseMoveEvent : public Event
	{
	public:

		MouseMoveEvent(const Vector2i& coords);

		DECLARE_EVENT(0xA2A93B2B)

		//////////////////////////////////////////////////////////////
		/// Gets the coordinates of the mouse.
		//////////////////////////////////////////////////////////////
		const Vector2i& GetCoords() const;

	private:

		Vector2i mCoords;
	};
}

#endif // MOUSE_MOVE_EVENT_HPP