#ifndef VXMATRIX4_HPP_D7972082_7C08_4F85_97E7_696E330AD836
#define VXMATRIX4_HPP_D7972082_7C08_4F85_97E7_696E330AD836

//////////////////////////////////////////////////////////////
/// VoxE
//////////////////////////////////////////////////////////////
#include <VoxE/Native/Atomic.hpp>
#include <VoxE/Core/Vector.hpp>
#include <VoxE/Math/MathUtils.hpp>

#include <cstring>

namespace VX
{
	//////////////////////////////////////////////////////////////
	/// \struct TMatrix4
	/// \brief Templatized class for matrix transformations.
	/// \tparam Value The value type of the matrix.
	//////////////////////////////////////////////////////////////
	template<typename Value>
	struct TMatrix4
	{
	public:
		typedef Value ValueType;
		typedef Vector4<ValueType> ColType;
		typedef Vector4<ValueType> RowType;

		//////////////////////////////////////////////////////////////
		/// \brief Constructs a matrix.
		///
		/// Initializes the diagonal of the matrix with the given
		/// value.
		//////////////////////////////////////////////////////////////
		TMatrix4(const ValueType& v = Value(1))
		{
			(*this)[0] = Vector4<ValueType>(Value(0), Value(0), Value(0), Value(0));
			(*this)[1] = Vector4<ValueType>(Value(0), Value(0), Value(0), Value(0));
			(*this)[2] = Vector4<ValueType>(Value(0), Value(0), Value(0), Value(0));
			(*this)[3] = Vector4<ValueType>(Value(0), Value(0), Value(0), Value(0));
			(*this)[0][0] = v;
			(*this)[1][1] = v;
			(*this)[2][2] = v;
			(*this)[3][3] = v;
		}
		//////////////////////////////////////////////////////////////
		/// \brief Initialization constructor.
		/// \param x
		/// \param y
		/// \param z
		/// \param w
		//////////////////////////////////////////////////////////////
		TMatrix4(const ColType& x, const ColType& y, const ColType& z, const ColType& w)
		{
			(*this)[0] = x;
			(*this)[1] = y;
			(*this)[2] = z;
			(*this)[3] = w;
		}
		//////////////////////////////////////////////////////////////
		/// \brief Intialization constructor.
		/// \param x0
		/// \param x1
		/// \param x2
		/// \param x3
		/// \param y0
		/// \param y1
		/// \param y2
		/// \param y3
		/// \param z0
		/// \param z1
		/// \param z2
		/// \param z3
		/// \param w0
		/// \param w1
		/// \param w2
		/// \param w3
		//////////////////////////////////////////////////////////////
		TMatrix4(const ValueType& x0, const ValueType& y0, const ValueType& z0, const ValueType& w0,
			const ValueType& x1, const ValueType& y1, const ValueType& z1, const ValueType& w1,
			const ValueType& x2, const ValueType& y2, const ValueType& z2, const ValueType& w2,
			const ValueType& x3, const ValueType& y3, const ValueType& z3, const ValueType& w3)
		{
			(*this)[0] = ColType(x0, y0, z0, w0);
			(*this)[1] = ColType(x1, y1, z1, w1);
			(*this)[2] = ColType(x2, y2, z2, w2);
			(*this)[3] = ColType(x3, y3, z3, w3);
		}
		//////////////////////////////////////////////////////////////
		/// \brief Copy constructor.
		//////////////////////////////////////////////////////////////
		TMatrix4(const TMatrix4<ValueType>& b)
		{
			(*this)[0] = b[0];
			(*this)[1] = b[1];
			(*this)[2] = b[2];
			(*this)[3] = b[3];
		}

		ValueType* Data()
		{
			return &(*this)[0][0];
		}

		const ValueType* Data() const
		{
			return &(*this)[0][0];
		}

		//////////////////////////////////////////////////////////////
		/// \brief Calculates the transpose of this matrix.
		//////////////////////////////////////////////////////////////
		TMatrix4<ValueType> Transpose() const
		{
			TMatrix4<ValueType> transpose;
			transpose[0][0] = m[0][0];
			transpose[0][1] = m[1][0];
			transpose[0][1] = m[2][0];
			transpose[0][1] = m[3][0];

			transpose[1][0] = m[0][1];
			transpose[1][1] = m[1][1];
			transpose[1][2] = m[2][1];
			transpose[1][3] = m[3][1];

			transpose[2][0] = m[0][2];
			transpose[2][1] = m[1][2];
			transpose[2][2] = m[2][2];
			transpose[2][3] = m[3][2];

			transpose[3][0] = m[0][3];
			transpose[3][1] = m[1][3];
			transpose[3][2] = m[2][3];
			transpose[3][3] = m[3][3];

			return transpose;
		}

		//////////////////////////////////////////////////////////////
		/// \brief Calculates the determinant of this matrix.
		//////////////////////////////////////////////////////////////
		ValueType Determinant() const
		{
			ValueType det;
			ValueType a = m[0][0] * (m[1][1] * m[2][2] * m[3][3] + m[2][1] * m[3][2] * m[1][3] + m[3][1] * m[1][2] * m[2][3] -
				m[3][1] * m[2][2] * m[1][3] - m[2][1] * m[1][2] * m[3][3] - m[1][1] * m[3][2] * m[2][3]);
			ValueType b = m[1][0] * (m[0][1] * m[2][2] * m[3][3] + m[2][1] * m[3][2] * m[0][3] + m[3][1] * m[0][2] * m[2][3] -
				m[3][1] * m[2][2] * m[0][3] - m[2][1] * m[0][2] * m[3][3] - m[0][1] * m[3][2] * m[2][3]);
			ValueType c = m[2][0] * (m[0][1] * m[1][2] * m[3][3] + m[1][1] * m[3][2] * m[0][3] + m[3][1] * m[0][2] * m[1][3] -
				m[3][1] * m[1][2] * m[0][3] - m[1][1] * m[0][2] * m[3][3] - m[0][1] * m[3][2] * m[1][3]);
			ValueType d = m[3][0] * (m[0][1] * m[1][2] * m[2][3] + m[1][1] * m[2][2] * m[0][3] + m[2][1] * m[0][2] * m[1][3] -
				m[2][1] * m[1][2] * m[0][3] - m[1][1] * m[0][2] * m[2][3] - m[0][1] * m[2][2] * m[1][3]);
			det = a - b + c - d;
			return det;
		}

		TMatrix4<ValueType> Inverse() const
		{
			ValueType c00 = m[2][2] * m[3][3] - m[3][2] * m[2][3];
			ValueType c02 = m[1][2] * m[3][3] - m[3][2] * m[1][3];
			ValueType c03 = m[1][2] * m[2][3] - m[2][2] * m[1][3];

			ValueType c04 = m[2][1] * m[3][3] - m[3][1] * m[2][3];
			ValueType c06 = m[1][1] * m[3][3] - m[3][1] * m[1][3];
			ValueType c07 = m[1][1] * m[2][3] - m[2][1] * m[1][3];

			ValueType c08 = m[2][1] * m[3][2] - m[3][1] * m[2][2];
			ValueType c10 = m[1][1] * m[3][2] - m[3][1] * m[1][2];
			ValueType c11 = m[1][1] * m[2][2] - m[2][1] * m[1][2];

			ValueType c12 = m[2][0] * m[3][3] - m[3][0] * m[2][3];
			ValueType c14 = m[1][0] * m[3][3] - m[3][0] * m[1][3];
			ValueType c15 = m[1][0] * m[2][3] - m[2][0] * m[1][3];

			ValueType c16 = m[2][0] * m[3][2] - m[3][0] * m[2][2];
			ValueType c18 = m[1][0] * m[3][2] - m[3][0] * m[1][2];
			ValueType c19 = m[1][0] * m[2][2] - m[2][0] * m[1][2];

			ValueType c20 = m[2][0] * m[3][1] - m[3][0] * m[2][1];
			ValueType c22 = m[1][0] * m[3][1] - m[3][0] * m[1][1];
			ValueType c23 = m[1][0] * m[2][1] - m[2][0] * m[1][1];

			Vector4<ValueType> sigA = Vector4<ValueType>(Value(1), Value(-1), Value(1), Value(-1));
			Vector4<ValueType> sigB = Vector4<ValueType>(Value(-1), Value(1), Value(-1), Value(1));

			Vector4<ValueType> f0(c00, c00, c02, c03);
			Vector4<ValueType> f1(c04, c04, c06, c07);
			Vector4<ValueType> f2(c08, c08, c10, c11);
			Vector4<ValueType> f3(c12, c12, c14, c15);
			Vector4<ValueType> f4(c16, c16, c18, c19);
			Vector4<ValueType> f5(c20, c20, c22, c23);

			Vector4<ValueType> v0(m[1][0], m[0][0], m[0][0], m[0][0]);
			Vector4<ValueType> v1(m[1][1], m[0][1], m[0][1], m[0][1]);
			Vector4<ValueType> v2(m[1][2], m[0][2], m[0][2], m[0][2]);
			Vector4<ValueType> v3(m[1][3], m[0][3], m[0][3], m[0][3]);

			Vector4<ValueType> i0 = sigA * (v1 * f0 - v2 * f1 + v3 * f2);
			Vector4<ValueType> i1 = sigB * (v0 * f0 - v2 * f3 + v3 * f4);
			Vector4<ValueType> i2 = sigA * (v0 * f1 - v1 * f3 + v3 * f5);
			Vector4<ValueType> i3 = sigB * (v0 * f2 - v1 * f4 + v2 * f5);

			TMatrix4<ValueType> inverse(i0, i1, i2, i3);

			ValueType det = Dot(Vector4<ValueType>(inverse[0][0], inverse[1][0], inverse[2][0], inverse[3][0]), m[0]);
			inverse /= det;

			return inverse;
		}

		//////////////////////////////////////////////////////////////
		/// \brief Creates a translation matrix.
		/// \param x X-Translation.
		/// \param y Y-Translation.
		/// \param z Z-Translation.
		//////////////////////////////////////////////////////////////
		template<typename T>
		static TMatrix4<T> Translate(const T& x, const T& y, const T&z)
		{
			TMatrix4<T> m;
			m[3][0] = x;
			m[3][1] = y;
			m[3][2] = z;
			return m;
		}

		//////////////////////////////////////////////////////////////
		/// \brief Creates a translation matrix.
		/// \param v The translation vector.
		//////////////////////////////////////////////////////////////
		template<typename T>
		static TMatrix4<T> Translate(const Vector3<T>& v)
		{
			return Translate(v.x, v.y, v.z);
		}

		//////////////////////////////////////////////////////////////
		/// \brief Creates a rotation matrix.
		/// \param an The axis to rotate around.
		/// \param angle The angle to rotate about.
		//////////////////////////////////////////////////////////////
		template<typename T>
		static TMatrix4<T> Rotate(const Vector3<T>& an, const T& angle)
		{
#if defined(VOXE_MATH_USE_RADIANS)
			const T a = angle;
#else
			const T a = Radians(angle);
#endif

			const T s = std::sin(a);
			const T c = std::cos(a);

			const Vector3<T> axis = Normalize(an);
			const T _c = (1.0f - c);

			TMatrix4<T> m;
			m[0][0] = axis[0] * axis[0] * _c + c;
			m[0][1] = axis[1] * axis[0] * _c + s * axis[2];
			m[0][2] = axis[2] * axis[0] * _c - s * axis[1];

			m[1][0] = axis[0] * axis[1] * _c - s * axis[2];
			m[1][1] = axis[1] * axis[1] * _c + c;
			m[1][2] = axis[2] * axis[1] * _c + s * axis[0];

			m[2][0] = axis[0] * axis[2] * _c + s * axis[1];
			m[2][1] = axis[1] * axis[2] * _c - s * axis[0];
			m[2][2] = axis[2] * axis[2] * _c + c;

			return m;
		}

		//////////////////////////////////////////////////////////////
		/// \brief Creates a rotation matrix around the X-Axis.
		/// \param angle The angle to rotate about.
		//////////////////////////////////////////////////////////////
		template<typename T>
		static TMatrix4<T> RotateX(const T& angle)
		{
			return Rotate(Vector3<T>(T(1), T(0), T(0)), angle);
		}

		//////////////////////////////////////////////////////////////
		/// \brief Creates a rotation matrix around the Y-Axis.
		/// \param angle The angle to rotate about.
		//////////////////////////////////////////////////////////////
		template<typename T>
		static TMatrix4<T> RotateY(const T& angle)
		{
			return Rotate(Vector3<T>(T(0), T(1), T(0)), angle);
		}

		//////////////////////////////////////////////////////////////
		/// \brief Creates a rotation matrix around the ZAxis.
		/// \param angle The angle to rotate about.
		//////////////////////////////////////////////////////////////
		template<typename T>
		static TMatrix4<T> RotateZ(const T& angle)
		{
			return Rotate(Vector3<T>(T(0), T(0), T(1)), angle);
		}

		//////////////////////////////////////////////////////////////
		/// \brief Creates a scaling matrix.
		/// \param x The X scaling to apply.
		/// \param y The Y scaling to apply.
		/// \param z The Z scaling to apply.
		//////////////////////////////////////////////////////////////
		template<typename T>
		static TMatrix4<T> Scale(const T& x, const T& y, const T& z)
		{
			TMatrix4<T> m;
			m[0][0] = x;
			m[1][1] = y;
			m[2][2] = z;
			return m;
		}

		//////////////////////////////////////////////////////////////
		/// \brief Creates a scaling matrix.
		/// \param s The scaling to apply.
		//////////////////////////////////////////////////////////////
		template<typename T>
		static TMatrix4<T> Scale(const Vector3<T>& s)
		{
			return Scale(s.x, s.y, s.z);
		}

		//////////////////////////////////////////////////////////////
		/// \brief Creates an orthographic projection matrix.
		/// \param left
		/// \param right
		/// \param bottom
		/// \param top
		/// \param zNear
		/// \param zFar
		//////////////////////////////////////////////////////////////
		template<typename T>
		static TMatrix4<T> Orthographic(const T& left, const T& right, const T& bottom, const T& top, const T& zNear, const T& zFar)
		{
			TMatrix4<T> m;
			m[0][0] = T(2) / (right - left);
			m[1][1] = T(2) / (top - bottom);
			m[2][2] = T(-2) / (zFar - zNear);
			m[3][0] = -(right + left) / (right - left);
			m[3][1] = -(top + bottom) / (top - bottom);
			m[3][2] = -(zFar + zNear) / (zFar - zNear);
			return m;
		}

		//////////////////////////////////////////////////////////////
		/// \brief Creates a frustrum (projection) matrix.
		/// \param left
		/// \param right
		/// \param bottom
		/// \param top
		/// \param zNear
		/// \param zFar
		//////////////////////////////////////////////////////////////
		template<typename T>
		static TMatrix4<T> Frustrum(const T& left, const T& right, const T& bottom, const T& top, const T& zNear, const T& zFar)
		{
			TMatrix4<T> m(T(0));
			m[0][0] = (T(2) * zNear) / (right - left);
			m[1][1] = (T(2) * zNear) / (top - bottom);
			m[2][0] = (right + left) / (right - left);
			m[2][1] = (top + bottom) / (top - bottom);
			m[2][2] = -(zFar + zNear) / (zFar - zNear);
			m[2][3] = T(-1);
			m[3][2] = -(T(2) * zFar * zNear) / (zFar - zNear);
			return m;
		}

		//////////////////////////////////////////////////////////////
		/// \brief Creates a perspective matrix.
		/// \param fovy
		/// \param aspect
		/// \param zNear
		/// \param zFar
		//////////////////////////////////////////////////////////////
		template<typename T>
		static TMatrix4<T> Perspective(const T& fovy, const T& aspect, const T& zNear, const T& zFar)
		{
#if defined(VOXE_MATH_USE_RADIANS)
			const T a = fovy;
#else
			const T a = Radians(fovy);
#endif

			TMatrix4<T> m(T(0));
			const T tanFovOver2 = tan(a / T(2));
			m[0][0] = T(1) / (aspect * tanFovOver2);
			m[1][1] = T(1) / tanFovOver2;
			m[2][2] = -(zFar + zNear) / (zFar - zNear);
			m[2][3] = T(-1);
			m[3][2] = -(T(2) * zFar * zNear) / (zFar - zNear);
			return m;
		}

		//////////////////////////////////////////////////////////////
		/// \brief Creates a view matrix.
		/// \param eye
		/// \param center
		/// \param up
		//////////////////////////////////////////////////////////////
		template<typename T>
		static TMatrix4<T> LookAt(const Vector3<T>& eye, const Vector3<T>& center, const Vector3<T>& up)
		{
			Vector3<T> forward = Normalize(center - eye);
			Vector3<T> _up = Normalize(up);
			Vector3<T> right = Normalize(Cross(forward, _up));
			_up = Cross(right, forward);

			TMatrix4<T> m;
			m[0][0] = right.x;
			m[1][0] = right.y;
			m[2][0] = right.z;
			m[0][1] = _up.x;
			m[1][1] = _up.y;
			m[2][1] = _up.z;
			m[0][2] = -forward.x;
			m[1][2] = -forward.y;
			m[2][2] = -forward.z;
			m[3][0] = -Dot(right, eye);
			m[3][1] = -Dot(_up, eye);
			m[3][2] = Dot(forward, eye);

			return m;
		}

		//////////////////////////////////////////////////////////////
		/// \brief Returns the column at index n.
		//////////////////////////////////////////////////////////////
		const ColType& operator[](const vxU32 n) const
		{
			return m[n];
		}
		//////////////////////////////////////////////////////////////
		/// \brief Returns the column at index n.
		//////////////////////////////////////////////////////////////
		ColType& operator[](const vxU32 n)
		{
			return m[n];
		}

		//////////////////////////////////////////////////////////////
		/// Matrix unary operators
		//////////////////////////////////////////////////////////////
		TMatrix4<ValueType>& operator=(const TMatrix4<ValueType>& b)
		{
			(*this)[0] = b[0];
			(*this)[1] = b[1];
			(*this)[2] = b[2];
			(*this)[3] = b[3];
			return *this;
		}

		TMatrix4<ValueType>& operator+=(const ValueType& b)
		{
			(*this)[0] += b;
			(*this)[1] += b;
			(*this)[2] += b;
			(*this)[3] += b;
			return *this;
		}

		TMatrix4<ValueType>& operator+=(const TMatrix4<ValueType>& b)
		{
			(*this)[0] += b[0];
			(*this)[1] += b[1];
			(*this)[2] += b[2];
			(*this)[3] += b[3];
			return *this;
		}

		TMatrix4<ValueType>& operator-=(const ValueType& b)
		{
			(*this)[0] -= b;
			(*this)[1] -= b;
			(*this)[2] -= b;
			(*this)[3] -= b;
			return *this;
		}

		TMatrix4<ValueType>& operator-=(const TMatrix4<ValueType>& b)
		{
			(*this)[0] -= b[0];
			(*this)[1] -= b[1];
			(*this)[2] -= b[2];
			(*this)[3] -= b[3];
			return *this;
		}

		TMatrix4<ValueType>& operator*=(const ValueType& b)
		{
			(*this)[0] *= b;
			(*this)[1] *= b;
			(*this)[2] *= b;
			(*this)[3] *= b;
			return *this;
		}

		TMatrix4<ValueType>& operator*=(const TMatrix4<ValueType>& b)
		{
			(*this) = (*this) * b;
			return *this;
		}

		TMatrix4<ValueType>& operator/=(const ValueType& b)
		{
			(*this)[0] /= b;
			(*this)[1] /= b;
			(*this)[2] /= b;
			(*this)[3] /= b;
			return *this;
		}

		TMatrix4<ValueType>& operator/=(const TMatrix4<ValueType>& b)
		{
			(*this) = (*this) / b;
			return *this;
		}

		TMatrix4<ValueType> operator~() const
		{
			return Transpose();
		}

		TMatrix4<ValueType> operator-() const
		{
			return Inverse();
		}

		vxBool operator== (const TMatrix4<ValueType>& b)
		{
			return (std::memcmp(m, b.m, 16 * sizeof(ValueType)) == 0);
		}

		vxBool operator!= (const TMatrix4<ValueType>& b)
		{
			return !((*this) == b);
		}

	private:
		ColType m[4];
	};

	namespace Priv
	{
		//////////////////////////////////////////////////////////////
		/// \param m
		//////////////////////////////////////////////////////////////
		template<typename T>
		TMatrix4<T> _Inverse(const TMatrix4<T>& m)
		{
			T det00 = m[2][2] * m[3][3] - m[3][2] * m[2][3];
			T det01 = m[2][1] * m[3][3] - m[3][1] * m[2][3];
			T det02 = m[2][1] * m[3][2] - m[3][1] * m[2][2];
			T det03 = m[2][0] * m[3][3] - m[3][0] * m[2][3];
			T det04 = m[2][0] * m[3][2] - m[3][0] * m[2][2];
			T det05 = m[2][0] * m[3][1] - m[3][0] * m[2][1];
			T det06 = m[1][2] * m[3][3] - m[3][2] * m[1][3];
			T det07 = m[1][1] * m[3][3] - m[3][1] * m[1][3];
			T det08 = m[1][1] * m[3][2] - m[3][1] * m[1][2];
			T det09 = m[1][0] * m[3][3] - m[3][0] * m[1][3];
			T det10 = m[1][0] * m[3][2] - m[3][0] * m[1][2];
			T det11 = m[1][1] * m[3][3] - m[3][1] * m[1][3];
			T det12 = m[1][0] * m[3][1] - m[3][0] * m[1][1];
			T det13 = m[1][2] * m[2][3] - m[2][2] * m[1][3];
			T det14 = m[1][1] * m[2][3] - m[2][1] * m[1][3];
			T det15 = m[1][1] * m[2][2] - m[2][1] * m[1][2];
			T det16 = m[1][0] * m[2][3] - m[2][0] * m[1][3];
			T det17 = m[1][0] * m[2][2] - m[2][0] * m[1][2];
			T det18 = m[1][0] * m[2][1] - m[2][0] * m[1][1];

			TMatrix4<T> inverse(
				+m[1][1] * det00 - m[1][2] * det01 + m[1][3] * det02,
				-m[1][0] * det00 + m[1][2] * det03 - m[1][3] * det04,
				+m[1][0] * det01 - m[1][1] * det03 + m[1][3] * det05,
				-m[1][0] * det02 + m[1][1] * det04 - m[1][2] * det05,

				-m[0][1] * det00 + m[0][2] * det01 - m[0][3] * det02,
				+m[0][0] * det00 - m[0][2] * det03 + m[0][3] * det04,
				-m[0][0] * det01 + m[0][1] * det03 - m[0][3] * det05,
				+m[0][0] * det02 - m[0][1] * det04 + m[0][2] * det05,

				+m[0][1] * det06 - m[0][2] * det07 + m[0][3] * det08,
				-m[0][0] * det06 + m[0][2] * det09 - m[0][3] * det10,
				+m[0][0] * det11 - m[0][1] * det09 + m[0][3] * det12,
				-m[0][0] * det08 + m[0][1] * det10 - m[0][2] * det12,

				-m[0][1] * det13 + m[0][2] * det14 - m[0][3] * det15,
				+m[0][0] * det13 - m[0][2] * det16 + m[0][3] * det17,
				-m[0][0] * det14 + m[0][1] * det16 - m[0][3] * det18,
				+m[0][0] * det15 - m[0][1] * det17 + m[0][2] * det18
				);

			T det =
				+m[0][0] * inverse[0][0]
				+ m[0][1] * inverse[1][0]
				+ m[0][2] * inverse[2][0]
				+ m[0][3] * inverse[3][0];

			inverse /= det;
			return inverse;
		}
	}

	//////////////////////////////////////////////////////////////
	/// Matrix binary operators
	//////////////////////////////////////////////////////////////
	template<typename T>
	TMatrix4<T> operator+(const TMatrix4<T>& a, const TMatrix4<T>& b)
	{
		TMatrix4<T> m(a);
		m += b;
		return m;
	}

	template<typename T>
	TMatrix4<T> operator-(const TMatrix4<T>& a, const TMatrix4<T>& b)
	{
		TMatrix4<T> m(a);
		m -= b;
		return m;
	}

	template<typename T>
	TMatrix4<T> operator*(const TMatrix4<T>& a, const TMatrix4<T>& b)
	{
		TMatrix4<T> m(a);
		typename TMatrix4<T>::ColType ColA0 = a[0];
		typename TMatrix4<T>::ColType ColA1 = a[1];
		typename TMatrix4<T>::ColType ColA2 = a[2];
		typename TMatrix4<T>::ColType ColA3 = a[3];

		typename TMatrix4<T>::ColType ColB0 = b[0];
		typename TMatrix4<T>::ColType ColB1 = b[1];
		typename TMatrix4<T>::ColType ColB2 = b[2];
		typename TMatrix4<T>::ColType ColB3 = b[3];

		m[0] = ColA0 * ColB0[0] + ColA1 * ColB0[1] + ColA2 * ColB0[2] + ColA3 * ColB0[3];
		m[1] = ColA0 * ColB1[0] + ColA1 * ColB1[1] + ColA2 * ColB1[2] + ColA3 * ColB1[3];
		m[2] = ColA0 * ColB2[0] + ColA1 * ColB2[1] + ColA2 * ColB2[2] + ColA3 * ColB2[3];
		m[3] = ColA0 * ColB3[0] + ColA1 * ColB3[1] + ColA2 * ColB3[2] + ColA3 * ColB3[3];

		return m;
	}

	template<typename T>
	TMatrix4<T> operator/(const TMatrix4<T>& a, const TMatrix4<T>& b)
	{
		return a * Priv::_Inverse(b);
	}

	template<typename T>
	typename TMatrix4<T>::ColType operator*(const TMatrix4<T>& a, typename TMatrix4<T>::RowType const& b)
	{
		return typename TMatrix4<T>::ColType(
			a[0][0] * b.x + a[1][0] * b.y + a[2][0] * b.z + a[3][0] * b.w,
			a[0][1] * b.x + a[1][1] * b.y + a[2][1] * b.z + a[3][1] * b.w,
			a[0][2] * b.x + a[1][2] * b.y + a[2][2] * b.z + a[3][2] * b.w,
			a[0][3] * b.x + a[1][3] * b.y + a[2][3] * b.z + a[3][3] * b.w);
	}

	template<typename T>
	typename TMatrix4<T>::RowType operator*(typename TMatrix4<T>::ColType const& b, const TMatrix4<T>& a)
	{
		return typename TMatrix4<T>::RowType(
			a[0][0] * b.x + a[0][1] * b.y + a[0][2] * b.z + a[0][3] * b.w,
			a[1][0] * b.x + a[1][1] * b.y + a[1][2] * b.z + a[1][3] * b.w,
			a[2][0] * b.x + a[2][1] * b.y + a[2][2] * b.z + a[2][3] * b.w,
			a[3][0] * b.x + a[3][1] * b.y + a[3][2] * b.z + a[3][3] * b.w);
	}

	template<typename T>
	Vector3<T> operator*(const TMatrix4<T>& a, const Vector3<T>& b)
	{
		Vector4f v = a * Vector4f(b.x, b.y, b.z, T(1));
		return Vector3f(v.x, v.y, v.z);
	}

	template<typename T>
	Vector3<T> operator*(const Vector3<T>& a, const TMatrix4<T>& b)
	{
		Vector4f v = Vector4f(b.x, b.y, b.z, T(1)) * b;
		return Vector3f(v.x, v.y, v.z);
	}

	template<typename T>
	typename TMatrix4<T>::ColType operator/(const TMatrix4<T>& a, typename TMatrix4<T>::RowType const& b)
	{
		return Priv::_Inverse(a) * b;
	}

	template<typename T>
	typename TMatrix4<T>::RowType operator/(typename TMatrix4<T>::ColType const& b, const TMatrix4<T>& a)
	{
		return b * Priv::_Inverse(a);
	}

	template<typename T>
	TMatrix4<T> operator+(const TMatrix4<T>& a, typename TMatrix4<T>::ValueType const& b)
	{
		TMatrix4<T> tmp(a);
		tmp[0] += b;
		tmp[1] += b;
		tmp[2] += b;
		tmp[3] += b;
		return tmp;
	}

	template<typename T>
	TMatrix4<T> operator+(typename TMatrix4<T>::ValueType const& b, const TMatrix4<T>& a)
	{
		TMatrix4<T> tmp(a);
		tmp[0] += b;
		tmp[1] += b;
		tmp[2] += b;
		tmp[3] += b;
		return tmp;
	}

	template<typename T>
	TMatrix4<T> operator-(const TMatrix4<T>& a, typename TMatrix4<T>::ValueType const& b)
	{
		TMatrix4<T> tmp(a);
		tmp[0] -= b;
		tmp[1] -= b;
		tmp[2] -= b;
		tmp[3] -= b;
		return tmp;
	}

	template<typename T>
	TMatrix4<T> operator-(typename TMatrix4<T>::ValueType const& b, const TMatrix4<T>& a)
	{
		TMatrix4<T> tmp(
			b - a[0],
			b - a[1],
			b - a[2],
			b - a[3]
			);
		return tmp;
	}

	template<typename T>
	TMatrix4<T> operator*(const TMatrix4<T>& a, typename TMatrix4<T>::ValueType const& b)
	{
		TMatrix4<T> tmp(a);
		tmp[0] *= b;
		tmp[1] *= b;
		tmp[2] *= b;
		tmp[3] *= b;
		return tmp;
	}

	template<typename T>
	TMatrix4<T> operator*(typename TMatrix4<T>::ValueType const& b, const TMatrix4<T>& a)
	{
		TMatrix4<T> tmp(a);
		tmp[0] *= b;
		tmp[1] *= b;
		tmp[2] *= b;
		tmp[3] *= b;
		return tmp;
	}

	template<typename T>
	TMatrix4<T> operator/(const TMatrix4<T>& a, typename TMatrix4<T>::ValueType const& b)
	{
		TMatrix4<T> tmp(a);
		tmp[0] /= b;
		tmp[1] /= b;
		tmp[2] /= b;
		tmp[3] /= b;
		return tmp;
	}

	template<typename T>
	TMatrix4<T> operator/(typename TMatrix4<T>::ValueType const& b, const TMatrix4<T>& a)
	{
		TMatrix4<T> tmp(
			b / a[0],
			b / a[1],
			b / a[2],
			b / a[3]
			);
		return tmp;
	}

	typedef TMatrix4<vxF32> Matrix4, mat4;
}

#endif
