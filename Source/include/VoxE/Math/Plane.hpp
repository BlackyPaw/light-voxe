/*
 * Plane.hpp
 *
 *  Created on: 29.07.2014
 *      Author: euaconlabs
 */

#ifndef PLANE_HPP_
#define PLANE_HPP_

#include <VoxE/Core/Vector.hpp>

namespace VX
{
	//////////////////////////////////////////////////////////////
	/// \class Plane
	/// \brief Represents three-dimensional planes.
	/// Used to represent planes in a game world and running
	/// several tests with them.
	//////////////////////////////////////////////////////////////
	class Plane
	{
	public:

		//////////////////////////////////////////////////////////////
		/// The plane's components.
		//////////////////////////////////////////////////////////////
		struct
		{
			vxF32 a;
			vxF32 b;
			vxF32 c;
			vxF32 d;
		};

		//////////////////////////////////////////////////////////////
		/// Constructs the basic plane.
		//////////////////////////////////////////////////////////////
		Plane();
		//////////////////////////////////////////////////////////////
		/// Constructs the plane given a point on it and its normal.
		/// \param p One point on the plane.
		/// \param n The plane's normal.
		//////////////////////////////////////////////////////////////
		Plane(const Vector3f& p, const Vector3f& n);
		//////////////////////////////////////////////////////////////
		/// Constructs the plane given three distinct points lying on
		/// it.
		/// \param p1 The first point.
		/// \param p2 The second point.
		/// \param p3 The third point.
		//////////////////////////////////////////////////////////////
		Plane(const Vector3f& p1, const Vector3f& p2, const Vector3f& p3);
		//////////////////////////////////////////////////////////////
		/// Constructs a plane given its raw values for a, b, c, and d.
		/// \param a
		/// \param b
		/// \param c
		/// \param d
		//////////////////////////////////////////////////////////////
		Plane(const vxF32 a, const vxF32 b, const vxF32 c, const vxF32 d);

		//////////////////////////////////////////////////////////////
		/// Constructs the plane given a point on it and its normal.
		/// \param p One point on the plane.
		/// \param n The plane's normal.
		//////////////////////////////////////////////////////////////
		void Create(const Vector3f& p, const Vector3f& n);
		//////////////////////////////////////////////////////////////
		/// Constructs the plane given three distinct points lying on
		/// it.
		/// \param p1 The first point.
		/// \param p2 The second point.
		/// \param p3 The third point.
		//////////////////////////////////////////////////////////////
		void Create(const Vector3f& p1, const Vector3f& p2, const Vector3f& p3);
		//////////////////////////////////////////////////////////////
		/// Constructs a plane given its raw values for a, b, c, and d.
		/// \param a
		/// \param b
		/// \param c
		/// \param d
		//////////////////////////////////////////////////////////////
		void Create(const vxF32 a, const vxF32 b, const vxF32 c, const vxF32 d);

		//////////////////////////////////////////////////////////////
		/// Calculates the distance from the plane to the given
		/// point P.
		/// \param p The point to calculate the distance to.
		//////////////////////////////////////////////////////////////
		vxF32 DistanceTo(const Vector3f& p) const;
		//////////////////////////////////////////////////////////////
		/// Calculates the distance from this plane to the given plane
		/// if the two planes are parallel. Otherwise 0 will be
		/// returned as the two planes definitely intersect each other
		/// somewhere.
		/// \param p The other plane.
		//////////////////////////////////////////////////////////////
		vxF32 DistanceTo(const Plane& p) const;
		//////////////////////////////////////////////////////////////
		/// Checks whether the plane is parallel to the other plane
		/// given.
		/// \param p The other plane.
		//////////////////////////////////////////////////////////////
		vxBool ParallelTo(const Plane& p) const;
		//////////////////////////////////////////////////////////////
		/// Calculates the angle between this plane and the given one.
		/// The angle returned is given in radians and not in degrees.
		/// \param p The plane to calculate the angle to.
		//////////////////////////////////////////////////////////////
		vxF32 AngleTo(const Plane& p) const;
	};
}

#endif /* PLANE_HPP_ */
