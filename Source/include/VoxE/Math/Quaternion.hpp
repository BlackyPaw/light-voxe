/*
 * Quaternion.hpp
 *
 *  Created on: 25.07.2014
 *      Author: euaconlabs
 */

#ifndef QUATERNION_HPP_
#define QUATERNION_HPP_

#include <VoxE/Math/MathUtils.hpp>
#include <VoxE/Math/Matrix4.hpp>
#include <VoxE/Math/Trigonometry.hpp>
#include <VoxE/Native/Atomic.hpp>

#include <cmath>

namespace VX
{
	template<typename T>
	class TQuaternion
	{
	public:

		typedef T ValueType;

		//////////////////////////////////////////////////////////////
		// Internal components:
		struct
		{
			ValueType w;
			ValueType x;
			ValueType y;
			ValueType z;
		};

		//////////////////////////////////////////////////////////////
		// Set of constructors:

		TQuaternion() :
			w(1),
			x(0),
			y(0),
			z(0)
		{
		}

		TQuaternion(const ValueType& w, const ValueType& x, const ValueType& y, const ValueType& z) :
			w(w),
			x(x),
			y(y),
			z(z)
		{
		}

		//////////////////////////////////////////////////////////////
		/// Calculates the quaternion representing the same rotation
		/// as the given euler angles.
		//////////////////////////////////////////////////////////////
		TQuaternion(const Vector3<ValueType>& angles)
		{
			FromEulerAngles(angles);
		}

		//////////////////////////////////////////////////////////////
		/// Calculates the quaternion corresponding to a rotation
		/// about an angle a around the axis V.
		//////////////////////////////////////////////////////////////
		static TQuaternion Rotate(const ValueType& a, const Vector3<ValueType>& V)
		{
#if defined(MATH_USE_RADIANS)
			const ValueType radians = a * 0.5f;
#else
			const ValueType radians = Deg2Rad(a) * 0.5f;
#endif

			// Normalize the axis (IMPORTANT!):
			Vector3<ValueType> axis = Normalize(V);

			const ValueType s = std::sin(radians);
			return TQuaternion(std::cos(radians), axis.x * s, axis.y * s, axis.z * s).Normalize();
		}

		//////////////////////////////////////////////////////////////
		/// Rotates the given quaternion about the angle a around
		/// the axis V.
		//////////////////////////////////////////////////////////////
		static TQuaternion Rotate(const TQuaternion& p, const ValueType& a, const Vector3<ValueType>& V)
		{
#if defined(MATH_USE_RADIANS)
			const ValueType radians = a * 0.5f;
#else
			const ValueType radians = Deg2Rad(a) * 0.5f;
#endif

			// Normalize the axis (IMPORTANT!):
			Vector3<ValueType> axis = V.Normalize();

			const ValueType s = std::sin(radians);
			return (p * TQuaternion(std::cos(radians), axis.x * s, axis.y * s, axis.z * s)).Normalize();
		}

		//////////////////////////////////////////////////////////////
		/// Calculates the quaternion given its euler angles
		/// representation.
		//////////////////////////////////////////////////////////////
		TQuaternion& FromEulerAngles(const Vector3<ValueType>& angles)
		{
			ValueType factor(0.5);

			ValueType sx = std::sin(Rad2Deg(angles.x) * factor);
			ValueType sy = std::sin(Rad2Deg(angles.y) * factor);
			ValueType sz = std::sin(Rad2Deg(angles.z) * factor);

			ValueType cx = std::cos(Rad2Deg(angles.x) * factor);
			ValueType cy = std::cos(Rad2Deg(angles.y) * factor);
			ValueType cz = std::cos(Rad2Deg(angles.z) * factor);

			w = cx * cy * cz - sx * sy * sz;
			x = sx * sy * cz + cx * cy * sz;
			y = sx * cy * cz + cx * sy * sz;
			z = cx * sy * cz - sx * cy * sz;

			return Normalize();
		}

		//////////////////////////////////////////////////////////////
		/// Normalizes the quaternion.
		//////////////////////////////////////////////////////////////
		TQuaternion& Normalize()
		{
			vxF32 magOverOne = ValueType(1) / std::sqrt(w * w + x * x + y * y + z * z);
			w *= magOverOne;
			x *= magOverOne;
			y *= magOverOne;
			z *= magOverOne;
			return *this;
		}

		//////////////////////////////////////////////////////////////
		/// Calculates the conjugate of the quaternion.
		//////////////////////////////////////////////////////////////
		TQuaternion Conjugate() const
		{
			return TQuaternion<ValueType>(w, -x, -y, -z);
		}

		//////////////////////////////////////////////////////////////
		/// Calculates the dot product of this quaternion and the
		/// given one.
		//////////////////////////////////////////////////////////////
		ValueType Dot(const TQuaternion& q) const
		{
			return (w * q.w + x * q.x + x * q.x + z * q.z);
		}

		//////////////////////////////////////////////////////////////
		/// Calculates the inverse of the quaternion.
		//////////////////////////////////////////////////////////////
		TQuaternion Inverse() const
		{
			return Conjugate() / Dot(*this);
		}

		//////////////////////////////////////////////////////////////
		/// Calculates the euler angles representing the same
		/// rotation as the quaternion does.
		//////////////////////////////////////////////////////////////
		Vector3<ValueType> EulerAngles() const
		{
			Vector3<ValueType> angles;
			ValueType test = x*y + z*w;
			if(test > ValueType(0.499))
			{
				// North pole singularity:
				angles.x = Rad2Deg(2 * std::atan2(x, w));
				angles.y = Rad2Deg(vxPI * ValueType(0.5));
				angles.z = Rad2Deg(ValueType(0));
				return angles;
			}
			else if(test < ValueType(-0.499))
			{
				// South pole singularity:
				angles.x = Rad2Deg(-2 * std::atan2(x, w));
				angles.y = Rad2Deg(-vxPI * ValueType(0.5));
				angles.z = Rad2Deg(ValueType(0));
				return angles;
			}

			ValueType sqx = x*x;
			ValueType sqy = y*y;
			ValueType sqz = z*z;
			ValueType sqw = w*w;

			angles.x = Rad2Deg(std::atan2(ValueType(2) * (y * z + x * w), sqw - sqx - sqy + sqz));
			angles.y = Rad2Deg(std::asin (ValueType(-2) * (x * z - y * w)));
			angles.z = Rad2Deg(std::atan2(ValueType(2) * (x * y + z * w), sqw + sqx - sqy - sqz));
			return angles;
		}

		//////////////////////////////////////////////////////////////
		/// Converts the quaternion to a matrix.
		//////////////////////////////////////////////////////////////
		TMatrix4<ValueType> ToMatrix4() const
		{
			TMatrix4<ValueType> r;
			r[0][0] = 1 - 2 * y*y - 2 * z*z;
			r[0][1] = 2 * x * y - 2 * z * w;
			r[0][2] = 2 * x * z + 2 * y * w;
			r[0][3] = ValueType(0);
			r[1][0] = 2 * x * y + 2 * z * w;
			r[1][1] = 1 - 2 * x*x - 2 * z*z;
			r[1][2] = 2 * y * z - 2 * x * w;
			r[1][3] = ValueType(0);
			r[2][0] = 2 * x * z - 2 * y * w;
			r[2][1] = 2 * y * z + 2 * x * w;
			r[2][2] = 1 - 2 * x*x - 2 * y*y;
			r[2][3] = ValueType(0);
			r[3][0] = ValueType(0);
			r[3][1] = ValueType(0);
			r[3][2] = ValueType(0);
			r[3][3] = ValueType(1);
			return r;
		}

		//////////////////////////////////////////////////////////////
		// Overloaded operators:
		TQuaternion& operator+= (const TQuaternion& q)
		{
			w += q.w;
			x += q.x;
			y += q.y;
			z += q.z;
			return *this;
		}

		TQuaternion& operator*= (const TQuaternion& q)
		{
			TQuaternion p = *this;

			w = p.w * q.w - p.x * q.x - p.y * q.y - p.z * q.z;
			x = p.w * q.x + p.x * q.w + p.y * q.z - p.z * q.y;
			y = p.w * q.y - p.x * q.z + p.y * q.w + p.z * q.x;
			z = p.w * q.z + p.x * q.y - p.y * q.x + p.z * q.w;

			return *this;
		}

		TQuaternion& operator*= (const ValueType& s)
		{
			w *= s;
			x *= s;
			y *= s;
			z *= s;
			return *this;
		}

		TQuaternion& operator/= (const ValueType& s)
		{
			w /= s;
			x /= s;
			y /= s;
			z /= s;
			return *this;
		}
	};

	template<typename T>
	TQuaternion<T> operator+ (const TQuaternion<T>& p, const TQuaternion<T>& q)
	{
		return TQuaternion<T>(p) += q;
	}

	template<typename T>
	TQuaternion<T> operator* (const TQuaternion<T>& p, const TQuaternion<T>& q)
	{
		return TQuaternion<T>(p) *= q;
	}

	template<typename T>
	Vector3<T> operator* (const TQuaternion<T>& p, const Vector3<T>& v)
	{
		// See: http://molecularmusings.wordpress.com/2013/05/24/a-faster-quaternion-vector-multiplication/
		Vector3<T> u = Vector3<T>(p.x, p.y, p.z);
		Vector3<T> t = T(2) * u.Cross(v);
		return v + p.w * t + u.Cross(t);
	}

	template<typename T>
	Vector3<T> operator* (const Vector3<T>& v, const TQuaternion<T>& q)
	{
		return q.Inverse() * v;
	}

	template<typename T>
	TQuaternion<T> operator* (const T& s, const TQuaternion<T>& q)
	{
		return TQuaternion<T>(q) *= s;
	}

	template<typename T>
	TQuaternion<T> operator* (const TQuaternion<T>& q, const T& s)
	{
		return TQuaternion<T>(q) *= s;
	}

	template<typename T>
	TQuaternion<T> operator/ (const TQuaternion<T>& p, const T& s)
	{
		return TQuaternion<T>(p) /= s;
	}

	typedef TQuaternion<vxF32> Quaternion;
}

#endif /* QUATERNION_HPP_ */
