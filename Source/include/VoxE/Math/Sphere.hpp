/*
 * Sphere.hpp
 *
 *  Created on: 29.07.2014
 *      Author: euaconlabs
 */

#ifndef SPHERE_HPP_
#define SPHERE_HPP_

#include <VoxE/Core/Vector.hpp>
#include <VoxE/Native/Atomic.hpp>

namespace VX
{
	//////////////////////////////////////////////////////////////
	/// \class Sphere
	/// \brief Represents a three-dimensional sphere.
	/// Used to represent spheres made up of a center point C
	/// and a radius r.
	//////////////////////////////////////////////////////////////
	class Sphere
	{
	public:

		Vector3f C;
		vxF32 r;

		//////////////////////////////////////////////////////////////
		/// Constructs a unit sphere centered at the origin with a
		/// radius of 1.
		//////////////////////////////////////////////////////////////
		Sphere();
		//////////////////////////////////////////////////////////////
		/// Constructs a sphere centered at C with the radius r.
		/// \param c The sphere's center point.
		/// \param r The sphere's radius.
		//////////////////////////////////////////////////////////////
		Sphere(const Vector3f& c, const vxF32 r);

		//////////////////////////////////////////////////////////////
		/// Tests whether the given point P lies inside the sphere or
		/// not.
		/// \param p The point to test.
		//////////////////////////////////////////////////////////////
		vxBool Inside(const Vector3f& p) const;
		//////////////////////////////////////////////////////////////
		/// Tests whether the given point P lies on the sphere's
		/// surface or not.
		/// \param p The point to test.
		//////////////////////////////////////////////////////////////
		vxBool OnSurface(const Vector3f& p) const;
		//////////////////////////////////////////////////////////////
		/// Tests whether the given point P lies either inside of the
		/// sphere or on its surface or neither of both.
		/// \param p The point to test.
		//////////////////////////////////////////////////////////////
		vxBool Intersects(const Vector3f& p) const;
	};
}

#endif /* SPHERE_HPP_ */
