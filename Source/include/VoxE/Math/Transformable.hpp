#ifndef TRANSFORM_HPP
#define TRANSFORM_HPP

#include <VoxE/Math/ITransformable.hpp>

namespace VX
{
	//////////////////////////////////////////////////////////////
	/// \class Transformable
	/// This class may be used to create positionable, rotatable,
	/// and scalable object in 3-dimensional space.
	//////////////////////////////////////////////////////////////
	class Transformable : public ITransformable
	{
	public:

		Transformable();

		void Translate(const float x, const float y, const float z);
		void Translate(const Vector3f& trans);
		void Rotate(const float pitch, const float yaw, const float roll);
		void Rotate(const Vector3f& rot);
		void Scale(const float x, const float y, const float z);
		void Scale(const Vector3f& scale);

		void SetPosition(const float x, const float y, const float z);
		void SetPosition(const Vector3f& pos);
		void SetRotation(const float pitch, const float yaw, const float roll);
		void SetRotation(const Vector3f& rot);
		void SetRotation(const Quaternion& rot);
		void SetScale(const float x, const float y, const float z);
		void SetScale(const Vector3f& scale);
		virtual void SetPivot(const vxF32 x, const vxF32 y, const vxF32 z);
		virtual void SetPivot(const Vector3f& pivot);

		const Vector3f& GetPosition() const;
		const Quaternion& GetRotation() const;
		const Vector3f GetEulerAngles() const;
		const Vector3f& GetScale() const;
		const Vector3f& GetPivot() const;

		FloatRect TransformRect(const FloatRect& rect) const;

		Transformable& CopyTransform(const ITransformable& transformable);
		const Matrix4& GetTransform() const;

	protected:

		Vector3f mPosition;
		Quaternion mRotation;
		Vector3f mScale;
		Vector3f mPivot;

		mutable Matrix4 mTransform;
		mutable bool mUpdate;
	};
}

#endif
