/*
 * Trigonometry.hpp
 *
 *  Created on: 25.07.2014
 *      Author: euaconlabs
 */

#ifndef TRIGONOMETRY_HPP_
#define TRIGONOMETRY_HPP_

#include <VoxE/Native/Atomic.hpp>

namespace VX
{
	extern vxF64 vxPI;

	vxF32 Deg2Rad(const vxF32 a);
	vxF32 Rad2Deg(const vxF32 a);
	vxF64 Deg2Rad(const vxF64 a);
	vxF64 Rad2Deg(const vxF64 a);
}

#endif /* TRIGONOMETRY_HPP_ */
