#ifndef MEM_BASE_HPP
#define MEM_BASE_HPP

#include <VoxE/Native/Atomic.hpp>
#include <new>

VX::vxU64 GetAllocMem();

void* operator new		(std::size_t size) throw(std::bad_alloc);
void  operator delete		(void* p) throw();

void* operator new[]		(std::size_t size) throw(std::bad_alloc);
void  operator delete[]	(void* p) throw();

#endif // MEM_BASE_HPP
