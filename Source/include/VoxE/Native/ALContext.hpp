/*
 * ALContext.hpp
 *
 *  Created on: 04.07.2014
 *      Author: euaconlabs
 */

#ifndef ALCONTEXT_HPP_
#define ALCONTEXT_HPP_

#include <VoxE/Native/Atomic.hpp>

namespace VX
{
	/////////////////////////////////////////////////////////////
	/// \class ALContext
	/// \brief Wrapper class around a basic OpenAL context.
	/// This class wraps around the functionality of an OpenAL
	/// context. It is self-reference counting so that it may be
	/// passed by reference.
	/////////////////////////////////////////////////////////////
	class ALContext
	{
	public:

		ALContext();
		ALContext(const ALContext& c);
		~ALContext();

		/////////////////////////////////////////////////////////////
		/// Creates an OpenAL context ready for use.
		/////////////////////////////////////////////////////////////
		void Create();

		/////////////////////////////////////////////////////////////
		/// Makes the context current when multiple contexts are used.
		/////////////////////////////////////////////////////////////
		void MakeCurrent();

		/////////////////////////////////////////////////////////////
		/// Checks whether this instance manages a valid OpenAL
		/// context or not.
		/////////////////////////////////////////////////////////////
		const vxBool IsValid() const;
		/////////////////////////////////////////////////////////////
		/// Destroys the context immediately thereby releasing the
		/// whole underlying OpenAL context.
		/////////////////////////////////////////////////////////////
		void Destroy();

		ALContext& operator= (const ALContext& c);

	private:

		void Copy(const ALContext& c);

		vxU32* mReferences;
		void* mDevice, *mContext;
	};
}

#endif /* ALCONTEXT_HPP_ */
