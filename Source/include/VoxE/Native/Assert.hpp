#ifndef ASSERT_HPP
#define ASSERT_HPP

#if defined(_MSC_VER)
#include <Windows.h>
#define DBG_BREAK() DebugBreak()
#else
#include <signal.h>
#define DBG_BREAK() raise(SIGINT)
#endif

namespace VX
{
#if defined(VOXE_DEBUG) && defined(VOXE_ENABLE_ASSERTIONS)

#define ASSERT(exp) \
	if (exp) {} \
	else \
	{ \
		DBG_BREAK(); \
	}

#else

#define ASSERT(exp)

#endif // VOXE_DEBUG
}

#endif // ASSERT_HPP