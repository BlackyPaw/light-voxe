#ifndef ATOMIC_HPP
#define ATOMIC_HPP

//////////////////////////////////////////////////////////////
/// This header defines all atomic data types used by the engine
/// in a platform or more specifically compiler independent manner.
//////////////////////////////////////////////////////////////

namespace VX
{
#if defined(_MSC_VER)

	// Integer types
	typedef signed __int8  vxI8;
	typedef signed __int16 vxI16;
	typedef signed __int32 vxI32;
	typedef signed __int64 vxI64;

	typedef unsigned __int8  vxU8;
	typedef unsigned __int16 vxU16;
	typedef unsigned __int32 vxU32;
	typedef unsigned __int64 vxU64;

	// Floating-point:
	typedef float  vxF32;
	typedef double vxF64;

	// Boolean
	typedef bool vxBool;
	
	// Void
	typedef void vxVoid;

	// Characters:
	typedef char  vxChar;
	typedef char* vxCharPtr;
	typedef const char* vxCCharPtr;

	typedef wchar_t* vxWCharPtr;
	typedef const wchar_t* vxCWCharPtr;

#elif defined(__GNUC__) || defined(__GNUG__)
	
	// Integer types
	typedef signed char	 	  vxI8;
	typedef signed short	 	  vxI16;
	typedef signed int		  vxI32;
	typedef signed long int    vxI64;

	typedef unsigned char	      vxU8;
	typedef unsigned short	  vxU16;
	typedef unsigned int	   	  vxU32;
	typedef unsigned long int  vxU64;

	// Floating-point:
	typedef float  vxF32;
	typedef double vxF64;

	// Boolean
	typedef bool vxBool;

	// Void
	typedef void vxVoid;

	// Characters:
	typedef char  vxChar;
	typedef char* vxCharPtr;
	typedef const char* vxCCharPtr;

	typedef wchar_t* vxWCharPtr;
	typedef const wchar_t* wcCWCharPtr;

#else

#error "Unsupported compiler. The engine does currently not have any atomic data types defined!"

#endif
}

#endif // ATOMIC_HPP
