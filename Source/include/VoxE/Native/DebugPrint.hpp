#ifndef DEBUG_PRINT_HPP
#define DEBUG_PRINT_HPP

#include <VoxE/Native/Atomic.hpp>
#include <cstdarg>

namespace VX
{
	#define DebugPrintExt(msg, ...) DebugPrint("[%s:%i] > "msg, __FILE__, __LINE__, __VA_ARGS__)
	//////////////////////////////////////////////////////////////
	/// Outputs the given string to the native debugging console.
	/// \param msg The string to output.
	//////////////////////////////////////////////////////////////
	void NativeDebugOutput(const vxChar* msg);
	//////////////////////////////////////////////////////////////
	/// Outputs a debug string to the native debuggging console.
	/// This function works just as the standard C/C++ printf()
	/// function. It takes a format input string and any number
	/// of arguments following. In order for the method to actually
	/// print something VOXE_DEBUG has to be defined.
	/// \param format The format input string to output.
	//////////////////////////////////////////////////////////////
	void DebugPrint(const vxChar* format, ...);
}

#endif // DEBUG_PRINT_HPP
