#ifndef DELEGATE_THREAD_HPP
#define DELEGATE_THREAD_HPP

#include <VoxE/Core/Delegate.hpp>
#include <VoxE/Native/Thread.hpp>

namespace VX
{
	typedef Delegate0<vxU32> Runnable;

	//////////////////////////////////////////////////////////////
	/// \class DelegateThread
	/// This class wraps the thread class around in order to
	/// provide a way of inserting delegates as being the Run(..)
	/// function of a thread without having to derive the Thread
	/// class first.
	/// \see Thread
	//////////////////////////////////////////////////////////////
	class DelegateThread : public Thread
	{
	public:

		DelegateThread();
		DelegateThread(const Runnable& runnable);

		//////////////////////////////////////////////////////////////
		/// Sets the runnable executed when the thread is started.
		/// Setting the runnable after the thread was already started
		/// can leave the thread in an error prone state under some
		/// circumstances.
		//////////////////////////////////////////////////////////////
		void SetRunnable(const Runnable& runnable);

		vxU32 Run();

	private:

		Runnable mRunnable;
	};
}

#endif // DELEGATE_THREAD_HPP