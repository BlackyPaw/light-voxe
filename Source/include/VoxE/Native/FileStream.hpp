#ifndef FILE_STREAM_HPP
#define FILE_STREAM_HPP

#include <VoxE/Native/File.hpp>
#include <VoxE/Native/Stream.hpp>

namespace VX
{
	//////////////////////////////////////////////////////////////
	/// \class FileInputStream
	/// Stream class for reading the data of a file.
	//////////////////////////////////////////////////////////////
	class FileInputStream : public InputStream
	{
	public:

		FileInputStream();
		FileInputStream(const File& file);
		FileInputStream(const FileInputStream& in);
		virtual ~FileInputStream();

		//////////////////////////////////////////////////////////////
		/// Opens a file input stream to the given file.
		/// \param file The file to open.
		//////////////////////////////////////////////////////////////
		virtual vxBool Open(const File& file);
		virtual vxU32 Read(const vxU32 len, void* buffer);
		virtual vxU32 GetCur() const;
		virtual void Seek(const StreamSeekDir& dir, const vxI32 off = 0);
		virtual void Close();
		virtual vxBool EndOf() const;
		//////////////////////////////////////////////////////////////
		/// Checks whether the stream has been opened to a file or not.
		//////////////////////////////////////////////////////////////
		virtual vxBool IsOpen() const;

		FileInputStream& operator= (const FileInputStream& in);

	private:

		void Copy(const FileInputStream& in);
		void Destroy();

		vxU32* mReferences;
		void* mFile;
	};

	//////////////////////////////////////////////////////////////
	/// \class FileOutputStream
	/// Stream class for writing data to a file.
	//////////////////////////////////////////////////////////////
	class FileOutputStream : public OutputStream
	{
	public:

		FileOutputStream();
		FileOutputStream(const File& file);
		FileOutputStream(const FileOutputStream& out);
		virtual ~FileOutputStream();

		//////////////////////////////////////////////////////////////
		/// Opens a file output stream to the given file.
		/// \param file The file to open.
		//////////////////////////////////////////////////////////////
		virtual vxBool Open(const File& file);
		virtual vxU32 Write(const vxU32 len, const void* buffer);
		virtual vxU32 GetCur() const;
		virtual void Seek(const StreamSeekDir& dir, const vxI32 off = 0);
		virtual void Close();
		//////////////////////////////////////////////////////////////
		/// Checks whether the stream has been opened to a file or not.
		//////////////////////////////////////////////////////////////
		virtual vxBool IsOpen() const;

		FileOutputStream& operator= (const FileOutputStream& out);

	private:

		void Copy(const FileOutputStream& out);
		void Destroy();

		vxU32* mReferences;
		void* mFile;
	};
}

#endif // FILE_STREAM_HPP