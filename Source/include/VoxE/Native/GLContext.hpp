#ifndef GL_CONTEXT_HPP
#define GL_CONTEXT_HPP

#include <VoxE/Native/Atomic.hpp>
#include <VoxE/Native/GLContextAttributes.hpp>
#include <VoxE/Native/Platform.hpp>

#if defined(VOXE_PLATFORM_WINDOWS)
struct HDC__;
struct HGLRC__;
namespace VX
{
	typedef HDC__* DeviceHandle;
	typedef HGLRC__* ContextHandle;
	typedef const char* (__stdcall *GetExtensionsStringFunc)(DeviceHandle device);
	typedef vxBool(__stdcall *SwapIntervalExtFunc)(int interval);
	typedef HGLRC__* (__stdcall *CreateContextAttribsArbFunc)(HDC__* hDC, HGLRC__* hShareContext, const int* attributes);
}
#elif defined(VOXE_PLATFORM_UNIX)
struct _XDisplay;
struct __GLXcontextRec;
namespace VX
{
	typedef __GLXcontextRec* ContextHandle; typedef _XDisplay* DisplayHandle; typedef vxU64 DeviceHandle;
	typedef void (*SwapIntervalExtFunc)(DisplayHandle dpy, vxU64 drawable, vxI32 interval);
}
#endif

namespace VX
{
	/* DEPRECATED */
	/*
	 * As of the implementation of the Linux port, only the new context creation scheme is allowed.
	 * It states that the context is created together with the window and that its attributes are
	 * to be defined through passing a structure which contains all of the wished settings.
	 * The reason for this massive code change is simplicity for one thing but also a kinda annoying
	 * fact about the X11 window system: The visuals needed by the context to be set on the according
	 * window must be set on the window's creation. This prevents us from creating a context AFTER the
	 * window has already been created. At least the visual has to be known beforehand and in order to
	 * be able to do so, the settings have to be known in advance. Also the array settings scheme has proven
	 * to be misleading and unsupportable on Linux and at least annoying on Windows.
	 */

	 /*
	//////////////////////////////////////////////////////////////
	/// Specifies the number of bits used in the color buffer.
	/// It is followed by another integer which specifies the
	/// actual number respectively.
	//////////////////////////////////////////////////////////////
	static const vxU32 GL_CONTEXT_COLOR_BITS		= 1;
	//////////////////////////////////////////////////////////////
	/// Specifies the number of bits used in the depth buffer.
	/// It is followed by another integer which specifies the
	/// actual number respectively.
	//////////////////////////////////////////////////////////////
	static const vxU32 GL_CONTEXT_DEPTH_BITS		= 2;
	//////////////////////////////////////////////////////////////
	/// Specifies the number of bits used in the stencil buffer.
	/// It is followed by another integer which specifies the
	/// actual number respectively.
	//////////////////////////////////////////////////////////////
	static const vxU32 GL_CONTEXT_STENCIL_BITS		= 4;
	//////////////////////////////////////////////////////////////
	/// Tells the context that it should be created with a front
	/// and a back buffer.
	//////////////////////////////////////////////////////////////
	static const vxU32 GL_CONTEXT_DOUBLEBUFFERED	= 8;

	//////////////////////////////////////////////////////////////
	/// This array may be passed to the GLContext's Create()
	/// function in order to create a context with the settings
	/// proposed by default.
	//////////////////////////////////////////////////////////////
	static const vxU32 kDefaultGLContextAttribs[] = { GL_CONTEXT_COLOR_BITS, 32, GL_CONTEXT_DEPTH_BITS, 24, GL_CONTEXT_STENCIL_BITS, 8, GL_CONTEXT_DOUBLEBUFFERED, 0 };
	*/

	//////////////////////////////////////////////////////////////
	/// \class GLContext
	/// This class provides the functionality of a basic OpenGL
	/// context that allows for accessing common native functions
	/// in a platform independent manner.
	//////////////////////////////////////////////////////////////
	class GLContext
	{
	public:

		GLContext();
		GLContext(const GLContext& context);
		~GLContext();

#if defined(VOXE_PLATFORM_UNIX)
		static void* FindBestVisual(DisplayHandle dpy, const GLContextAttributes& attribs);
#endif

		//////////////////////////////////////////////////////////////
		/// Creates the context on the given device using the given
		/// context attributes. The attribs parameter is a 0-terminated
		/// array consisting of values specifying the attributes the
		/// context should be created with.
		/// The function returns true on success or false on failure.
		//////////////////////////////////////////////////////////////
		vxBool Create(const DeviceHandle& dev, const GLContextAttributes& attribs);

		//////////////////////////////////////////////////////////////
		/// Clears all contents of the currently bound framebuffer.
		//////////////////////////////////////////////////////////////
		void FullClear();
		//////////////////////////////////////////////////////////////
		/// Performs a native buffer swap between the back, front and
		/// possible the middle buffer, too.
		//////////////////////////////////////////////////////////////
		void SwapBuffers();
		//////////////////////////////////////////////////////////////
		/// Makes the context current on the device it has been created
		/// on.
		//////////////////////////////////////////////////////////////
		void MakeCurrent();
		//////////////////////////////////////////////////////////////
		/// Enables or disables the vertical synchronisation if the
		/// respective extension is found and available.
		/// \param vsync
		//////////////////////////////////////////////////////////////
		void EnableVSync(const vxBool vsync = true);
		//////////////////////////////////////////////////////////////
		/// Checks whether the context has been brought to a valid
		/// state using the Create(...) function or not.
		/// \see Create()
		//////////////////////////////////////////////////////////////
		vxBool IsValid() const;

		GLContext& operator= (const GLContext& context);

	private:

		void Copy(const GLContext& context);
		void Destroy();

#if defined(VOXE_PLATFORM_UNIX)
		DisplayHandle mDisplay;
		vxI32 mScreen;
#endif

		vxU32* mReferences;
		DeviceHandle mDevice;
		ContextHandle mContext;
		SwapIntervalExtFunc mSwapInterval;
	};
}

#endif // GL_CONTEXT_HPP
