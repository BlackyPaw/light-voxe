#ifndef GLCONTEXTATTRIBUTES_HPP
#define GLCONTEXTATTRIBUTES_HPP

#include <VoxE/Native/Atomic.hpp>

namespace VX
{
	struct GLContextAttributes
	{
		vxI32 mMajorVersion;
		vxI32 mMinorVersion;

		vxU32 mColorBits;
		vxU32 mDepthBits;
		vxU32 mStencilBits;

		vxBool mDoublebuffer;
		vxBool mDebugContext;
		vxBool mCompatibility;
	};
}

#endif // GLCONTEXTATTRIBUTES_HPP
