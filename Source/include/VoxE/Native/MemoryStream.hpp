#ifndef MEMORY_STREAM_HPP
#define MEMORY_STREAM_HPP

#include <VoxE/Native/Stream.hpp>

namespace VX
{
	//////////////////////////////////////////////////////////////
	/// \enum MemoryStreamMode
	/// Describes a mode to apply to a memory buffer given to a
	/// memory stream. Multiple values may be OR-ed together using
	/// the bitwise OR (|) operator.
	//////////////////////////////////////////////////////////////
	enum MemoryStreamMode
	{
		//////////////////////////////////////////////////////////////
		/// Tells the stream not to discard the memory buffer after use.
		//////////////////////////////////////////////////////////////
		MEMORY_STREAM_NO_DISCARD = 1
	};

	//////////////////////////////////////////////////////////////
	/// \class MemoryInputStream
	/// This input stream class allows for "streaming" data which
	/// is already present in the memory.
	//////////////////////////////////////////////////////////////
	class MemoryInputStream : public InputStream
	{
	public:

		MemoryInputStream();
		MemoryInputStream(const MemoryInputStream& in);
		virtual ~MemoryInputStream();

		//////////////////////////////////////////////////////////////
		/// Opens a stream to an in-memory resource.
		/// \param buffer The buffer to read the data from.
		/// \param size The number of bytes to read from the buffer.
		/// \param mode The memory mode to apply to the given buffer.
		//////////////////////////////////////////////////////////////
		virtual vxBool Open(const void* buffer, const vxU32 size, const vxU32 mode = MEMORY_STREAM_NO_DISCARD);
		virtual vxU32 Read(const vxU32 len, void* buffer);
		virtual vxU32 GetCur() const;
		virtual void Seek(const StreamSeekDir& dir, const vxI32 off = 0);
		virtual void Close();
		virtual vxBool EndOf() const;

		MemoryInputStream& operator= (const MemoryInputStream& in);

	private:

		void Copy(const MemoryInputStream& in);
		void Destroy();

		vxU32* mReferences;
		vxU32 mMode;
		const void* mBuffer;
		vxU32 mSize;
		vxU32 mCur;
	};
}

#endif // MEMORY_STREAM_HPP
