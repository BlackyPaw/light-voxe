#ifndef MUTEX_HPP
#define MUTEX_HPP

#include <VoxE/Native/Atomic.hpp>
#include <VoxE/Native/Platform.hpp>

#if defined(VOXE_PLATFORM_WINDOWS)
namespace VX { typedef void* MutexHandle; }
#elif defined(VOXE_PLATFORM_UNIX)
#include <pthread.h>
namespace VX { typedef pthread_mutex_t MutexHandle; }
#endif

namespace VX
{
	//////////////////////////////////////////////////////////////
	/// \class Mutex
	/// Simple mutex class used for safely accessing data in a
	/// multithreaded environment.
	//////////////////////////////////////////////////////////////
	class Mutex
	{
	public:

		Mutex();
#if defined(VOXE_PLATFORM_UNIX)
		Mutex(const Mutex& m);
		Mutex& operator= (const Mutex& m);
		void Copy(const Mutex& m);
		void Destroy();
#endif
		~Mutex();

		//////////////////////////////////////////////////////////////
		/// Waits until the mutex could successfully be locked.
		/// Therefore it performs a blocking call.
		//////////////////////////////////////////////////////////////
		void Lock();
		//////////////////////////////////////////////////////////////
		/// Tries to lock the mutex and returns true on success (or
		/// false on failure). It performs a non-blocking call and
		/// therefore returns immediately.
		/// \remark If the function returns true the mutex lock was
		/// acquired. Please do NOT forget to release the mutex then
		/// but not if the function returned false.
		//////////////////////////////////////////////////////////////
		vxBool TryLock();
		//////////////////////////////////////////////////////////////
		/// Releases the mutex lock.
		//////////////////////////////////////////////////////////////
		void Release();

	private:

#if defined(VOXE_PLATFORM_UNIX)
		vxU32* mReferences;
#endif
		MutexHandle mHandle;
	};
}

#endif // MUTEX_HPP
