#ifndef PLATFORM_HPP
#define PLATFORM_HPP

//////////////////////////////////////////////////////////////
/// This header is supposed to detect the OS of the platform
/// the engine is built for.
//////////////////////////////////////////////////////////////

namespace VX
{
#if defined(_WIN32) || defined(_WIN64)
#define VOXE_PLATFORM_WINDOWS
#elif defined(__linux__)
#define VOXE_PLATFORM_UNIX
#endif
}

#endif // PLATFORM_HPP