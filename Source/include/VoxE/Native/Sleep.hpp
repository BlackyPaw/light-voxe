#ifndef SLEEP_HPP
#define SLEEP_HPP

#include <VoxE/Native/Atomic.hpp>

namespace VX
{
	//////////////////////////////////////////////////////////////
	/// Puts the calling thread for the given amount of milliseconds
	/// to sleep. There may be variations of the actual time the
	/// thread sleeps due to the ticks of the clock of the OS.
	//////////////////////////////////////////////////////////////
	void Sleep(const vxU32 milliseconds);
}

#endif // SLEEP_HPP