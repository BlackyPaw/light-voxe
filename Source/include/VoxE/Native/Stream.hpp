#ifndef STREAM_HPP
#define STREAM_HPP

#include <VoxE/Native/Atomic.hpp>
#include <memory>

namespace VX
{
	//////////////////////////////////////////////////////////////
	/// \enum StreamSeekDir
	/// Describes the direction to seek inside a stream.
	//////////////////////////////////////////////////////////////
	enum StreamSeekDir
	{
		STREAM_SEEK_BEGIN = 1,
		STREAM_SEEK_CUR = 2,
		STREAM_SEEK_END = 4
	};

	//////////////////////////////////////////////////////////////
	/// \class Stream
	/// The base class for all deriving stream types.
	//////////////////////////////////////////////////////////////
	class Stream
	{
	public:

		Stream();
		virtual ~Stream();

		//////////////////////////////////////////////////////////////
		/// Closes the stream.
		//////////////////////////////////////////////////////////////
		virtual void Close() = 0;
	};


	//////////////////////////////////////////////////////////////
	/// \class InputStream
	/// The base class for all deriving stream that provide some
	/// kind of input.
	//////////////////////////////////////////////////////////////
	class InputStream : public Stream
	{
	public:

		InputStream();
		virtual ~InputStream();

		//////////////////////////////////////////////////////////////
		/// Reads the given number of bytes from the stream into the
		/// given buffer and returns the number of bytes actually
		/// read.
		/// \param len The number of bytes to read at maximum.
		/// \param buffer The buffer to write the read bytes into.
		/// \returns The number of bytes actually read from the stream.
		//////////////////////////////////////////////////////////////
		virtual vxU32 Read(const vxU32 len, void* buffer) = 0;
		//////////////////////////////////////////////////////////////
		/// Returns the current position in the stream from the
		/// begin.
		//////////////////////////////////////////////////////////////
		virtual vxU32 GetCur() const = 0;
		//////////////////////////////////////////////////////////////
		/// Sets the stream marker to the given position. It is
		/// calculated by adding the given offset to the position
		/// described by the given stream seek direction.
		/// \param dir The stream seek direction.
		/// \param off The offset in bytes to add to the position described
		///		by the direction.
		//////////////////////////////////////////////////////////////
		virtual void Seek(const StreamSeekDir& dir, const vxI32 off = 0) = 0;
		//////////////////////////////////////////////////////////////
		/// Closes the stream.
		//////////////////////////////////////////////////////////////
		virtual void Close() = 0;
		//////////////////////////////////////////////////////////////
		/// Checks whether the stream has reached the 'end-of-file'
		/// marker meaning that there's no more input available (this
		/// means that the next call to Read() will return 0 as the
		/// number of read bytes).
		/// \see Read()
		//////////////////////////////////////////////////////////////
		virtual vxBool EndOf() const = 0;

		virtual vxU16 ReadLE16();
		virtual vxU32 ReadLE32();
		virtual vxU64 ReadLE64();

		virtual vxU16 ReadBE16();
		virtual vxU32 ReadBE32();
		virtual vxU64 ReadBE64();

	};

	class OutputStream : public Stream
	{
	public:

		OutputStream();
		virtual ~OutputStream();

		//////////////////////////////////////////////////////////////
		/// Writes / Copies the given number of bytes from the given
		/// buffer into the stream and returns the number of bytes
		/// actually written to the stream.
		/// \param len The number of bytes to write.
		/// \param buffer The buffer to copy the bytes from.
		/// \returns The number of bytes actually written into the stream.
		//////////////////////////////////////////////////////////////
		virtual vxU32 Write(const vxU32 len, const void* buffer) = 0;
		//////////////////////////////////////////////////////////////
		/// Returns the current position in the stream from the
		/// begin.
		//////////////////////////////////////////////////////////////
		virtual vxU32 GetCur() const = 0;
		//////////////////////////////////////////////////////////////
		/// Sets the stream marker to the given position. It is
		/// calculated by adding the given offset to the position
		/// described by the given stream seek direction.
		/// \param dir The stream seek direction.
		/// \param off The offset in bytes to add to the position described
		///		by the direction.
		//////////////////////////////////////////////////////////////
		virtual void Seek(const StreamSeekDir& dir, const vxI32 off = 0) = 0;
		//////////////////////////////////////////////////////////////
		/// Closes the stream.
		//////////////////////////////////////////////////////////////
		virtual void Close() = 0;

		virtual void WriteLE16(vxU16 c);
		virtual void WriteLE32(vxU32 c);
		virtual void WriteLE64(vxU64 c);

		virtual void WriteBE16(vxU16 c);
		virtual void WriteBE32(vxU32 c);
		virtual void WriteBE64(vxU64 c);
	};

	typedef std::shared_ptr<InputStream> InputStreamPtr;
	typedef std::shared_ptr<OutputStream> OutputStreamPtr;
}

#endif // STREAM_HPP