#ifndef THREAD_HPP
#define THREAD_HPP

#include <VoxE/Native/Atomic.hpp>
#include <VoxE/Native/Platform.hpp>

#if defined(VOXE_PLATFORM_WINDOWS)
namespace VX { typedef void* ThreadHandle; }
#elif defined(VOXE_PLATFORM_UNIX)
#include <pthread.h>
namespace VX { typedef vxU64 ThreadHandle; }
#endif

namespace VX
{
	static const vxU32 kInvalidReturnCode = -1;

	//////////////////////////////////////////////////////////////
	/// \class Thread
	/// This class provides an interface for creating custom
	/// threads on all supported platforms without the need of
	/// calling the native API's functions.
	//////////////////////////////////////////////////////////////
	class Thread
	{
#if defined(VOXE_PLATFORM_UNIX)
		friend void* UnixThreadEntryPoint(void* arg);
#endif
	public:

		Thread();
		virtual ~Thread();

		//////////////////////////////////////////////////////////////
		/// Starts the thread. Please note that this function must
		/// only be called once. A second call will have no effect.
		/// The only way to re-start a thread is to create another
		/// thread instance that possesses the same Run() function.
		//////////////////////////////////////////////////////////////
		void Start();
		//////////////////////////////////////////////////////////////
		/// Waits for the termination of the thread using a blocking
		/// call.
		//////////////////////////////////////////////////////////////
		void Wait();
		//////////////////////////////////////////////////////////////
		/// The Run() function of the thread usually is the same as
		/// the main thread's main(...) function. It does the work
		/// of the thread and should therefore be overwritten by derived
		/// classes. Please note not to call this function directly since
		/// otherwise the calling thread will have to wait until the function
		/// returns which might be never. A thread should always call
		/// Start() which then makes a call to Run().
		//////////////////////////////////////////////////////////////
		virtual vxU32 Run();
		//////////////////////////////////////////////////////////////
		/// Terminates the thread. Please note that this function
		/// should only be called in very rare cases since it can
		/// leave threads in an error prone state.
		//////////////////////////////////////////////////////////////
		void Terminate();
		//////////////////////////////////////////////////////////////
		/// Checks whether the thread is still running or not.
		//////////////////////////////////////////////////////////////
		vxBool IsRunning() const;
		//////////////////////////////////////////////////////////////
		/// Gets the exit code the thread returned if the thread
		/// is already finished or returns kInvalidReturnCode.
		/// \see kInvalidReturnCode
		//////////////////////////////////////////////////////////////
		vxU32 GetExitCode() const;

	private:

		ThreadHandle mHandle;
#if defined(VOXE_PLATFORM_UNIX)
		vxBool mIsRunning;
		mutable pthread_mutex_t mMutex;
#endif
	};
}

#endif // THREAD_HPP
