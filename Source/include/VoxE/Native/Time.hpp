#ifndef TIME_HPP
#define TIME_HPP

#include <VoxE/Native/Atomic.hpp>

namespace VX
{
	//////////////////////////////////////////////////////////////
	/// \class Time
	/// Simple wrapper class around the functionality of a
	/// high-resolution timer implemented using native querying
	/// functions.
	//////////////////////////////////////////////////////////////
	class Time
	{
	public:

		//////////////////////////////////////////////////////////////
		/// Default constructor. Initializes the time object to the
		/// given amount of microseconds.
		/// \param microseconds
		//////////////////////////////////////////////////////////////
		Time(const vxU64 microseconds = 0);

		static Time Now();

		vxU64 Microseconds() const;
		vxU32 Milliseconds() const;
		vxF64 Secondsd() const;
		vxF32 Seconds() const;

		Time& operator+= (const Time& t);
		Time& operator-= (const Time& t);
		Time& operator*= (const Time& t);
		Time& operator/= (const Time& t);

	private:

		vxU64 mMicroseconds;
	};

	Time operator+ (const Time& a, const Time& b);
	Time operator- (const Time& a, const Time& b);
	Time operator* (const Time& a, const Time& b);
	Time operator/ (const Time& a, const Time& b);
}

#endif // TIMER_HPP