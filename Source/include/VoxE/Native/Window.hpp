#ifndef WINDOW_HPP
#define WINDOW_HPP

#include <VoxE/Core/Vector.hpp>
#include <VoxE/Native/GLContext.hpp>
#include <VoxE/Native/Platform.hpp>
#include <VoxE/Native/WindowIcon.hpp>

#include <string>

#if defined(VOXE_PLATFORM_WINDOWS)
#include <windows.h>
namespace VX { typedef HWND__* WindowHandle; typedef HDC__* DeviceHandle; }
#elif defined(VOXE_PLATFORM_UNIX)
struct _XDisplay;
namespace VX { typedef _XDisplay* DisplayHandle; typedef vxU64 AtomHandle, CursorHandle, WindowHandle; }
#endif

namespace VX
{
	class GLContext;
	struct GLContextAttributes;

	//////////////////////////////////////////////////////////////
	/// \class Window
	/// This class allows for creating and managing windows on
	/// all supported platforms. These windows may then be used
	/// to display the actual game using the graphics wrapper(s).
	//////////////////////////////////////////////////////////////
	class Window
	{
	public:

		Window();
		Window(const Window& window);
		~Window();

		void Create(const GLContextAttributes& attribs);
		//////////////////////////////////////////////////////////////
		/// Updates the window and processes all native messages.
		//////////////////////////////////////////////////////////////
		void Update();
		//////////////////////////////////////////////////////////////
		/// Shows or hides the window.
		/// \param show
		//////////////////////////////////////////////////////////////
		void Show(const vxBool show = true);
		//////////////////////////////////////////////////////////////
		/// Gets the position of the upper-left corner of the window.
		//////////////////////////////////////////////////////////////
		Vector2i GetPosition() const;
		//////////////////////////////////////////////////////////////
		/// Gets the size of the window.
		//////////////////////////////////////////////////////////////
		Vector2ui GetSize() const;
		//////////////////////////////////////////////////////////////
		/// Sets the position of the upper-left corner of the window.
		/// \param pos
		//////////////////////////////////////////////////////////////
		void SetPosition(const Vector2i& pos);
		//////////////////////////////////////////////////////////////
		/// Sets the size of the window.
		/// \param size
		//////////////////////////////////////////////////////////////
		void SetSize(const Vector2ui& size);
		//////////////////////////////////////////////////////////////
		/// Sets the icon to display in the window's title bar.
		/// \param ico The icon to display.
		//////////////////////////////////////////////////////////////
		void SetIcon(const WindowIcon& ico);

		//////////////////////////////////////////////////////////////
		/// Sets the icon to display in the window's title bar.
		/// \param width The width of the icon in pixels.
		/// \param height The height of the icon in pixels.
		/// \param The pixel data in RGBA, unsigned 8-bit integer format.
		//////////////////////////////////////////////////////////////
		void SetIcon(const vxU32 width, const vxU32 height, const vxU8* pixels);
		//////////////////////////////////////////////////////////////
		/// Sets the window's title.
		/// \param title
		//////////////////////////////////////////////////////////////
		void SetTitle(const std::string& title);
		//////////////////////////////////////////////////////////////
		/// Shows or hides the cursor depending on the given value.
		/// \param show
		//////////////////////////////////////////////////////////////
		void ShowCursor(const vxBool show = true);
		//////////////////////////////////////////////////////////////
		/// Checks whether the window is still opened (was not closed
		/// up to now) or not.
		//////////////////////////////////////////////////////////////
		vxBool IsOpen() const;
		//////////////////////////////////////////////////////////////
		/// Gets the OpenGL rendering context associated with this
		/// window.
		//////////////////////////////////////////////////////////////
		const GLContext& GetGLContext() const;
		/////////////////////////////////////////////////////////////
		/// Closes the window.
		/////////////////////////////////////////////////////////////
		void Close();
		//////////////////////////////////////////////////////////////
		/// Makes the window fullscreen using the given fullscreen
		/// description. Returns true on success or false on failure.
		/// \param desc
		//////////////////////////////////////////////////////////////
		//vxBool SetFullscreen(const FullscreenDesc& desc);

		//////////////////////////////////////////////////////////////
		/// Makes the window windowed.
		//////////////////////////////////////////////////////////////
		//void SetWindowed();

		Window& operator= (const Window& window);

	private:

#if defined(VOXE_PLATFORM_WINDOWS)
		static LRESULT CALLBACK WindowProc(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam);
		static void RegisterWindowClass();
#endif

		void Destroy();
		void Copy(const Window& window);

	private:

		Vector2ui mLastSize;
#if defined(VOXE_PLATFORM_WINDOWS)
		HICON mIcon;
		vxBool mIsSizing;
		DeviceHandle mDevice;
#elif defined(VOXE_PLATFORM_UNIX)
		DisplayHandle mDisplay;
		vxI32 mScreen;
		AtomHandle mCloseAtom;
		CursorHandle mCursor;
#endif

		vxU32* mReferences;
		WindowHandle mHandle;
		vxBool mKeys[0xFF];
		vxBool mIsOpen;
		GLContext mContext;
	};
}

#endif // WINDOW_HPP
