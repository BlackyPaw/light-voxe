#ifndef WINDOW_ICON_HPP
#define WINDOW_ICON_HPP

#include <VoxE/Native/File.hpp>

namespace VX
{
	//////////////////////////////////////////////////////////////
	/// \class WindowIcon
	/// This class is able to load icons from files and is used
	/// to set a window's icon which is displayed in the window's
	/// title bar.
	/// It is self-reference counting.
	//////////////////////////////////////////////////////////////
	class WindowIcon
	{
		friend class Window;
	public:

		WindowIcon();
		WindowIcon(const File& f);
		WindowIcon(const WindowIcon& ico);
		~WindowIcon();

		//////////////////////////////////////////////////////////////
		/// Loads the icon from a file. Supported file formats are:
		/// PNG, JPG, TGA, DDS, and BMP.
		/// \param f The file to load the icon from.
		//////////////////////////////////////////////////////////////
		vxBool LoadFile(const File& f);

		WindowIcon& operator= (const WindowIcon& ico);

	private:

		void Copy(const WindowIcon& ico);
		void Destroy();

		vxU32* mReferences;
		vxU32 mWidth;
		vxU32 mHeight;
		vxU8* mPixels;
	};
}

#endif // WINDOW_ICON_HPP