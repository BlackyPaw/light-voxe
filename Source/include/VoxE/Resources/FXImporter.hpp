/*
 * FXImporter.hpp
 *
 *  Created on: 28.06.2014
 *      Author: euaconlabs
 */

#ifndef FXIMPORTER_HPP_
#define FXIMPORTER_HPP_

#include <VoxE/Graphics/ShaderProgram.hpp>
#include <VoxE/Native/File.hpp>

namespace VX
{
	//////////////////////////////////////////////////////////////
	/// \class FXImporter
	/// Imports FX file and compiles them into shader programs.
	//////////////////////////////////////////////////////////////
	class FXImporter
	{
	public:

		FXImporter();
		~FXImporter();

		//////////////////////////////////////////////////////////////
		/// Loads a FX file.
		//////////////////////////////////////////////////////////////
		vxBool LoadFile(const File& file);
		//////////////////////////////////////////////////////////////
		/// Gets a valid shader object if the file has been loaded
		/// successfully.
		//////////////////////////////////////////////////////////////
		const ShaderProgram& GetShaderProgram() const;

	private:

		void Preprocessor(vxString& contents);

		ShaderProgram mProgram;
		vxString mAllShaders;
	};
}

#endif /* FXIMPORTER_HPP_ */
