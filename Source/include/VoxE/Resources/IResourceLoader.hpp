/*
 * IResourceLoader.hpp
 *
 *  Created on: 14.06.2014
 *      Author: euaconlabs
 */

#ifndef IRESOURCELOADER_HPP_
#define IRESOURCELOADER_HPP_

#include <VoxE/Core/String.hpp>
#include <VoxE/Native/File.hpp>
#include <typeinfo>

namespace VX
{
	class IResourceLoader
	{
	public:

		IResourceLoader(const std::type_info* type) : mTypeInfo(type) {}
		virtual ~IResourceLoader() {}

		virtual vxBool LoadResource(const File& file, void** outDataPtr, vxU32* outSizeInBytesd) = 0;

		const std::type_info* GetTypeInfo() const { return mTypeInfo; }

	private:

		/* The return type of the resource loader. */
		const std::type_info* mTypeInfo;
	};
}

#endif /* IRESOURCELOADER_HPP_ */
