/*
 * AnimationLoader.hpp
 *
 *  Created on: 15.06.2014
 *      Author: euaconlabs
 */

#ifndef ANIMATIONLOADER_HPP_
#define ANIMATIONLOADER_HPP_

#include <VoxE/Resources/IResourceLoader.hpp>

namespace VX
{
	class AnimationLoader : public IResourceLoader
	{
	public:

		AnimationLoader();
		~AnimationLoader();

		vxBool LoadResource(const File& file, void** outDataPtr, vxU32* outSizeInBytes);
	};
}

#endif /* ANIMATIONLOADER_HPP_ */
