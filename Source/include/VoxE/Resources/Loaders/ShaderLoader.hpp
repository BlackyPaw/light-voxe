/*
 * ShaderLoader.hpp
 *
 *  Created on: 28.06.2014
 *      Author: euaconlabs
 */

#ifndef SHADERLOADER_HPP_
#define SHADERLOADER_HPP_

#include <VoxE/Resources/IResourceLoader.hpp>

namespace VX
{
	class ShaderLoader : public IResourceLoader
	{
	public:

		ShaderLoader();
		~ShaderLoader();

		vxBool LoadResource(const File& file, void** outDataPtr, vxU32* outSizeInBytes);
	};
}

#endif /* SHADERLOADER_HPP_ */
