/*
 * SoundLoader.hpp
 *
 *  Created on: 18.07.2014
 *      Author: euaconlabs
 */

#ifndef SOUNDLOADER_HPP_
#define SOUNDLOADER_HPP_

#include <VoxE/Resources/IResourceLoader.hpp>

namespace VX
{
	/////////////////////////////////////////////////////////////
	/// \class SoundLoader
	/// \brief Loads a given sound file and allocates an audio
	///		buffer for its PCM data.
	/////////////////////////////////////////////////////////////
	class SoundLoader : public IResourceLoader
	{
	public:

		SoundLoader();
		~SoundLoader();

		vxBool LoadResource(const File& file, void** outDataPtr, vxU32* outSizeInBytes);
	};
}

#endif /* SOUNDLOADER_HPP_ */
