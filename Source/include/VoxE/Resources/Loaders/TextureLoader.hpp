/*
 * TextureLoader.hpp
 *
 *  Created on: 14.06.2014
 *      Author: euaconlabs
 */

#ifndef TEXTURELOADER_HPP_
#define TEXTURELOADER_HPP_

#include <VoxE/Resources/IResourceLoader.hpp>

namespace VX
{
	class TextureLoader : public IResourceLoader
	{
	public:

		TextureLoader();
		~TextureLoader();

		vxBool LoadResource(const File& file, void** outDataPtr, vxU32* outSizeInBytes);
	};
}

#endif /* TEXTURELOADER_HPP_ */
