/*
 * ResourceHandle.hpp
 *
 *  Created on: 14.06.2014
 *      Author: euaconlabs
 */

#ifndef RESOURCEHANDLE_HPP_
#define RESOURCEHANDLE_HPP_

#include <VoxE/Resources/ResourceHandleBase.hpp>
#include <VoxE/Resources/ResourceManager.hpp>

namespace VX
{
	class ResourceManager;

	template<typename T>
	class ResourceHandle : public ResourceHandleBase
	{
	public:

		ResourceHandle() :
			ResourceHandleBase(&typeid(T))
		{
		}

		ResourceHandle(ResourceManager* manager, const ResourceID& id) :
			ResourceHandleBase(&typeid(T))
		{
			mResourceManager = manager;
			mResourceID		 = id;
		}

		virtual ~ResourceHandle()
		{
		}

		T* Get() { return Dereference(); }
		const T* Get() const { return Dereference(); }

		T* operator-> () { return Dereference(); }
		const T* operator-> () const { return Dereference(); }

		T* operator* () { return Dereference(); }
		const T* operator* () const { return Dereference(); }

	private:

		T* Dereference()
		{
			if(!IsValid())
				return nullptr;
			/* Dereference the handle here: */
			return mResourceManager->GetManagedResource<T>(mResourceID);
		}

		const T* Dereference() const
		{
			if(!IsValid())
				return nullptr;
			/* Dereference the handle here again: */
			return mResourceManager->GetManagedResource<T>(mResourceID);
		}
	};
}

#endif /* RESOURCEHANDLE_HPP_ */
