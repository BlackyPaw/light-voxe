/*
 * ResourceHandleBase.hpp
 *
 *  Created on: 14.06.2014
 *      Author: euaconlabs
 */

#ifndef RESOURCEHANDLEBASE_HPP_
#define RESOURCEHANDLEBASE_HPP_

#include <VoxE/Core/String.hpp>
#include <VoxE/Native/Atomic.hpp>
#include <typeinfo>

namespace VX
{
	class ResourceManager;

	typedef vxString ResourceID;
	static const ResourceID kInvalidResourceID = "<invalid>";

	class ResourceHandleBase
	{
	public:

		ResourceHandleBase(const std::type_info* type);
		virtual ~ResourceHandleBase();

		ResourceManager* GetResourceManager();
		const ResourceManager* GetResourceManager() const;

		const ResourceID& GetResourceID() const;
		const std::type_info* GetTypeInfo() const;

		vxBool IsValid() const;

		vxBool operator== (const ResourceHandleBase& h);
		vxBool operator!= (const ResourceHandleBase& h);

	protected:

		/* The resource manager the handle manages resources of. */
		ResourceManager* mResourceManager;
		/* The unique resource identifier describing the resource in cache. */
		ResourceID mResourceID;

	private:

		/* The type info of the cached resource for type-safety and lookup in the manager. */
		const std::type_info* mTypeInfo;
	};
}

#endif /* RESOURCEHANDLEBASE_HPP_ */
