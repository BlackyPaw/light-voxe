/*
 * ResourceManager.hpp
 *
 *  Created on: 14.06.2014
 *      Author: euaconlabs
 */

#ifndef RESOURCEMANAGER_HPP_
#define RESOURCEMANAGER_HPP_

#include <VoxE/Core/ISingleton.hpp>
#include <VoxE/Native/File.hpp>
#include <VoxE/Resources/IResourceLoader.hpp>
#include <VoxE/Resources/ResourceHandleBase.hpp>

#include <cstdio>
#include <map>
#include <vector>

namespace VX
{
	template<typename T> class ResourceHandle;

	class ResourceManager : public ISingleton<ResourceManager>
	{
		template<typename T> friend class ResourceHandle;
	public:

		ResourceManager();
		~ResourceManager();

		vxBool Initialize();

		template<typename T>
		ResourceHandle<T> LoadResource(const ResourceID& id)
		{
			// Make sure the resource is actually loaded into the cache:
			GetManagedResource<T>(id);
			return ResourceHandle<T>(this, id);
		}

	private:

		struct ManagedResourceBase
		{
		public:

			virtual ~ManagedResourceBase() {}

			vxU32 mSizeInBytes;
			void* mDataPtr;
		};

		template<typename T>
		struct ManagedResource : ManagedResourceBase
		{
		public:

			virtual ~ManagedResource()
			{
				delete reinterpret_cast<T*>(mDataPtr);
			}
		};

		typedef std::map<ResourceID, ManagedResourceBase*> ResourceCache;
		typedef std::map<const std::type_info*, ResourceCache> TypedCache;
		typedef std::vector<IResourceLoader*> ResourceLoaderList;
		typedef std::map<ResourceID, File> ResourceTable;

		template<typename T>
		T* GetManagedResource(const ResourceID& id)
		{
			T* findResource = FindResource<T>(id);
			if(findResource == nullptr)
				return LoadResourceIntoCache<T>(id);
			return findResource;
		}

		template<typename T>
		const T* GetManagedResource(const ResourceID& id) const
		{
			T* findResource = FindResource<T>(id);
			if(findResource == nullptr)
				return LoadResourceIntoCache<T>(id);
			return findResource;
		}

		template<typename T>
		T* FindResource(const ResourceID& id) const
		{
			TypedCache::const_iterator findCache = mCache.find(&typeid(T));
			if(findCache == mCache.end())
				return nullptr;
			const ResourceCache& cache = findCache->second;
			ResourceCache::const_iterator findResource = cache.find(id);
			if(findResource == cache.end())
				return nullptr;
			return reinterpret_cast<T*>(findResource->second->mDataPtr);
		}

		template<typename T>
		T* LoadResourceIntoCache(const ResourceID& id) const
		{
			ResourceTable::const_iterator findFile = mResourceTable.find(id);
			if(findFile == mResourceTable.end())
				return nullptr;

			const File& file = findFile->second;

			const std::type_info* type = &typeid(T);
			IResourceLoader* loader = nullptr;
			for(typename ResourceLoaderList::const_iterator it = mResourceLoaders.begin(); it != mResourceLoaders.end(); ++it)
			{
				if((*it)->GetTypeInfo() == type)
				{
					loader = (*it);
					break;
				}
			}

			if(loader == nullptr)
				return nullptr;

			void* dataPtr = nullptr;
			vxU32 sizeInBytes = 0;
			if(!loader->LoadResource(file, &dataPtr, &sizeInBytes))
				return nullptr;

			ManagedResource<T>* resource = new ManagedResource<T>();
			resource->mDataPtr = dataPtr;
			resource->mSizeInBytes = sizeInBytes;

			ResourceCache& cache = mCache[type];
			cache.insert(std::make_pair(id, resource));

			return reinterpret_cast<T*>(resource->mDataPtr);
		}

		mutable TypedCache mCache;
		mutable ResourceLoaderList mResourceLoaders;
		ResourceTable mResourceTable;
	};
}

#endif /* RESOURCEMANAGER_HPP_ */
