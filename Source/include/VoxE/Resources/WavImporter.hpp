/*
 * WavImporter.hpp
 *
 *  Created on: 18.07.2014
 *      Author: euaconlabs
 */

#ifndef WAVIMPORTER_HPP_
#define WAVIMPORTER_HPP_

#include <VoxE/Audio/AudioBuffer.hpp>
#include <VoxE/Native/Atomic.hpp>
#include <VoxE/Native/File.hpp>

namespace VX
{
	/////////////////////////////////////////////////////////////
	/// \class WavImporter
	/// \brief Reads a Wav file and post-processes its contents.
	/////////////////////////////////////////////////////////////
	class WavImporter
	{
	public:

		WavImporter();
		~WavImporter();

		/////////////////////////////////////////////////////////////
		/// Loads a Wav file.
		/////////////////////////////////////////////////////////////
		vxBool LoadFile(const File& f);
		/////////////////////////////////////////////////////////////
		/// Gets the audio buffer containing the raw PCM data if the
		/// file could be loaded correctly.
		/////////////////////////////////////////////////////////////
		const AudioBuffer& GetAudioBuffer() const;

	private:

		AudioBuffer mAudioBuffer;
	};
}

#endif /* WAVIMPORTER_HPP_ */
