/*
 * Camera.hpp
 *
 *  Created on: 12.06.2014
 *      Author: euaconlabs
 */

#ifndef CAMERA_HPP_
#define CAMERA_HPP_

#include <VoxE/Math/Transformable.hpp>

namespace VX
{
	class Camera : public Transformable
	{
	public:

		Camera();
		~Camera();

		void Orthographic(const vxF32 left, const vxF32 right, const vxF32 bottom, const vxF32 top, const vxF32 near, const vxF32 far);
		void Perspective(const vxF32 fovy, const vxF32 aspect, const vxF32 near, const vxF32 far);

		const Matrix4& GetProjectionMatrix() const;

	private:

		Matrix4 mProjection;
	};
}

#endif /* CAMERA_HPP_ */
