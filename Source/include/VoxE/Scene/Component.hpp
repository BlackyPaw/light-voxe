/*
 * Component.hpp
 *
 *  Created on: 03.07.2014
 *      Author: euaconlabs
 */

#ifndef COMPONENT_HPP_
#define COMPONENT_HPP_

#include <VoxE/Native/Atomic.hpp>

namespace VX
{
	class GameObject;

	/////////////////////////////////////////////////////////////
	/// \class Component
	/// \brief A basic interface for creating custom actor components.
	///	Any GameObject can possess one or more so called actor
	/// components. These are small sets of functionality which
	/// get updated or rendered everytime the actor gets so.
	/// They can implement any kind of functionality or can even
	/// connect the game object system with other engine subsystems
	/// like the Audio System [planned: Scripting System].
	/////////////////////////////////////////////////////////////
	class Component
	{
		friend class GameObject;
	public:

		Component();
		virtual ~Component();

		/////////////////////////////////////////////////////////////
		/// Updates the component based on the delta time passed
		/// since the last frame.
		/// \param delta The passed time.
		/////////////////////////////////////////////////////////////
		virtual void Update(const vxF32 delta);

		//////////////////////////////////////////////////////////////
		/// Gets the game object the component belongs to.
		//////////////////////////////////////////////////////////////
		GameObject* GetOwner() const;

	protected:

		//////////////////////////////////////////////////////////////
		/// As soon as the component is attached to a game object
		/// this member will point to the component's owner.
		//////////////////////////////////////////////////////////////
		GameObject* mOwner;
	};
}

#endif /* COMPONENT_HPP_ */
