/*
 * GameObject.hpp
 *
 *  Created on: 03.07.2014
 *      Author: euaconlabs
 */

#ifndef GAMEOBJECT_HPP_
#define GAMEOBJECT_HPP_

#include <VoxE/Native/Atomic.hpp>
#include <VoxE/Scene/Component.hpp>
#include <VoxE/Scene/SceneNode.hpp>

#include <vector>

namespace VX
{
	typedef vxU32 GameObjectID;

	/////////////////////////////////////////////////////////////
	/// \class GameObject
	/// \brief Basic entity in a game world.
	/// A game object is used to represent all kinds of entities
	/// in a game world. It may be updated and rendered depending
	/// on which components are attached to it.
	/////////////////////////////////////////////////////////////
	class GameObject
	{
	public:

		GameObject(SceneNode* node);
		~GameObject();

		/////////////////////////////////////////////////////////////
		/// Adds the given component to the actor's list of components.
		/// \param com The component to add.
		/////////////////////////////////////////////////////////////
		void AddComponent(Component* com);

		/////////////////////////////////////////////////////////////
		/// Sets whether this game object is currently active or not.
		/// An inactive game object will not be updated in an update.
		/////////////////////////////////////////////////////////////
		void SetActive(const vxBool active);
		/////////////////////////////////////////////////////////////
		/// Check whether the game object is active.
		/////////////////////////////////////////////////////////////
		const vxBool IsActive() const;

		/////////////////////////////////////////////////////////////
		/// Gets the scene node the game objects owns.
		/////////////////////////////////////////////////////////////
		SceneNode* GetSceneNode() const;

		/////////////////////////////////////////////////////////////
		/// Updates the game object and all components attached to
		/// it given the time passed since the last frame.
		/// \param delta The time passed since the last frame.
		/////////////////////////////////////////////////////////////
		void Update(const vxF32 delta);

	private:

		vxBool mActive;
		std::vector<Component*> mComponents;
		SceneNode* mSceneNode;
	};
}

#endif /* GAMEOBJECT_HPP_ */
