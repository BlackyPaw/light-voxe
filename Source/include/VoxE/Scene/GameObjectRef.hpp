/*
 * GameObjectRef.hpp
 *
 *  Created on: 24.07.2014
 *      Author: euaconlabs
 */

#ifndef GAMEOBJECTREF_HPP_
#define GAMEOBJECTREF_HPP_

#include <VoxE/Scene/ISceneNodeData.hpp>

namespace VX
{
	class GameObject;

	/////////////////////////////////////////////////////////////
	/// \class GameObjectRef
	/// \brief Used to indirectly attach a game object to a
	///		scene node by adding an extra layer between the
	///		game object and the scene node.
	/// This class takes the game object it should reference
	/// and calls the respective method on it when the respective
	/// callback is invoked.
	/////////////////////////////////////////////////////////////
	class GameObjectRef : public ISceneNodeData
	{
	public:

		GameObjectRef(GameObject* ref);
		virtual ~GameObjectRef();

		virtual void OnUpdate(const vxF32 delta);
		GameObject* GetReference() const;

	private:

		GameObject* mReference;
	};
}

#endif /* GAMEOBJECTREFERENCE_HPP_ */
