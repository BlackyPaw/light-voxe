/*
 * ISceneNodeData.hpp
 *
 *  Created on: 23.07.2014
 *      Author: euaconlabs
 */

#ifndef ISCENENODEDATA_HPP_
#define ISCENENODEDATA_HPP_

#include <VoxE/Native/Atomic.hpp>

namespace VX
{
	class SceneNode;

	/////////////////////////////////////////////////////////////
	/// \class ISceneNodeData
	/// \brief Interface which subclasses may implement to be used
	///		a scene node's data.
	/// Every scene node can have one data object associated with
	/// itself. This data provides several callbacks which it can
	/// implement in order to build enhanced functionalities and
	/// redirect function calls (as with GameObjects for example).
	/////////////////////////////////////////////////////////////
	class ISceneNodeData
	{
	public:

		virtual ~ISceneNodeData() {}

		/////////////////////////////////////////////////////////////
		/// Called whenever the scene node is updated.
		/////////////////////////////////////////////////////////////
		virtual void OnUpdate(const vxF32 delta) = 0;
	};
}

#endif /* SCENENODEATTACHMENT_HPP_ */
