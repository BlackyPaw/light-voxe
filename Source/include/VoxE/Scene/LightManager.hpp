/*
 * LightManager.hpp
 *
 *  Created on: 22.07.2014
 *      Author: euaconlabs
 */

#ifndef LIGHTMANAGER_HPP_
#define LIGHTMANAGER_HPP_

#include <VoxE/Graphics/LightSource.hpp>
#include <VoxE/Memory/PoolAllocator.hpp>

#include <vector>

namespace VX
{
	/////////////////////////////////////////////////////////////
	/// \class LightManager
	/// \brief This class manages all available light sources in
	///		a given environment.
	/// First of all please note that this class is not a singleton.
	/// This is because there might be multiple game worlds in
	/// memory at once (i.e. for streaming) which is why there
	/// could be multiple light managers. For this reason every
	/// game world has its own light manager.
	/// <br/>
	/// Light managers mostly fulfill the task of providing a list
	/// of all active light source's in the environment. Whenever
	/// a subsystem wants to create any type of light source it
	/// has to allocate one here which it might use then. When a
	/// light source is no longer to be used it must be freed
	/// using the manager as well.
	/////////////////////////////////////////////////////////////
	class LightManager
	{
	public:

		LightManager();
		~LightManager();

		/////////////////////////////////////////////////////////////
		/// Gets the maxmimum number of lights allowed.
		/////////////////////////////////////////////////////////////
		static vxU32 GetMaxLightSources();

		/////////////////////////////////////////////////////////////
		/// Tries to allocate a light source. If the limit of
		/// possible light sources is reached 0 will be returned.
		/////////////////////////////////////////////////////////////
		LightSource* AllocateSource();

		/////////////////////////////////////////////////////////////
		/// Frees a given light source so that it might be used
		/// for allocation again.
		/// \param source The light source to free.
		/////////////////////////////////////////////////////////////
		void FreeSource(LightSource* source);

		/////////////////////////////////////////////////////////////
		/// Gets the number of allocated light sources.
		/////////////////////////////////////////////////////////////
		vxU32 GetNumLights() const;

		/////////////////////////////////////////////////////////////
		/// Gets an array containing all allocated light sources.
		/////////////////////////////////////////////////////////////
		LightSource* const* GetLights() const;

	private:

		static const vxU32 kMaxLightSources = 32;

		std::vector<LightSource*> mAllocatedSources;
		PoolAllocatorT<LightSource> mLightPool;
	};
}

#endif /* LIGHTMANAGER_HPP_ */
