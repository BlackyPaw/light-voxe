/*
 * MatrixStack.hpp
 *
 *  Created on: 12.06.2014
 *      Author: euaconlabs
 */

#ifndef MATRIXSTACK_HPP_
#define MATRIXSTACK_HPP_

#include <VoxE/Math/Matrix4.hpp>
#include <vector>

namespace VX
{
	class MatrixStack
	{
	public:

		void Push();
		void Pop();

		Matrix4& Top();
		const Matrix4& Top() const;

		void Clear();

	private:

		std::vector<Matrix4> mStack;
		Matrix4 mTop;
	};
}

#endif /* MATRIXSTACK_HPP_ */
