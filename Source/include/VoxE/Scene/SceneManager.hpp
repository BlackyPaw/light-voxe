/*
 * SceneManager.hpp
 *
 *  Created on: 23.07.2014
 *      Author: euaconlabs
 */

#ifndef SCENEMANAGER_HPP_
#define SCENEMANAGER_HPP_

#include <VoxE/Scene/SceneNode.hpp>

namespace VX
{
	/////////////////////////////////////////////////////////////
	/// \class SceneManager
	/// \brief Manages all scene nodes contained in a scene.
	/// This class is responsible for managing, updating and
	/// querying information about all scene nodes in a scene.
	/// It is owned by a world object which uses it to manages
	/// all visible objects in it.
	/////////////////////////////////////////////////////////////
	class SceneManager
	{
	public:

		SceneManager();
		~SceneManager();

		/////////////////////////////////////////////////////////////
		/// Attaches the given scene node to the scene's root node.
		/////////////////////////////////////////////////////////////
		void AddSceneNode(SceneNode* node);
		/////////////////////////////////////////////////////////////
		/// Removes the given scene node from the scene.
		/////////////////////////////////////////////////////////////
		void RemoveSceneNode(SceneNode* node);

		/////////////////////////////////////////////////////////////
		/// Updates all scene nodes in the scene.
		/////////////////////////////////////////////////////////////
		void Update(const vxF32 delta);

	private:

		SceneNode mRootNode;
	};
}

#endif /* SCENEMANAGER_HPP_ */
