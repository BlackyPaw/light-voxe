/*
 * SceneNode.hpp
 *
 *  Created on: 23.07.2014
 *      Author: euaconlabs
 */

#ifndef SCENENODE_HPP_
#define SCENENODE_HPP_

#include <VoxE/Math/Transformable.hpp>
#include <VoxE/Scene/ISceneNodeData.hpp>

#include <vector>

namespace VX
{
	class SceneNode;
	class World;
	typedef std::vector<SceneNode*> SceneNodeList;

	/////////////////////////////////////////////////////////////
	/// \class SceneNode
	/// \brief Simple class for describing a node in a scene.
	/// A SceneNode basically represents a node in a scene (graph).
	/// Therefore it provides several functions for manipulating
	/// the scene. Every node can have any number of children
	/// and every node may be attached to one single parent node.
	/// Using several subclasses of SceneNode even links to other
	/// system may be created.
	/// <br/>
	/// SceneNodes should never be allocated on one's own but
	/// rather be allocated using a World's NewSceneNode()
	/// function. When no longer in use it should be freed using
	/// FreeSceneNode().
	/// <br/>
	/// Last but not least every scene node is capable of holding
	/// several scene node attachments. This allows for
	/// implementing extended behavior and callback functionality
	/// on scene nodes.
	/// \see World::NewSceneNode()
	/// \see World::FreeSceneNode()
	/////////////////////////////////////////////////////////////
	class SceneNode : public Transformable
	{
	public:

		SceneNode(World* world);
		~SceneNode();

		//////////////////////////////////////////////////////////////
		/// Gets the scene node's position in world coordinates, i.e.
		/// not in its own local coordinate system.
		//////////////////////////////////////////////////////////////
		Vector3f GetWorldPosition() const;
		//////////////////////////////////////////////////////////////
		/// Gets the scene node's rotation in world coordinates, i.e.
		/// not in its own local coordinate system.
		//////////////////////////////////////////////////////////////
		Quaternion GetWorldRotation() const;

		/////////////////////////////////////////////////////////////
		// Gets the parent this scene node is attached to, if any.
		/// If this scene node has no parent 0 will be returned.
		/////////////////////////////////////////////////////////////
		SceneNode* GetParent() const;
		/////////////////////////////////////////////////////////////
		/// Gets a list of all children this scene node possesses.
		/////////////////////////////////////////////////////////////
		const SceneNodeList& GetChildren() const;
		/////////////////////////////////////////////////////////////
		/// Adds the given child node to the scene node. This
		/// will fail if the given node is already a child of the
		/// scene node.
		/// \param child
		/////////////////////////////////////////////////////////////
		void AddChild(SceneNode* child);
		/////////////////////////////////////////////////////////////
		/// Removes the given child node from the scene node. This
		/// will fail if the given node is not a child of the scene
		/// node.
		/// \param child
		/////////////////////////////////////////////////////////////
		void RemoveChild(SceneNode* child);
		/////////////////////////////////////////////////////////////
		/// Removes the scene node from its parent node if it is
		/// attached to any.
		/////////////////////////////////////////////////////////////
		void DetachFromParent();
		/////////////////////////////////////////////////////////////
		/// Gets the world object this scene node belongs to.
		/////////////////////////////////////////////////////////////
		World* GetWorld() const;

		/////////////////////////////////////////////////////////////
		/// Sets the scene node data that should be associated with
		/// the scene node. If there exists already a scene node data
		/// it will not get deleted but only overwritten. The data
		/// will only be auto-deleted on destruction.
		/////////////////////////////////////////////////////////////
		void SetData(ISceneNodeData* data);
		/////////////////////////////////////////////////////////////
		/// Gets the data attached to this scene node if any.
		/////////////////////////////////////////////////////////////
		ISceneNodeData* GetData();
		/////////////////////////////////////////////////////////////
		/// Gets the data attached to this scene node if any.
		/////////////////////////////////////////////////////////////
		const ISceneNodeData* GetData() const;

		/////////////////////////////////////////////////////////////
		/// Updates the scene node.
		/////////////////////////////////////////////////////////////
		virtual void Update(const vxF32 delta);

	protected:

		World* mWorld;

	private:

		SceneNode* mParent;
		SceneNodeList mChildren;
		ISceneNodeData* mData;
	};
}

#endif /* SCENENODE_HPP_ */
