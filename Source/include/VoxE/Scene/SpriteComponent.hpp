/*
 * SpriteComponent.hpp
 *
 *  Created on: 12.07.2014
 *      Author: euaconlabs
 */

#ifndef SPRITECOMPONENT_HPP_
#define SPRITECOMPONENT_HPP_

#include <VoxE/Graphics/2D/Sprite.hpp>
#include <VoxE/Scene/Component.hpp>

namespace VX
{
	/////////////////////////////////////////////////////////////
	/// \class SpriteComponent
	/// \brief Attaches a sprite to a game object.
	/// This component attaches a sprite to a game object.
	/////////////////////////////////////////////////////////////
	class SpriteComponent : public Component
	{
	public:

		SpriteComponent();
		virtual ~SpriteComponent();

		virtual void Update(const vxF32 delta);

	private:

		Sprite* mSprite;
	};
}

#endif /* SPRITECOMPONENT_HPP_ */
