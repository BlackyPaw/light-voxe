/*
 * World.hpp
 *
 *  Created on: 23.07.2014
 *      Author: euaconlabs
 */

#ifndef WORLD_HPP_
#define WORLD_HPP_

#include <VoxE/Memory/PoolAllocator.hpp>
#include <VoxE/Scene/GameObject.hpp>
#include <VoxE/Scene/SceneManager.hpp>

namespace VX
{
	/////////////////////////////////////////////////////////////
	/// \class World
	/// \brief This class manages all entities in a dynamic game
	///		world as well as a scene graph containing the all
	///		scene nodes placed in the world.
	/// The World class is responsible for updating its scene
	/// thereby allowing each dynamic element to update itself.
	/// It is also responsible for specialized memory management
	/// as for scene nodes in order to speed up memory allocations
	/// at runtime. As almost always a pool allocator is used for
	/// this purpose.
	/////////////////////////////////////////////////////////////
	class World
	{
	public:

		World();
		~World();

		/////////////////////////////////////////////////////////////
		/// Updates all dynamic objects in the world including all
		/// scene nodes.
		/////////////////////////////////////////////////////////////
		void Update(const vxF32 delta);

		/////////////////////////////////////////////////////////////
		/// Allocates a new game object immediately connecting it to
		/// a scene node.
		/////////////////////////////////////////////////////////////
		GameObject* NewGameObject();
		/////////////////////////////////////////////////////////////
		/// Frees a game object and its scene node.
		/////////////////////////////////////////////////////////////
		void FreeGameObject(GameObject* object);

		/////////////////////////////////////////////////////////////
		/// Adds the given game object to the world so that it will
		/// be updated.
		/////////////////////////////////////////////////////////////
		void AddGameObject(GameObject* object);
		/////////////////////////////////////////////////////////////
		/// Removes the given game object from the world.
		/////////////////////////////////////////////////////////////
		void RemoveGameObject(GameObject* object);

		/////////////////////////////////////////////////////////////
		/// Allocates a new scene node and makes it ready for use
		/// with this world object. Please note that a scene node
		/// allocated in the context of one world must not be used
		/// in the context of another world.
		/////////////////////////////////////////////////////////////
		SceneNode* NewSceneNode();
		/////////////////////////////////////////////////////////////
		/// Frees the given scene node thereby making its memory
		/// available again and removing it from the scene
		/// automatically if still attached to it.
		/////////////////////////////////////////////////////////////
		void FreeSceneNode(SceneNode* node);
		/////////////////////////////////////////////////////////////
		/// Adds the given scene node to the world's scene. This
		/// function will fail if the scene node was not created in
		/// this world's context. Furthermore the world will take
		/// ownership of the given node and will delete it when it
		/// is deleted itself if the node gets not removed before.
		/////////////////////////////////////////////////////////////
		void AddSceneNode(SceneNode* node);
		/////////////////////////////////////////////////////////////
		/// Removes the given scene node from the world's scene.
		/// This function will fail if the scene node was not created
		/// in this world's context. The world will no longer take
		/// ownership of the scene node.
		/////////////////////////////////////////////////////////////
		void RemoveSceneNode(SceneNode* node);

		/////////////////////////////////////////////////////////////
		/// Gets the world's scene manager thereby granting access
		/// to the actual scene structure used to represent the
		/// world's scenery.
		/////////////////////////////////////////////////////////////
		SceneManager* GetSceneManager() const;

	private:

		PoolAllocator mSceneNodeAllocator;
		PoolAllocator mGameObjectAllocator;

		SceneManager mSceneManager;
	};
}

#endif /* WORLD_HPP_ */
