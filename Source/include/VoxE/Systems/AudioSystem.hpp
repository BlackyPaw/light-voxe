/*
 * AudioSystem.hpp
 *
 *  Created on: 24.07.2014
 *      Author: euaconlabs
 */

#ifndef AUDIOSYSTEM_HPP_
#define AUDIOSYSTEM_HPP_

#include <VoxE/Core/ISingleton.hpp>
#include <VoxE/Native/ALContext.hpp>
#include <VoxE/Native/Atomic.hpp>

namespace VX
{
	/////////////////////////////////////////////////////////////
	/// \class AudioSystem
	/// \brief Engine subsystem related to audio playback.
	/// This subsystem of VoxE is completely concentrated on
	/// audio management. It is responsible for setting up a valid
	/// audio context for later audio playback.
	/////////////////////////////////////////////////////////////
	class AudioSystem : public ISingleton<AudioSystem>
	{
	public:

		AudioSystem();
		~AudioSystem();

		/////////////////////////////////////////////////////////////
		/// Initializes the audio system by setting up a context and
		/// initializing everything into a valid state. Returns True
		/// on success or False on failure.
		/////////////////////////////////////////////////////////////
		vxBool Initialize();

		/////////////////////////////////////////////////////////////
		/// Updates the audio system and all of its underlying
		/// mechanics.
		/////////////////////////////////////////////////////////////
		void Update(const vxF32 delta);

	private:

		ALContext mALContext;
	};
}

#endif /* AUDIOSYSTEM_HPP_ */
