/*
 * Core.hpp
 *
 *  Created on: 24.07.2014
 *      Author: euaconlabs
 */

#ifndef CORE_HPP_
#define CORE_HPP_

#include <VoxE/Core/ISingleton.hpp>
#include <VoxE/Native/Atomic.hpp>
#include <VoxE/Native/Window.hpp>

namespace VX
{
	class GLContext;
	class Window;

	/////////////////////////////////////////////////////////////
	/// \class Core
	/// \brief The heart of VoxE.
	/// This class actually is the heart of VoxE. It is the
	/// underlying system that actually manages communication
	/// with all native OS resources. It therefore always needs
	/// to be initialized first!
	/// <br/>
	/// It is responsible for creating and updating the game
	/// window (i.e. the main render target) and takes care of
	/// the underlying windows message loop.
	/////////////////////////////////////////////////////////////
	class Core : public ISingleton<Core>
	{
	public:

		Core();
		~Core();

		/////////////////////////////////////////////////////////////
		/// Tries to initialize all native OS parts necessary to
		/// run VoxE. If any of these initialization steps it will
		/// return False. If everything could be set up correctly
		/// it will return True.
		/////////////////////////////////////////////////////////////
		vxBool Initialize();

		/////////////////////////////////////////////////////////////
		/// Updates all native OS parts which require so.
		/////////////////////////////////////////////////////////////
		void Update();

		/////////////////////////////////////////////////////////////
		/// Checks whether the game is still running or whether it
		/// has been terminated.
		/////////////////////////////////////////////////////////////
		vxBool IsRunning() const;

		/////////////////////////////////////////////////////////////
		/// Forces the immediate termination of the game.
		/////////////////////////////////////////////////////////////
		void ForceTermination();

		/////////////////////////////////////////////////////////////
		/// Gets the native OpenGL context managed by the Core.
		/////////////////////////////////////////////////////////////
		GLContext& GetGLContext();

	private:

		Window mGameWindow;
		GLContext mGLContext;
	};
}

#endif /* CORE_HPP_ */
