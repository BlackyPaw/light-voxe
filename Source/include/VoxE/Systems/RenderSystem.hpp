/*
 * RenderSystem.hpp
 *
 *  Created on: 24.07.2014
 *      Author: euaconlabs
 */

#ifndef RENDERSYSTEM_HPP_
#define RENDERSYSTEM_HPP_

#include <VoxE/Core/Event.hpp>
#include <VoxE/Core/ISingleton.hpp>
#include <VoxE/Graphics/Renderable.hpp>
#include <VoxE/Native/Atomic.hpp>
#include <VoxE/Memory/PoolAllocator.hpp>

#include <vector>

namespace VX
{
	class Sprite;
	class WindowSizeEvent;

	/////////////////////////////////////////////////////////////
	/// \class RenderSystem
	/// \brief The engine subsystems that manages all tasks related
	///		to rendering objects, scenes and everything else.
	/// This class is completely responsible for everything rendered
	/// in VoxE. It is therefore considered to be a subsystem of
	/// VoxE. As every other subsystem it needs to be initialized,
	/// and destroyed. In return it provides several functions
	/// all doing a specific task of the system like rendering
	/// the whole geometry which is to be rendered. It also
	/// encapsulates the graphics system in its entirety.
	/////////////////////////////////////////////////////////////
	class RenderSystem : public ISingleton<RenderSystem>
	{
	public:

		RenderSystem();
		~RenderSystem();

		/////////////////////////////////////////////////////////////
		/// Allocates memory for a sprite if available. If not 0
		/// is returned.
		/// <br/>
		/// Whenever a renderable object is allocated it will be
		/// managed by the rendering engine. This means that it will
		/// be available for rendering and will be integrated into
		/// the render pass immediately.
		/////////////////////////////////////////////////////////////
		Sprite* AllocateSprite();
		/////////////////////////////////////////////////////////////
		/// Frees the memory of the given sprite. When freed a
		/// renderable object is also removed from the render queue.
		/////////////////////////////////////////////////////////////
		void FreeSprite(Sprite* sprite);

		/////////////////////////////////////////////////////////////
		/// Initializes the rendering system and everything related
		/// to it. Returns false if an error occurred or true if
		/// the initialization went correctly.
		/////////////////////////////////////////////////////////////
		vxBool Initialize();
		/////////////////////////////////////////////////////////////
		/// Cleans up all resources the rendering system used and
		/// allocated which do not get freed automatically.
		/////////////////////////////////////////////////////////////
		void Cleanup();

		/////////////////////////////////////////////////////////////
		/// Renders everything that needs to be rendered and performs
		/// a buffer swap afterwards so that the new newly rendered
		/// image will be presented.
		/////////////////////////////////////////////////////////////
		void RenderAndSwapBuffers();

	private:

		typedef std::vector<Renderable*> RenderQueue;

		// The size of one memory block in the renderable allocator.
		static const vxU32 kAllocatorSize;
		// The number of renderables to reserve memory in the custom allocator for.
		static const vxU32 kNumRenderableObjects;

		void OnResize(EventPtr& e);

		// A list of renderable objects managed by the render system.
		RenderQueue mRenderQueue;
		// Pool allocator for allocating renderable objects:
		PoolAllocator mRenderableAllocator;
	};
}

#endif /* RENDERSYSTEM_HPP_ */
