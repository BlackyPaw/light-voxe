/*
 * AudioBuffer.cpp
 *
 *  Created on: 18.07.2014
 *      Author: euaconlabs
 */

#include <VoxE/Audio/AudioBuffer.hpp>

#include <AL/al.h>

namespace VX
{
	AudioBuffer::AudioBuffer() :
		mReferences(nullptr),
		mHandle(0)
	{
	}

	AudioBuffer::AudioBuffer(const AudioBuffer& b) :
		mReferences(nullptr)
	{
		Copy(b);
	}

	AudioBuffer::~AudioBuffer()
	{
		try
		{
			Destroy();
		}
		catch(...)
		{
			/* Swallow exception. */
		}
	}

	void AudioBuffer::Create(const AudioFormat& format, const void* pcm, const vxU32 size, const vxI32 frequency)
	{
		try { Destroy(); } catch(...) { /* Swallow exception. */ }

		vxU32 handle = 0;
		alGenBuffers(1, &handle);
		if(handle == 0)
			return;

		alBufferData(handle, static_cast<ALenum>(format), pcm, size, frequency);

		mHandle = handle;
		mReferences = new vxU32(1);
	}

	vxU32 AudioBuffer::GetHandle() const
	{
		return mHandle;
	}

	vxBool AudioBuffer::IsValid() const
	{
		return (mReferences != nullptr && mHandle != 0);
	}

	AudioBuffer& AudioBuffer::operator= (const AudioBuffer& b)
	{
		Copy(b);
		return *this;
	}

	void AudioBuffer::Copy(const AudioBuffer& b)
	{
		// Handle self-assignment:
		if(this == &b)
			return;

		try { Destroy(); } catch(...) { /* Swallow exception. */ }

		mReferences = b.mReferences;
		mHandle = b.mHandle;

		if(mReferences != nullptr)
			(*mReferences)++;
	}

	void AudioBuffer::Destroy()
	{
		if(mReferences != nullptr)
		{
			(*mReferences)--;
			if((*mReferences) == 0)
			{
				alDeleteBuffers(1, &mHandle);
				delete mReferences;
			}
		}

		mReferences = nullptr;
		mHandle = 0;
	}
}
