/*
 * AudioListener.cpp
 *
 *  Created on: 04.07.2014
 *      Author: euaconlabs
 */

#include <VoxE/Audio/AudioListener.hpp>

#include <AL/al.h>

namespace VX
{
	AudioListener::AudioListener()
	{
	}

	AudioListener::~AudioListener()
	{
	}

	void AudioListener::SetPosition(const vxF32 x, const vxF32 y, const vxF32 z)
	{
		SetPosition(Vector3f(x, y, z));
	}

	void AudioListener::SetPosition(const Vector3f& pos)
	{
		mPosition = pos;
		alListenerfv(AL_POSITION, &pos[0]);
	}

	const Vector3f& AudioListener::GetPosition() const
	{
		return mPosition;
	}
}
