/*
 * AudioSource.cpp
 *
 *  Created on: 04.07.2014
 *      Author: euaconlabs
 */

#include <VoxE/Audio/AudioSource.hpp>

#include <AL/al.h>

namespace VX
{
	AudioSource::AudioSource() :
		mReferences(nullptr),
		mHandle(0)
	{
	}

	AudioSource::AudioSource(const AudioSource& s) :
		mReferences(nullptr)
	{
		Copy(s);
	}

	AudioSource::~AudioSource()
	{
		try
		{
			Destroy();
		}
		catch(...)
		{
			/* Swallow exception. */
		}
	}

	void AudioSource::Create()
	{
		try { Destroy(); } catch(...) { /* Swallow exception. */ }

		vxU32 handle = 0;
		alGenSources(1, &handle);

		if(handle != 0)
		{
			mHandle = handle;
			mReferences = new vxU32(1);
		}
	}

	void AudioSource::SetPosition(const Vector3f& pos)
	{
		alSource3f(mHandle, AL_POSITION, pos.x, pos.y, pos.z);
	}

	void AudioSource::SetVelocity(const Vector3f& vel)
	{
		alSource3f(mHandle, AL_VELOCITY, vel.x, vel.y, vel.z);
	}

	void AudioSource::SetLooping(const vxBool loop)
	{
		alSourcei(mHandle, AL_LOOPING, (loop ? AL_TRUE : AL_FALSE));
	}

	void AudioSource::SetBuffer(const ResourceHandle<AudioBuffer>& buffer)
	{
		alSourcei(mHandle, AL_BUFFER, buffer->GetHandle());
	}

	void AudioSource::Play()
	{
		alSourcePlay(mHandle);
	}

	void AudioSource::Pause()
	{
		alSourcePause(mHandle);
	}

	void AudioSource::Stop()
	{
		alSourceStop(mHandle);
	}

	void AudioSource::Rewind()
	{
		alSourceRewind(mHandle);
	}

	vxBool AudioSource::IsPlaying() const
	{
		ALenum state = 0;
		alGetSourcei(mHandle, AL_SOURCE_STATE, &state);
		return (state == AL_PLAYING ? true : false);
	}

	vxBool AudioSource::IsValid() const
	{
		return (mReferences != nullptr && mHandle != 0);
	}

	AudioSource& AudioSource::operator= (const AudioSource& s)
	{
		Copy(s);
		return *this;
	}

	void AudioSource::Copy(const AudioSource& s)
	{
		// Handle self-assignment:
		if(this == &s)
			return;

		try { Destroy(); } catch(...) { /* Swallow exception. */ }

		mReferences = s.mReferences;
		mHandle = s.mHandle;

		if(mReferences != nullptr)
			(*mReferences)++;
	}

	void AudioSource::Destroy()
	{
		if(mReferences != nullptr)
		{
			(*mReferences)--;
			if((*mReferences) == 0)
			{
				alDeleteSources(1, &mHandle);
				delete mReferences;
			}
		}

		mReferences = nullptr;
		mHandle = 0;
	}
}
