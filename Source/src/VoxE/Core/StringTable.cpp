/*
 * StringTable.cpp
 *
 *  Created on: 08.07.2014
 *      Author: euaconlabs
 */

#include <VoxE/Core/StringTable.hpp>

#include <cstring>

namespace VX
{
	StringTable::StringTable()
	{
	}

	StringTable::~StringTable()
	{
		// Free all memory occupied by the interned strings:
		for(auto it = mLookup.begin(); it != mLookup.end(); ++it)
		{
			delete[] it->second;
		}
	}

	StringID StringTable::InternString(const vxChar* str, const vxU32 len)
	{
		vxU32 length = len;
		if(length == 0)
			length = std::strlen(str);
		StringID hash = HashString(str, length);

		vxChar* strcpy = new vxChar[length+1];
		std::memcpy(strcpy, str, length);
		strcpy[length] = '\0';

		mLookup[hash] = strcpy;
		return hash;
	}

	const vxChar* StringTable::LookupString(const StringID& id)
	{
		auto find = mLookup.find(id);
		if(find == mLookup.end())
			return nullptr;
		return find->second;
	}

	StringID StringTable::HashString(const vxChar* str, const vxU32 len)
	{
		StringID hash = 0x811C9DC5;
		vxU32 i = 0;
		while(i < len)
		{
			hash ^= str[i];
			hash *= 0x01000193;
			i++;
		}
		return hash;
	}
}
