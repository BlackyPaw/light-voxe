/*
 * Sprite.cpp
 *
 *  Created on: 26.06.2014
 *      Author: euaconlabs
 */

#include <VoxE/Graphics/2D/Sprite.hpp>
#include <VoxE/Graphics/2D/SpriteBatch.hpp>

#include <VoxE/Graphics/Renderer.hpp>

namespace VX
{
	Sprite::Sprite()
	{
	}

	Sprite::Sprite(const ResourceHandle<Texture>& texture)
	{
		SetTexture(texture);
	}

	Sprite::Sprite(const ResourceHandle<Texture>& texture, const IntRect& region) :
		mTexture(texture),
		mRegion(region)
	{
	}

	Sprite::~Sprite()
	{
	}

	const FloatRect Sprite::GetLocalBounds() const
	{
		return FloatRect(0.0f, 0.0f, static_cast<vxF32>(mRegion.mWidth), static_cast<vxF32>(mRegion.mHeight));
	}

	void Sprite::SetTexture(const ResourceHandle<Texture>& texture, const vxBool reset)
	{
		mTexture = texture;
		if(reset && *mTexture != nullptr)
		{
			mRegion = IntRect(0, 0, mTexture->GetWidth(), mTexture->GetHeight());
			SetAnchor(mAnchor);
		}
	}

	const ResourceHandle<Texture>& Sprite::GetTexture() const
	{
		return mTexture;
	}

	void Sprite::SetRegion(const IntRect& rect)
	{
		mRegion = rect;
		SetAnchor(mAnchor);
	}

	const IntRect& Sprite::GetRegion() const
	{
		return mRegion;
	}

	void Sprite::SetColor(const Color& color)
	{
		mColor = color;
	}

	const Color& Sprite::GetColor() const
	{
		return mColor;
	}

	void Sprite::SetAnchor(const vxF32 x, const vxF32 y)
	{
		SetAnchor(Vector2f(x, y));
	}

	void Sprite::SetAnchor(const Vector2f& anchor)
	{
		Texture* texture = *mTexture;
		if(texture == nullptr)
			return;

		mAnchor = anchor;
		Vector3f pivot = Vector3f(0.0f, 0.0f, 0.0f);
		pivot.x = mAnchor.x * static_cast<vxF32>(texture->GetWidth());
		pivot.y = mAnchor.y * static_cast<vxF32>(texture->GetHeight());
		SetPivot(pivot);
	}

	void Sprite::PreRender()
	{
	}

	void Sprite::Render()
	{
		Renderer::Get()->GetSpriteBatch()->Draw(mTexture,
												GetLocalBounds(),
												GetTransform(),
												mRegion,
												mColor);
	}

	void Sprite::PostRender()
	{
	}
}
