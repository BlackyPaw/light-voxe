/*
 * SpriteBatch.cpp
 *
 *  Created on: 27.06.2014
 *      Author: euaconlabs
 */

#include <VoxE/Graphics/OpenGL.hpp>
#include <VoxE/Graphics/Renderer.hpp>
#include <VoxE/Graphics/2D/SpriteBatch.hpp>
#include <VoxE/Native/DebugPrint.hpp>
#include <VoxE/Resources/ResourceManager.hpp>

namespace VX
{
	SpriteBatch::SpriteBatch(const vxU32 maxSprites) :
		mMaxSprites(maxSprites),
		mBlendingEnabled(false),
		mBlendSrc(BlendFactor::SrcAlpha), mBlendDst(BlendFactor::OneMinusSrcAlpha),
		mNumSprites(0),
		mVertices(nullptr), mIndices(nullptr),
		mInverseWidth(0.0f), mInverseHeight(0.0f),
		mIsDrawing(false)
	{
		// -----
		// Get a default shader.
		mShader = ResourceManager::Get()->LoadResource<ShaderProgram>("default-shader-2d");

		// -----
		// Allocate all buffers:
		mVertexBuffer.Allocate(4 * 8 * mMaxSprites * sizeof(vxF32), BufferUsage::StreamDraw);
		mIndexBuffer.Allocate(6 * mMaxSprites * sizeof(vxU32), BufferUsage::StreamDraw);
		mProjection = Matrix4::Orthographic(0.0f, 512.0f, 512.0f, 0.0f, -1.0f, 1.0f);

		// -----
		// Set up the vertex description
		VertexDescription vdesc;
		vdesc.AddAttribute(VertexAttribute(AttributeSemantic::Position, 0, 3, DataType::Float));
		vdesc.AddAttribute(VertexAttribute(AttributeSemantic::TextureCoordinates, 12, 2, DataType::Float));
		vdesc.AddAttribute(VertexAttribute(AttributeSemantic::Color, 20, 4, DataType::UnsignedByte, true));
		mVertexBuffer.SetVertexDescription(vdesc);
	}

	SpriteBatch::~SpriteBatch()
	{
	}

	void SpriteBatch::Begin()
	{
		if(mIsDrawing)
			DebugPrint("SpriteBatch > WARNING: Begin() while already drawing! => Unflushed data will be overwritten!\n");

		mVertices = reinterpret_cast<vxF32*>(mVertexBuffer.Map(BufferAccess::WriteOnly));
		mIndices = reinterpret_cast<vxU32*>(mIndexBuffer.Map(BufferAccess::WriteOnly));

		mNumSprites = 0;

		// Set the blending function accordingly:
		if(mBlendingEnabled)
		{
			glEnable(GL_BLEND);
			glBlendFunc(static_cast<GLenum>(mBlendSrc), static_cast<GLenum>(mBlendDst));
		}
		else
		{
			glDisable(GL_BLEND);
		}

		mIsDrawing = true;
	}

	void SpriteBatch::Draw(const ResourceHandle<Texture>& texture,
							  const FloatRect& bounds,
							  const Matrix4& transform,
							  const IntRect& region,
							  const Color& color)
	{
		if(!mIsDrawing)
			return;

		if(mNumSprites + 1 >= mMaxSprites)
			Flush(); // We have to flush here, no way around.

		SwitchTexture(texture);

		// Calculate the texture coordinates:
		FloatRect inverseCoords(0.0f, 0.0f, 1.0f, 1.0f);
		const Texture* rawTexture = *texture;
		if(rawTexture)
		{
			inverseCoords.mLeft = mInverseWidth * static_cast<vxF32>(region.mLeft);
			inverseCoords.mTop = mInverseHeight * static_cast<vxF32>(region.mTop);
			inverseCoords.mWidth = mInverseWidth * static_cast<vxF32>(region.mWidth);
			inverseCoords.mHeight = mInverseHeight * static_cast<vxF32>(region.mHeight);
		}

		// Pre-transform all vertices:
		Vector3f vertices[4];
		vertices[0] = transform * Vector3f(bounds.mLeft, bounds.mTop, 0.0f);
		vertices[1] = transform * Vector3f(bounds.mLeft + bounds.mWidth, bounds.mTop, 0.0f);
		vertices[2] = transform * Vector3f(bounds.mLeft + bounds.mWidth, bounds.mTop + bounds.mHeight, 0.0f);
		vertices[3] = transform * Vector3f(bounds.mLeft, bounds.mTop + bounds.mHeight, 0.0f);

		// Pack the color:
		union u32tof32 { vxU32 u; vxF32 f; } conv;
		conv.u = color.Pack();

		vxF32 packedColor = conv.f;

		// Now create the vertex data:
		*mVertices = vertices[0].x; mVertices++;
		*mVertices = vertices[0].y; mVertices++;
		*mVertices = vertices[0].z; mVertices++;
		*mVertices = inverseCoords.mLeft; mVertices++;
		*mVertices = inverseCoords.mTop; mVertices++;
		*mVertices = packedColor; mVertices++;
		*mVertices = vertices[1].x; mVertices++;
		*mVertices = vertices[1].y; mVertices++;
		*mVertices = vertices[1].z; mVertices++;
		*mVertices = inverseCoords.mLeft + inverseCoords.mWidth; mVertices++;
		*mVertices = inverseCoords.mTop; mVertices++;
		*mVertices = packedColor; mVertices++;
		*mVertices = vertices[2].x; mVertices++;
		*mVertices = vertices[2].y; mVertices++;
		*mVertices = vertices[2].z; mVertices++;
		*mVertices = inverseCoords.mLeft + inverseCoords.mWidth; mVertices++;
		*mVertices = inverseCoords.mTop + inverseCoords.mHeight; mVertices++;
		*mVertices = packedColor; mVertices++;
		*mVertices = vertices[3].x; mVertices++;
		*mVertices = vertices[3].y; mVertices++;
		*mVertices = vertices[3].z; mVertices++;
		*mVertices = inverseCoords.mLeft; mVertices++;
		*mVertices = inverseCoords.mTop + inverseCoords.mHeight; mVertices++;
		*mVertices = packedColor; mVertices++;

		// And now, finally, set up the index data:
		vxU32 base = mNumSprites * 6;
		*mIndices = base; mIndices++;
		*mIndices = base + 1; mIndices++;
		*mIndices = base + 2; mIndices++;
		*mIndices = base; mIndices++;
		*mIndices = base + 2; mIndices++;
		*mIndices = base + 3; mIndices++;

		mNumSprites++;
	}

	void SpriteBatch::Flush()
	{
		if(!mIsDrawing)
			return;

		mVertexBuffer.UnMap();
		mIndexBuffer.UnMap();

		mVertexBuffer.Bind();
		mIndexBuffer.Bind();

		Renderer::Get()->SetupShader(*mShader, mVertexBuffer.GetVertexDescription());
		Renderer::Get()->SetProjectionViewMatrix(mProjection * mView);
		Renderer::Get()->SetWorldMatrix(Matrix4());
		Renderer::Get()->SetDiffuseTexture(mTexture);
		Renderer::Get()->DrawElements(PolygonMode::Triangles, mNumSprites * 6, DataType::UnsignedInt, (void*)(0));


		mVertices = reinterpret_cast<vxF32*>(mVertexBuffer.Map(BufferAccess::WriteOnly));
		mIndices = reinterpret_cast<vxU32*>(mIndexBuffer.Map(BufferAccess::WriteOnly));

		mNumSprites = 0;
	}

	void SpriteBatch::End()
	{
		if(!mIsDrawing)
			DebugPrint("SpriteBatch > WARNING: End() -ing an invalid drawing session!\n");

		Flush();

		mVertexBuffer.UnMap();
		mIndexBuffer.UnMap();

		mVertices = nullptr;
		mIndices = nullptr;

		mIsDrawing = false;
	}

	void SpriteBatch::EnableBlending()
	{
		if(mIsDrawing)
			Flush();
		mBlendingEnabled = true;
	}

	void SpriteBatch::DisableBlending()
	{
		if(mIsDrawing)
			Flush();
		mBlendingEnabled = false;
	}

	const vxBool SpriteBatch::IsBlendingEnabled() const
	{
		return mBlendingEnabled;
	}

	void SpriteBatch::SetBlendFunc(const BlendFactor& src, const BlendFactor& dst)
	{
		if(mIsDrawing)
			Flush();
		mBlendSrc = src;
		mBlendDst = dst;
	}

	const BlendFactor& SpriteBatch::GetBlendSrc() const
	{
		return mBlendSrc;
	}

	const BlendFactor& SpriteBatch::GetBlendDst() const
	{
		return mBlendDst;
	}

	void SpriteBatch::SetProjectionMatrix(const Matrix4& projection)
	{
		if(mIsDrawing)
			Flush();
		mProjection = projection;
	}

	const Matrix4& SpriteBatch::GetProjectionMatrix() const
	{
		return mProjection;
	}

	void SpriteBatch::SetViewMatrix(const Matrix4& view)
	{
		if(mIsDrawing)
			Flush();
		mView = view;
	}

	const Matrix4& SpriteBatch::GetViewMatrix() const
	{
		return mView;
	}

	const vxBool SpriteBatch::IsDrawing() const
	{
		return mIsDrawing;
	}

	void SpriteBatch::SwitchTexture(const ResourceHandle<Texture>& texture)
	{
		if(mTexture != texture && mIsDrawing)
			Flush();
		mTexture = texture;
		const Texture* rawTexture = *texture;
		if(rawTexture)
		{
			mInverseWidth = 1.0f / rawTexture->GetWidth();
			mInverseHeight = 1.0f / rawTexture->GetHeight();
		}
		else
		{
			mInverseWidth = 0.0f;
			mInverseHeight = 0.0f;
		}
	}
}
