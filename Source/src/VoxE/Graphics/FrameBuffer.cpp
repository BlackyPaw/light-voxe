/*
 * GLFrameBuffer.cpp
 *
 *  Created on: 11.06.2014
 *      Author: euaconlabs
 */

#include <VoxE/Graphics/FrameBuffer.hpp>
#include <VoxE/Graphics/OpenGL.hpp>

#include <cstdio>

namespace VX
{
	FrameBuffer::FrameBuffer(const ImageFormat& f, const vxU32 width, const vxU32 height, const vxBool depth) :
		mReferences(nullptr),
		mHandle(0),
		mFormat(f),
		mWidth(width), mHeight(height),
		mDepth(depth),
		mDepthAttachment(0)
	{
		Setup();
	}

	FrameBuffer::FrameBuffer(const FrameBuffer& fb)
	{
		Copy(fb);
	}

	FrameBuffer::~FrameBuffer()
	{
		try
		{
			Destroy();
		}
		catch(...)
		{
			/* Swallow exception. The user had his chance. */
		}
	}

	void FrameBuffer::Bind() const
	{
		GLint params[4] = { 0, 0, 0, 0 };
		glGetIntegerv(GL_VIEWPORT, params);
		mOldViewport = IntRect(params[0], params[1], params[2], params[3]);

		glBindFramebuffer(GL_FRAMEBUFFER, mHandle);
		glViewport(0, 0, static_cast<vxI32>(mWidth), static_cast<vxI32>(mHeight));
	}

	void FrameBuffer::BindDirect() const
	{
		glBindFramebuffer(GL_FRAMEBUFFER, mHandle);
		glViewport(0, 0, static_cast<vxI32>(mWidth), static_cast<vxI32>(mHeight));
	}

	void FrameBuffer::UnBind() const
	{
		glBindFramebuffer(GL_FRAMEBUFFER, 0);
		glViewport(mOldViewport.mLeft, mOldViewport.mTop, mOldViewport.mWidth, mOldViewport.mHeight);
	}

	void FrameBuffer::UnBindDirect() const
	{
		glBindFramebuffer(GL_FRAMEBUFFER, 0);
	}

	Texture* FrameBuffer::GetColorAttachment()
	{
		return &mColorAttachment;
	}

	const Texture* FrameBuffer::GetColorAttachment() const
	{
		return &mColorAttachment;
	}

	const vxU32 FrameBuffer::GetWidth() const
	{
		return mWidth;
	}

	const vxU32 FrameBuffer::GetHeight() const
	{
		return mHeight;
	}

	const vxBool FrameBuffer::HasDepth() const
	{
		return mDepth;
	}

	FrameBuffer& FrameBuffer::operator= (const FrameBuffer& fb)
	{
		Copy(fb);
		return *this;
	}

	void FrameBuffer::Setup()
	{
		vxU32 handle = 0;
		glGenFramebuffers(1, &handle);

		Texture colorAttachment = Texture::Create(ImageFormat::RGBA8888, mWidth, mHeight);
		colorAttachment.Bind();
		colorAttachment.SetFilterDirect(TextureFilter::Linear, TextureFilter::Linear);
		colorAttachment.SetWrapDirect(TextureWrap::ClampToEdge, TextureWrap::ClampToEdge);

		vxU32 depthAttachment = 0;
		if(mDepth)
		{
			glGenRenderbuffers(1, &depthAttachment);
			glBindRenderbuffer(GL_RENDERBUFFER, depthAttachment);
			glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH_COMPONENT24, mWidth, mHeight);
		}

		glBindFramebuffer(GL_FRAMEBUFFER, handle);

		glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, colorAttachment.GetHandle(), 0);
		if(mDepth)
			glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_RENDERBUFFER, depthAttachment);

		GLint result = glCheckFramebufferStatus(GL_FRAMEBUFFER);

		glBindFramebuffer(GL_FRAMEBUFFER, 0);
		glBindRenderbuffer(GL_RENDERBUFFER, 0);
		glBindTexture(GL_TEXTURE_2D, 0);

		if(result != GL_FRAMEBUFFER_COMPLETE)
		{
			printf("[Graphics] Failed to create frame buffer!\n");

			try { colorAttachment.Destroy(); } catch(...) { /* Swallow exception. */ }
			if(mDepth)
				glDeleteRenderbuffers(1, &depthAttachment);

			glDeleteFramebuffers(1, &handle);

			return;
		}

		mReferences = new vxU32(1);
		mHandle = handle;
		mColorAttachment = colorAttachment;
		mDepthAttachment = depthAttachment;
	}

	void FrameBuffer::Copy(const FrameBuffer& fb)
	{
		// Handle self-assignment:
		if(this == &fb)
			return;

		try { Destroy(); } catch(...) { /* Swallow exception. */ }

		mReferences = fb.mReferences;
		mHandle = fb.mHandle;
		mFormat = fb.mFormat;
		mWidth = fb.mWidth;
		mHeight = fb.mHeight;
		mDepth = fb.mDepth;
		mColorAttachment = fb.mColorAttachment;
		mDepthAttachment = fb.mDepthAttachment;

		if(mReferences != nullptr)
			(*mReferences)++;
	}

	void FrameBuffer::Destroy()
	{
		if(mReferences != nullptr)
		{
			(*mReferences)--;
			if((*mReferences) == 0)
			{
				try  { mColorAttachment.Destroy(); } catch(...) { /* Swallow exception. */ }
				if(mDepth)
					glDeleteRenderbuffers(1, &mDepthAttachment);
				glDeleteFramebuffers(1, &mHandle);
				delete mReferences;
			}
		}

		mReferences = nullptr;
		mHandle = 0;
		mWidth = 0;
		mHeight = 0;
		mDepth = false;
		mDepthAttachment = 0;
	}
}
