/*
 * ImageData.cpp
 *
 *  Created on: 04.06.2014
 *      Author: euaconlabs
 */

#include <VoxE/Graphics/ImageData.hpp>
#include <SOIL/SOIL.h>

namespace VX
{
	ImageData::ImageData() :
		mReferences(nullptr),
		mFormat(ImageFormat::RGBA8888),
		mPixels(nullptr),
		mWidth(0), mHeight(0)
	{
	}

	ImageData::ImageData(const ImageFormat& f, const vxU32 width, const vxU32 height, void* pixels) :
		mReferences(new vxU32(1)),
		mFormat(f),
		mPixels(pixels),
		mWidth(width), mHeight(height)
	{
	}

	ImageData::ImageData(const ImageData& i) :
		mReferences(nullptr)
	{
		Copy(i);
	}

	ImageData::~ImageData()
	{
		try
		{
			Destroy();
		}
		catch(...)
		{ /* Swallow exception. */ }
	}

	ImageData ImageData::LoadFromFile(const File& f)
	{
		ImageData i;
		if(!f.GetPath().Exists())
			return i;

		vxI32 width, height, channels;
		vxU8* pixels = SOIL_load_image(f.GetPath().ToString().c_str(), &width, &height, &channels, SOIL_LOAD_RGBA);

		if(pixels == nullptr)
			return i;

		i.mReferences = new vxU32(1);
		i.mFormat = ImageFormat::RGBA8888;
		i.mPixels = pixels;
		i.mWidth = static_cast<vxU32>(width);
		i.mHeight = static_cast<vxU32>(height);

		return i;
	}

	const ImageFormat& ImageData::GetFormat() const
	{
		return mFormat;
	}

	const void* ImageData::GetPixels() const
	{
		return mPixels;
	}

	const vxU32 ImageData::GetWidth() const
	{
		return mWidth;
	}

	const vxU32 ImageData::GetHeight() const
	{
		return mHeight;
	}

	vxBool ImageData::IsValid() const
	{
		return (mPixels != nullptr);
	}

	ImageData& ImageData::operator=(const ImageData& i)
	{
		Copy(i);
		return *this;
	}

	void ImageData::Copy(const ImageData& i)
	{
		if(this == &i)
			return;

		try { Destroy(); } catch(...) { /* Swallow exception. */ }

		mReferences = i.mReferences;
		mFormat = i.mFormat;
		mPixels = i.mPixels;
		mWidth = i.mWidth;
		mHeight = i.mHeight;

		if(mReferences != nullptr)
			(*mReferences)++;
	}

	void ImageData::Destroy()
	{
		if(mReferences != nullptr)
		{
			(*mReferences)--;
			if((*mReferences) == 0)
			{
				if(mPixels != nullptr)
					delete[] reinterpret_cast<vxU8*>(mPixels);
				delete mReferences;
			}
		}

		mReferences = nullptr;
		mPixels = nullptr;
		mWidth = 0;
		mHeight = 0;
	}
}
