/*
 * IndexBuffer.cpp
 *
 *  Created on: 25.06.2014
 *      Author: euaconlabs
 */

#include <VoxE/Graphics/IndexBuffer.hpp>
#include <VoxE/Graphics/OpenGL.hpp>

#include <cstdio>

namespace VX
{
	IndexBuffer::IndexBuffer() :
		mReferences(nullptr),
		mHandle(0),
		mIndexType(DataType::UnsignedInt)
	{
	}

	IndexBuffer::IndexBuffer(const IndexBuffer& b) :
		mReferences(nullptr)
	{
		Copy(b);
	}

	IndexBuffer::~IndexBuffer()
	{
		if(IsValid())
		{
			try
			{
				Destroy();
			}
			catch(...)
			{
				/* Swallow exception. */
			}
		}
	}

	void IndexBuffer::Allocate(const vxU64 size, const BufferUsage& usage, const void* data)
	{
		vxU32 handle = 0;
		glGenBuffers(1, &handle);
		if(handle == 0)
			return;

		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, handle);
		glBufferData(GL_ELEMENT_ARRAY_BUFFER, size, data, static_cast<GLenum>(usage));

		try { Destroy(); } catch(...) { /* Swallow exception. */ }

		mReferences = new vxU32(1);
		mHandle = handle;
	}

	void IndexBuffer::CopyData(const vxU64 size, const void* data, const vxI64 offset, const vxBool direct)
	{
		if(!direct)
			glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, mHandle);
		glBufferSubData(GL_ELEMENT_ARRAY_BUFFER, offset, size, data);
	}

	void IndexBuffer::SetIndexType(const DataType& type)
	{
		mIndexType = type;
	}

	const DataType& IndexBuffer::GetIndexType() const
	{
		return mIndexType;
	}

	void* IndexBuffer::Map(const BufferAccess& access, const vxBool direct)
	{
		if(!direct)
			glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, mHandle);
		return glMapBuffer(GL_ELEMENT_ARRAY_BUFFER, static_cast<GLenum>(access));
	}

	void IndexBuffer::UnMap(const vxBool direct)
	{
		if(!direct)
			glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, mHandle);
		glUnmapBuffer(GL_ELEMENT_ARRAY_BUFFER);
	}

	void IndexBuffer::Bind() const
	{
		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, mHandle);
	}

	void IndexBuffer::UnBind() const
	{
		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
	}

	vxBool IndexBuffer::IsValid() const
	{
		return (mReferences != nullptr && mHandle != 0);
	}

	IndexBuffer& IndexBuffer::operator= (const IndexBuffer& b)
	{
		try { Destroy(); } catch(...) { /* Swallow exception. */ }
		Copy(b);
		return *this;
	}


	void IndexBuffer::Destroy()
	{
		if(mReferences != nullptr)
		{
			(*mReferences)--;
			if((*mReferences) == 0)
			{
				glDeleteBuffers(1, &mHandle);
				delete mReferences;
			}
		}

		mReferences = nullptr;
		mHandle = 0;
		mIndexType = DataType::UnsignedInt;
	}

	void IndexBuffer::Copy(const IndexBuffer& b)
	{
		// Handle self-assignment:
		if(this == &b)
			return;

		mReferences = b.mReferences;
		mHandle = b.mHandle;
		mIndexType = b.mIndexType;

		if(mReferences != nullptr)
			(*mReferences)++;
	}
}
