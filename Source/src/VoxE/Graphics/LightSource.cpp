/*
 * LightSource.cpp
 *
 *  Created on: 21.07.2014
 *      Author: euaconlabs
 */

#include <VoxE/Graphics/LightSource.hpp>

namespace VX
{
	LightSource::LightSource() :
		mType(LightType::Point)
	{
	}

	LightSource::~LightSource()
	{
	}

	Color LightSource::GetColor() const
	{
		return mColor;
	}

	void LightSource::SetColor(Color color)
	{
		mColor = color;
	}

	vxF32 LightSource::GetIntensity() const
	{
		return mIntensity;
	}

	void LightSource::SetIntensity(vxF32 intensity)
	{
		mIntensity = intensity;
	}

	Vector3f LightSource::GetOrientation() const
	{
		return mOrientation;
	}

	void LightSource::SetOrientation(Vector3f orientation)
	{
		mOrientation = orientation;
	}

	vxF32 LightSource::GetRange() const
	{
		return mRange;
	}

	void LightSource::SetRange(vxF32 range)
	{
		mRange = range;
	}

	LightType LightSource::GetType() const
	{
		return mType;
	}

	void LightSource::SetType(LightType type)
	{
		mType = type;
	}

}
