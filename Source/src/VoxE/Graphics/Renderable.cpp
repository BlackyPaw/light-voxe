/*
 * Renderable.cpp
 *
 *  Created on: 24.07.2014
 *      Author: euaconlabs
 */

#include <VoxE/Graphics/Renderable.hpp>

namespace VX
{
	Renderable::Renderable() :
		mVisible(true)
	{
	}

	Renderable::~Renderable()
	{
	}

	void Renderable::PreRender()
	{
	}

	void Renderable::Render()
	{
	}

	void Renderable::PostRender()
	{
	}

	vxBool Renderable::IsVisible() const
	{
		return mVisible;
	}

	void Renderable::SetVisible(const vxBool visible)
	{
		mVisible = visible;
	}

}
