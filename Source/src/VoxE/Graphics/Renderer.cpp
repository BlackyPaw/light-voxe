/*
 * Renderer.cpp
 *
 *  Created on: 30.06.2014
 *      Author: euaconlabs
 */

#include <VoxE/Graphics/OpenGL.hpp>
#include <VoxE/Graphics/Renderer.hpp>

namespace VX
{
	static const vxU64 kMatrixSize = 4 * 4 * sizeof(vxF32);

	vxI32 AttributeTypeToLocation(const AttributeSemantic& type, const vxU32 unit)
	{
		switch(type)
		{
			case AttributeSemantic::Position: return ShaderProgram::kPositionLocation;
			case AttributeSemantic::Normal: return ShaderProgram::kNormalLocation;
			case AttributeSemantic::Tangent: return ShaderProgram::kTangentLocation;
			case AttributeSemantic::BiTangent: return ShaderProgram::kBitangentLocation;
			case AttributeSemantic::TextureCoordinates:
			{
				if(unit > 0)
					return -1;
				return ShaderProgram::kTexCoord0Location + unit;
			}
			break;
			case AttributeSemantic::Color: return ShaderProgram::kColorLocation;
		}
		return -1;
	}

	Renderer::Renderer()
	{
		mMatrixBuffer.Allocate(kMatrixSize * 2, BufferUsage::StreamDraw);

		SetWorldMatrix(Matrix4());
		SetProjectionViewMatrix(Matrix4::Orthographic(-1.0f, 1.0f, -1.0f, 1.0f, -1.0f, 1.0f));
	}

	Renderer::~Renderer()
	{
	}

	void Renderer::BeginSession()
	{
		mMatrixStack.Clear();

		mSpriteBatch.SetBlendFunc(BlendFactor::SrcAlpha, BlendFactor::OneMinusSrcAlpha);
		mSpriteBatch.EnableBlending();
		mSpriteBatch.Begin();
	}

	MatrixStack& Renderer::GetMatrixStack()
	{
		return mMatrixStack;
	}

	SpriteBatch* Renderer::GetSpriteBatch()
	{
		return &mSpriteBatch;
	}

	void Renderer::SetupShader(const ShaderProgram* shader, const VertexDescription& vdesc)
	{
		if(shader == nullptr)
			return;

		shader->Use();
		vxU32 program = shader->GetHandle();

		// Set up the uniform blocks / buffers:
		// Query all necessary information:
		vxU32 iMatrices = glGetUniformBlockIndex(program, "Matrices");
		glUniformBlockBinding(program, iMatrices, 0);
		mMatrixBuffer.BindRange(0);

		// Set up all uniforms which have to be are constantly defined:

		// Diffuse texture is always bound to unit 0:
		shader->SetUniformi("DiffuseTexture", 0);

		// Set up the vertex attributes:
		const VertexAttribute* attributes = vdesc.GetAttributes();
		for(vxU32 i = 0; i < vdesc.GetNumAttributes(); ++i)
		{
			const VertexAttribute& attribute = attributes[i];
			vxI32 loc = AttributeTypeToLocation(attribute.GetUsage(), attribute.GetUnit());
			shader->EnableVertexAttribute(loc);
			shader->SetVertexAttribute(loc,
									   attribute.GetElements(),
									   attribute.GetType(),
									   attribute.IsNormalized(),
									   vdesc.GetStride(),
									   reinterpret_cast<void*>(static_cast<std::intptr_t>(attribute.GetOffset())));
		}
	}

	void Renderer::SetWorldMatrix(const Matrix4& world)
	{
		mMatrixBuffer.CopyData(kMatrixSize, world.Data(), 0);
	}

	void Renderer::SetProjectionViewMatrix(const Matrix4& projView)
	{
		mMatrixBuffer.CopyData(kMatrixSize, projView.Data(), kMatrixSize);
	}

	void Renderer::SetDiffuseTexture(const ResourceHandle<Texture>& texture)
	{
		const Texture* textureptr = *texture;
		if(textureptr != nullptr)
			textureptr->Bind(0);
	}

	void Renderer::DrawArrays(const PolygonMode& mode, const vxI32 first, const vxI32 count)
	{
		glDrawArrays(static_cast<GLenum>(mode), first, count);
	}

	void Renderer::DrawElements(const PolygonMode& mode, const vxI32 count, const DataType& type, const void* indices)
	{
		glDrawElements(static_cast<GLenum>(mode), count, static_cast<GLenum>(type), indices);
	}

	void Renderer::EndSession()
	{
		mSpriteBatch.End();
	}
}
