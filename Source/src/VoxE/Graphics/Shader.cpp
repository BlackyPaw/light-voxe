/*
 * GLShader.cpp
 *
 *  Created on: 08.06.2014
 *      Author: euaconlabs
 */

#include <VoxE/Graphics/Shader.hpp>
#include <VoxE/Graphics/OpenGL.hpp>

#include <VoxE/Native/DebugPrint.hpp>
#include <VoxE/Native/FileStream.hpp>

namespace VX
{
	vxU32 ShaderTypeToGLEnum(const ShaderType& t)
	{
		switch(t)
		{
			default:
			case ShaderType::Vertex: return GL_VERTEX_SHADER;
			case ShaderType::Fragment: return GL_FRAGMENT_SHADER;
			case ShaderType::TessEvaluation: return GL_TESS_EVALUATION_SHADER;
			case ShaderType::TessControl: return GL_TESS_CONTROL_SHADER;
			case ShaderType::Geometry: return GL_GEOMETRY_SHADER;
		}
	}

	Shader::Shader() :
		mReferences(nullptr),
		mType(ShaderType::Vertex),
		mHandle(0)
	{
	}

	Shader::Shader(const Shader& s) :
		mReferences(nullptr)
	{
		Copy(s);
	}

	Shader::~Shader()
	{
		try
		{
			Destroy();
		}
		catch(...)
		{ /* Swallow exception. */ }
	}

	Shader Shader::LoadFromFile(const ShaderType& t, const File& f)
	{
		vxU32 size = f.GetSize();
		vxU8* buffer = new vxU8[size];
		FileInputStream fis;
		fis.Open(f);
		fis.Read(size, buffer);
		fis.Close();
		return Shader::LoadFromSource(t, reinterpret_cast<vxChar*>(buffer), size);
	}

	Shader Shader::LoadFromSource(const ShaderType& t, const vxChar* s, const vxU32 sz)
	{
		if(s == nullptr || sz == 0)
			return Shader();

		vxU32 handle = glCreateShader(ShaderTypeToGLEnum(t));
		if(handle == 0)
			return Shader();

		glShaderSource(handle, 1, &s, reinterpret_cast<const GLint*>(&sz));
		glCompileShader(handle);

		GLint i;
		glGetShaderiv(handle, GL_COMPILE_STATUS, &i);
		if(i == GL_FALSE)
		{
			glGetShaderiv(handle, GL_INFO_LOG_LENGTH, &i);
			vxChar* buffer = new vxChar[i+1];
			glGetShaderInfoLog(handle, i, &i, buffer);
			buffer[i] = '\0';
			DebugPrint("[Shader] > Compilation failed: %s\n", buffer);
			delete[] buffer;
			glDeleteShader(handle);
			return Shader();
		}

		Shader r;
		r.mReferences = new vxU32(1);
		r.mType = t;
		r.mHandle = handle;

		return r;
	}

	const vxU32 Shader::GetHandle() const
	{
		return mHandle;
	}

	const ShaderType Shader::GetType() const
	{
		return mType;
	}

	vxBool Shader::IsValid() const
	{
		return (mHandle != 0);
	}

	Shader& Shader::operator= (const Shader& s)
	{
		Copy(s);
		return *this;
	}

	void Shader::Copy(const Shader& s)
	{
		// Handle self-assignments:
		if(this == &s)
			return;

		try { Destroy(); } catch(...) { /* Swallow exception. */ }

		mReferences = s.mReferences;
		mType = s.mType;
		mHandle = s.mHandle;

		if(mReferences != nullptr)
			(*mReferences)++;
	}

	void Shader::Destroy()
	{
		if(mReferences != nullptr)
		{
			(*mReferences)--;
			if((*mReferences) == 0)
			{
				if(mHandle != 0)
					glDeleteShader(mHandle);
				delete mReferences;
			}
		}

		mReferences = nullptr;
		mHandle = 0;
	}
}
