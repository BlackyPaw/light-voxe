/*
 * GLShaderProgram.cpp
 *
 *  Created on: 08.06.2014
 *      Author: euaconlabs
 */

#include <VoxE/Graphics/ShaderProgram.hpp>
#include <VoxE/Graphics/OpenGL.hpp>

namespace VX
{
	const char* ShaderProgram::POSITION_ATTRIBUTE = "aPosition\0";
	const char* ShaderProgram::NORMAL_ATTRIBUTE = "aNormal\0";
	const char* ShaderProgram::TANGENT_ATTRIBUTE = "aTangent\0";
	const char* ShaderProgram::COLOR_ATTRIBUTE = "aColor\0";
	const char* ShaderProgram::BITANGENT_ATTRIBUTE = "aBiTangent\0";
	const char* ShaderProgram::TEXCOORD0_ATTRIBUTE = "aTexCoord0\0";
	const char* ShaderProgram::TEXCOORD1_ATTRIBUTE = "aTexCoord1\0";

	const vxI32 ShaderProgram::kPositionLocation 		= 0;
	const vxI32 ShaderProgram::kNormalLocation			= 1;
	const vxI32 ShaderProgram::kTangentLocation			= 2;
	const vxI32 ShaderProgram::kBitangentLocation		= 3;
	const vxI32 ShaderProgram::kColorLocation			= 4;
	const vxI32 ShaderProgram::kTexCoord0Location		= 5;

	ShaderProgram::ShaderProgram() :
		mReferences(nullptr),
		mHandle(0)
	{
	}

	ShaderProgram::ShaderProgram(const ShaderProgram& p) :
		mReferences(nullptr)
	{
		Copy(p);
	}

	ShaderProgram::~ShaderProgram()
	{
		try
		{
			Destroy();
		}
		catch(...)
		{ /* Swallow exception. */ }
	}

	void ShaderProgram::AddShader(const Shader& s)
	{
		if(mHandle != 0)
			return;

		mShaders.push_back(s);
	}

	vxBool ShaderProgram::Link()
	{
		if(mShaders.size() == 0)
			return false;

		vxU32 handle = glCreateProgram();
		for(auto it = mShaders.begin(); it != mShaders.end(); ++it)
			glAttachShader(handle, it->GetHandle());
		// Release all references:
		mShaders.clear();

		glLinkProgram(handle);

		GLint i;
		glGetProgramiv(handle, GL_LINK_STATUS, &i);
		if(i == GL_FALSE)
		{
			glDeleteProgram(handle);
			return false;
		}

		mReferences = new vxU32(1);
		mHandle = handle;

		return true;
	}

	const vxU32 ShaderProgram::GetHandle() const
	{
		return mHandle;
	}

	void ShaderProgram::Use() const
	{
		glUseProgram(mHandle);
	}

	//////////////////////////////////////////////////////////////
	// Uniform modifers
	//////////////////////////////////////////////////////////////
	vxI32 ShaderProgram::GetUniformLocation(const vxString& name) const
	{
		return glGetUniformLocation(mHandle, name.c_str());
	}

	void ShaderProgram::SetUniformi(const vxI32 location, const vxI32 v0) const
	{ glUniform1i(location, v0); }
	void ShaderProgram::SetUniformi(const vxI32 location, const vxI32 v0, const vxI32 v1) const
	{ glUniform2i(location, v0, v1); }
	void ShaderProgram::SetUniformi(const vxI32 location, const vxI32 v0, const vxI32 v1, const vxI32 v2) const
	{ glUniform3i(location, v0, v1, v2); }
	void ShaderProgram::SetUniformi(const vxI32 location, const vxI32 v0, const vxI32 v1, const vxI32 v2, const vxI32 v3) const
	{ glUniform4i(location, v0, v1, v2, v3); }
	void ShaderProgram::SetUniformi(const vxString& name, const vxI32 v0) const
	{ glUniform1i(GetUniformLocation(name), v0); }
	void ShaderProgram::SetUniformi(const vxString& name, const vxI32 v0, const vxI32 v1) const
	{ glUniform2i(GetUniformLocation(name), v0, v1); }
	void ShaderProgram::SetUniformi(const vxString& name, const vxI32 v0, const vxI32 v1, const vxI32 v2) const
	{ glUniform3i(GetUniformLocation(name), v0, v1, v2); }
	void ShaderProgram::SetUniformi(const vxString& name, const vxI32 v0, const vxI32 v1, const vxI32 v2, const vxI32 v3) const
	{ glUniform4i(GetUniformLocation(name), v0, v1, v2, 3); }

	void ShaderProgram::SetUniform1iv(const vxI32 location, const vxU32 count, const vxI32* values) const
	{ glUniform1iv(location, count, values); }
	void ShaderProgram::SetUniform2iv(const vxI32 location, const vxU32 count, const vxI32* values) const
	{ glUniform2iv(location, count, values); }
	void ShaderProgram::SetUniform3iv(const vxI32 location, const vxU32 count, const vxI32* values) const
	{ glUniform3iv(location, count, values); }
	void ShaderProgram::SetUniform4iv(const vxI32 location, const vxU32 count, const vxI32* values) const
	{ glUniform4iv(location, count, values); }
	void ShaderProgram::SetUniform1iv(const vxString& name, const vxU32 count, const vxI32* values) const
	{ glUniform1iv(GetUniformLocation(name), count, values); }
	void ShaderProgram::SetUniform2iv(const vxString& name, const vxU32 count, const vxI32* values) const
	{ glUniform2iv(GetUniformLocation(name), count, values); }
	void ShaderProgram::SetUniform3iv(const vxString& name, const vxU32 count, const vxI32* values) const
	{ glUniform3iv(GetUniformLocation(name), count, values); }
	void ShaderProgram::SetUniform4iv(const vxString& name, const vxU32 count, const vxI32* values) const
	{ glUniform4iv(GetUniformLocation(name), count, values); }

	void ShaderProgram::SetUniformf(const vxI32 location, const vxF32 v0) const
	{ glUniform1f(location, v0); }
	void ShaderProgram::SetUniformf(const vxI32 location, const vxF32 v0, const vxF32 v1) const
	{ glUniform2f(location, v0, v1); }
	void ShaderProgram::SetUniformf(const vxI32 location, const vxF32 v0, const vxF32 v1, const vxF32 v2) const
	{ glUniform3f(location, v0, v1, v2); }
	void ShaderProgram::SetUniformf(const vxI32 location, const vxF32 v0, const vxF32 v1, const vxF32 v2, const vxF32 v3) const
	{ glUniform4f(location, v0, v1, v2, v3); }
	void ShaderProgram::SetUniformf(const vxString& name, const vxF32 v0) const
	{ glUniform1f(GetUniformLocation(name), v0); }
	void ShaderProgram::SetUniformf(const vxString& name, const vxF32 v0, const vxF32 v1) const
	{ glUniform2f(GetUniformLocation(name), v0, v1); }
	void ShaderProgram::SetUniformf(const vxString& name, const vxF32 v0, const vxF32 v1, const vxF32 v2) const
	{ glUniform3f(GetUniformLocation(name), v0, v1, v2); }
	void ShaderProgram::SetUniformf(const vxString& name, const vxF32 v0, const vxF32 v1, const vxF32 v2, const vxF32 v3) const
	{ glUniform4f(GetUniformLocation(name), v0, v1, v2, v3); }

	void ShaderProgram::SetUniform1fv(const vxI32 location, const vxU32 count, const vxF32* values) const
	{ glUniform1fv(location, count, values); }
	void ShaderProgram::SetUniform2fv(const vxI32 location, const vxU32 count, const vxF32* values) const
	{ glUniform2fv(location, count, values); }
	void ShaderProgram::SetUniform3fv(const vxI32 location, const vxU32 count, const vxF32* values) const
	{ glUniform3fv(location, count, values); }
	void ShaderProgram::SetUniform4fv(const vxI32 location, const vxU32 count, const vxF32* values) const
	{ glUniform4fv(location, count, values); }
	void ShaderProgram::SetUniform1fv(const vxString& name, const vxU32 count, const vxF32* values) const
	{ glUniform1fv(GetUniformLocation(name), count, values); }
	void ShaderProgram::SetUniform2fv(const vxString& name, const vxU32 count, const vxF32* values) const
	{ glUniform2fv(GetUniformLocation(name), count, values); }
	void ShaderProgram::SetUniform3fv(const vxString& name, const vxU32 count, const vxF32* values) const
	{ glUniform3fv(GetUniformLocation(name), count, values); }
	void ShaderProgram::SetUniform4fv(const vxString& name, const vxU32 count, const vxF32* values) const
	{ glUniform4fv(GetUniformLocation(name), count, values); }

	void ShaderProgram::SetUniformMatrix(const vxI32 location, const Matrix4& values, const vxBool transpose) const
	{ glUniformMatrix4fv(location, 1, transpose, values.Data()); }
	void ShaderProgram::SetUniformMatrix(const vxString& name, const Matrix4& values, const vxBool transpose) const
	{ glUniformMatrix4fv(GetUniformLocation(name), 1, transpose, values.Data()); }

	//////////////////////////////////////////////////////////////
	// Vertex attribute modifiers
	//////////////////////////////////////////////////////////////
	vxI32 ShaderProgram::GetAttributeLocation(const vxString& name) const
	{
		return glGetAttribLocation(mHandle, name.c_str());
	}

	void ShaderProgram::EnableVertexAttribute(const vxI32 location) const
	{
		glEnableVertexAttribArray(location);
	}

	void ShaderProgram::EnableVertexAttribute(const vxString& name) const
	{
		glEnableVertexAttribArray(GetAttributeLocation(name));
	}

	void ShaderProgram::DisableVertexAttribute(const vxI32 location) const
	{
		glDisableVertexAttribArray(location);
	}

	void ShaderProgram::DisableVertexAttribute(const vxString& name) const
	{
		glDisableVertexAttribArray(GetAttributeLocation(name));
	}

	void ShaderProgram::SetVertexAttribute(const vxI32 location, const vxI32 size, const DataType& type, const vxBool normalized, const vxI32 stride, const void* pointer) const
	{
		glVertexAttribPointer(location, size, static_cast<GLenum>(type), normalized, stride, pointer);
	}
	void ShaderProgram::SetVertexAttribute(const vxString& name, const vxI32 size, const DataType& type, const vxBool normalized, const vxI32 stride, const void* pointer) const
	{
		glVertexAttribPointer(GetAttributeLocation(name), size, static_cast<GLenum>(type), normalized, stride, pointer);
	}

	vxBool ShaderProgram::IsValid() const
	{
		return (mHandle != 0);
	}

	ShaderProgram& ShaderProgram::operator= (const ShaderProgram& p)
	{
		Copy(p);
		return *this;
	}

	void ShaderProgram::Copy(const ShaderProgram& p)
	{
		// Handle self-assignments:
		if(this == &p)
			return;

		try { Destroy(); } catch(...) { /* Swallow exception. */ }

		mReferences = p.mReferences;
		mHandle = p.mHandle;

		if(mReferences != nullptr)
			(*mReferences)++;
	}

	void ShaderProgram::Destroy()
	{
		if(mReferences != nullptr)
		{
			(*mReferences)--;
			if((*mReferences) == 0)
			{
				if(mHandle != 0)
					glDeleteProgram(mHandle);
				delete mReferences;
			}
		}

		mReferences = nullptr;
		mHandle = 0;
	}
}
