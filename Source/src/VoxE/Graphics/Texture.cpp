/*
 * GTexture.cpp
 *
 *  Created on: 04.06.2014
 *      Author: euaconlabs
 */

#include <VoxE/Graphics/OpenGL.hpp>
#include <VoxE/Graphics/Texture.hpp>

namespace VX
{
	vxU32 TextureFilterToGLEnum(const TextureFilter& f)
	{
		switch(f)
		{
			default:
			case TextureFilter::Linear: return GL_LINEAR;
			case TextureFilter::MipMapLinearLinear: return GL_LINEAR_MIPMAP_LINEAR;
			case TextureFilter::MipMapLinearNearest: return GL_LINEAR_MIPMAP_NEAREST;
			case TextureFilter::MipMapNearestLinear: return GL_NEAREST_MIPMAP_LINEAR;
			case TextureFilter::MipMapNearestNearest: return GL_NEAREST_MIPMAP_NEAREST;
			case TextureFilter::Nearest: return GL_NEAREST;
		}
	}

	vxU32 TextureWrapToGLEnum(const TextureWrap& w)
	{
		switch(w)
		{
			default:
			case TextureWrap::ClampToEdge: return GL_CLAMP_TO_EDGE;
			case TextureWrap::Mirror: return GL_MIRRORED_REPEAT;
			case TextureWrap::Repeat: return GL_REPEAT;
		}
	}

	vxU32 ImageFormatToGLEnumInternal(const ImageFormat& f)
	{
		switch(f)
		{
			default:
			case ImageFormat::RGBA8888: return GL_RGBA8;
			case ImageFormat::RGB888: return GL_RGB8;
		}
	}

	vxU32 ImageFormatToGLEnum(const ImageFormat& f)
	{
		switch(f)
		{
			default:
			case ImageFormat::RGBA8888: return GL_RGBA;
			case ImageFormat::RGB888: return GL_RGB;
		}
	}

	vxU32 ImageFormatToGLEnumType(const ImageFormat& f)
	{
		switch(f)
		{
			default:
			case ImageFormat::RGBA8888:
			case ImageFormat::RGB888:
				return GL_UNSIGNED_BYTE;
		}
	}

	Texture::Texture() :
		mReferences(nullptr),
		mHandle(0),
		mWidth(0), mHeight(0),
		mTarget(0),
		mMinFilter(TextureFilter::Linear), mMagFilter(TextureFilter::Linear),
		mWrapU(TextureWrap::ClampToEdge), mWrapV(TextureWrap::ClampToEdge)
	{
	}

	Texture::Texture(const Texture& t) :
		mReferences(nullptr)
	{
		Copy(t);
	}

	Texture::~Texture()
	{
		try
		{
			Destroy();
		}
		catch(...)
		{ /* Swallow exception. */ }
	}

	Texture Texture::Create(const ImageData& i)
	{
		Texture t;
		if(i.GetPixels() == nullptr)
			return t;

		t.mReferences = new vxU32(1);
		t.mTarget = GL_TEXTURE_2D;
		t.mWidth = i.GetWidth();
		t.mHeight = i.GetHeight();
		const ImageFormat& f = i.GetFormat();
		glGenTextures(1, &t.mHandle);
		glBindTexture(t.mTarget, t.mHandle);
		// Upload the image data:
		glTexImage2D(
				GL_TEXTURE_2D,
				0,
				ImageFormatToGLEnumInternal(f),
				t.mWidth,
				t.mHeight,
				0,
				ImageFormatToGLEnum(f),
				ImageFormatToGLEnumType(f),
				i.GetPixels());
		// Now simply use the direct functions to set the texture
		// parameters easily:
		t.SetFilterDirect(t.GetMinFilter(), t.GetMagFilter());
		t.SetWrapDirect(t.GetWrapU(), t.GetWrapV());

		return t;
	}

	Texture Texture::Create(const ImageFormat& f, const vxU32 width, const vxU32 height)
	{
		Texture t;
		t.mReferences = new vxU32(1);
		t.mTarget = GL_TEXTURE_2D;
		t.mWidth = width;
		t.mHeight = height;
		glGenTextures(1, &t.mHandle);
		glBindTexture(t.mTarget, t.mHandle);
		glTexImage2D(
			GL_TEXTURE_2D,
			0,
			ImageFormatToGLEnumInternal(f),
			width,
			height,
			0,
			ImageFormatToGLEnum(f),
			ImageFormatToGLEnumType(f),
			0
		);

		t.SetFilterDirect(t.GetMinFilter(), t.GetMagFilter());
		t.SetWrapDirect(t.GetWrapU(), t.GetWrapV());

		return t;
	}

	const vxU32 Texture::GetHandle() const
	{
		return mHandle;
	}

	const TextureFilter& Texture::GetMinFilter() const
	{
		return mMinFilter;
	}

	const TextureFilter& Texture::GetMagFilter() const
	{
		return mMagFilter;
	}

	const TextureWrap& Texture::GetWrapU() const
	{
		return mWrapU;
	}

	const TextureWrap& Texture::GetWrapV() const
	{
		return mWrapV;
	}

	const vxU32 Texture::GetWidth() const
	{
		return mWidth;
	}

	const vxU32 Texture::GetHeight() const
	{
		return mHeight;
	}

	void Texture::SetFilter(const TextureFilter& min, const TextureFilter& mag)
	{
		Bind();
		mMinFilter = min;
		mMagFilter = mag;

		glTexParameteri(mTarget, GL_TEXTURE_MIN_FILTER, TextureFilterToGLEnum(mMinFilter));
		glTexParameteri(mTarget, GL_TEXTURE_MAG_FILTER, TextureFilterToGLEnum(mMagFilter));
	}

	void Texture::SetWrap(const TextureWrap& u, const TextureWrap& v)
	{
		Bind();
		mWrapU = u;
		mWrapV = v;

		glTexParameteri(mTarget, GL_TEXTURE_WRAP_S, TextureWrapToGLEnum(mWrapU));
		glTexParameteri(mTarget, GL_TEXTURE_WRAP_T, TextureWrapToGLEnum(mWrapV));
	}

	void Texture::SetFilterDirect(const TextureFilter& min, const TextureFilter& mag)
	{
		mMinFilter = min;
		mMagFilter = mag;

		glTexParameteri(mTarget, GL_TEXTURE_MIN_FILTER, TextureFilterToGLEnum(mMinFilter));
		glTexParameteri(mTarget, GL_TEXTURE_MAG_FILTER, TextureFilterToGLEnum(mMagFilter));
	}

	void Texture::SetWrapDirect(const TextureWrap& u, const TextureWrap& v)
	{
		mWrapU = u;
		mWrapV = v;

		glTexParameteri(mTarget, GL_TEXTURE_WRAP_S, TextureWrapToGLEnum(mWrapU));
		glTexParameteri(mTarget, GL_TEXTURE_WRAP_T, TextureWrapToGLEnum(mWrapV));
	}

	void Texture::UploadImage2D(const ImageData& i)
	{
		Bind();
		mWidth = i.GetWidth();
		mHeight = i.GetHeight();
		ImageFormat f = i.GetFormat();
		glTexImage2D(
			mTarget,
			0,
			ImageFormatToGLEnumInternal(f),
			mWidth,
			mHeight,
			0,
			ImageFormatToGLEnum(f),
			ImageFormatToGLEnumType(f),
			i.GetPixels()
		);
	}

	void Texture::UploadImage2DDirect(const ImageData& i)
	{
		mWidth = i.GetWidth();
		mHeight = i.GetHeight();
		ImageFormat f = i.GetFormat();
		glTexImage2D(
				mTarget,
				0,
				ImageFormatToGLEnumInternal(f),
				mWidth,
				mHeight,
				0,
				ImageFormatToGLEnum(f),
				ImageFormatToGLEnumType(f),
				i.GetPixels()
		);
	}

	ImageData Texture::GetImageData() const
	{
		glBindTexture(mTarget, mHandle);
		void* pixels = reinterpret_cast<void*>(new vxU8[mWidth * mHeight * 4]);
		glGetTexImage(mTarget, 0, GL_RGBA, GL_UNSIGNED_BYTE, pixels);
		return ImageData(ImageFormat::RGBA8888, mWidth, mHeight, pixels);
	}

	void Texture::Bind() const
	{
		glBindTexture(mTarget, mHandle);
	}

	void Texture::Bind(const vxU32 unit) const
	{
		glActiveTexture(GL_TEXTURE0 + unit);
		glBindTexture(mTarget, mHandle);
	}

	vxBool Texture::IsValid() const
	{
		return (mHandle != 0);
	}

	Texture& Texture::operator=(const Texture& t)
	{
		Copy(t);
		return *this;
	}

	void Texture::Copy(const Texture& t)
	{
		// Avoid self-assignments:
		if(this == &t)
			return;

		try { Destroy(); } catch(...) { /* Swallow exception. */ }

		mReferences = t.mReferences;
		mHandle = t.mHandle;
		mWidth = t.mWidth;
		mHeight = t.mHeight;
		mTarget = t.mTarget;
		mMinFilter = t.mMinFilter;
		mMagFilter = t.mMagFilter;
		mWrapU = t.mWrapU;
		mWrapV = t.mWrapV;

		if(mReferences != nullptr)
			(*mReferences)++;
	}

	void Texture::Destroy()
	{
		if(mReferences != nullptr)
		{
			(*mReferences)--;
			if((*mReferences) == 0)
			{
				if(mHandle != 0)
					glDeleteTextures(1, &mHandle);
				delete mReferences;
			}
		}

		mReferences = nullptr;
		mHandle = 0;
		mWidth = 0;
		mHeight = 0;
		mTarget = 0;
		// Leave filter modes and wrap modes as they are.
	}
}
