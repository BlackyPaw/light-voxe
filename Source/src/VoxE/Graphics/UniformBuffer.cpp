/*
 * UniformBuffer.cpp
 *
 *  Created on: 30.06.2014
 *      Author: euaconlabs
 */

#include <VoxE/Graphics/OpenGL.hpp>
#include <VoxE/Graphics/UniformBuffer.hpp>

namespace VX
{
	UniformBuffer::UniformBuffer() :
		mReferences(nullptr),
		mHandle(0)
	{
	}

	UniformBuffer::UniformBuffer(const UniformBuffer& b) :
		mReferences(nullptr)
	{
		Copy(b);
	}

	UniformBuffer::~UniformBuffer()
	{
		try
		{
			Destroy();
		}
		catch(...)
		{
			/* Swallow exception. */
		}
	}

	void UniformBuffer::Allocate(const vxU64 size, const BufferUsage& usage, const void* data)
	{
		vxU32 handle = 0;
		glGenBuffers(1, &handle);
		if(handle == 0)
			return;

		glBindBuffer(GL_UNIFORM_BUFFER, handle);
		glBufferData(GL_UNIFORM_BUFFER, size, data, static_cast<GLenum>(usage));

		try { Destroy(); } catch(...) { /* Swallow exception. */ }

		mReferences = new vxU32(1);
		mHandle = handle;
	}

	void UniformBuffer::CopyData(const vxU64 size, const void* data, const vxI64 offset, const vxBool direct)
	{
		if(!direct)
			glBindBuffer(GL_UNIFORM_BUFFER, mHandle);
		glBufferSubData(GL_UNIFORM_BUFFER, offset, size, data);
	}

	void* UniformBuffer::Map(const BufferAccess& access, const vxBool direct)
	{
		if(!direct)
			glBindBuffer(GL_UNIFORM_BUFFER, mHandle);
		return glMapBuffer(GL_UNIFORM_BUFFER, static_cast<GLenum>(access));
	}

	void UniformBuffer::UnMap(const vxBool direct)
	{
		if(!direct)
			glBindBuffer(GL_UNIFORM_BUFFER, mHandle);
		glUnmapBuffer(GL_UNIFORM_BUFFER);
	}

	void UniformBuffer::Bind() const
	{
		glBindBuffer(GL_UNIFORM_BUFFER, mHandle);
	}

	void UniformBuffer::BindRange(const vxU32 binding) const
	{
		glBindBufferBase(GL_UNIFORM_BUFFER, binding, mHandle);
	}

	void UniformBuffer::BindRange(const vxU32 binding, const vxI64 offset, const vxI64 size) const
	{
		glBindBufferRange(GL_UNIFORM_BUFFER, binding, mHandle, offset, size);
	}

	void UniformBuffer::UnBind() const
	{
		glBindBuffer(GL_UNIFORM_BUFFER, 0);
	}

	vxBool UniformBuffer::IsValid() const
	{
		return (mReferences != nullptr && mHandle != 0);
	}

	UniformBuffer& UniformBuffer::operator= (const UniformBuffer& b)
	{
		Copy(b);
		return *this;
	}

	void UniformBuffer::Copy(const UniformBuffer& b)
	{
		// Handle self-assignment:
		if(this == &b)
			return;

		try { Destroy(); } catch(...) { /* Swallow exception. */ }

		mReferences = b.mReferences;
		mHandle = b.mHandle;

		if(mReferences != nullptr)
			(*mReferences)++;
	}

	void UniformBuffer::Destroy()
	{
		if(mReferences != nullptr)
		{
			(*mReferences)--;
			if((*mReferences) == 0)
			{
				if(mHandle != 0)
					glDeleteBuffers(1, &mHandle);
				delete mReferences;
			}
		}

		mReferences = nullptr;
		mHandle = 0;
	}
}
