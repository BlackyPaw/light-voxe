/*
 * VertexBuffer.cpp
 *
 *  Created on: 25.06.2014
 *      Author: euaconlabs
 */

#include <VoxE/Graphics/VertexBuffer.hpp>
#include <VoxE/Graphics/OpenGL.hpp>

#include <cstdio>

namespace VX
{
	VertexBuffer::VertexBuffer() :
		mReferences(nullptr),
		mHandle(0)
	{
	}

	VertexBuffer::VertexBuffer(const VertexBuffer& b) :
		mReferences(nullptr)
	{
		Copy(b);
	}

	VertexBuffer::~VertexBuffer()
	{
		if(IsValid())
		{
			try
			{
				Destroy();
			}
			catch(...)
			{
				/* Swallow exception. */
			}
		}
	}

	void VertexBuffer::Allocate(const vxU64 size, const BufferUsage& usage, const void* data)
	{
		vxU32 handle = 0;
		glGenBuffers(1, &handle);
		if(handle == 0)
			return;

		glBindBuffer(GL_ARRAY_BUFFER, handle);
		glBufferData(GL_ARRAY_BUFFER, size, data, static_cast<GLenum>(usage));

		try { Destroy(); } catch(...) { /* Swallow exception. */ }

		mReferences = new vxU32(1);
		mHandle = handle;
	}

	void VertexBuffer::CopyData(const vxU64 size, const void* data, const vxI64 offset, const vxBool direct)
	{
		if(!direct)
			glBindBuffer(GL_ARRAY_BUFFER, mHandle);
		glBufferSubData(GL_ARRAY_BUFFER, offset, size, data);
	}

	void VertexBuffer::SetVertexDescription(const VertexDescription& desc)
	{
		mVertexDesc = desc;
	}

	const VertexDescription& VertexBuffer::GetVertexDescription() const
	{
		return mVertexDesc;
	}

	void* VertexBuffer::Map(const BufferAccess& access, const vxBool direct)
	{
		if(!direct)
			glBindBuffer(GL_ARRAY_BUFFER, mHandle);
		return glMapBuffer(GL_ARRAY_BUFFER, static_cast<GLenum>(access));
	}

	void VertexBuffer::UnMap(const vxBool direct)
	{
		if(!direct)
			glBindBuffer(GL_ARRAY_BUFFER, mHandle);
		if(!glUnmapBuffer(GL_ARRAY_BUFFER))
			std::printf("> Failure in VertexBuffer!\n");
	}

	void VertexBuffer::Bind() const
	{
		glBindBuffer(GL_ARRAY_BUFFER, mHandle);
	}

	void VertexBuffer::UnBind() const
	{
		glBindBuffer(GL_ARRAY_BUFFER, 0);
	}

	vxBool VertexBuffer::IsValid() const
	{
		return (mReferences != nullptr && mHandle != 0);
	}

	VertexBuffer& VertexBuffer::operator= (const VertexBuffer& b)
	{
		try { Destroy(); } catch(...) { /* Swallow exception. */ }
		Copy(b);
		return *this;
	}

	void VertexBuffer::Destroy()
	{
		if(mReferences != nullptr)
		{
			(*mReferences)--;
			if((*mReferences) == 0)
			{
				glDeleteBuffers(1, &mHandle);
				delete mReferences;
			}
		}

		mReferences = nullptr;
		mHandle = 0;
		mVertexDesc = VertexDescription();
	}

	void VertexBuffer::Copy(const VertexBuffer& b)
	{
		// Handle self-assignment:
		if(this == &b)
			return;

		mReferences = b.mReferences;
		mHandle = b.mHandle;
		mVertexDesc = b.mVertexDesc;

		if(mReferences != nullptr)
			(*mReferences)++;
	}
}
