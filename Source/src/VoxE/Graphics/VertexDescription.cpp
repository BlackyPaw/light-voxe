/*
 * VertexDescription.cpp
 *
 *  Created on: 24.06.2014
 *      Author: euaconlabs
 */

#include <VoxE/Graphics/VertexDescription.hpp>

namespace VX
{
	vxU32 DataTypeSize(const DataType& type)
	{
		switch(type)
		{
			case DataType::Byte:
			case DataType::UnsignedByte:
				return 1;
			case DataType::Short:
			case DataType::UnsignedShort:
				return 2;
			case DataType::Int:
			case DataType::UnsignedInt:
			case DataType::Float:
				return 4;
			case DataType::Double:
				return 8;

		}
		return 0;
	}

	VertexDescription::VertexDescription() :
		mStride(0)
	{
	}

	VertexDescription::~VertexDescription()
	{
	}

	void VertexDescription::AddAttribute(const VertexAttribute& attrib)
	{
		mAttributes.push_back(attrib);
		mStride += attrib.GetElements() * DataTypeSize(attrib.GetType());
	}

	const VertexAttribute* VertexDescription::GetAttributes() const
	{
		return &mAttributes[0];
	}

	vxU32 VertexDescription::GetNumAttributes() const
	{
		return mAttributes.size();
	}

	vxU32 VertexDescription::GetStride() const
	{
		return mStride;
	}
}
