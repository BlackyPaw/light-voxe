#include <VoxE/HID/KeyUpEvent.hpp>

namespace VX
{
	KeyUpEvent::KeyUpEvent(const KeyboardKeys& key) :
		Event(GetEventID()),
		mKey(key)
	{
	}

	const KeyboardKeys& KeyUpEvent::GetKey() const
	{
		return mKey;
	}
}