#include <VoxE/HID/MouseMoveEvent.hpp>

namespace VX
{
	MouseMoveEvent::MouseMoveEvent(const Vector2i& coords) :
		Event(GetEventID()),
		mCoords(coords)
	{
	}

	const Vector2i& MouseMoveEvent::GetCoords() const
	{
		return mCoords;
	}
}