/*
 * Main.cpp
 *
 *  Created on: 04.06.2014
 *      Author: euaconlabs
 */

#include <VoxE/Core/EventManager.hpp>
#include <VoxE/Native/Time.hpp>
#include <VoxE/Resources/ResourceManager.hpp>
#include <VoxE/Systems/Core.hpp>
#include <VoxE/Systems/AudioSystem.hpp>
#include <VoxE/Systems/RenderSystem.hpp>

#include <VoxE/Scene/SpriteComponent.hpp>

using namespace VX;

#define VOXE_NO_ERROR 0
#define VOXE_CORE_ERROR 1
#define VOXE_AUDIO_ERROR 2
#define VOXE_RENDER_ERROR 3
#define VOXE_RESOURCE_ERROR 4

int main(int argc, char* argv[])
{
	// ---------------- Step 1: Initialize all subsystems:
	if(!Core::Get()->Initialize())
		return VOXE_CORE_ERROR;

	if(!AudioSystem::Get()->Initialize())
		return VOXE_AUDIO_ERROR;

	if(!RenderSystem::Get()->Initialize())
		return VOXE_RENDER_ERROR;

	if(!ResourceManager::Get()->Initialize())
		return VOXE_RESOURCE_ERROR;

	// ---------------- Step 2: The game loop:
	Time timeLastTick = Time::Now();
	Time timeNow;

	while(Core::Get()->IsRunning())
	{
		timeNow = Time::Now();
		vxF32 delta = (timeNow - timeLastTick).Seconds();
		// ----- Avoid huge game leaks when returning from inspecting the debugger:
		if(delta > 0.033f)
			delta = 0.033f;

		// ----- Flush all queued events:
		EventManager::Get()->Flush();
		// ----- Update core elements:
		Core::Get()->Update();

		// ----- More specific engine subsystems:
		AudioSystem::Get()->Update(delta);
		RenderSystem::Get()->RenderAndSwapBuffers();

		timeLastTick = timeNow;
	}

	// ---------------- Step 3: Cleanup:
	RenderSystem::Get()->Cleanup();

	return VOXE_NO_ERROR;
}
