/*
 * Plane.cpp
 *
 *  Created on: 29.07.2014
 *      Author: euaconlabs
 */

#include <VoxE/Math/Plane.hpp>

namespace VX
{
	Plane::Plane() :
		a(0.0f),
		b(0.0f),
		c(0.0f),
		d(0.0f)
	{
	}

	Plane::Plane(const Vector3f& p, const Vector3f& n)
	{
		Create(p, n);
	}

	Plane::Plane(const Vector3f& p1, const Vector3f& p2, const Vector3f& p3)
	{
		Create(p1, p2, p3);
	}

	Plane::Plane(const vxF32 a, const vxF32 b, const vxF32 c, const vxF32 d)
	{
		Create(a, b, c, d);
	}

	void Plane::Create(const Vector3f& p, const Vector3f& n)
	{
		// Normalize the normal:
		Vector3f nn = n.Normalize();
		a = nn.x;
		b = nn.y;
		c = nn.z;

		// Now calculate D, i.e. the distance from the origin:
		d = a * p.x + b * p.y + c * p.z;
	}

	void Plane::Create(const Vector3f& p1, const Vector3f& p2, const Vector3f& p3)
	{
		// Calculate the two vectors:
		Vector3f edge1 = p2 - p1;
		Vector3f edge2 = p3 - p1;

		// Now we can calculate the plane's normal:
		Vector3f n = edge1.Cross(edge2);

		// Now simply pass all necessary information to the first
		// Create()-Method:
		Create(p1, n);
	}

	void Plane::Create(const vxF32 _a, const vxF32 _b, const vxF32 _c, const vxF32 _d)
	{
		a = _a;
		b = _b;
		c = _c;
		d = _d;
	}

	vxF32 Plane::DistanceTo(const Vector3f& p) const
	{
		// We can simply omit the term std::sqrt(A*A + B*B + C*C) here as it will
		// always result in 1 since we have normalized the plane's normal everywhere.

		// Typical formula: d = ((A * p.x + B * p.y + C * p.z + D) / std::sqrt(A*A + B*B + C*C)
		return (a * p.x + b * p.y + c * p.z + d);
	}

	vxF32 Plane::DistanceTo(const Plane& p) const
	{
		// Check whether the two planes are parallel first:
		// => See ParallelTo(...) for a little discussion about this:
		if(a == p.a && b == p.b && c == p.c)
			return 0.0f;

		// Original formula: (D2 - D1) / std::sqrt(A*A, B*B, C*C).
		// As before we can omit the square root as we already know that
		// our planes' normals' are normalized:
		return (p.d - d);
	}

	vxBool Plane::ParallelTo(const Plane& p) const
	{
		// If two planes are parallel their normals have to be multiples of each other.
		// Two possibilities: Either normalize both and then compare them (expensive)
		// or check whether their cross product is equal to 0 (efficient).

		// OOORRR: We do it even better as we know we already HAVE normalized the
		// normals so we can simply compare the two vectors! :)
		return Vector3f(a, b, c) == Vector3f(p.a, p.b, p.c);
	}

	vxF32 Plane::AngleTo(const Plane& p) const
	{
		// Original formula:
		// cos(a) = std::abs(a1*a2 + b1*b2 + c1*c2) / (std::sqrt(a1*a1 + b1*b1 + c1*c1) * std::sqrt(a2*a2 + b2*b2 + c2*c2));
		//
		// We can leave out the square roots again, so we have:
		// cos(a) = std::abs(a1*a2 + b1*b2 + c1*c2).

		// If parallel, return 0.0f;
		if(a == p.a && b == p.b && c == p.c)
			return 0.0f;

		return std::acos(std::abs(a*p.a + b*p.b + c*p.c));
	}
}
