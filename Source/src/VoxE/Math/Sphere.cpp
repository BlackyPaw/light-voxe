/*
 * Sphere.cpp
 *
 *  Created on: 29.07.2014
 *      Author: euaconlabs
 */

#include <VoxE/Math/Sphere.hpp>

namespace VX
{
	Sphere::Sphere() :
		C(0.0f, 0.0f, 0.0f),
		r(1.0f)
	{
	}

	Sphere::Sphere(const Vector3f& _c, const vxF32 _r) :
		C(_c),
		r(_r)
	{
	}

	vxBool Sphere::Inside(const Vector3f& p) const
	{
		Vector3f l = C - p;
		vxF32 distSq = l.Dot(l);
		return ((distSq < (r * r)));
	}

	vxBool Sphere::OnSurface(const Vector3f& p) const
	{
		Vector3f l = C - p;
		vxF32 distSq = l.Dot(l);
		return ((distSq == (r * r)));
	}

	vxBool Sphere::Intersects(const Vector3f& p) const
	{
		Vector3f l = C - p;
		vxF32 distSq = l.Dot(l);
		return ((distSq <= (r * r)));
	}
}
