#include <VoxE/Math/Transformable.hpp>
#include <VoxE/Native/DebugPrint.hpp>

#include <cmath>
#include <cstdio>

namespace VX
{
	Transformable::Transformable() :
		mPosition(0.0f, 0.0f, 0.0f),
		mScale(1.0f, 1.0f, 1.0f),
		mPivot(0.0f, 0.0f, 0.0f),
		mTransform(1.0f),
		mUpdate(false)
	{
	}

	void Transformable::Translate(const vxF32 x, const vxF32 y, const vxF32 z)
	{
		Translate(Vector3f(x, y, z));
	}
	void Transformable::Translate(const Vector3f& trans)
	{
		mPosition += trans;
		mUpdate = true;
	}
	void Transformable::Rotate(const vxF32 pitch, const vxF32 yaw, const vxF32 roll)
	{
		Rotate(Vector3f(pitch, yaw, roll));
	}
	void Transformable::Rotate(const Vector3f& rot)
	{
		mRotation = mRotation * Quaternion(Vector3f(Radians(rot.x), Radians(rot.y), Radians(rot.z)));
		mUpdate = true;
	}
	void Transformable::Scale(const vxF32 x, const vxF32 y, const vxF32 z)
	{
		Scale(Vector3f(x, y, z));
	}
	void Transformable::Scale(const Vector3f& scale)
	{
		mScale *= scale;
		mUpdate = true;
	}

	void Transformable::SetPosition(const vxF32 x, const vxF32 y, const vxF32 z)
	{
		SetPosition(Vector3f(x, y, z));
	}
	void Transformable::SetPosition(const Vector3f& pos)
	{
		mPosition = pos;
		mUpdate = true;
	}
	void Transformable::SetRotation(const vxF32 pitch, const vxF32 yaw, const vxF32 roll)
	{
		SetRotation(Vector3f(pitch, yaw, roll));
	}
	void Transformable::SetRotation(const Vector3f& rot)
	{
		mRotation = Quaternion(rot);
		mUpdate = true;
	}
	void Transformable::SetRotation(const Quaternion& rot)
	{
		mRotation = rot;
		mUpdate = true;
	}
	void Transformable::SetScale(const vxF32 x, const vxF32 y, const vxF32 z)
	{
		SetScale(Vector3f(x, y, z));
	}
	void Transformable::SetScale(const Vector3f& scale)
	{
		mScale = scale;
		mUpdate = true;
	}
	void Transformable::SetPivot(const vxF32 x, const vxF32 y, const vxF32 z)
	{
		SetPivot(Vector3f(x, y, z));
	}
	void Transformable::SetPivot(const Vector3f& pivot)
	{
		mPivot = pivot;
		mUpdate = true;
	}

	const Vector3f& Transformable::GetPosition() const
	{
		return mPosition;
	}
	const Quaternion& Transformable::GetRotation() const
	{
		return mRotation;
	}
	const Vector3f Transformable::GetEulerAngles() const
	{
		return mRotation.EulerAngles();
	}
	const Vector3f& Transformable::GetScale() const
	{
		return mScale;
	}
	const Vector3f& Transformable::GetPivot() const
	{
		return mPivot;
	}

	FloatRect Transformable::TransformRect(const FloatRect& rect) const
	{
		const Matrix4& m = GetTransform();
		Vector2f v[4] =
		{
			Vector2f(rect.mLeft, rect.mTop),
			Vector2f(rect.mLeft + rect.mWidth, rect.mTop),
			Vector2f(rect.mLeft + rect.mWidth, rect.mTop + rect.mHeight),
			Vector2f(rect.mLeft, rect.mTop + rect.mHeight)
		};

		for (vxU32 i = 0; i < 4; ++i)
		{
			Vector4f v_i = m * Vector4f(v[i].x, v[i].y, 0.0f, 1.0f);
			v[i] = Vector2f(v_i.x, v_i.y);
		}

		vxF32 r_min = v[0].x;
		vxF32 s_min = v[0].y;
		vxF32 r_max = v[0].x;
		vxF32 s_max = v[0].y;

		for (vxU32 i = 0; i < 4; ++i)
		{
			if (v[i].x < r_min) r_min = v[i].x;
			else if (v[i].x > r_max) r_max = v[i].x;
			if (v[i].y < s_min) s_min = v[i].y;
			else if (v[i].y > s_max) s_max = v[i].y;
		}

		return FloatRect(r_min, s_min, r_max - r_min, s_max - s_min);
	}

	Transformable& Transformable::CopyTransform(const ITransformable& transformable)
	{
		mPosition = transformable.GetPosition();
		mRotation = transformable.GetRotation();
		mScale	  = transformable.GetScale();
		mPivot 	  = transformable.GetPivot();
		mUpdate	  = true;
		return 	  	*this;
	}

	const Matrix4& Transformable::GetTransform() const
	{
		if (mUpdate)
		{
			mTransform = Matrix4::Translate(mPosition) * mRotation.ToMatrix4() * Matrix4::Scale(mScale) * Matrix4::Translate(-mPivot);
			mUpdate = false;
		}
		return mTransform;
	}
}
