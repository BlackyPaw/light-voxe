/*
 * Trigonometry.cpp
 *
 *  Created on: 25.07.2014
 *      Author: euaconlabs
 */

#include <VoxE/Math/Trigonometry.hpp>

namespace VX
{
	vxF64 vxPI = 3.1415926;

	vxF32 Deg2Rad(const vxF32 a)
	{
		return (a * (static_cast<vxF32>(vxPI) / 180.0f));
	}

	vxF32 Rad2Deg(const vxF32 a)
	{
		return (a * (180.0f / static_cast<vxF32>(vxPI)));
	}

	vxF64 Deg2Rad(const vxF64 a)
	{
		return (a * (vxPI / 180.0));
	}

	vxF64 Rad2Deg(const vxF64 a)
	{
		return (a * (180.0 / vxPI));
	}
}
