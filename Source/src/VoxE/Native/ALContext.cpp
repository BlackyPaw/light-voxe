/*
 * ALContext.cpp
 *
 *  Created on: 04.07.2014
 *      Author: euaconlabs
 */

#include <VoxE/Native/ALContext.hpp>

#include <AL/alc.h>

namespace VX
{
	ALContext::ALContext() :
		mReferences(nullptr),
		mDevice(nullptr), mContext(nullptr)
	{
	}

	ALContext::ALContext(const ALContext& c) :
		mReferences(nullptr)
	{
		Copy(c);
	}

	ALContext::~ALContext()
	{
		try
		{
			Destroy();
		}
		catch(...)
		{
			/* Swallow exception. */
		}
	}

	ALContext& ALContext::operator= (const ALContext& c)
	{
		Copy(c);
		return *this;
	}

	void ALContext::Create()
	{
		try { Destroy(); } catch(...) { /* Swallow exception. */ }

		ALCdevice* device;
		ALCcontext* context;

		device = alcOpenDevice(0);
		if(!device)
			return;

		context = alcCreateContext(device, 0);
		if(!context)
		{
			alcCloseDevice(device);
			return;
		}

		alcMakeContextCurrent(context);

		mReferences = new vxU32(1);
		mDevice = device;
		mContext = context;
	}

	void ALContext::MakeCurrent()
	{
		alcMakeContextCurrent(static_cast<ALCcontext*>(mContext));
	}

	const vxBool ALContext::IsValid() const
	{
		return (mReferences != nullptr && mDevice != nullptr && mContext != nullptr);
	}

	void ALContext::Copy(const ALContext& c)
	{
		// Handle self-assignment:
		if(this == &c)
			return;

		try { Destroy(); } catch(...) { /* Swallow exception. */ }

		mReferences = c.mReferences;
		mDevice = c.mDevice;
		mContext = c.mContext;

		if(mReferences != nullptr)
			(*mReferences)++;
	}

	void ALContext::Destroy()
	{
		if(mReferences != nullptr)
		{
			(*mReferences)--;
			if((*mReferences) == 0)
			{
				alcMakeContextCurrent(0);
				alcDestroyContext(static_cast<ALCcontext*>(mContext));
				alcCloseDevice(static_cast<ALCdevice*>(mDevice));
				delete mReferences;
			}
		}

		mReferences = nullptr;
		mDevice = nullptr;
		mContext = nullptr;
	}
}
