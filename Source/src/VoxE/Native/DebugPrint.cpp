#include <VoxE/Native/DebugPrint.hpp>
#include <cstdio>

namespace VX
{
	void __DebugPrintF__(const char* format, va_list argList)
	{
#if defined(VOXE_DEBUG)
		static const vxU32 kMaxCharacters = 5119;
		static vxChar kBuffer[kMaxCharacters + 1];

		vsnprintf(kBuffer, kMaxCharacters, format, argList);
		kBuffer[kMaxCharacters] = '\0';

		NativeDebugOutput(kBuffer);
#endif // VOXE_DEBUG
	}

	void DebugPrint(const char* format, ...)
	{
#if defined(VOXE_DEBUG)
		va_list argList;
		va_start(argList, format);
		__DebugPrintF__(format, argList);
		va_end(argList);
#endif // VOXE_DEBUG
	}
}
