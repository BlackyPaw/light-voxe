#include <VoxE/Native/DelegateThread.hpp>

namespace VX
{
	DelegateThread::DelegateThread()
	{
	}

	DelegateThread::DelegateThread(const Runnable& runnable) :
		mRunnable(runnable)
	{
	}

	void DelegateThread::SetRunnable(const Runnable& runnable)
	{
		mRunnable = runnable;
	}

	vxU32 DelegateThread::Run()
	{
		if (mRunnable.Empty())
			return kInvalidReturnCode;
		return mRunnable.Invoke();
	}
}