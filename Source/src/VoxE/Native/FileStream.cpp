#include <VoxE/Native/FileStream.hpp>
#include <cstdio>

namespace VX
{
	//////////////////////////////////////////////////////////////
	// FileInputStream
	//////////////////////////////////////////////////////////////
	FileInputStream::FileInputStream() :
		mReferences(nullptr),
		mFile(nullptr)
	{
	}

	FileInputStream::FileInputStream(const File& file) :
		mReferences(nullptr)
	{
		Open(file);
	}

	FileInputStream::FileInputStream(const FileInputStream& in) :
		mReferences(nullptr),
		mFile(nullptr)
	{
		Copy(in);
	}

	FileInputStream::~FileInputStream()
	{
		Destroy();
	}

	vxBool FileInputStream::Open(const File& file)
	{
		Destroy();

		FILE* f = fopen(file.GetPath().ToString().c_str(), "rb");
		if (f == nullptr)
			return false;

		mReferences = new vxU32(1);
		mFile = f;

		return true;
	}

	vxU32 FileInputStream::Read(const vxU32 len, void* buffer)
	{
		if (mFile == nullptr)
			return 0;

		return fread(buffer, 1, len, static_cast<FILE*>(mFile));
	}

	vxU32 FileInputStream::GetCur() const
	{
		if (mFile != nullptr)
			return ftell(static_cast<FILE*>(mFile));
		return 0;
	}

	void FileInputStream::Seek(const StreamSeekDir& dir, const vxI32 off)
	{
		if (mFile != nullptr)
			fseek(static_cast<FILE*>(mFile), off, (dir == STREAM_SEEK_CUR ? SEEK_CUR : (dir == STREAM_SEEK_END ? SEEK_END : SEEK_SET)));
	}

	void FileInputStream::Close()
	{
		Destroy();
	}

	vxBool FileInputStream::EndOf() const
	{
		if (mFile != nullptr)
			return (feof(static_cast<FILE*>(mFile)) != 0);
		return true;
	}

	vxBool FileInputStream::IsOpen() const
	{
		return (mFile != nullptr);
	}

	FileInputStream& FileInputStream::operator= (const FileInputStream& in)
	{
		Copy(in);
		return *this;
	}

	void FileInputStream::Copy(const FileInputStream& in)
	{
		Destroy();

		mReferences = in.mReferences;
		mFile = in.mFile;

		if (mReferences != nullptr)
			(*mReferences)++;
	}

	void FileInputStream::Destroy()
	{
		if (mReferences != nullptr)
		{
			(*mReferences)--;
			if ((*mReferences) == 0)
			{
				delete mReferences;
				if (mFile != nullptr)
					fclose(static_cast<FILE*>(mFile));
			}
		}

		mReferences = nullptr;
		mFile = nullptr;
	}

	//////////////////////////////////////////////////////////////
	// FileOutputStream
	//////////////////////////////////////////////////////////////
	FileOutputStream::FileOutputStream() :
		mReferences(nullptr),
		mFile(nullptr)
	{
	}

	FileOutputStream::FileOutputStream(const File& file) :
		mReferences(nullptr)
	{
		Open(file);
	}

	FileOutputStream::FileOutputStream(const FileOutputStream& out) :
		mReferences(nullptr),
		mFile(nullptr)
	{
		Copy(out);
	}

	FileOutputStream::~FileOutputStream()
	{
		Destroy();
	}

	vxBool FileOutputStream::Open(const File& file)
	{
		Destroy();

		FILE* f = fopen(file.GetPath().ToString().c_str(), "wb");
		if (f == nullptr)
			return false;

		mReferences = new vxU32(1);
		mFile = f;

		return true;
	}

	vxU32 FileOutputStream::Write(const vxU32 len, const void* buffer)
	{
		if (mFile == nullptr)
			return 0;
		return fwrite(buffer, 1, len, static_cast<FILE*>(mFile));
	}

	vxU32 FileOutputStream::GetCur() const
	{
		if (mFile != nullptr)
			return ftell(static_cast<FILE*>(mFile));
		return 0;
	}

	void FileOutputStream::Seek(const StreamSeekDir& dir, const vxI32 off)
	{
		if (mFile != nullptr)
			fseek(static_cast<FILE*>(mFile), off, (dir == STREAM_SEEK_CUR ? SEEK_CUR : (dir == STREAM_SEEK_END ? SEEK_END : SEEK_SET)));
	}

	void FileOutputStream::Close()
	{
		Destroy();
	}

	vxBool FileOutputStream::IsOpen() const
	{
		return (mFile != nullptr);
	}
	
	FileOutputStream& FileOutputStream::operator= (const FileOutputStream& out)
	{
		Copy(out);
		return *this;
	}

	void FileOutputStream::Copy(const FileOutputStream& out)
	{
		Destroy();

		mReferences = out.mReferences;
		mFile = out.mFile;

		if (mReferences != nullptr)
			(*mReferences)++;
	}

	void FileOutputStream::Destroy()
	{
		if (mReferences != nullptr)
		{
			(*mReferences)--;
			if ((*mReferences) == 0)
			{
				delete mReferences;
				if (mFile != nullptr)
					fclose(static_cast<FILE*>(mFile));
			}
		}

		mReferences = nullptr;
		mFile = nullptr;
	}
}