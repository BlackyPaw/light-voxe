#include <VoxE/Native/MemoryStream.hpp>

#include <cstring>

namespace VX
{
	MemoryInputStream::MemoryInputStream() :
		mReferences(nullptr),
		mMode(0),
		mBuffer(nullptr),
		mSize(0),
		mCur(0)
	{
	}

	MemoryInputStream::MemoryInputStream(const MemoryInputStream& in) :
		mReferences(nullptr),
		mMode(0),
		mBuffer(nullptr),
		mSize(0),
		mCur(0)
	{
		Copy(in);
	}

	MemoryInputStream::~MemoryInputStream()
	{
		Destroy();
	}

	vxBool MemoryInputStream::Open(const void* buffer, const vxU32 size, const vxU32 mode)
	{
		Destroy();

		mReferences = new vxU32(1);
		mBuffer = buffer;
		mSize = size;
		mMode = mode;
		mCur = 0;

		return true;
	}

	vxU32 MemoryInputStream::Read(const vxU32 len, void* buffer)
	{
		if (EndOf())
			return 0;

		vxU32 n = (len < (mSize - mCur) ? len : (mSize - mCur));
		std::memcpy(buffer, (const void*)(reinterpret_cast<const vxU8*>(mBuffer) + mCur), static_cast<vxU64>(n));
		mCur += n;

		return n;
	}

	vxU32 MemoryInputStream::GetCur() const
	{
		return mCur;
	}

	void MemoryInputStream::Seek(const StreamSeekDir& dir, const vxI32 off)
	{
		switch (dir)
		{
			case STREAM_SEEK_BEGIN:
				mCur = off;
				break;
			case STREAM_SEEK_CUR:
				mCur += off;
				break;
			case STREAM_SEEK_END:
				mCur = mSize + off;
				break;
		}
	}

	void MemoryInputStream::Close()
	{
		Destroy();
	}

	vxBool MemoryInputStream::EndOf() const
	{
		return (mCur == mSize);
	}

	MemoryInputStream& MemoryInputStream::operator= (const MemoryInputStream& in)
	{
		Copy(in);
		return *this;
	}

	void MemoryInputStream::Copy(const MemoryInputStream& in)
	{
		Destroy();

		mReferences = in.mReferences;
		mMode = in.mMode;
		mBuffer = in.mBuffer;
		mSize = in.mSize;
		mCur = in.mCur;

		if (mReferences != nullptr)
			(*mReferences)++;
	}

	void MemoryInputStream::Destroy()
	{
		if (mReferences != nullptr)
		{
			(*mReferences)--;
			if ((*mReferences) == 0)
			{
				delete mReferences;
				if ((mMode & MEMORY_STREAM_NO_DISCARD) != MEMORY_STREAM_NO_DISCARD)
					delete[] reinterpret_cast<const vxU8*>(mBuffer);
			}
		}

		mReferences = nullptr;
		mMode = 0;
		mBuffer = nullptr;
		mSize = 0;
		mCur = 0;
	}
}
