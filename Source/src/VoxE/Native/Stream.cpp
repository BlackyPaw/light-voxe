#include <VoxE/Core/BinUtil.hpp>
#include <VoxE/Native/Stream.hpp>

namespace VX
{
	Stream::Stream()
	{
	}

	Stream::~Stream()
	{
	}

	InputStream::InputStream()
	{
	}

	InputStream::~InputStream()
	{
	}

	vxU16 InputStream::ReadLE16()
	{
		vxU16 v;
		Read(sizeof(vxU16), &v);
		if (isBigEndian())
			v = swap16(v);
		return v;
	}

	vxU32 InputStream::ReadLE32()
	{
		vxU32 v;
		Read(sizeof(vxU32), &v);
		if (isBigEndian())
			v = swap32(v);
		return v;
	}

	vxU64 InputStream::ReadLE64()
	{
		vxU64 v;
		Read(sizeof(vxU64), &v);
		if (isBigEndian())
			v = swap64(v);
		return v;
	}

	vxU16 InputStream::ReadBE16()
	{
		vxU16 v;
		Read(sizeof(vxU16), &v);
		if (isLittleEndian())
			v = swap16(v);
		return v;
	}

	vxU32 InputStream::ReadBE32()
	{
		vxU32 v;
		Read(sizeof(vxU32), &v);
		if (isLittleEndian())
			v = swap32(v);
		return v;
	}

	vxU64 InputStream::ReadBE64()
	{
		vxU64 v;
		Read(sizeof(vxU64), &v);
		if (isLittleEndian())
			v = swap64(v);
		return v;
	}

	OutputStream::OutputStream()
	{
	}

	OutputStream::~OutputStream()
	{
	}

	void OutputStream::WriteLE16(vxU16 c)
	{
		if (isBigEndian())
			c = swap16(c);
		Write(sizeof(vxU16), &c);
	}

	void OutputStream::WriteLE32(vxU32 c)
	{
		if (isBigEndian())
			c = swap32(c);
		Write(sizeof(vxU32), &c);
	}

	void OutputStream::WriteLE64(vxU64 c)
	{
		if (isBigEndian())
			c = swap64(c);
		Write(sizeof(vxU64), &c);
	}

	void OutputStream::WriteBE16(vxU16 c)
	{
		if (isLittleEndian())
			c = swap16(c);
		Write(sizeof(vxU16), &c);
	}

	void OutputStream::WriteBE32(vxU32 c)
	{
		if (isLittleEndian())
			c = swap32(c);
		Write(sizeof(vxU32), &c);
	}

	void OutputStream::WriteBE64(vxU64 c)
	{
		if (isLittleEndian())
			c = swap64(c);
		Write(sizeof(vxU64), &c);
	}
}