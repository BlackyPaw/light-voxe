/*
 * UnixDebugPrintImpl.cpp
 *
 *  Created on: 11.05.2014
 *      Author: euaconlabs
 */

#include <VoxE/Native/Platform.hpp>

#if defined(VOXE_PLATFORM_UNIX)

#include <VoxE/Native/DebugPrint.hpp>
#include <cstdio>

namespace VX
{
	void NativeDebugOutput(char const* msg)
	{
		std::printf(msg);
	}
}

#endif // VOXE_PLATFORM_UNIX
