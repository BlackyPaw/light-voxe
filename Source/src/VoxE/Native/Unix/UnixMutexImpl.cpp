/*
 * UnixMutexImpl.cpp
 *
 *  Created on: 11.05.2014
 *      Author: euaconlabs
 */

#include <VoxE/Native/Platform.hpp>

#if defined(VOXE_PLATFORM_UNIX)

#include <VoxE/Native/Mutex.hpp>

namespace VX
{
	Mutex::Mutex() :
		mReferences(nullptr)
	{
		pthread_mutex_init(&mHandle, NULL);
		mReferences = new vxU32(1);
	}

	Mutex::Mutex(const Mutex& m) :
		mReferences(nullptr)
	{
		Copy(m);
	}

	Mutex& Mutex::operator= (const Mutex& m)
	{
		Copy(m);
		return *this;
	}

	void Mutex::Copy(const Mutex& m)
	{
		Destroy();

		mReferences = m.mReferences;
		mHandle = m.mHandle;

		if(mReferences != nullptr)
			(*mReferences)++;
	}

	void Mutex::Destroy()
	{
		if(mReferences != nullptr)
		{
			(*mReferences)--;
			if((*mReferences) == 0)
			{
				delete mReferences;
				pthread_mutex_destroy(&mHandle);
			}
		}

		mReferences = nullptr;
		mHandle = pthread_mutex_t();
	}

	Mutex::~Mutex()
	{
		Destroy();
	}

	void Mutex::Lock()
	{
		pthread_mutex_lock(&mHandle);
	}

	vxBool Mutex::TryLock()
	{
		return (pthread_mutex_trylock(&mHandle) == 0);
	}

	void Mutex::Release()
	{
		pthread_mutex_unlock(&mHandle);
	}
}

#endif // VOXE_PLATFORM_UNIX
