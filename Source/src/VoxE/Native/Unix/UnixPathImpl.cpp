/*
 * UnixPathImpl.cpp
 *
 *  Created on: 11.05.2014
 *      Author: euaconlabs
 */

#include <VoxE/Native/Platform.hpp>

#if defined(VOXE_PLATFORM_UNIX)

#include <VoxE/Native/Path.hpp>
#include <linux/limits.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <vector>

namespace VX
{
	Path::Path() :
		mPath("")
	{
	}

	Path::Path(const vxString& path) :
		mPath(path)
	{
	}

	vxBool Path::Exists() const
	{
		struct stat st;
		if(lstat(mPath.c_str(), &st) != 0)
		{
			if(errno == ENOENT)
				return false;
		}
		return true;
	}

	vxBool Path::IsDirectory() const
	{
		struct stat st;
		if(lstat(mPath.c_str(), &st) != 0)
			return false;
		return (S_ISDIR(st.st_mode));
	}

	vxBool Path::IsFile() const
	{
		struct stat st;
		if(lstat(mPath.c_str(), &st) != 0)
			return false;
		return (S_ISREG(st.st_mode));
	}

	Path Path::Canonicalize() const
	{
		std::vector<vxChar> buffer(PATH_MAX);
		realpath(&mPath.c_str()[0], &buffer[0]);
		vxString path = vxString(&buffer[0]);
		return Path(path.substr(0, path.find('\0')));
	}

	vxString Path::GetFileExtension() const
	{
		vxU32 n = mPath.find_last_of('.');
		if(n == mPath.npos)
			return "";
		return mPath.substr(n);
	}

	vxString Path::GetFileName() const
	{
		vxU32 n = mPath.find_last_not_of('/');
		if(n == mPath.npos)
			return mPath;
		return mPath.substr(n+1);
	}

	vxBool Path::IsRelative() const
	{
		if(mPath.size() == 0)
			return false;
		return (mPath[0] == '/' ? true : false);
	}

	Path Path::FullyQualifiedPath() const
	{
		std::vector<vxChar> buffer(PATH_MAX);
		realpath(&mPath.c_str()[0], &buffer[0]);
		vxString path = vxString(&buffer[0]);
		return Path(path.substr(0, path.find('\0')));
	}

	const vxString& Path::ToString() const
	{
		return mPath;
	}
}

#endif // VOXE_PLATFORM_UNIX
