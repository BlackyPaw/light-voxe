/*
 * UnixSleepImpl.cpp
 *
 *  Created on: 11.05.2014
 *      Author: euaconlabs
 */

#include <VoxE/Native/Platform.hpp>

#if defined(VOXE_PLATFORM_UNIX)

#include <VoxE/Native/Sleep.hpp>
#include <unistd.h>

namespace VX
{
	void Sleep(const vxU32 milliseconds)
	{
		usleep(milliseconds * 1000);
	}
}

#endif // VOXE_PLATFORM_UNIX
