/*
 * UnixThreadImpl.cpp
 *
 *  Created on: 11.05.2014
 *      Author: euaconlabs
 */

#include <VoxE/Native/Platform.hpp>

#if defined(VOXE_PLATFORM_UNIX)

#include <VoxE/Native/Thread.hpp>
#include <cstdint>
#include <pthread.h>

namespace VX
{
	void* UnixThreadEntryPoint(void* arg)
	{
		pthread_setcanceltype(PTHREAD_CANCEL_ASYNCHRONOUS, NULL);

		Thread* pThread = (Thread*)arg;
		if(pThread == nullptr)
			return (void*)0;

		vxU32 retCode = pThread->Run();

		pthread_mutex_lock(&pThread->mMutex);
		pThread->mIsRunning = false;
		pthread_mutex_unlock(&pThread->mMutex);
		pthread_exit((vxU32*)(std::uintptr_t)retCode);
		return (vxU32*)(std::uintptr_t)retCode;
	}

	Thread::Thread() :
		mHandle(0),
		mIsRunning(false)
	{
	}

	Thread::~Thread()
	{
		Terminate();
		pthread_mutex_destroy(&mMutex);
	}

	void Thread::Start()
	{
		if(mHandle != 0)
			return;

		pthread_attr_t attr;
		pthread_attr_init(&attr);
		pthread_attr_setdetachstate(&attr, PTHREAD_CREATE_JOINABLE);

		pthread_create(&mHandle, &attr, UnixThreadEntryPoint, (void*)this);
		if(mHandle == 0)
			return;

		mIsRunning = true;
		pthread_mutex_init(&mMutex, NULL);

		pthread_attr_destroy(&attr);
	}

	void Thread::Wait()
	{
		if(mHandle != 0)
		{
			void* status;
			pthread_join(mHandle, &status);
		}
	}

	vxU32 Thread::Run()
	{
		return 0;
	}

	void Thread::Terminate()
	{
		if(mHandle != 0)
		{
			pthread_cancel(mHandle);
			mIsRunning = false;
		}
	}

	vxBool Thread::IsRunning() const
	{
		vxBool res = false;
		pthread_mutex_lock(&mMutex);
		res = mIsRunning;
		pthread_mutex_unlock(&mMutex);
		return res;
	}

	vxU32 Thread::GetExitCode() const
	{
		return kInvalidReturnCode;
	}
}

#endif // VOXE_PLATFORM_UNIX
