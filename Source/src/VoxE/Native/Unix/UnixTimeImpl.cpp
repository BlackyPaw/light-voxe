/*
 * UnixTimeImpl.cpp
 *
 *  Created on: 11.05.2014
 *      Author: euaconlabs
 */

#include <VoxE/Native/Platform.hpp>

#if defined(VOXE_PLATFORM_UNIX)

#include <VoxE/Native/Time.hpp>
#include <time.h>

namespace VX
{
	Time Time::Now()
	{
		timespec time;
		clock_gettime(CLOCK_MONOTONIC, &time);
		vxU64 microseconds = (static_cast<vxU64>(time.tv_sec) * 1000000 + static_cast<vxU64>(time.tv_nsec) / 1000);
		return Time(microseconds);
	}
}

#endif // VOXE_PLATFORM_UNIX
