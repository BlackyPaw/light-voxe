#include <VoxE/Native/Platform.hpp>

#if defined(VOXE_PLATFORM_UNIX)

#include <VoxE/Core/EventManager.hpp>
#include <VoxE/HID/KeyDownEvent.hpp>
#include <VoxE/HID/KeyUpEvent.hpp>
#include <VoxE/HID/MouseDownEvent.hpp>
#include <VoxE/HID/MouseUpEvent.hpp>
#include <VoxE/HID/MouseMoveEvent.hpp>
#include <VoxE/Native/GLContext.hpp>
#include <VoxE/Native/Window.hpp>
#include <VoxE/Native/WindowSizeEvent.hpp>

// X11
#include <X11/Xlib.h>
#include <X11/Xutil.h>
#include <X11/keysym.h>

// STL
#include <cstring>
#include <vector>

namespace VX
{

	KeyboardKeys TranslateKeySym(const KeySym& sym)
	{
		KeySym lower, upper;
		XConvertCase(sym, &lower, &upper);
		switch(upper)
		{
			case XK_A: return KEYBOARD_KEY_A;
			case XK_B: return KEYBOARD_KEY_B;
			case XK_C: return KEYBOARD_KEY_C;
			case XK_D: return KEYBOARD_KEY_D;
			case XK_E: return KEYBOARD_KEY_E;
			case XK_F: return KEYBOARD_KEY_F;
			case XK_G: return KEYBOARD_KEY_G;
			case XK_H: return KEYBOARD_KEY_H;
			case XK_I: return KEYBOARD_KEY_I;
			case XK_J: return KEYBOARD_KEY_J;
			case XK_K: return KEYBOARD_KEY_K;
			case XK_L: return KEYBOARD_KEY_L;
			case XK_M: return KEYBOARD_KEY_M;
			case XK_N: return KEYBOARD_KEY_N;
			case XK_O: return KEYBOARD_KEY_O;
			case XK_P: return KEYBOARD_KEY_P;
			case XK_Q: return KEYBOARD_KEY_Q;
			case XK_R: return KEYBOARD_KEY_R;
			case XK_S: return KEYBOARD_KEY_S;
			case XK_T: return KEYBOARD_KEY_T;
			case XK_U: return KEYBOARD_KEY_U;
			case XK_V: return KEYBOARD_KEY_V;
			case XK_W: return KEYBOARD_KEY_W;
			case XK_X: return KEYBOARD_KEY_X;
			case XK_Y: return KEYBOARD_KEY_Y;
			case XK_Z: return KEYBOARD_KEY_Z;
			case XK_0: return KEYBOARD_KEY_0;
			case XK_1: return KEYBOARD_KEY_1;
			case XK_2: return KEYBOARD_KEY_2;
			case XK_3: return KEYBOARD_KEY_3;
			case XK_4: return KEYBOARD_KEY_4;
			case XK_5: return KEYBOARD_KEY_5;
			case XK_6: return KEYBOARD_KEY_6;
			case XK_7: return KEYBOARD_KEY_7;
			case XK_8: return KEYBOARD_KEY_8;
			case XK_9: return KEYBOARD_KEY_9;
			case XK_KP_0: return KEYBOARD_KEY_NUM_0;
			case XK_KP_1: return KEYBOARD_KEY_NUM_1;
			case XK_KP_2: return KEYBOARD_KEY_NUM_2;
			case XK_KP_3: return KEYBOARD_KEY_NUM_3;
			case XK_KP_4: return KEYBOARD_KEY_NUM_4;
			case XK_KP_5: return KEYBOARD_KEY_NUM_5;
			case XK_KP_6: return KEYBOARD_KEY_NUM_6;
			case XK_KP_7: return KEYBOARD_KEY_NUM_7;
			case XK_KP_8: return KEYBOARD_KEY_NUM_8;
			case XK_KP_9: return KEYBOARD_KEY_NUM_9;
			case XK_F1: return KEYBOARD_KEY_F1;
			case XK_F2: return KEYBOARD_KEY_F2;
			case XK_F3: return KEYBOARD_KEY_F3;
			case XK_F4: return KEYBOARD_KEY_F4;
			case XK_F5: return KEYBOARD_KEY_F5;
			case XK_F6: return KEYBOARD_KEY_F6;
			case XK_F7: return KEYBOARD_KEY_F7;
			case XK_F8: return KEYBOARD_KEY_F8;
			case XK_F9: return KEYBOARD_KEY_F9;
			case XK_F10: return KEYBOARD_KEY_F10;
			case XK_F11: return KEYBOARD_KEY_F11;
			case XK_F12: return KEYBOARD_KEY_F12;

			case XK_Escape: return KEYBOARD_KEY_ESC;
			case XK_Control_L: return KEYBOARD_KEY_LCTRL;
			case XK_Control_R: return KEYBOARD_KEY_RCTRL;
			case XK_Shift_L: return KEYBOARD_KEY_LSHIFT;
			case XK_Shift_R: return KEYBOARD_KEY_RSHIFT;
			case XK_space: return KEYBOARD_KEY_SPACE;
			case XK_BackSpace: return KEYBOARD_KEY_BACKSPACE;
			/* Enter / Return has got two variants: Keyboard + Numpad: */
			case XK_Return: return KEYBOARD_KEY_ENTER;
			case XK_KP_Enter: return KEYBOARD_KEY_ENTER;
			case XK_Tab: return KEYBOARD_KEY_TAB;
			case XK_Left: return KEYBOARD_KEY_LEFT;
			case XK_Up: return KEYBOARD_KEY_UP;
			case XK_Right: return KEYBOARD_KEY_RIGHT;
			case XK_Down: return KEYBOARD_KEY_DOWN;
			case XK_Clear: return KEYBOARD_KEY_CLEAR;
			case XK_Pause: return KEYBOARD_KEY_PAUSE;
			case XK_Prior: return KEYBOARD_KEY_PAGE_UP;
			case XK_Next: return KEYBOARD_KEY_PAGE_DOWN;
			case XK_End: return KEYBOARD_KEY_END;
			case XK_Home: return KEYBOARD_KEY_HOME;
			case XK_Select: return KEYBOARD_KEY_SELECT;
			case XK_Print: return KEYBOARD_KEY_PRINT;
			case XK_Execute: return KEYBOARD_KEY_EXECUTE;
			case XK_Insert: return KEYBOARD_KEY_INSERT;
			case XK_Delete: return KEYBOARD_KEY_DELETE;
			case XK_Help: return KEYBOARD_KEY_HELP;
			case XK_KP_Multiply: return KEYBOARD_KEY_MULTIPLY;
			case XK_KP_Add: return KEYBOARD_KEY_ADD;
			case XK_KP_Subtract: return KEYBOARD_KEY_SUBTRACT;
			case XK_KP_Divide: return KEYBOARD_KEY_DIVIDE;
			case XK_KP_Separator: return  KEYBOARD_KEY_SEPARATOR;
			case XK_KP_Decimal: return KEYBOARD_KEY_DECIMAL;
			case XK_Num_Lock: return KEYBOARD_KEY_NUM_LOCK;
			case XK_Scroll_Lock: return KEYBOARD_KEY_SCROLL_LOCK;
			case XK_plus: return KEYBOARD_KEY_PLUS;
			case XK_comma: return KEYBOARD_KEY_COMMA;
			case XK_minus: return KEYBOARD_KEY_MINUS;
			case XK_period: return KEYBOARD_KEY_PERIOD;
			case XK_semicolon: return KEYBOARD_KEY_SEMICOLON;
			case XK_slash: return KEYBOARD_KEY_SLASH;
			case XK_dead_grave: return KEYBOARD_KEY_TILDE;
			case XK_bracketleft: return KEYBOARD_KEY_OPEN_BRACKET;
			case XK_bracketright: return KEYBOARD_KEY_CLOSE_BRACKET;
			case XK_backslash: return KEYBOARD_KEY_BACKSLASH;
			case XK_dead_acute: return KEYBOARD_KEY_QUOTES;
		}
		return KEYBOARD_NUM_KEYS;
	}

    Window::Window() :
    	mReferences(nullptr)
    {
    	// Init with an empty object:
    	Destroy();
    }

    Window::Window(const Window& wnd) :
    	mReferences(nullptr)
    {
    	Copy(wnd);
    }

    Window::~Window()
    {
    	Destroy();
    }

    void Window::Create(const GLContextAttributes& attribs)
    {
    	mReferences = new vxU32(1);

    	// Open the display:
    	mDisplay = XOpenDisplay(0);
    	mScreen = DefaultScreen(mDisplay);

    	// Create the window:
    	WindowHandle root = RootWindow(mDisplay, mScreen);
    	XSetWindowAttributes swa;
    	swa.event_mask = KeyPressMask | KeyReleaseMask |
    					 ButtonPressMask | ButtonReleaseMask | PointerMotionMask |
    					 StructureNotifyMask;

    	// Get the best visual for our window and the given attributes:
    	Visual* visual; XVisualInfo* bestVisual = reinterpret_cast<XVisualInfo*>(GLContext::FindBestVisual(mDisplay, attribs));
    	if(bestVisual != nullptr)
    	{
    		visual = bestVisual->visual;
    		delete bestVisual;
    	}
    	else
    		visual = DefaultVisual(mDisplay, mScreen);

		swa.colormap = XCreateColormap(mDisplay, RootWindow(mDisplay, mScreen), visual, AllocNone);
		mHandle = XCreateWindow(mDisplay, root,
								  1, 1, 500, 500, 0,
								  DefaultDepth(mDisplay, mScreen),
								  InputOutput,
								  visual,
								  CWColormap | CWEventMask, &swa);

		// Create the close atom used for intercepting close events:
		mCloseAtom = XInternAtom(mDisplay, "WM_DELETE_WINDOW", False);
		XSetWMProtocols(mDisplay, mHandle, &mCloseAtom, 1);

		// Create a hidden cursor which won't show if we set the mouse cursor to be invisble.
		// X11 does not support to hide cursors so we do a workaround which creates an empty
		// pixmap and use it as the cursor if we want to hide it.
		Pixmap cursor_pixmap = XCreatePixmap(mDisplay, mHandle, 1, 1, 1);
		GC gc = XCreateGC(mDisplay, cursor_pixmap, 0, 0);
		XDrawPoint(mDisplay, cursor_pixmap, gc, 0, 0);
		XFreeGC(mDisplay, gc);

		XColor color;
		color.flags = DoRed | DoGreen | DoBlue;
		color.red = 0;
		color.green = 0;
		color.blue = 0;
		mCursor = XCreatePixmapCursor(mDisplay, cursor_pixmap, cursor_pixmap, &color, &color, 0, 0);

		// Don't forget to avoid memory leaks:
		XFreePixmap(mDisplay, cursor_pixmap);

		mContext = GLContext();
		mContext.Create(mHandle, attribs);

		mIsOpen = true;

		mLastSize = GetSize();
    }

    void Window::Update()
    {
    	XEvent evt;
    	while(XPending(mDisplay))
    	{
    		XNextEvent(mDisplay, &evt);

    		if(evt.xany.window != mHandle)
    			continue;

    		// Check for possible KeyEvent duplicates:
    		if(evt.type == KeyRelease && XEventsQueued(mDisplay, QueuedAfterReading) > 0)
    		{
    			XEvent next;
    			// Peek the next event without removing it from the queue:
    			XPeekEvent(mDisplay, &next);
    			// Now check, if the event is just a simple duplicate:
    			if(next.xany.window == mHandle && next.type == KeyPress && next.xkey.time == evt.xkey.time && next.xkey.keycode == evt.xkey.keycode)
    			{
    				// Remove this event from the queue and also skip the KeyRelease event:
    				XNextEvent(mDisplay, &next);
    				continue;
    			}
    		}

			// Close event:
			if(evt.type == ClientMessage)
			{
				if((AtomHandle)evt.xclient.data.l[0] == mCloseAtom)
				{
					mIsOpen = false;
					continue;
				}
			}
			// Resize event:
			else if(evt.type == ConfigureNotify)
			{
				if(static_cast<vxU32>(evt.xconfigure.width) != mLastSize.x ||
				   static_cast<vxU32>(evt.xconfigure.height) != mLastSize.y)
				{
					mLastSize = Vector2ui(static_cast<vxU32>(evt.xconfigure.width),
										   static_cast<vxU32>(evt.xconfigure.height));
					WindowSizeEvent* pEvent = new WindowSizeEvent(mLastSize);
					EventManager::Get()->QueueEvent(EventPtr(pEvent));
				}
			}
			// KeyPress event:
			else if(evt.type == KeyPress)
			{
				vxChar buffer[128];
				KeySym sym;
				XLookupString(&evt.xkey, buffer, 32, &sym, 0);
				KeyboardKeys key = TranslateKeySym(sym);
				KeyDownEvent* pEvent = new KeyDownEvent(key);
				EventManager::Get()->QueueEvent(EventPtr(pEvent));
			}
			// KeyRelease event:
			else if(evt.type == KeyRelease)
			{
				vxChar buffer[128];
				KeySym sym;
				XLookupString(&evt.xkey, buffer, 32, &sym, 0);
				KeyboardKeys key = TranslateKeySym(sym);
				KeyUpEvent* pEvent = new KeyUpEvent(key);
				EventManager::Get()->QueueEvent(EventPtr(pEvent));
			}
			// ButtonPress event:
			else if(evt.type == ButtonPress)
			{
				MouseButtons button = MOUSE_NUM_BUTTONS;
				switch(evt.xbutton.button)
				{
					case Button1: button = MOUSE_BUTTON_LEFT; break;
					case Button2: button = MOUSE_BUTTON_MIDDLE; break;
					case Button3: button = MOUSE_BUTTON_RIGHT; break;
				}
				if(button != MOUSE_NUM_BUTTONS)
				{
					Vector2i coords = Vector2i(evt.xbutton.x, evt.xbutton.y);
					MouseDownEvent* pEvent = new MouseDownEvent(button, coords);
					EventManager::Get()->QueueEvent(EventPtr(pEvent));
				}
			}
			// ButtonRelease event:
			else if(evt.type == ButtonRelease)
			{
				MouseButtons button = MOUSE_NUM_BUTTONS;
				switch(evt.xbutton.button)
				{
					case Button1: button = MOUSE_BUTTON_LEFT; break;
					case Button2: button = MOUSE_BUTTON_MIDDLE; break;
					case Button3: button = MOUSE_BUTTON_RIGHT; break;
				}
				if(button != MOUSE_NUM_BUTTONS)
				{
					Vector2i coords = Vector2i(evt.xbutton.x, evt.xbutton.y);
					MouseUpEvent* pEvent = new MouseUpEvent(button, coords);
					EventManager::Get()->QueueEvent(EventPtr(pEvent));
				}
			}
			// MouseMove event:
			else if(evt.type == MotionNotify)
			{
				Vector2i coords = Vector2i(evt.xmotion.x, evt.xmotion.y);
				MouseMoveEvent* pEvent = new MouseMoveEvent(coords);
				EventManager::Get()->QueueEvent(EventPtr(pEvent));
			}
    	}
    }

    void Window::Show(const vxBool show)
    {
    	if(show)
    		XMapWindow(mDisplay, mHandle);
		else
			XUnmapWindow(mDisplay, mHandle);
		XFlush(mDisplay);
    }

    Vector2i Window::GetPosition() const
    {
    	WindowHandle root, child;
    	vxI32 lx, x, ly, y;
    	vxU32 w, h, b, d;
		XGetGeometry(mDisplay, mHandle, &root, &lx, &ly, &w, &h, &b, &d);
		XTranslateCoordinates(mDisplay, mHandle, root, lx, ly, &x, &y, &child);
		return Vector2i(x, y);
    }

    Vector2ui Window::GetSize() const
    {
    	XWindowAttributes wa;
    	XGetWindowAttributes(mDisplay, mHandle, &wa);
    	return Vector2ui(wa.width, wa.height);
    }

    void Window::SetPosition(const Vector2i& pos)
    {
    	XMoveWindow(mDisplay, mHandle, pos.x, pos.y);
    	XFlush(mDisplay);
    }

    void Window::SetSize(const Vector2ui& size)
    {
    	XResizeWindow(mDisplay, mHandle, size.x, size.y);
    	XFlush(mDisplay);
    }

    void Window::SetIcon(const vxU32 width, const vxU32 height, const vxU8* pixels)
    {
    	// Never deleted because it will be freed by XDestroyImage();.
		vxU8* bgra = reinterpret_cast<vxU8*>(std::malloc(width * height * 4));
		for(vxU32 j = 0; j < width * height; ++j)
		{
			bgra[j * 4 + 0] = pixels[j * 4 + 2];
			bgra[j * 4 + 1] = pixels[j * 4 + 1];
			bgra[j * 4 + 2] = pixels[j * 4 + 0];
			bgra[j * 4 + 3] = pixels[j * 4 + 3];
		}

		Visual* vis = DefaultVisual(mDisplay, mScreen);
		vxU32 depth = DefaultDepth(mDisplay, mScreen);
		XImage* img  = XCreateImage(mDisplay, vis, depth, ZPixmap, 0, reinterpret_cast<char*>(&bgra[0]), width, height, 32, 0);

		if(img == nullptr)
			return;

		Pixmap icon_pixmap = XCreatePixmap(mDisplay, RootWindow(mDisplay, mScreen), width, height, depth);
		XGCValues dummy;
		GC gc = XCreateGC(mDisplay, icon_pixmap, 0, &dummy);
		XPutImage(mDisplay, icon_pixmap, gc, img, 0, 0, 0, 0, width, height);
		XFreeGC(mDisplay, gc);
		XDestroyImage(img);

		// Finally create a mask.
		// The mask is simply a little image where there is one single bit for each
		// pixel in the icon. If the bit is set to 1 the pixel is assumed to be drawn
		// if set to 0 Xlib assumes, that the pixels is not to be drawn. This method is
		// used for non-rectangular shapes.
		// In fact this makes it so that we only use one 8-bit integer for 8 pixels.
		// Therefore we need to evaluate the necessary number of bits per row in order
		// to then allocate the memory in the vector:

		// the + 7 guarantess that for example on an image with 17 pixels we use at least
		// 3 bytes (24 bits) and not 2 (16 bits) so that we can fully cover the 17 pixels.
		// The / 8 then calculates the number of bytes to allocate (1 byte = 8 bits):
		vxU32 bytesPerRow = (width + 7) / 8;
		std::vector<vxU8> mask(bytesPerRow * height);
		// Clear the memory:
		std::memset(&mask[0], bytesPerRow * height, 0);
		for(vxU32 j = 0; j < height; ++j)
		{
			// For each byte in the row we set the according bits:
			for(vxU32 k = 0; k < bytesPerRow; ++k)
			{
				// Now for all 8 bits we set or unset them per byte:
				for(vxU32 l = 0; l < 8; ++l)
				{
					// Safety-check: If we attempt to set a bit whose index in the sequence of
					// bits is larger or equal to the icon's width we'll have to leave it blank,
					// since our image is not that tall:
					if(k * 8 + l >= width)
						continue;

					// Check if the alpha channel of the according pixel is set to 0.
					// If so ignore the pixel since it is not supposed to be drawn anyways
					// and displaying it anyways could cause issues:
					vxU8 flag = ((pixels[(j * width + k * 8 + l) * 4 + 3] > 0) ? 1 : 0);
					// Now set the according bit:
					mask[j * bytesPerRow + k] |= (flag << l);
				}
			}
		}
		Pixmap icon_mask = XCreatePixmapFromBitmapData(mDisplay, mHandle, (char*)(&mask[0]), width, height, 1, 0, 1);

		// Phew, the mask is done! This was the most complicated thing since it did not have any
		// documentation except for a tiny notice in the Xlib programming manual. And then it's
		// such a strange format. I mean what use comes from this? Isn't this just too complicated
		// to be understood at all?!

		// Anyways, we can now set the WMHints with the icon, FINALLY!!!! :D
		XWMHints* wmHints = XAllocWMHints();
		wmHints->flags			= IconPixmapHint | IconMaskHint;
		wmHints->icon_pixmap 	= icon_pixmap;
		wmHints->icon_mask		= icon_mask;
		XSetWMHints(mDisplay, mHandle, wmHints);
		XFree(wmHints);
    }

    void Window::SetIcon(const WindowIcon& icon)
    {
    	SetIcon(icon.mWidth, icon.mHeight, icon.mPixels);
    }

    void Window::SetTitle(const std::string& title)
    {
    	XStoreName(mDisplay, mHandle, title.c_str());
    }

    void Window::ShowCursor(const vxBool show)
    {
    	if(show)
    		// Use default cursor:
    		XUndefineCursor(mDisplay, mHandle);
		else
			// Use our invisble cursor:
			XDefineCursor(mDisplay, mHandle, mCursor);
    	XFlush(mDisplay);
    }

    vxBool Window::IsOpen() const
    {
		return mIsOpen;
    }

    const GLContext& Window::GetGLContext() const
    {
    	return mContext;
    }

    void Window::Close()
    {
    	mIsOpen = false;
    }

    Window& Window::operator= (const Window& wnd)
    {
    	Copy(wnd);
    	return *this;
    }

    void Window::Copy(const Window& wnd)
    {
		Destroy();

		mReferences = wnd.mReferences;
		mCursor = wnd.mCursor;
		mDisplay = wnd.mDisplay;
		mHandle = wnd.mHandle;
		mCloseAtom = wnd.mCloseAtom;
		mScreen = wnd.mScreen;
		mIsOpen = wnd.mIsOpen;
		mContext = wnd.mContext;

		if(mReferences != nullptr)
			(*mReferences)++;
    }

    void Window::Destroy()
    {
		if(mReferences != nullptr)
		{
			(*mReferences)--;
			if((*mReferences) == 0)
			{
				delete mReferences;
				if(mCursor != 0)
					XFreeCursor(mDisplay, mCursor);
				if(mHandle != 0)
					XDestroyWindow(mDisplay, mHandle);
				if(mDisplay != nullptr)
					XCloseDisplay(mDisplay);
			}
		}

		mReferences = nullptr;
		mCursor = 0;
		mHandle = 0;
		mDisplay = nullptr;
		mCloseAtom = 0;
		mScreen = 0;
		mIsOpen = false;
		mContext = GLContext();
    }

}

#endif // VOXE_PLATFORM_UNIX
