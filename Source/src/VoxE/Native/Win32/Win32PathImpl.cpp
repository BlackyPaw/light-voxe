#include <VoxE/Native/Platform.hpp>

#if defined(VOXE_PLATFORM_WINDOWS)

#include <VoxE/Native/Path.hpp>

#pragma comment(lib, "Shlwapi.lib")
#include <Shlwapi.h>

#include <vector>

namespace VX
{
	Path::Path()
	{
	}

	Path::Path(const vxString& path) :
		mPath(path)
	{
	}

	vxBool Path::Exists() const
	{
		return (PathFileExists(mPath.c_str()) == TRUE ? true : false);
	}

	vxBool Path::IsDirectory() const
	{
		return (PathIsDirectory(mPath.c_str()) == TRUE ? true : false);
	}

	vxBool Path::IsFile() const
	{
		return (PathIsFileSpec(mPath.c_str()) == TRUE ? true : false);
	}

	Path Path::Canonicalize() const
	{
		std::vector<vxChar> buffer(MAX_PATH);
		PathCanonicalize(&buffer[0], mPath.c_str());
		return Path(vxString(&buffer[0]));
	}

	vxString Path::GetFileExtension() const
	{
		return vxString(PathFindExtension(mPath.c_str()));
	}

	vxString Path::GetFileName() const
	{
		return vxString(PathFindFileName(mPath.c_str()));
	}

	vxBool Path::IsRelative() const
	{
		return (PathIsRelative(mPath.c_str()) == TRUE ? true : false);
	}

	Path Path::FullyQualifiedPath() const
	{
		std::vector<vxChar> buffer(MAX_PATH);
		PathSearchAndQualify(mPath.c_str(), &buffer[0], MAX_PATH);
		return Path(vxString(&buffer[0]));
	}

	const vxString& Path::ToString() const
	{
		return mPath;
	}
}

#endif // VOXE_PLATFORM_WIN32