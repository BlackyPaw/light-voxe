#include <VoxE/Native/Platform.hpp>

#if defined(VOXE_PLATFORM_WINDOWS)

#include <VoxE/Native/Sleep.hpp>
#include <windows.h>

namespace VX
{
	void Sleep(const vxU32 milliseconds)
	{
		::Sleep(milliseconds);
	}
}

#endif