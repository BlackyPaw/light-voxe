#include <VoxE/Native/Platform.hpp>

#if defined(VOXE_PLATFORM_WINDOWS)

#include <VoxE/Native/Assert.hpp>
#include <VoxE/Native/Time.hpp>
#include <windows.h>

namespace VX
{
	LARGE_INTEGER GetFrequency()
	{
		LARGE_INTEGER freq;
		QueryPerformanceFrequency(&freq);
		return freq;
	}

	Time Time::Now()
	{
		HANDLE hThread = GetCurrentThread();
		DWORD_PTR mask = SetThreadAffinityMask(hThread, 1);

		static LARGE_INTEGER kFrequency = GetFrequency();

		LARGE_INTEGER count;
		QueryPerformanceCounter(&count);

		SetThreadAffinityMask(hThread, mask);

		return Time(1000000 * count.QuadPart / kFrequency.QuadPart);
	}
}

#endif