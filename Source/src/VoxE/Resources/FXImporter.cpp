/*
 * FXImporter.cpp
 *
 *  Created on: 28.06.2014
 *      Author: euaconlabs
 */

#include <VoxE/Resources/FXImporter.hpp>
#include <VoxE/Native/FileStream.hpp>

#include <sstream>

namespace VX
{
	void ReplaceAll(vxString& str, const vxString& from, const vxString& to)
	{
		std::size_t off = 0;
		while((off = str.find(from, off)) != str.npos)
		{
			str.replace(off, from.length(), to);
			off += to.length();
		}
	}

	template<typename T>
	vxString to_string(const T& val)
	{
		std::ostringstream oss;
		oss << val;
		return oss.str();
	}

	FXImporter::FXImporter()
	{
	}

	FXImporter::~FXImporter()
	{
	}

	vxBool FXImporter::LoadFile(const File& file)
	{
		if(!file.GetPath().Exists())
			return false;

		FileInputStream stream;
		if(!stream.Open(file))
			return false;

		mProgram = ShaderProgram();

		vxU32 size = file.GetSize();
		vxU8* buffer = new vxU8[size + 1];
		stream.Read(size, buffer);
		buffer[size] = '\0';

		stream.Close();

		vxString string = reinterpret_cast<const char*>(buffer);
		delete[] buffer;

		std::vector<std::pair<vxString, vxString>> sections;

		// Now find all sections:
		std::size_t off = 0;
		while((off = string.find('@', off)) != string.npos)
		{
			// Find the tag:
			std::size_t idx = off + 1;
			vxString tag = "";
			while(!std::isspace(string[idx]))
				tag += string[idx++];

			// Get the end:
			std::size_t begin = idx;
			off = string.find('@', idx);
			std::size_t end = off;
			off++;

			vxString contents = string.substr(begin, end - begin);

			if(tag == "all-shaders")
				mAllShaders = contents;
			else
				sections.push_back(std::pair<vxString, vxString>(tag, contents));
		}

		std::vector<Shader> shaders;

		// Now parse all sections:
		for(vxU32 i = 0; i < sections.size(); ++i)
		{
			ShaderType type;
			if(sections[i].first == "vertex-shader")
				type = ShaderType::Vertex;
			else if(sections[i].first == "fragment-shader")
				type = ShaderType::Fragment;
			else if(sections[i].first == "geometry-shader")
				type = ShaderType::Geometry;
			else if(sections[i].first == "tess-control-shader")
				type = ShaderType::TessControl;
			else if(sections[i].first == "tess-evaluation-shader")
				type = ShaderType::TessEvaluation;
			else
				return false;

			std::string full = mAllShaders + sections[i].second;
			Preprocessor(full);
			Shader shader = Shader::LoadFromSource(type, full.c_str(), full.size());
			if(shader.IsValid())
				shaders.push_back(shader);
			else
				return false;
		}

		// Finally, try to link the shader:
		for(vxU32 i = 0; i < shaders.size(); ++i)
			mProgram.AddShader(shaders[i]);
		return mProgram.Link();
	}

	const ShaderProgram& FXImporter::GetShaderProgram() const
	{
		return mProgram;
	}

	void FXImporter::Preprocessor(vxString& contents)
	{
		ReplaceAll(contents, "POSITION", to_string(ShaderProgram::kPositionLocation));
		ReplaceAll(contents, "NORMAL", to_string(ShaderProgram::kNormalLocation));
		ReplaceAll(contents, "TANGENT", to_string(ShaderProgram::kTangentLocation));
		ReplaceAll(contents, "BITANGENT", to_string(ShaderProgram::kBitangentLocation));
		ReplaceAll(contents, "COLOR", to_string(ShaderProgram::kColorLocation));
		ReplaceAll(contents, "TEX_COORD0", to_string(ShaderProgram::kTexCoord0Location));
	}
}
