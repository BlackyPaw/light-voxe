/*
 * AnimationLoader.cpp
 *
 *  Created on: 15.06.2014
 *      Author: euaconlabs
 */

#include <VoxE/Resources/Loaders/AnimationLoader.hpp>
#include <VoxE/Graphics/2D/CelAnimation.hpp>

namespace VX
{
	AnimationLoader::AnimationLoader() :
		IResourceLoader(&typeid(CelAnimation))
	{
	}

	AnimationLoader::~AnimationLoader()
	{
	}

	vxBool AnimationLoader::LoadResource(const File& file, void** outDataPtr, vxU32* outSizeInBytes)
	{
		if(!file.GetPath().Exists())
			return false;

		CelAnimation c = CelAnimation::LoadFromFile(file);

		(*outDataPtr) 	  = new CelAnimation(c);
		(*outSizeInBytes) = sizeof(CelAnimation);

		return true;
	}
}
