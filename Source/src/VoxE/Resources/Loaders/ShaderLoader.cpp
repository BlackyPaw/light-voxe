/*
 * ShaderLoader.cpp
 *
 *  Created on: 28.06.2014
 *      Author: euaconlabs
 */

#include <VoxE/Graphics/ShaderProgram.hpp>
#include <VoxE/Resources/FXImporter.hpp>
#include <VoxE/Resources/Loaders/ShaderLoader.hpp>

namespace VX
{
	ShaderLoader::ShaderLoader() :
		IResourceLoader(&typeid(ShaderProgram))
	{
	}

	ShaderLoader::~ShaderLoader()
	{
	}

	vxBool ShaderLoader::LoadResource(const File& file, void** outDataPtr, vxU32* outSizeInBytes)
	{
		FXImporter importer;
		if(!importer.LoadFile(file))
			return false;

		(*outDataPtr)		= new ShaderProgram(importer.GetShaderProgram());
		(*outSizeInBytes)	= sizeof(ShaderProgram);

		return true;
	}
}
