/*
 * SoundLoader.cpp
 *
 *  Created on: 18.07.2014
 *      Author: euaconlabs
 */

#include <VoxE/Resources/WavImporter.hpp>
#include <VoxE/Resources/Loaders/SoundLoader.hpp>

namespace VX
{
	SoundLoader::SoundLoader() :
		IResourceLoader(&typeid(AudioBuffer))
	{
	}

	SoundLoader::~SoundLoader()
	{
	}

	vxBool SoundLoader::LoadResource(const File& file, void** outDataPtr, vxU32* outSizeInBytes)
	{
		WavImporter importer;
		if(!importer.LoadFile(file))
			return false;

		(*outDataPtr) = new AudioBuffer(importer.GetAudioBuffer());
		(*outSizeInBytes) = sizeof(AudioBuffer);

		return true;
	}
}
