/*
 * TextureLoader.cpp
 *
 *  Created on: 14.06.2014
 *      Author: euaconlabs
 */

#include <VoxE/Graphics/Texture.hpp>
#include <VoxE/Resources/Loaders/TextureLoader.hpp>


namespace VX
{
	TextureLoader::TextureLoader() :
		IResourceLoader(&typeid(Texture))
	{
	}

	TextureLoader::~TextureLoader()
	{
	}

	vxBool TextureLoader::LoadResource(const File& file, void** outDataPtr, vxU32* outSizeInBytes)
	{
		Texture t = Texture::Create(ImageData::LoadFromFile(file));
		if(!t.IsValid())
			return false;

		t.Bind();
		t.SetFilterDirect(TextureFilter::Nearest, TextureFilter::Nearest);
		t.SetWrapDirect(TextureWrap::Repeat, TextureWrap::Repeat);

		(*outDataPtr) 	  = new Texture(t);
		(*outSizeInBytes) = sizeof(Texture);

		return true;
	}
}
