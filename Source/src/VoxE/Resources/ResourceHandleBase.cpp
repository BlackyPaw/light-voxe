/*
 * ResourceHandleBase.cpp
 *
 *  Created on: 14.06.2014
 *      Author: euaconlabs
 */

#include <VoxE/Resources/ResourceHandleBase.hpp>

namespace VX
{
	ResourceHandleBase::ResourceHandleBase(const std::type_info* type) :
		mResourceManager(nullptr),
		mResourceID(kInvalidResourceID),
		mTypeInfo(type)
	{
	}

	ResourceHandleBase::~ResourceHandleBase()
	{
	}

	ResourceManager* ResourceHandleBase::GetResourceManager()
	{
		return mResourceManager;
	}

	const ResourceManager* ResourceHandleBase::GetResourceManager() const
	{
		return mResourceManager;
	}

	const ResourceID& ResourceHandleBase::GetResourceID() const
	{
		return mResourceID;
	}

	const std::type_info* ResourceHandleBase::GetTypeInfo() const
	{
		return mTypeInfo;
	}

	vxBool ResourceHandleBase::IsValid() const
	{
		return (mResourceManager != nullptr && mResourceID != kInvalidResourceID && mTypeInfo != nullptr);
	}

	vxBool ResourceHandleBase::operator== (const ResourceHandleBase& h)
	{
		return (mResourceManager == h.mResourceManager && mResourceID == h.mResourceID && mTypeInfo == h.mTypeInfo);
	}

	vxBool ResourceHandleBase::operator!= (const ResourceHandleBase& h)
	{
		return (!((*this) == h));
	}
}
