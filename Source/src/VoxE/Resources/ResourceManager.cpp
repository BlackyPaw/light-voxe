/*
 * ResourceManager.cpp
 *
 *  Created on: 14.06.2014
 *      Author: euaconlabs
 */

#include <VoxE/Resources/ResourceManager.hpp>
#include <VoxE/Resources/Loaders/ShaderLoader.hpp>
#include <VoxE/Resources/Loaders/SoundLoader.hpp>
#include <VoxE/Resources/Loaders/TextureLoader.hpp>

#include <sstream>
#include <tinyxml2/tinyxml2.h>

namespace VX
{
	ResourceID CastResourceID(const vxChar* str)
	{
		if(str == nullptr)
			return kInvalidResourceID;

		std::istringstream iss(str);
		ResourceID _val;
		iss >> _val;
		return _val;
	}

	ResourceManager::ResourceManager()
	{
	}

	ResourceManager::~ResourceManager()
	{
		try
		{
			for(typename ResourceLoaderList::iterator it = mResourceLoaders.begin(); it != mResourceLoaders.end(); ++it)
			{
				delete (*it);
			}

			for(auto cacheIt = mCache.begin(); cacheIt != mCache.end(); ++cacheIt)
			{
				ResourceCache& cache = cacheIt->second;
				for(auto resourceIt = cache.begin(); resourceIt != cache.end(); ++resourceIt)
				{
					delete resourceIt->second;
				}
			}
		}
		catch(...)
		{
			/* Swallow exception. */
		}
	}

	vxBool ResourceManager::Initialize()
	{
		if(mResourceLoaders.size() != 0)
			return false;

		/*********************************
		 * Register all resource loaders:
		 */
		mResourceLoaders.push_back(new ShaderLoader());
		mResourceLoaders.push_back(new SoundLoader());
		mResourceLoaders.push_back(new TextureLoader());

		/*********************************
		 * Load the resource table:
		 */
		using namespace tinyxml2;

		XMLDocument doc;
		if(doc.LoadFile("Assets/Resources.xml") != XML_NO_ERROR)
			return false;

		XMLElement* xmlResourceTable = doc.FirstChildElement("ResourceTable");
		if(xmlResourceTable == nullptr)
			return false;

		for(XMLElement* xmlResource = xmlResourceTable->FirstChildElement("Resource"); xmlResource != nullptr; xmlResource = xmlResource->NextSiblingElement())
		{
			ResourceID id = xmlResource->Attribute("id");
			File file = File(xmlResource->Attribute("path"));
			mResourceTable.insert(std::make_pair(id, file));
		}

		return true;
	}
}
