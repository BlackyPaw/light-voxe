/*
 * WavImporter.cpp
 *
 *  Created on: 18.07.2014
 *      Author: euaconlabs
 */

#include <VoxE/Native/FileStream.hpp>
#include <VoxE/Resources/WavImporter.hpp>

namespace VX
{
	/////////////////////////////////////////////////////////////
	/// \enum WavChunk
	/// \brief All chunks of interest found in a Wave sound file.
	/////////////////////////////////////////////////////////////
	enum WavChunk
	{
		WAV_CHUNK_RIFF = 0x46464952,
		WAV_CHUNK_FORMAT = 0x20746D66,
		WAV_CHUNK_DATA = 0x61746164
	};

	WavImporter::WavImporter()
	{
	}

	WavImporter::~WavImporter()
	{
	}

	vxBool WavImporter::LoadFile(const File& f)
	{
		// Destroy any previous instance:
		mAudioBuffer.Destroy();

		struct tagRIFF
		{
			tagRIFF() :
				size(0),
				format(0)
			{
			}

			vxU32 size;
			vxU32 format;
		} riff;

		struct tagFMT
		{
			tagFMT() :
				formatsize(0),
				format(0),
				channels(0),
				samplerate(0),
				bpp(0),
				blockalign(0),
				bitdepth(0)
			{
			}

			vxU32 formatsize;
			vxU16 format;
			vxU16 channels;
			vxU32 samplerate;
			vxU32 bpp;
			vxU16 blockalign;
			vxU16 bitdepth;
		} format;

		struct tagDATA
		{
			tagDATA() :
				datasize(0)
			{
			}

			vxU32 datasize;
		} data;

		FileInputStream in;
		in.Open(f);

		if(!in.IsOpen())
			return false;

		vxBool hasData = false;
		vxU32 chunkid;

		while(!hasData)
		{
			chunkid = in.ReadLE32();
			switch(chunkid)
			{
				case WAV_CHUNK_RIFF:
				{
					riff.size = in.ReadLE32();
					riff.format = in.ReadLE32();
				}
				break;
				case WAV_CHUNK_FORMAT:
				{
					format.formatsize = in.ReadLE32();
					format.format = in.ReadLE16();
					format.channels = in.ReadLE16();
					format.samplerate = in.ReadLE32();
					format.bpp = in.ReadLE32();
					format.blockalign = in.ReadLE16();
					format.bitdepth = in.ReadLE16();
					// Skip the extra parameters if present:
					if(format.formatsize == 18)
					{
						vxU16 extra = in.ReadLE16();
						in.Seek(StreamSeekDir::STREAM_SEEK_CUR, extra);
					}
				}
				break;
				case WAV_CHUNK_DATA:
				{
					data.datasize = in.ReadLE32();
					hasData = true;
				}
				break;
				// Handle generic / unsupported chunks:
				default:
				{
					vxU32 chunksize = in.ReadLE32();
					in.Seek(StreamSeekDir::STREAM_SEEK_CUR, chunksize);
				}
				break;
			}
		}

		if(data.datasize == 0)
			return false;

		// Hold on to the limitations set by OpenAL:
		if(format.channels > 2 || format.bitdepth > 16)
			return false;

		AudioFormat audioformat;
		switch(format.channels)
		{
			case 1:
				audioformat = (format.bitdepth == 8 ? AudioFormat::Mono8 : AudioFormat::Mono16);
				break;
			case 2:
				audioformat = (format.bitdepth == 8 ? AudioFormat::Stereo8 : AudioFormat::Stereo16);
				break;
		}

		vxU8* pcm = new vxU8[data.datasize];
		in.Read(data.datasize, pcm);

		mAudioBuffer.Create(audioformat, pcm, data.datasize, format.samplerate);

		delete[] pcm;

		return true;
	}

	const AudioBuffer& WavImporter::GetAudioBuffer() const
	{
		return mAudioBuffer;
	}
}
