/*
 * Component.cpp
 *
 *  Created on: 30.07.2014
 *      Author: euaconlabs
 */

#include <VoxE/Scene/Component.hpp>

namespace VX
{
	Component::Component() :
		mOwner(0)
	{
	}

	Component::~Component()
	{
	}

	void Component::Update(const vxF32 delta)
	{
	}

	GameObject* Component::GetOwner() const
	{
		return mOwner;
	}
}
