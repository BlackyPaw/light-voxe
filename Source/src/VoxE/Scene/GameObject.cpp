/*
 * GameObject.cpp
 *
 *  Created on: 03.07.2014
 *      Author: euaconlabs
 */

#include <VoxE/Scene/GameObject.hpp>

#include <cstdio>

namespace VX
{
	GameObject::GameObject(SceneNode* node) :
		mSceneNode(node)
	{
	}

	GameObject::~GameObject()
	{
		for(auto it = mComponents.begin(); it != mComponents.end(); ++it)
			delete (*it);
		mComponents.clear();
	}

	void GameObject::AddComponent(Component* com)
	{
		com->mOwner = this;
		mComponents.push_back(com);
	}

	void GameObject::SetActive(const vxBool active)
	{
		mActive = active;
	}

	const vxBool GameObject::IsActive() const
	{
		return mActive;
	}

	SceneNode* GameObject::GetSceneNode() const
	{
		return mSceneNode;
	}

	void GameObject::Update(const vxF32 delta)
	{
		if(mComponents.size() == 0)
			return;

		for(auto it = mComponents.begin(); it != mComponents.end(); ++it)
			(*it)->Update(delta);
	}
}
