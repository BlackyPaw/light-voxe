/*
 * GameObjectRef.cpp
 *
 *  Created on: 24.07.2014
 *      Author: euaconlabs
 */

#include <VoxE/Scene/GameObject.hpp>
#include <VoxE/Scene/GameObjectRef.hpp>

namespace VX
{
	GameObjectRef::GameObjectRef(GameObject* ref) :
		mReference(ref)
	{
	}

	GameObjectRef::~GameObjectRef()
	{
	}

	void GameObjectRef::OnUpdate(const vxF32 delta)
	{
		mReference->Update(delta);
	}

	GameObject* GameObjectRef::GetReference() const
	{
		return mReference;
	}
}
