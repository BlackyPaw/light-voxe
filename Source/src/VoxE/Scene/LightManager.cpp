/*
 * LightManager.cpp
 *
 *  Created on: 22.07.2014
 *      Author: euaconlabs
 */

#include <VoxE/Scene/LightManager.hpp>

#include <algorithm>

namespace VX
{
	LightManager::LightManager() :
		// Avoid memory reallocations by reserving enough space:
		mAllocatedSources(kMaxLightSources)
	{
		mLightPool.Create(kMaxLightSources);
	}

	LightManager::~LightManager()
	{
	}

	vxU32 LightManager::GetMaxLightSources()
	{
		return kMaxLightSources;
	}

	LightSource* LightManager::AllocateSource()
	{
		LightSource* source = mLightPool.Allocate();
		mAllocatedSources.push_back(source);
		return source;
	}

	void LightManager::FreeSource(LightSource* source)
	{
		auto find = std::find(mAllocatedSources.begin(), mAllocatedSources.end(), source);
		if(find == mAllocatedSources.end())
			return;

		mAllocatedSources.erase(find);
		mLightPool.Free(source);
	}

	vxU32 LightManager::GetNumLights() const
	{
		return mAllocatedSources.size();
	}

	LightSource* const* LightManager::GetLights() const
	{
		return &mAllocatedSources[0];
	}
}
