/*
 * MatrixStack.cpp
 *
 *  Created on: 12.06.2014
 *      Author: euaconlabs
 */

#include <VoxE/Scene/MatrixStack.hpp>

namespace VX
{
	void MatrixStack::Push()
	{
		mStack.push_back(mTop);
	}

	void MatrixStack::Pop()
	{
		if(mStack.size() == 0)
			mTop = Matrix4();
		else
		{
			mTop = mStack.back();
			mStack.pop_back();
		}
	}

	Matrix4& MatrixStack::Top()
	{
		return mTop;
	}

	const Matrix4& MatrixStack::Top() const
	{
		return mTop;
	}

	void MatrixStack::Clear()
	{
		mStack.clear();
		mTop = Matrix4();
	}
}
