/*
 * SceneManager.cpp
 *
 *  Created on: 23.07.2014
 *      Author: euaconlabs
 */

#include <VoxE/Scene/SceneManager.hpp>

namespace VX
{
	SceneManager::SceneManager() :
		mRootNode(0)
	{
	}

	SceneManager::~SceneManager()
	{
	}

	void SceneManager::AddSceneNode(SceneNode* node)
	{
		if(node->GetParent() != 0)
			return;

		mRootNode.AddChild(node);
	}

	void SceneManager::RemoveSceneNode(SceneNode* node)
	{
		node->DetachFromParent();
	}

	void SceneManager::Update(const vxF32 delta)
	{
		mRootNode.Update(delta);
	}
}
