/*
 * SceneNode.cpp
 *
 *  Created on: 23.07.2014
 *      Author: euaconlabs
 */

#include <VoxE/Scene/SceneNode.hpp>
#include <VoxE/Scene/World.hpp>

#include <algorithm>

namespace VX
{
	SceneNode::SceneNode(World* world) :
		mWorld(world),
		mParent(nullptr),
		mData(nullptr)
	{
	}

	SceneNode::~SceneNode()
	{
		DetachFromParent();
		for(auto it = mChildren.begin(); it != mChildren.end(); ++it)
		{
			// Delete the scene node's children here:
			mWorld->FreeSceneNode((*it));
		}

		// Finally, delete the scene node data:
		if(mData != 0)
			delete mData;
	}

	SceneNode* SceneNode::GetParent() const
	{
		return mParent;
	}

	const SceneNodeList& SceneNode::GetChildren() const
	{
		return mChildren;
	}

	void SceneNode::AddChild(SceneNode* child)
	{
		if(child->GetParent() != 0)
			return;

		child->mParent = this;
		mChildren.push_back(child);
	}

	void SceneNode::RemoveChild(SceneNode* child)
	{
		if(child->GetParent() != this)
			return;

		child->mParent = 0;
		mChildren.erase(std::find(mChildren.begin(), mChildren.end(), child));
	}

	void SceneNode::DetachFromParent()
	{
		if(mParent != 0)
		{
			mParent->RemoveChild(this);
		}
	}

	World* SceneNode::GetWorld() const
	{
		return mWorld;
	}

	void SceneNode::SetData(ISceneNodeData* data)
	{
		mData = data;
	}

	ISceneNodeData* SceneNode::GetData()
	{
		return mData;
	}

	const ISceneNodeData* SceneNode::GetData() const
	{
		return mData;
	}

	void SceneNode::Update(const vxF32 delta)
	{
		// Invoke appropriate data callback:
		if(mData)
			mData->OnUpdate(delta);

		// Update children:
		for(auto it = mChildren.begin(); it != mChildren.end(); ++it)
		{
			(*it)->Update(delta);
		}
	}
}
