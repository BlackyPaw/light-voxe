/*
 * SpriteComponent.cpp
 *
 *  Created on: 12.07.2014
 *      Author: euaconlabs
 */

#include <VoxE/Graphics/Renderer.hpp>
#include <VoxE/Resources/ResourceManager.hpp>
#include <VoxE/Scene/GameObject.hpp>
#include <VoxE/Scene/SpriteComponent.hpp>
#include <VoxE/Systems/RenderSystem.hpp>

namespace VX
{
	SpriteComponent::SpriteComponent()
	{
		mSprite = RenderSystem::Get()->AllocateSprite();
		mSprite->SetTexture(ResourceManager::Get()->LoadResource<Texture>("logo"));
	}

	SpriteComponent::~SpriteComponent()
	{
		RenderSystem::Get()->FreeSprite(mSprite);
	}

	void SpriteComponent::Update(const vxF32 delta)
	{
		mSprite->CopyTransform(*mOwner->GetSceneNode());
	}
}
