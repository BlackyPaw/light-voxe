/*
 * World.cpp
 *
 *  Created on: 23.07.2014
 *      Author: euaconlabs
 */

#include <VoxE/Scene/GameObjectRef.hpp>
#include <VoxE/Scene/World.hpp>

namespace VX
{
	World::World()
	{
		mSceneNodeAllocator.Create(sizeof(SceneNode), 512);
		mGameObjectAllocator.Create(sizeof(GameObject), 256);
	}

	World::~World()
	{
	}

	void World::Update(const vxF32 delta)
	{
		mSceneManager.Update(delta);
	}

	GameObject* World::NewGameObject()
	{
		SceneNode* node = NewSceneNode();
		if(node == 0)
			return 0;
		void* storage = mGameObjectAllocator.Allocate();
		if(storage == 0)
		{
			FreeSceneNode(node);
			return 0;
		}
		GameObject* object = new(storage) GameObject(node);
		node->SetData(new GameObjectRef(object));
		return object;
	}

	void World::FreeGameObject(GameObject* object)
	{
		FreeSceneNode(object->GetSceneNode());
		mGameObjectAllocator.Free(object);
	}

	void World::AddGameObject(GameObject* object)
	{
		// Adding simplifies to adding the game object's scene node:
		AddSceneNode(object->GetSceneNode());
	}

	void World::RemoveGameObject(GameObject* object)
	{
		RemoveSceneNode(object->GetSceneNode());
	}

	SceneNode* World::NewSceneNode()
	{
		void* storage = mSceneNodeAllocator.Allocate();
		if(storage == 0)
			return 0;
		return new(storage) SceneNode(this);
	}

	void World::FreeSceneNode(SceneNode* node)
	{
		mSceneNodeAllocator.Free(node);
	}

	void World::AddSceneNode(SceneNode* node)
	{
		if(node->GetWorld() != this)
			return;

		mSceneManager.AddSceneNode(node);
	}

	void World::RemoveSceneNode(SceneNode* node)
	{
		if(node->GetWorld() != this)
			return;

		node->DetachFromParent();
	}

	SceneManager* World::GetSceneManager() const
	{
		return &const_cast<SceneManager&>(mSceneManager);
	}
}
