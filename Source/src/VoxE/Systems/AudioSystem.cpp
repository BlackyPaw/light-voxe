/*
 * AudioSystem.cpp
 *
 *  Created on: 24.07.2014
 *      Author: euaconlabs
 */

#include <VoxE/Systems/AudioSystem.hpp>

namespace VX
{
	AudioSystem::AudioSystem()
	{
	}

	AudioSystem::~AudioSystem()
	{
	}

	vxBool AudioSystem::Initialize()
	{
		mALContext.Create();
		if(!mALContext.IsValid())
			return false;

		mALContext.MakeCurrent();

		return true;
	}

	void AudioSystem::Update(const vxF32 delta)
	{
	}
}
