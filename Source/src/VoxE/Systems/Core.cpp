/*
 * Core.cpp
 *
 *  Created on: 24.07.2014
 *      Author: euaconlabs
 */

// Native components:
#include <VoxE/Native/GLContextAttributes.hpp>
#include <VoxE/Native/Window.hpp>

#include <VoxE/Systems/Core.hpp>

namespace VX
{
	Core::Core()
	{
	}

	Core::~Core()
	{
	}

	vxBool Core::Initialize()
	{
		// -------- Step 1: Set up OpenGL and the game window
		GLContextAttributes attributes;
		attributes.mMajorVersion = 3;
		attributes.mMinorVersion = 3;
		attributes.mDebugContext = false;
		attributes.mCompatibility = true;
		attributes.mColorBits = 32;
		attributes.mDepthBits = 24;
		attributes.mStencilBits = 8;
		attributes.mDoublebuffer = true;
		mGameWindow.Create(attributes);

		if(!mGameWindow.IsOpen())
			return false;

		mGameWindow.Show();
		mGameWindow.SetTitle("VoxE");

		mGLContext = mGameWindow.GetGLContext();
		if(!mGLContext.IsValid())
			return false;

		return true;
	}

	void Core::Update()
	{
		mGameWindow.Update();
	}

	vxBool Core::IsRunning() const
	{
		return mGameWindow.IsOpen();
	}

	void Core::ForceTermination()
	{
		mGameWindow.Close();
	}

	GLContext& Core::GetGLContext()
	{
		return mGLContext;
	}
}
