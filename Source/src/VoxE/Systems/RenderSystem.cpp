/*
 * RenderSystem.cpp
 *
 *  Created on: 24.07.2014
 *      Author: euaconlabs
 */

// Dependencies:
#include <VoxE/Core/EventManager.hpp>

#include <VoxE/Graphics/OpenGL.hpp>
#include <VoxE/Graphics/Renderer.hpp>
#include <VoxE/Graphics/2D/Sprite.hpp>

#include <VoxE/Native/WindowSizeEvent.hpp>

#include <VoxE/Systems/Core.hpp>
#include <VoxE/Systems/RenderSystem.hpp>

#include <algorithm>

namespace VX
{
	const vxU32 RenderSystem::kAllocatorSize = static_cast<vxU32>(sizeof(Sprite));
	const vxU32 RenderSystem::kNumRenderableObjects = 256;

	RenderSystem::RenderSystem()
	{
	}

	RenderSystem::~RenderSystem()
	{
	}

	Sprite* RenderSystem::AllocateSprite()
	{
		void* storage = mRenderableAllocator.Allocate();
		if(storage == 0)
			// Allocate from general heap: :(
			storage = new vxU8[kAllocatorSize];
		// Placement-New Syntax:
		Sprite* sprite = new(storage) Sprite();

		// Manage sprite:
		mRenderQueue.push_back(sprite);

		return sprite;
	}

	void RenderSystem::FreeSprite(Sprite* sprite)
	{
		// Remove sprite from render queue:
		mRenderQueue.erase(std::find(mRenderQueue.begin(), mRenderQueue.end(), sprite));

		if(mRenderableAllocator.IsManaged(sprite))
			mRenderableAllocator.Free(sprite);
		else
			delete[] reinterpret_cast<vxU8*>(sprite);
	}

	vxBool RenderSystem::Initialize()
	{
		if(!mRenderableAllocator.Create(kAllocatorSize, kNumRenderableObjects))
			return false;

		mRenderQueue.reserve(kNumRenderableObjects);

		// Prepare rendering state:
		glEnable(GL_TEXTURE_2D);

		EventManager::Get()->RegisterListener(WindowSizeEvent::GetEventID(), EventListener(this, &RenderSystem::OnResize));

		return true;
	}

	void RenderSystem::Cleanup()
	{
		EventManager::Get()->UnRegisterListener(WindowSizeEvent::GetEventID(), EventListener(this, &RenderSystem::OnResize));
	}

	void RenderSystem::RenderAndSwapBuffers()
	{
		Core::Get()->GetGLContext().FullClear();

		Renderer::Get()->BeginSession();

		for(auto it = mRenderQueue.begin(); it != mRenderQueue.end(); ++it)
		{
			Renderable* obj = (*it);
			if(obj->IsVisible())
			{
				obj->PreRender();
				obj->Render();
				obj->PostRender();
			}
		}

		Renderer::Get()->EndSession();

		Core::Get()->GetGLContext().SwapBuffers();
	}

	void RenderSystem::OnResize(EventPtr& e)
	{
		const Vector2ui& size = reinterpret_cast<WindowSizeEvent*>(e.get())->GetSize();
		glViewport(0, 0, size.x, size.y);
	}
}
